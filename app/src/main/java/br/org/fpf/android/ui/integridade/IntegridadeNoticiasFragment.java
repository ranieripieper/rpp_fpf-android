package br.org.fpf.android.ui.integridade;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.org.fpf.android.R;
import br.org.fpf.android.model.IntegridadeNoticia;
import br.org.fpf.android.ui.base.BaseFragment;
import br.org.fpf.android.ui.base.ViewUtil;
import br.org.fpf.android.ui.integridade.adapter.IntegridadeNoticiasViewHolder;
import uk.co.ribot.easyadapter.EasyRecyclerAdapter;

/**
 * Created by ranipieper on 8/29/16.
 */
@EFragment(R.layout.fragment_fpf_integridade_noticias)
public class IntegridadeNoticiasFragment extends BaseFragment implements IntegridadeNoticiasViewHolder. IntegridadeNoticiaHolderListener {

    @ViewById(R.id.recycler_view)
    RecyclerView mRecyclerView;

    public static IntegridadeNoticiasFragment newInstance() {
        Bundle args = new Bundle();

        IntegridadeNoticiasFragment fragment = new IntegridadeNoticiasFragment_();
        fragment.setArguments(args);

        return fragment;
    }


    @AfterViews
    public void afterViews() {
        super.afterViews();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        EasyRecyclerAdapter adapter = new EasyRecyclerAdapter<>(
                getActivity(),
                IntegridadeNoticiasViewHolder.class,
                getNoticias(),
                this);

        mRecyclerView.setAdapter(adapter);
    }

    private List<IntegridadeNoticia> getNoticias() {
        List<IntegridadeNoticia> result = new ArrayList<>();
        result.add(new IntegridadeNoticia("Posicionamento da FPF", "Diante das denúncias sobre supostas tentativas de manipulação de resultados em partidas de futebol…", "http://www.fpf.org.br/Not%C3%ADcias/%C3%9Altimas+Not%C3%ADcias/2016-03/96346/Posicionamento+da+FPF+a+respeito+das+recentes+den%C3%BAncias+no+futebol+paulista"));
        result.add(new IntegridadeNoticia("Máfia financia fraudes", "Segundo executivo de empresa especializada, principal base dos manipuladores é Cingapura…", "http://globoesporte.globo.com/futebol/noticia/2016/07/apostas-movimentam-r-48-trilhoes-em-2015-e-mafia-financia-fraudes.html"));
        result.add(new IntegridadeNoticia("Manipulação de resultados no Brasil", "Documentos obtidos pelo GloboEsporte.com foram base, ao lado de grampos telefônicos, para…", "http://globoesporte.globo.com/futebol/noticia/2016/07/relatorios-confidenciais-evidenciam-manipulacao-de-resultados-no-brasil.html"));
        result.add(new IntegridadeNoticia("Polícia prende 7 em ação contra fraude em placares de jogos de futebol", "Ex-goleiro do América de São José do Rio Preto é um dos presos.Grupo alterava resultados para…", "http://g1.globo.com/sao-paulo/noticia/2016/07/operacao-contra-fraudes-em-resultado-de-jogos-de-futebol-ocorre-em-sp-rj-e-ce.html"));
        result.add(new IntegridadeNoticia("Itália prende dez por manipulação", "Membros da camorra \"Vanella Grassi\" teriam fraudado dois jogos do Avellino na série B italiana...", "http://globoesporte.globo.com/futebol/futebol-internacional/futebol-italiano/noticia/2016/05/dez-pessoas-sao-presas-por-suspeita-de-manipulacao-em-jogos-na-italia.html"));
        result.add(new IntegridadeNoticia("Polícia investiga venda de resultados em SP", "Inquérito foi instaurado em outubro e inclui relatos de dirigente do América de Rio Preto, que teria…", "http://globoesporte.globo.com/sp/futebol/noticia/2016/03/em-sigilo-policia-de-sp-investiga-venda-de-resultados-no-estado-ha-5-meses.html"));
        result.add(new IntegridadeNoticia("Presidente desvenda máfia de aposta", "A Máfia das Apostas no futebol funciona praticamente como uma empresa. Tem jeito de agir...", "http://esporte.uol.com.br/futebol/ultimas-noticias/2016/03/09/presidente-desvenda-mafia-de-aposta-dinheiro-vivo-placar-e-tempo-do-gol.htm"));

        return result;
    }

    @Override
    public void onItemSelected(IntegridadeNoticia noticia) {
        ViewUtil.openBrowser(getContext(), noticia.getUrl());
    }

    @Override
    protected String getToolbarTitle() {
        return "";
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }
}

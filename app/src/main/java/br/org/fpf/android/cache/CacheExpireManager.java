package br.org.fpf.android.cache;

import java.util.List;

import br.org.fpf.android.cache.base.BaseCache;
import br.org.fpf.android.cache.base.CacheExpire;

/**
 * Created by ranieripieper on 2/3/16.
 */
public class CacheExpireManager extends BaseCache<CacheExpire> {

    private static CacheExpireManager instance = new CacheExpireManager();

    private CacheExpireManager() {
    }

    public static CacheExpireManager getInstance() {
        return instance;
    }

    @Override
    public void put(CacheExpire obj) {
        obj.setId(getCalculateId(obj));
        super.put(obj);
    }

    @Override
    public void putAll(List<CacheExpire> lst) {
        if (lst != null) {
            for (CacheExpire obj : lst) {
                obj.setId(getCalculateId(obj));
            }
            super.putAll(lst);
        }
    }

    private String getCalculateId(CacheExpire obj) {
        return String.format("%s_%d_%s", obj.getClassName(), obj.getIdSerie(), obj.getKey());
    }

    @Override
    public Class<CacheExpire> getReferenceClass() {
        return CacheExpire.class;
    }

    @Override
    protected long getCacheLifeTime() {
        return 1000 * 60 * 60 * 24 * 15; // 15 dias
    }
}

package br.org.fpf.android.ui.estatisticas;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import br.org.fpf.android.FpfApplication;
import br.org.fpf.android.R;
import br.org.fpf.android.cache.EquipeCacheManager;
import br.org.fpf.android.model.Equipe;
import br.org.fpf.android.model.EstatisticasTime;
import br.org.fpf.android.model.response.EstatisticasTimeResponse;
import br.org.fpf.android.service.RetrofitManager;
import br.org.fpf.android.ui.base.BaseFragment;
import br.org.fpf.android.ui.custom.FpfViewPagerAdapter;
import br.org.fpf.android.util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 3/8/16.
 */
@EFragment(R.layout.fragment_estatisticas_time)
public class EstatisticasTimeFragment extends BaseFragment {

    //Header
    @ViewById(R.id.lbl_time)
    TextView mLblTime;
    @ViewById(R.id.img_time)
    ImageView mImgTime;

    @ViewById(R.id.layout_content)
    View mLayoutContent;

    //pontuação
    @ViewById(R.id.lbl_total_pontos)
    TextView mLblTotalPontos;
    @ViewById(R.id.lbl_total_pontos_mandante)
    TextView mLblTotalPontosMandante;
    @ViewById(R.id.lbl_total_pontos_visitante)
    TextView mLblTotalPontosVisitante;

    //ViewPager
    @ViewById(R.id.viewpager)
    ViewPager mViewPager;
    FpfViewPagerAdapter mAdapter;

    @ViewById(R.id.lbl_titulo)
    TextView mLblTituloPage;
    @ViewById(R.id.img_arrow_left)
    ImageView mImgArrowLeft;
    @ViewById(R.id.img_arrow_right)
    ImageView mImgArrowRight;

    @ViewById(R.id.txt_error_no_results)
    TextView mTxtErrorNoResult;

    @FragmentArg(Constants.ARG_ID_TIME)
    Long mIdTime;

    public static EstatisticasTimeFragment newInstance(Long idTime) {
        Bundle args = new Bundle();

        EstatisticasTimeFragment_ fragment = new EstatisticasTimeFragment_();
        args.putLong(Constants.ARG_ID_TIME, idTime);

        fragment.setArguments(args);

        return fragment;
    }

    @AfterViews
    public void afterViews() {
        super.afterViews();
        initHeader();

        mLayoutContent.postDelayed(new Runnable() {
            @Override
            public void run() {
                loadData();
            }
        }, Constants.DELAY_VIEW_PAGE);

    }

    private void loadData() {
        mTxtErrorNoResult.setVisibility(View.GONE);
        showLoadingView();
        String where = String.format("{\"IdEquipe\":%d}", mIdTime);

        RetrofitManager.getInstance().getEstatisticasTimeService().getEstatisticas(where, new Callback<EstatisticasTimeResponse>() {
            @Override
            public void success(EstatisticasTimeResponse estatisticasTimeResponse, Response response) {
                if (isAdded()) {
                    processResult(estatisticasTimeResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error);
            }
        });
    }

    private void processResult(EstatisticasTimeResponse estatisticasTimeResponse) {
        hideLoadingView();
        if (estatisticasTimeResponse == null || estatisticasTimeResponse.isEmpty()) {
            showEmpty();
        } else {
            EstatisticasTime estatistica = estatisticasTimeResponse.getResults().get(0);
            setupPontos(estatistica);
            setupViewPager(estatistica);
            if (isAdded()) {
                mLayoutContent.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setupPontos(EstatisticasTime estatistica) {
        if (isAdded()) {
            mLblTotalPontos.setText(estatistica.getTotalPontos());
            mLblTotalPontosMandante.setText(String.valueOf(estatistica.getPontosComoMandante()));
            mLblTotalPontosVisitante.setText(String.valueOf(estatistica.getPontosComoVisitante()));
        }
    }

    private void setupViewPager(EstatisticasTime estatistica) {
        if (isAdded()) {
            mAdapter = new FpfViewPagerAdapter(getChildFragmentManager());
            mAdapter.addFrag(JogosEstatisticasTimeFragment.newInstance(estatistica), getString(R.string.jogos), isAdded());
            mAdapter.addFrag(GolsEstatisticasFragment.newInstance(estatistica, true), getString(R.string.gols_pro), isAdded());
            mAdapter.addFrag(GolsEstatisticasFragment.newInstance(estatistica, false), getString(R.string.gols_sofridos), isAdded());
            mAdapter.addFrag(ElencoEstatisticasTimeFragment.newInstance(estatistica), getString(R.string.elenco), isAdded());

            mViewPager.setAdapter(mAdapter);
            mViewPager.setCurrentItem(0);
            updateTitloPage(0);
            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    updateTitloPage(position);
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
        }
    }

    private void updateTitloPage(int position) {
        if (isAdded()) {
            mImgArrowLeft.setVisibility(View.VISIBLE);
            mImgArrowRight.setVisibility(View.VISIBLE);
            if (position == 0) {
                mImgArrowLeft.setVisibility(View.GONE);
            } else if (position == 3) {
                mImgArrowRight.setVisibility(View.GONE);
            }
            mLblTituloPage.setText(mAdapter.getPageTitle(position));
        }
    }

    @Click(R.id.img_arrow_left)
    void onImgArrowLeftClick() {
        mViewPager.arrowScroll(ViewPager.FOCUS_LEFT);
    }

    @Click(R.id.img_arrow_right)
    void onImgArrowRightClick() {
        mViewPager.arrowScroll(ViewPager.FOCUS_RIGHT);
    }

    private void showEmpty() {
        showErrorTxtError(R.string.error_estatisticas_vazia);
    }

    public void showError(RetrofitError error) {
        showErrorTxtError(R.string.error_generic_refresh);
    }

    public void showErrorTxtError(int resError) {
        if (isAdded()) {
            hideLoadingView();
            mTxtErrorNoResult.setText(resError);
            mTxtErrorNoResult.setVisibility(View.VISIBLE);
        }
    }

    private void initHeader() {
        Equipe equipe = EquipeCacheManager.getInstance().get(mIdTime);
        if (equipe == null) {
            return;
        }
        mLblTime.setText(equipe.getNome());
        FpfApplication.imageLoader.displayImage(equipe.getUrlLogo(), mImgTime);
    }

    @Click(R.id.txt_error_no_results)
    void onErrorClick() {
        loadData();;
    }

    @Override
    protected String getToolbarTitle() {
        return "";
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }
}
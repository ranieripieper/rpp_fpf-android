package br.org.fpf.android.ui.estatisticas;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.org.fpf.android.R;
import br.org.fpf.android.cache.EquipeCacheManager;
import br.org.fpf.android.model.Equipe;
import br.org.fpf.android.model.EstatisticasTime;
import br.org.fpf.android.model.Jogos;
import br.org.fpf.android.ui.estatisticas.adapter.JogosViewHolder;
import br.org.fpf.android.util.Constants;
import uk.co.ribot.easyadapter.EasyRecyclerAdapter;

/**
 * Created by ranipieper on 3/9/16.
 */
@EFragment(R.layout.fragment_estatisticas_time_jogos)
public class JogosEstatisticasTimeFragment extends EstatisticasTimeItemFragment {

    @ViewById(R.id.recycler_view_right)
    RecyclerView mRecyclerViewVisitante;

    @ViewById(R.id.lbl_jogos_mandante)
    TextView mLblJogosMandante;

    @ViewById(R.id.lbl_jogos_visitante)
    TextView mLblJogosVisitante;

    public static JogosEstatisticasTimeFragment newInstance(EstatisticasTime estatisticasTime) {
        Bundle args = new Bundle();

        JogosEstatisticasTimeFragment_ fragment = new JogosEstatisticasTimeFragment_();
        args.putParcelable(Constants.ARG_ESTATISTICAS_TIME, estatisticasTime);
        fragment.setArguments(args);

        return fragment;
    }

    @AfterViews
    public void afterViews() {
        super.afterViews();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerViewVisitante.setLayoutManager(layoutManager);
        mRecyclerViewVisitante.setHasFixedSize(true);
        mRecyclerViewVisitante.setAdapter(createAdapterVisitante());

        mLblJogosMandante.setText(String.valueOf(mEstatisticasTime.getJogosComoMandante().size()));
        mLblJogosVisitante.setText(String.valueOf(mEstatisticasTime.getJogosComoVisitante().size()));
    }

    @Override
    protected EasyRecyclerAdapter createAdapter() {
        return getAdapter(mEstatisticasTime.getJogosComoMandante(), true);
    }

    private EasyRecyclerAdapter getAdapter(List<Jogos> lst, boolean mandante) {
        Equipe equipe = EquipeCacheManager.getInstance().get(mEstatisticasTime.getIdEquipe());
        for (Jogos jogos : lst) {
            jogos.setEquipe(equipe);
            jogos.setMandante(mandante);
        }
        return new EasyRecyclerAdapter(
                getActivity(),
                JogosViewHolder.class,
                lst);
    }

    protected EasyRecyclerAdapter createAdapterVisitante() {
        return getAdapter(mEstatisticasTime.getJogosComoVisitante(), false);
    }
}
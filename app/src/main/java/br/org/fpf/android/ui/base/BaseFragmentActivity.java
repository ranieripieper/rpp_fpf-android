package br.org.fpf.android.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.View;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.org.fpf.android.R;

/**
 * Created by ranipieper on 1/14/16.
 */
@EActivity
public abstract class BaseFragmentActivity extends BaseActivity {

    protected List<String> mFragments = new ArrayList<>();

    @Nullable
    @ViewById(R.id.drawer_layout)
    public DrawerLayout mDrawerLayout;

    public ActionBarDrawerToggle mActionBarDrawerToggle;

    protected abstract int getLayoutFragmentContainer();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Adds a {@link Fragment} to this activity's layout.
     *
     * @param fragment The fragment to be added.
     */
    public void addFragment(BaseFragment fragment) {
        addFragment(null, fragment, null, false);
    }

    public void addFragment(BaseFragment fragment, boolean clearBackStack) {
        addFragment(null, fragment, null, clearBackStack);
    }

    public void addFragment(BaseFragment fragment, BaseFragment fragment2, boolean clearBackStack) {
        addFragment(null, fragment, fragment2, clearBackStack);
    }

    public void addFragment(BaseFragment actualFragment, BaseFragment fragment) {
        addFragment(actualFragment, fragment, null, false);
    }

    public void addFragment(BaseFragment actualFragment, BaseFragment fragment, BaseFragment fragment2, boolean clearBackStack) {
        if (!isFinishingOrDestroyed()) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            if (clearBackStack) {
                clearBackStack();
            }

            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
                    R.anim.slide_in_left, R.anim.slide_out_right);

            String tag = fragment.getClass().toString();
            transaction.add(getLayoutFragmentContainer(), fragment, tag).addToBackStack(tag);

            if (!isFinishingOrDestroyed()) {
                transaction.commitAllowingStateLoss();
            } else {
                return;
            }

            if (actualFragment != null) {
                fragment.setTargetFragment(actualFragment, -1);
            }

            mFragments.add(tag);
            //call executePendingTransactions() else findFragmentByTag() will return null
            getSupportFragmentManager().executePendingTransactions();

            if (fragment2 != null) {
                addFragment(fragment, fragment2, null, false);
            }
        }
    }

    private Fragment getActualFragment() {
        if (mFragments.size() > 0) {
            FragmentManager manager = getSupportFragmentManager();
            return manager.findFragmentByTag(mFragments.get(mFragments.size() - 1));
        }

        return null;
    }

    public void clearBackStack() {
        if (getSupportFragmentManager() != null && !isFinishingOrDestroyed()) {
            while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                FragmentManager.BackStackEntry first = getSupportFragmentManager().getBackStackEntryAt(0);
                if (getSupportFragmentManager() != null && !isFinishingOrDestroyed()) {
                    getSupportFragmentManager().popBackStackImmediate(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
            }
            mFragments = new ArrayList<>();
        }
    }

    private boolean isFinishingOrDestroyed() {
        /*
        boolean isDestroyed = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            isDestroyed = isDestroyed();
        }
        */
        return isFinishing() || isDestroying;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            callFragmentBackPressed();
            super.onBackPressed();
        }
    }

    protected void callFragmentBackPressed() {
        FragmentManager manager = getSupportFragmentManager();

        if (manager != null) {
            if (mFragments.size() > 0) {
                Fragment lastFragment = manager.findFragmentByTag(mFragments.get(mFragments.size() - 1));
                if (lastFragment != null && lastFragment instanceof BaseFragment) {
                    ((BaseFragment) lastFragment).onBackPressed();
                }

                //previous fragment
                if (mFragments.size() > 1) {
                    Fragment prevFragment = manager.findFragmentByTag(mFragments.get(mFragments.size() - 2));
                    if (lastFragment != null && lastFragment instanceof BaseFragment) {
                        ((BaseFragment) prevFragment).onResumeFromBackStack();
                    }
                }

                //remove o fragment
                mFragments.remove(mFragments.size() - 1);
            }
        }
    }

    /*
    private FragmentManager.OnBackStackChangedListener getListener() {
        FragmentManager.OnBackStackChangedListener result = new FragmentManager.OnBackStackChangedListener() {
            public void onBackStackChanged() {
                FragmentManager manager = getSupportFragmentManager();

                if (manager != null) {
                    int backStackEntryCount = manager.getBackStackEntryCount();
                    if (backStackEntryCount > 0) {

                        for (Fragment fragment : manager.getFragments()) {
                            if (fragment.isResumed()) {
                                if (fragment instanceof BaseFragment) {
                                    //((BaseFragment) fragment).onResumeFromBackStack();
                                }
                            }
                        }
                        //Fragment fragment = manager.getFragments().get(manager.getFragments().size()-1);
                        //fragment.isResumed()

                    }
                }
            }
        };

        return result;
    }
    */


    public void setNavBackMode() {
        if (mToolbar != null) {
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        if (mDrawerLayout != null) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
        if (mActionBarDrawerToggle != null) {
            mActionBarDrawerToggle.setDrawerIndicatorEnabled(false);
            mActionBarDrawerToggle.setHomeAsUpIndicator(getDrawerToggleDelegate().getThemeUpIndicator());
            mActionBarDrawerToggle.syncState();
        }
    }

    public void resetBackMode() {
        if (mToolbar != null) {
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
        }

        if (mDrawerLayout != null) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
        if (mActionBarDrawerToggle != null) {
            mActionBarDrawerToggle.setDrawerIndicatorEnabled(true);
            mActionBarDrawerToggle.syncState();
        }
    }

    public void hideDrawerAndBackButton() {
        if (mToolbar != null) {
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
        }

        if (mDrawerLayout != null) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
        if (mActionBarDrawerToggle != null) {
            mActionBarDrawerToggle.setHomeAsUpIndicator(android.R.color.transparent);
            mActionBarDrawerToggle.setDrawerIndicatorEnabled(false);
            mActionBarDrawerToggle.syncState();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }
}

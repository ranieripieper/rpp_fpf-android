package br.org.fpf.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ranieripieper on 5/4/16.
 */
public class Noticia {

    @Expose
    @SerializedName("data")
    private String data;
    @Expose
    @SerializedName("chamada")
    private String chamada;
    @Expose
    @SerializedName("titulo")
    private String titulo;
    @Expose
    @SerializedName("imagem")
    private String imagem;
    @Expose
    @SerializedName("url")
    private String url;

    @Expose
    @SerializedName("texto")
    private String texto;

    @Expose
    @SerializedName("linha_fina")
    private String linhaFina;

    /**
     * Gets the data
     *
     * @return data
     */
    public String getData() {
        return data;
    }

    /**
     * Sets the data
     *
     * @param data
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * Gets the chamada
     *
     * @return chamada
     */
    public String getChamada() {
        return chamada;
    }

    /**
     * Sets the chamada
     *
     * @param chamada
     */
    public void setChamada(String chamada) {
        this.chamada = chamada;
    }

    /**
     * Gets the titulo
     *
     * @return titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * Sets the titulo
     *
     * @param titulo
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * Gets the imagem
     *
     * @return imagem
     */
    public String getImagem() {
        return imagem;
    }

    /**
     * Sets the imagem
     *
     * @param imagem
     */
    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    /**
     * Gets the url
     *
     * @return url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the url
     *
     * @param url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Gets the texto
     *
     * @return texto
     */
    public String getTexto() {
        return texto;
    }

    /**
     * Sets the texto
     *
     * @param texto
     */
    public void setTexto(String texto) {
        this.texto = texto;
    }

    /**
     * Gets the linhaFina
     *
     * @return linhaFina
     */
    public String getLinhaFina() {
        return linhaFina;
    }

    /**
     * Sets the linhaFina
     *
     * @param linhaFina
     */
    public void setLinhaFina(String linhaFina) {
        this.linhaFina = linhaFina;
    }
}

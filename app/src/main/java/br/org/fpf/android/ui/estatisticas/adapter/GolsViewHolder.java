package br.org.fpf.android.ui.estatisticas.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.org.fpf.android.FpfApplication;
import br.org.fpf.android.R;
import br.org.fpf.android.cache.EquipeCacheManager;
import br.org.fpf.android.model.Equipe;
import br.org.fpf.android.model.GolsEquipe;
import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by broto on 10/14/15.
 */
@LayoutId(R.layout.row_estatisticas_gols)
public class GolsViewHolder extends ItemViewHolder<GolsEquipe> {

    @ViewId(R.id.lbl_time)
    TextView lblTime;

    @ViewId(R.id.lbl_gols)
    TextView lblGols;

    @ViewId(R.id.img_time)
    ImageView imgTime;

    public GolsViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(GolsEquipe item, PositionInfo positionInfo) {
        if (TextUtils.isEmpty(item.getJogador())) {
            lblTime.setText(item.getEquipe());
        } else {
            lblTime.setText(item.getJogador());
        }

        Equipe equipe = EquipeCacheManager.getInstance().get(item.getEquipeId());
        if (equipe != null) {
            FpfApplication.imageLoader.displayImage(equipe.getUrlLogo(), imgTime);
        } else {
            imgTime.setImageResource(R.drawable.ic_team_placeholder);
        }
        lblGols.setText(String.valueOf(item.getGols()));
        /*
        if (positionInfo.getPosition() %2 == 0) {
            cardView.setBackgroundColor(getContext().getResources().getColor(R.color.row_1));
        } else {
            cardView.setBackgroundColor(getContext().getResources().getColor(R.color.row_2));
        }*/

    }

    @Override
    public void onSetListeners() {
    }

}
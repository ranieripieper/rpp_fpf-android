package br.org.fpf.android.ui.campeonato.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.org.fpf.android.FpfApplication;
import br.org.fpf.android.R;
import br.org.fpf.android.cache.EquipeCacheManager;
import br.org.fpf.android.model.Artilharia;
import br.org.fpf.android.model.Equipe;
import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by broto on 10/14/15.
 */
@LayoutId(R.layout.row_artilharia)
public class ArtilhariaViewHolder extends ItemViewHolder<Artilharia> {

    @ViewId(R.id.card_view)
    LinearLayout cardView;

    @ViewId(R.id.lbl_posicao)
    TextView lblPosicao;

    @ViewId(R.id.lbl_jogador)
    TextView lblJogador;

    @ViewId(R.id.lbl_time)
    TextView lblTime;

    @ViewId(R.id.lbl_gols)
    TextView lblGols;

    @ViewId(R.id.img_time)
    ImageView imgTime;

    public ArtilhariaViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Artilharia item, PositionInfo positionInfo) {
        lblPosicao.setText(getContext().getString(R.string.position, item.getPosition()));
        lblJogador.setText(item.getJogador());
        lblTime.setText(item.getEquipe());
        lblGols.setText(String.valueOf(item.getTotal()));

        if (positionInfo.getPosition() %2 == 0) {
            cardView.setBackgroundColor(getContext().getResources().getColor(R.color.row_1));
        } else {
            cardView.setBackgroundColor(getContext().getResources().getColor(R.color.row_2));
        }
        Equipe equipe = EquipeCacheManager.getInstance().get(item.getIdEquipe());

        if (equipe == null) {
            imgTime.setImageResource(R.drawable.ic_team_placeholder);
        } else {
            FpfApplication.imageLoader.displayImage(equipe.getUrlLogo(), imgTime);
        }
    }

    @Override
    public void onSetListeners() {

    }
}
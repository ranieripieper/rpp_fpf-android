package br.org.fpf.android.helper;

import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;

import br.org.fpf.android.model.Noticia;

/**
 * Created by ranipieper on 5/5/16.
 */
public class NoticiaHelper {

    private static final String SCRIPT_TAG = "<script";
    private static final String SCRIPT_END_TAG = "</script>";

    private static final String A_HREF_TAG = "<a href";
    private static final String A_HREF_END_TAG = "</a>";

    public static Spanned getTexto(Noticia noticia) {
        if (noticia != null && !TextUtils.isEmpty(noticia.getTexto())) {
            String text = removeHef(removeScript(noticia.getTexto()));
            return Html.fromHtml(text);
        }

        return Html.fromHtml("");
    }

    private static String removeTag(String value, String startTag, String endTag) {

        //prevent loops
        int maxScripts = 20;
        int i = 0;
        while (value.toLowerCase().indexOf(startTag) >= 0) {
            int posInit = value.toLowerCase().indexOf(startTag);
            int posFinal = value.toLowerCase().indexOf(endTag) + endTag.length();
            if (posFinal >= value.length()) {
                posFinal = value.length()-1;
            }
            String newValue = value.substring(0, posInit);
            newValue += value.substring(posFinal, value.length());
            value = newValue;
            i++;
            if (i > maxScripts) {
                break;
            }
        }

        return value;
    }

    private static String removeScript(String value) {
        return removeTag(value, SCRIPT_TAG, SCRIPT_END_TAG);
    }

    private static String removeHef(String value) {
        return removeTag(value, A_HREF_TAG, A_HREF_END_TAG);
    }
}

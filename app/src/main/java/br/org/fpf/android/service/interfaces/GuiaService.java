package br.org.fpf.android.service.interfaces;

import br.org.fpf.android.model.response.GuiaResponse;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by ranipieper on 3/2/16.
 */
public interface GuiaService {

    @GET("/classes/Guia2Divisao")
    void getGuias(@Query("limit") int perPage, Callback<GuiaResponse> response);

    @GET("/classes/Guia2Divisao")
    void getGuias(@Query("where") String where, Callback<GuiaResponse> response);
}

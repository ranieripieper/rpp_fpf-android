package br.org.fpf.android.service.background;

import android.app.IntentService;
import android.content.Intent;

import org.androidannotations.annotations.EIntentService;
import org.androidannotations.annotations.ServiceAction;

import java.util.List;

/**
 * Created by ranipieper on 8/30/16.
 */
@EIntentService
public class UpdateVideosViewsService extends IntentService {
    private static String TAG = "UpdateVideosViewsService";

    public UpdateVideosViewsService() {
        super(UpdateVideosViewsService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
    }


    @ServiceAction
    void updateVideos(List<String> lstVideosIds) {
        VideoService.updateVideos(this, lstVideosIds);
    }
}

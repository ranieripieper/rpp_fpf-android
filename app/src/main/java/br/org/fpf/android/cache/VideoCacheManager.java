package br.org.fpf.android.cache;

import br.org.fpf.android.cache.base.BaseCache;
import br.org.fpf.android.model.Video;

/**
 * Created by ranipieper on 11/19/15.
 */
public class VideoCacheManager extends BaseCache<Video> {

    private static VideoCacheManager instance = new VideoCacheManager();

    private VideoCacheManager() {
    }

    public static VideoCacheManager getInstance() {
        return instance;
    }

    @Override
    public Class<Video> getReferenceClass() {
        return Video.class;
    }

}

package br.org.fpf.android.service.interfaces;

import br.org.fpf.android.model.Classificacao;
import br.org.fpf.android.model.response.ClassificacaoResponse;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by ranipieper on 1/26/16.
 */
public interface ClassificacaoService {

    @GET("/classes/Classificacao/{key}")
    void getClassificacaoByKey(@Path("key") String key, Callback<Classificacao> response);

    @GET("/classes/Classificacao")
    void getClassificacaoBy(@Query("where") String where, Callback<ClassificacaoResponse> response);
}

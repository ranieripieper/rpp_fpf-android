package br.org.fpf.android.cache.base;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by broto on 11/4/15.
 */
public class CacheExpire extends RealmObject {

    @Ignore
    public static final int DEFAULT_CACHE_TIME_LIMIT = 1000 * 60 * 30; // 30 minutes

    @PrimaryKey
    private String id;

    private String className;

    private String key;

    private Long idSerie;

    private long lastUpdated;

    public CacheExpire() {
    }

    public CacheExpire(String className, long lastUpdated) {
        this.className = className;
        this.lastUpdated = lastUpdated;
    }

    /**
     * Gets the key
     *
     * @return key
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the key
     *
     * @param key
     */
    public void setKey(String key) {
        this.key = key;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    /**
     * Gets the idSerie
     *
     * @return idSerie
     */
    public Long getIdSerie() {
        return idSerie;
    }

    /**
     * Sets the idSerie
     *
     * @param idSerie
     */
    public void setIdSerie(Long idSerie) {
        this.idSerie = idSerie;
    }

    /**
     * Gets the id
     *
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the id
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }
}

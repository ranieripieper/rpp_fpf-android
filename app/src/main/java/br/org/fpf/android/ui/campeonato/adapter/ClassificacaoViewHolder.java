package br.org.fpf.android.ui.campeonato.adapter;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;

import br.org.fpf.android.FpfApplication;
import br.org.fpf.android.R;
import br.org.fpf.android.cache.EquipeCacheManager;
import br.org.fpf.android.model.Classificacao;
import br.org.fpf.android.model.Equipe;
import br.org.fpf.android.util.Constants;
import br.org.fpf.android.util.SharedPrefManager;
import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by broto on 10/14/15.
 */
@LayoutId(R.layout.row_classificacao)
public class ClassificacaoViewHolder extends ItemViewHolder<Classificacao> {

    @ViewId(R.id.view_show_estatistica)
    View viewShowEstatistica;

    @ViewId(R.id.card_view)
    LinearLayout cardView;

    @ViewId(R.id.ripple)
    RelativeLayout mMaterialRippleLayout;

    @ViewId(R.id.lbl_posicao)
    TextView lblPosicao;

    @ViewId(R.id.lbl_time)
    TextView lblTime;

    @ViewId(R.id.lbl_pontos)
    TextView lblPontos;

    @ViewId(R.id.lbl_jogos)
    TextView lblJogos;

    @ViewId(R.id.lbl_vitorias)
    TextView lblVitorias;

    @ViewId(R.id.lbl_empates)
    TextView lblEmpates;

    @ViewId(R.id.lbl_derrotas)
    TextView lblDerrotas;

    @ViewId(R.id.lbl_saldo)
    TextView lblSaldo;

    @ViewId(R.id.img_time)
    ImageView imgTime;

    public ClassificacaoViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Classificacao item, PositionInfo positionInfo) {

        lblPosicao.setText(getContext().getString(R.string.position, item.getPosicao()));
        lblTime.setText(item.getNomeEquipe());

        lblPontos.setText(item.getPontos());
        lblJogos.setText(item.getJogos());
        lblVitorias.setText(item.getVitorias());
        lblEmpates.setText(item.getEmpates());
        lblDerrotas.setText(item.getDerrotas());
        lblSaldo.setText(item.getSaldoGols());

        setBackground(positionInfo.getPosition());

        Equipe equipe = EquipeCacheManager.getInstance().get(item.getIdEquipe());

        if (equipe == null) {
            imgTime.setImageResource(R.drawable.ic_team_placeholder);
        } else {
            FpfApplication.imageLoader.displayImage(equipe.getUrlLogo(), imgTime);
        }

        if (Constants.ID_A1.equals(item.getIdCampeonato())) {
            mMaterialRippleLayout.setEnabled(true);
        } else {
            mMaterialRippleLayout.setEnabled(false);
        }

        if (Constants.ID_A1.equals(item.getIdCampeonato()) && positionInfo.getPosition() == 0 && SharedPrefManager.getInstance().isShowInfoEstatisticaTime()) {
            createAnimation();
        } else {
            viewShowEstatistica.setVisibility(View.GONE);
        }
    }

    private void createAnimation() {
        viewShowEstatistica.setVisibility(View.VISIBLE);

        animate(viewShowEstatistica);

        MaterialDialog dialog = new MaterialDialog.Builder(getContext())
                .content(R.string.info_estatisticas_time)
                .backgroundColorRes(R.color.dialog_estatisticas_time)
                .contentColor(Color.WHITE)
                .contentGravity(GravityEnum.CENTER)
                .theme(Theme.DARK)
                .cancelable(true)
                .cancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        viewShowEstatistica.setVisibility(View.GONE);
                        SharedPrefManager.getInstance().setShowInfoEstatisticaTime(false);
                    }
                })
                .dismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        viewShowEstatistica.setVisibility(View.GONE);
                        SharedPrefManager.getInstance().setShowInfoEstatisticaTime(false);
                    }
                })
                .build();

        // make dialog itself transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // remove background dim
        dialog.getWindow().setDimAmount(0);
        dialog.show();
    }

    private void animate(final View viewShowEstatistica) {
        ViewCompat.animate(viewShowEstatistica).alpha(0).scaleX(22f).scaleY(22f).setDuration(750).withEndAction(new Runnable() {
            @Override
            public void run() {
                viewShowEstatistica.setScaleX(1f);
                viewShowEstatistica.setScaleY(1f);
                viewShowEstatistica.setAlpha(1f);
                animate(viewShowEstatistica);
            }
        });
    }

    private void setBackground(int position) {
        int resStroke;
        if (position % 2 == 0) {
            cardView.setBackgroundColor(getContext().getResources().getColor(R.color.row_1));
            mMaterialRippleLayout.setBackgroundDrawable(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.row_1_selector, null));
            resStroke = R.drawable.bg_stroke_left_row1;

        } else {
            cardView.setBackgroundColor(getContext().getResources().getColor(R.color.row_2));
            mMaterialRippleLayout.setBackgroundDrawable(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.row_2_selector, null));
            resStroke = R.drawable.bg_stroke_left_row2;
        }

        lblPontos.setBackgroundResource(resStroke);
        lblJogos.setBackgroundResource(resStroke);
        lblVitorias.setBackgroundResource(resStroke);
        lblEmpates.setBackgroundResource(resStroke);
        lblDerrotas.setBackgroundResource(resStroke);
        lblSaldo.setBackgroundResource(resStroke);
    }

    @Override
    public void onSetListeners() {
        mMaterialRippleLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClassificacaoHolderListener listener = getListener(ClassificacaoHolderListener.class);
                if (listener != null) {
                    listener.onTimeSelected(getItem());
                }

            }
        });
    }

    public interface ClassificacaoHolderListener {
        void onTimeSelected(Classificacao classificacao);
    }
}
package br.org.fpf.android.cache;

import java.util.ArrayList;
import java.util.List;

import br.org.fpf.android.cache.base.BaseCache;
import br.org.fpf.android.model.Campeonato;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by ranipieper on 11/19/15.
 */
public class CampeonatoCacheManager extends BaseCache<Campeonato> {

    String[] DEFAULT_ORDER_FIELDS = {"ordem", "nomeCampeonatoApp"};
    Sort[] DEFAULT_ORDER_SORT = {Sort.ASCENDING, Sort.ASCENDING};

    private static CampeonatoCacheManager instance = new CampeonatoCacheManager();

    private CampeonatoCacheManager() {
    }

    public static CampeonatoCacheManager getInstance() {
        return instance;
    }

    public List<Campeonato> getAll() {
        RealmResults<Campeonato> results = getRealm().where(Campeonato.class)
                .findAllSorted(DEFAULT_ORDER_FIELDS, DEFAULT_ORDER_SORT);
        if (results != null) {
            return getRealm().copyFromRealm(results);
        }
        return new ArrayList();

    }

    public List<Campeonato> getAllVisibles() {
        RealmResults<Campeonato> results = getRealm().where(Campeonato.class)
                .equalTo("visible", true)
                .findAllSorted(DEFAULT_ORDER_FIELDS, DEFAULT_ORDER_SORT);
        if (results != null) {
            return getRealm().copyFromRealm(results);
        }
        return new ArrayList();

    }

    @Override
    public Class<Campeonato> getReferenceClass() {
        return Campeonato.class;
    }

    @Override
    protected long getCacheLifeTime() {
        return 1000 * 60 * 60 * 24 * 15; // 15 dias
    }
}

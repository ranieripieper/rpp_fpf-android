package br.org.fpf.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ranieripieper on 1/30/16.
 */
public class EscalacaoJogador extends RealmObject {

    @PrimaryKey
    private String id;

    @Expose
    @SerializedName("Id")
    private String idJogador;

    @Expose
    @SerializedName("IdPartida")
    private Long idPartida;

    @Expose
    @SerializedName("IdJogadorSubstituto")
    private String idJogadorSubstituto;

    @Expose
    @SerializedName("Nome")
    private String nome;

    @Expose
    @SerializedName("Posicao")
    private String posicao;

    @Expose
    @SerializedName("Substituido")
    private String substituido;

    private boolean substitudo;

    private int nrCartaoAmarelo;
    private int nrCartaoVermelho;
    private int nrGols;
    private int nrGolsContra;

    /**
     * Gets the id
     *
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the id
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets the idJogador
     *
     * @return idJogador
     */
    public String getIdJogador() {
        return idJogador;
    }

    /**
     * Sets the idJogador
     *
     * @param idJogador
     */
    public void setIdJogador(String idJogador) {
        this.idJogador = idJogador;
    }

    /**
     * Gets the idPartida
     *
     * @return idPartida
     */
    public Long getIdPartida() {
        return idPartida;
    }

    /**
     * Sets the idPartida
     *
     * @param idPartida
     */
    public void setIdPartida(Long idPartida) {
        this.idPartida = idPartida;
    }

    /**
     * Gets the idJogadorSubstituto
     *
     * @return idJogadorSubstituto
     */
    public String getIdJogadorSubstituto() {
        return idJogadorSubstituto;
    }

    /**
     * Sets the idJogadorSubstituto
     *
     * @param idJogadorSubstituto
     */
    public void setIdJogadorSubstituto(String idJogadorSubstituto) {
        this.idJogadorSubstituto = idJogadorSubstituto;
    }

    /**
     * Gets the nome
     *
     * @return nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the nome
     *
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Gets the posicao
     *
     * @return posicao
     */
    public String getPosicao() {
        return posicao;
    }

    /**
     * Sets the posicao
     *
     * @param posicao
     */
    public void setPosicao(String posicao) {
        this.posicao = posicao;
    }

    /**
     * Gets the substituido
     *
     * @return substituido
     */
    public String getSubstituido() {
        return substituido;
    }

    /**
     * Sets the substituido
     *
     * @param substituido
     */
    public void setSubstituido(String substituido) {
        this.substituido = substituido;
    }

    /**
     * Gets the substitudo
     *
     * @return substitudo
     */
    public boolean isSubstitudo() {
        return substitudo;
    }

    /**
     * Sets the substitudo
     *
     * @param substitudo
     */
    public void setSubstitudo(boolean substitudo) {
        this.substitudo = substitudo;
    }

    /**
     * Gets the nrCartaoAmarelo
     *
     * @return nrCartaoAmarelo
     */
    public int getNrCartaoAmarelo() {
        return nrCartaoAmarelo;
    }

    /**
     * Sets the nrCartaoAmarelo
     *
     * @param nrCartaoAmarelo
     */
    public void setNrCartaoAmarelo(int nrCartaoAmarelo) {
        this.nrCartaoAmarelo = nrCartaoAmarelo;
    }

    /**
     * Gets the nrCartaoVermelho
     *
     * @return nrCartaoVermelho
     */
    public int getNrCartaoVermelho() {
        return nrCartaoVermelho;
    }

    /**
     * Sets the nrCartaoVermelho
     *
     * @param nrCartaoVermelho
     */
    public void setNrCartaoVermelho(int nrCartaoVermelho) {
        this.nrCartaoVermelho = nrCartaoVermelho;
    }

    /**
     * Gets the nrGols
     *
     * @return nrGols
     */
    public int getNrGols() {
        return nrGols;
    }

    /**
     * Sets the nrGols
     *
     * @param nrGols
     */
    public void setNrGols(int nrGols) {
        this.nrGols = nrGols;
    }

    /**
     * Gets the nrGolsContra
     *
     * @return nrGolsContra
     */
    public int getNrGolsContra() {
        return nrGolsContra;
    }

    /**
     * Sets the nrGolsContra
     *
     * @param nrGolsContra
     */
    public void setNrGolsContra(int nrGolsContra) {
        this.nrGolsContra = nrGolsContra;
    }

}

package br.org.fpf.android.ui.guia.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.org.fpf.android.R;
import br.org.fpf.android.model.Guia;
import br.org.fpf.android.util.Constants;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderAdapter;
import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;
import uk.co.ribot.easyadapter.ItemViewHolder;

/**
 * Created by ranipieper on 3/16/16.
 */
public class GuiaHomeAdapter<T> extends EasyLoadMoreRecyclerAdapter<T> implements StickyHeaderAdapter<GuiaHomeAdapter.HeaderHolder> {

    @Override
    public GuiaHomeAdapter.HeaderHolder onCreateHeaderViewHolder(ViewGroup viewGroup) {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.header_guia_home, viewGroup, false);
        return new HeaderHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(GuiaHomeAdapter.HeaderHolder headerHolder, int position) {
        Guia obj = (Guia) getItem(getRealPosition(position));

        String header = "";
        if (Constants.ID_A1.equals(obj.getIdCampeonato())) {
            header = mContext.getString(R.string.serie_a1);
        } else if (Constants.ID_A2.equals(obj.getIdCampeonato())) {
            header = mContext.getString(R.string.serie_a2);
        } else if (Constants.ID_A3.equals(obj.getIdCampeonato())) {
            header = mContext.getString(R.string.serie_a3);
        }
        headerHolder.mHeaderTextView.setText(header);
    }

    @Override
    public long getHeaderId(int position) {
        Guia obj = (Guia) getItem(getRealPosition(position));
        return obj.getIdCampeonato();
    }

    /**
     * Header View Holder.
     */
    protected class HeaderHolder extends RecyclerView.ViewHolder {

        public TextView mHeaderTextView;

        /**
         * Constructor with the View.
         *
         * @param view view object
         */
        public HeaderHolder(View view) {
            super(view);
            mHeaderTextView = (TextView) view.findViewById(R.id.lbl_header);
        }
    }

    /**
     * Constructs and EasyAdapter with a Context, an {@link ItemViewHolder} class, list of items and a generic listener.
     *
     * @param context             a valid Context
     * @param itemViewHolderClass your {@link ItemViewHolder} implementation class
     * @param listItems           the list of items to load in the Adapter
     * @param listener            a generic object that you can access from your {@link ItemViewHolder} by calling
     *                            {@link ItemViewHolder#getListener()}, This can be used to pass a listener to the view holder that then you
     *                            can cast and use as a callback.
     */
    public GuiaHomeAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, List<T> listItems, Object listener, Integer itemLayoutLoadMoreId) {
        super(context, itemViewHolderClass, listItems, listener, itemLayoutLoadMoreId);
    }

    /**
     * Constructs and EasyAdapter with a Context, an {@link ItemViewHolder} class, list of items and a generic listener.
     * @param context
     * @param itemViewHolderClass
     * @param listItems
     * @param listener
     * @param itemLayoutLoadMoreId
     * @param header
     */
    public GuiaHomeAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, List<T> listItems, Object listener, Integer itemLayoutLoadMoreId, View header) {
        super(context, itemViewHolderClass, listItems, listener, itemLayoutLoadMoreId, header);
    }


}
package br.org.fpf.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

/**
 * Created by ranipieper on 3/8/16.
 */
public class EstatisticasTime implements Parcelable {

    @Expose
    @SerializedName("IdEquipe")
    private long idEquipe;

    @Expose
    @SerializedName("Equipe")
    private String equipe;

    @Expose
    @SerializedName("GolsContra")
    private List<GolsEquipe> golsContra;

    @Expose
    @SerializedName("GolsPro")
    private List<GolsEquipe> golsPro;

    @Expose
    @SerializedName("JogosComoMandante")
    private List<Jogos> jogosComoMandante;

    @Expose
    @SerializedName("JogosComoVisitante")
    private List<Jogos> jogosComoVisitante;

    @Expose
    @SerializedName("PontosComoMandante")
    private long pontosComoMandante;

    @Expose
    @SerializedName("PontosComoVisitante")
    private long pontosComoVisitante;

    @Expose
    @SerializedName("TotalJogosComoMandante")
    private long totalJogosComoMandante;

    @Expose
    @SerializedName("TotalJogosComoVisitante")
    private long totalJogosComoVisitante;

    @Expose
    @SerializedName("objectId")
    private String objectId;

    @Expose
    @SerializedName("createdAt")
    private Date createdAt;

    @Expose
    @SerializedName("updatedAt")
    private Date updatedAt;


    public String getTotalPontos() {
        return String.valueOf((this.pontosComoMandante + this.pontosComoVisitante));
    }


    public long getIdEquipe() {
        return idEquipe;
    }

    public void setIdEquipe(long idEquipe) {
        this.idEquipe = idEquipe;
    }

    public String getEquipe() {
        return equipe;
    }

    public void setEquipe(String equipe) {
        this.equipe = equipe;
    }

    public List<GolsEquipe> getGolsContra() {
        return golsContra;
    }

    public void setGolsContra(List<GolsEquipe> golsContra) {
        this.golsContra = golsContra;
    }

    public List<GolsEquipe> getGolsPro() {
        return golsPro;
    }

    public void setGolsPro(List<GolsEquipe> golsPro) {
        this.golsPro = golsPro;
    }

    public List<Jogos> getJogosComoMandante() {
        return jogosComoMandante;
    }

    public void setJogosComoMandante(List<Jogos> jogosComoMandante) {
        this.jogosComoMandante = jogosComoMandante;
    }

    public List<Jogos> getJogosComoVisitante() {
        return jogosComoVisitante;
    }

    public void setJogosComoVisitante(List<Jogos> jogosComoVisitante) {
        this.jogosComoVisitante = jogosComoVisitante;
    }

    public long getPontosComoMandante() {
        return pontosComoMandante;
    }

    public void setPontosComoMandante(long pontosComoMandante) {
        this.pontosComoMandante = pontosComoMandante;
    }

    public long getPontosComoVisitante() {
        return pontosComoVisitante;
    }

    public void setPontosComoVisitante(long pontosComoVisitante) {
        this.pontosComoVisitante = pontosComoVisitante;
    }

    public long getTotalJogosComoMandante() {
        return totalJogosComoMandante;
    }

    public void setTotalJogosComoMandante(long totalJogosComoMandante) {
        this.totalJogosComoMandante = totalJogosComoMandante;
    }

    public long getTotalJogosComoVisitante() {
        return totalJogosComoVisitante;
    }

    public void setTotalJogosComoVisitante(long totalJogosComoVisitante) {
        this.totalJogosComoVisitante = totalJogosComoVisitante;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.idEquipe);
        dest.writeString(this.equipe);
        dest.writeTypedList(golsContra);
        dest.writeTypedList(golsPro);
        dest.writeTypedList(jogosComoMandante);
        dest.writeTypedList(jogosComoVisitante);
        dest.writeLong(this.pontosComoMandante);
        dest.writeLong(this.pontosComoVisitante);
        dest.writeLong(this.totalJogosComoMandante);
        dest.writeLong(this.totalJogosComoVisitante);
        dest.writeString(this.objectId);
        dest.writeLong(createdAt != null ? createdAt.getTime() : -1);
        dest.writeLong(updatedAt != null ? updatedAt.getTime() : -1);
    }

    public EstatisticasTime() {
    }

    protected EstatisticasTime(Parcel in) {
        this.idEquipe = in.readLong();
        this.equipe = in.readString();
        this.golsContra = in.createTypedArrayList(GolsEquipe.CREATOR);
        this.golsPro = in.createTypedArrayList(GolsEquipe.CREATOR);
        this.jogosComoMandante = in.createTypedArrayList(Jogos.CREATOR);
        this.jogosComoVisitante = in.createTypedArrayList(Jogos.CREATOR);
        this.pontosComoMandante = in.readLong();
        this.pontosComoVisitante = in.readLong();
        this.totalJogosComoMandante = in.readLong();
        this.totalJogosComoVisitante = in.readLong();
        this.objectId = in.readString();
        long tmpCreatedAt = in.readLong();
        this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
        long tmpUpdatedAt = in.readLong();
        this.updatedAt = tmpUpdatedAt == -1 ? null : new Date(tmpUpdatedAt);
    }

    public static final Creator<EstatisticasTime> CREATOR = new Creator<EstatisticasTime>() {
        @Override
        public EstatisticasTime createFromParcel(Parcel source) {
            return new EstatisticasTime(source);
        }

        @Override
        public EstatisticasTime[] newArray(int size) {
            return new EstatisticasTime[size];
        }
    };
}

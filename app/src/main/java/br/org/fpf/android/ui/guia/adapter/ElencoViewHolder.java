package br.org.fpf.android.ui.guia.adapter;

import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import br.org.fpf.android.R;
import br.org.fpf.android.model.Elenco;
import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by ranipieper on 3/3/16.
 */
@LayoutId(R.layout.row_guia_elenco)
public class ElencoViewHolder extends ItemViewHolder<Elenco> {

    @ViewId(R.id.card_view)
    CardView cardView;

    @ViewId(R.id.lbl_nome)
    TextView lblNome;

    @ViewId(R.id.lbl_data)
    TextView lblData;

    @ViewId(R.id.lbl_posicao)
    TextView lblPosicao;

    public ElencoViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Elenco item, PositionInfo positionInfo) {
        lblNome.setText(item.getNome());
        lblData.setText(item.getNascimento());
        lblPosicao.setText(item.getPosicao());

        if (positionInfo.getPosition() %2 == 0) {
            cardView.setCardBackgroundColor(getContext().getResources().getColor(R.color.row_1));
        } else {
            cardView.setCardBackgroundColor(getContext().getResources().getColor(R.color.row_2));
        }
    }

    @Override
    public void onSetListeners() {
    }
}
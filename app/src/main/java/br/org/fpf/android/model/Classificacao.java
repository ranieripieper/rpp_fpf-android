package br.org.fpf.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ranipieper on 1/26/16.
 */
public class Classificacao extends RealmObject {

    @PrimaryKey
    //id é formao por Grupo, Fase, IdCampeonato e idEquipe
    private String id;

    @SerializedName("Grupo")
    @Expose
    private String grupo;

    @SerializedName("IdCampeonato")
    @Expose
    private Long idCampeonato;

    @SerializedName("IdEquipe")
    @Expose
    private Long idEquipe;

    @SerializedName("Fase")
    @Expose
    private String fase;

    @SerializedName("Aproveitamento")
    @Expose
    private String aproveitamento;

    @SerializedName("Derrotas")
    @Expose
    private String derrotas;

    @SerializedName("DerrotasCasa")
    @Expose
    private String derrotasCasa;

    @SerializedName("DerrotasFora")
    @Expose
    private String derrotasFora;

    @SerializedName("Empates")
    @Expose
    private String empates;

    @SerializedName("EmpatesCasa")
    @Expose
    private String empatesCasa;

    @SerializedName("EmpatesFora")
    @Expose
    private String empatesFora;

    @SerializedName("GolsPro")
    @Expose
    private String golsPro;

    @SerializedName("GolsSofridos")
    @Expose
    private String golsSofridos;

    @SerializedName("Jogos")
    @Expose
    private String jogos;

    @SerializedName("NomeCampeonato")
    @Expose
    private String nomeCampeonato;

    @SerializedName("NomeEquipe")
    @Expose
    private String nomeEquipe;

    @SerializedName("PontoMaximo")
    @Expose
    private String pontoMaximo;

    @SerializedName("Pontos")
    @Expose
    private String pontos;

    @SerializedName("Posicao")
    @Expose
    private Integer posicao;

    @SerializedName("Rodada")
    @Expose
    private String rodada;

    @SerializedName("SaldoGols")
    @Expose
    private String saldoGols;

    @SerializedName("Taca")
    @Expose
    private String taca;

    @SerializedName("Vitorias")
    @Expose
    private String vitorias;

    @SerializedName("VitoriasCasa")
    @Expose
    private String vitoriasCasa;

    @SerializedName("VitoriasFora")
    @Expose
    private String vitoriasFora;

    public String getAproveitamento() {
        return aproveitamento;
    }

    public void setAproveitamento(String aproveitamento) {
        this.aproveitamento = aproveitamento;
    }

    public String getDerrotas() {
        return derrotas;
    }

    public void setDerrotas(String derrotas) {
        this.derrotas = derrotas;
    }

    public String getDerrotasCasa() {
        return derrotasCasa;
    }

    public void setDerrotasCasa(String derrotasCasa) {
        this.derrotasCasa = derrotasCasa;
    }

    public String getDerrotasFora() {
        return derrotasFora;
    }

    public void setDerrotasFora(String derrotasFora) {
        this.derrotasFora = derrotasFora;
    }

    public String getEmpates() {
        return empates;
    }

    public void setEmpates(String empates) {
        this.empates = empates;
    }

    public String getEmpatesCasa() {
        return empatesCasa;
    }

    public void setEmpatesCasa(String empatesCasa) {
        this.empatesCasa = empatesCasa;
    }

    public String getEmpatesFora() {
        return empatesFora;
    }

    public void setEmpatesFora(String empatesFora) {
        this.empatesFora = empatesFora;
    }

    public String getFase() {
        return fase;
    }

    public void setFase(String fase) {
        this.fase = fase;
    }

    public String getGolsPro() {
        return golsPro;
    }

    public void setGolsPro(String golsPro) {
        this.golsPro = golsPro;
    }

    public String getGolsSofridos() {
        return golsSofridos;
    }

    public void setGolsSofridos(String golsSofridos) {
        this.golsSofridos = golsSofridos;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public Long getIdCampeonato() {
        return idCampeonato;
    }

    public void setIdCampeonato(Long idCampeonato) {
        this.idCampeonato = idCampeonato;
    }

    public Long getIdEquipe() {
        return idEquipe;
    }

    public void setIdEquipe(Long idEquipe) {
        this.idEquipe = idEquipe;
    }

    public String getJogos() {
        return jogos;
    }

    public void setJogos(String jogos) {
        this.jogos = jogos;
    }

    public String getNomeCampeonato() {
        return nomeCampeonato;
    }

    public void setNomeCampeonato(String nomeCampeonato) {
        this.nomeCampeonato = nomeCampeonato;
    }

    public String getNomeEquipe() {
        return nomeEquipe;
    }

    public void setNomeEquipe(String nomeEquipe) {
        this.nomeEquipe = nomeEquipe;
    }

    public String getPontoMaximo() {
        return pontoMaximo;
    }

    public void setPontoMaximo(String pontoMaximo) {
        this.pontoMaximo = pontoMaximo;
    }

    public String getPontos() {
        return pontos;
    }

    public void setPontos(String pontos) {
        this.pontos = pontos;
    }

    public Integer getPosicao() {
        return posicao;
    }

    public void setPosicao(Integer posicao) {
        this.posicao = posicao;
    }

    public String getRodada() {
        return rodada;
    }

    public void setRodada(String rodada) {
        this.rodada = rodada;
    }

    public String getSaldoGols() {
        return saldoGols;
    }

    public void setSaldoGols(String saldoGols) {
        this.saldoGols = saldoGols;
    }

    public String getTaca() {
        return taca;
    }

    public void setTaca(String taca) {
        this.taca = taca;
    }

    public String getVitorias() {
        return vitorias;
    }

    public void setVitorias(String vitorias) {
        this.vitorias = vitorias;
    }

    public String getVitoriasCasa() {
        return vitoriasCasa;
    }

    public void setVitoriasCasa(String vitoriasCasa) {
        this.vitoriasCasa = vitoriasCasa;
    }

    public String getVitoriasFora() {
        return vitoriasFora;
    }

    public void setVitoriasFora(String vitoriasFora) {
        this.vitoriasFora = vitoriasFora;
    }

    /**
     * Gets the id
     *
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the id
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

}

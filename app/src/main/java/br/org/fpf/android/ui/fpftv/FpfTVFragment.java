package br.org.fpf.android.ui.fpftv;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.h6ah4i.android.tablayouthelper.TabLayoutHelper;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import br.org.fpf.android.R;
import br.org.fpf.android.ui.base.BaseFragment;
import br.org.fpf.android.ui.custom.FpfViewPagerAdapter;

/**
 * Created by ranipieper on 1/26/16.
 */
@EFragment(R.layout.fragment_fpf_tv)
public class FpfTVFragment extends BaseFragment {

    @ViewById(R.id.tablayout)
    TabLayout mTabLayout;

    @ViewById(R.id.viewpager)
    public ViewPager mViewPager;

    FpfViewPagerAdapter mAdapter;

    private TabLayoutHelper mTabLayoutHelper;

    public static FpfTVFragment newInstance() {
        Bundle args = new Bundle();

        FpfTVFragment fragment = FpfTVFragment_.builder().build();

        fragment.setArguments(args);

        return fragment;
    }

    @AfterViews
    public void afterViews() {
        super.afterViews();

        setupViewPager();
    }

    @Override
    public void onResumeFromBackStack() {
        super.onResumeFromBackStack();
        if (mAdapter != null && mViewPager != null) {
            BaseFragment baseFragment = (BaseFragment)mAdapter.getItem(mViewPager.getCurrentItem());
            if (baseFragment != null) {
                baseFragment.onResumeFromBackStack();
            }
        }
    }

    private void setupViewPager() {

        mAdapter = new FpfViewPagerAdapter(getChildFragmentManager());


        mAdapter.addFrag(FpfTVAoVivoFragment.newInstance(), getString(R.string.ao_vivo), isAdded());
        mAdapter.addFrag(FpfTVVideosFragment.newInstance(), getString(R.string.videos), isAdded());

        mViewPager.setAdapter(mAdapter);

        mViewPager.setOffscreenPageLimit(2);
        mTabLayout.setVisibility(View.VISIBLE);
        final Handler handler = new Handler();
        Runnable refresh = new Runnable() {
            public void run() {
                if (isAdded()) {
                    hideLoadingView();
                    TabLayoutHelper mTabLayoutHelper = new TabLayoutHelper(mTabLayout, mViewPager);
                    mTabLayoutHelper.setAutoAdjustTabModeEnabled(true);
                    mTabLayout.setVisibility(View.VISIBLE);
                    mViewPager.setVisibility(View.VISIBLE);
                }
            }
        };
        showLoadingView();
        handler.postDelayed(refresh, 100);

        hideLoadingView();
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.fpf_tv);
    }

    @Override
    public void onDestroyView() {
        // release the TabLayoutHelper instance
        if (mTabLayoutHelper != null) {
            mTabLayoutHelper.release();
            mTabLayoutHelper = null;
        }

        super.onDestroyView();
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.DRAWER;
    }
}

package br.org.fpf.android.util;

/**
 * Created by ranipieper on 1/26/16.
 */
public class Constants {

    public final static Long ID_A1 = 444l;
    public final static Long ID_A2 = 456l;
    public final static Long ID_A3 = 461l;
    public final static Long ID_LIBERTADORES = 449l;
    public final static Long ID_PAULISTA_2_DIVISAO = 478l;
    public final static Long ID_BRASILEIRAO = 475l;

    public static final Long YOUTUBE_MAX_RESULTS = 50l;

    //narracao
    public final static String NARRACAO_CARTAO_AMARELO = "Cartão amarelo";
    public final static String NARRACAO_CARTAO_VERMELHO = "Cartão vermelho";
    public final static String NARRACAO_SUBSTITUICAO = "Substituição";
    public final static String NARRACAO_GOL = "Gol";
    public final static String NARRACAO_FIM_JOGO = "Fim de jogo";

    public final static int DELAY_VIEW_PAGE = 300;

    public final static int REFRESH_NARRACAO = 15000;
    public final static int REG_POR_PAG = 20;
    public final static int REG_POR_PAG_MAX = 1000;

    public final static String PRIMEIRA_FASE = "PRIMEIRA FASE";
    public final static String SEGUNDA_FASE = "SEGUNDA FASE";
    public final static String PERIODO_PARTIDA_ENCERRADA = "Partida encerrada";
    public final static String PERIODO_PARTIDA_CANCELADA = "Partida cancelada";

    /* Arguments */
    public static final String ARG_ID_CAMPEONATO = "ARG_ID_CAMPEONATO";
    public static final String ARG_FASE = "ARG_FASE";
    public static final String ARG_RODADA = "ARG_RODADA";
    public static final String ARG_GROUP_PARTIDAS = "ARG_GROUP_PARTIDAS";
    public static final String ARG_ID_PARTIDA = "ARG_ID_PARTIDA";
    public static final String ARG_ID_TIME = "ARG_ID_TIME";
    public static final String ARG_ESTATISTICAS_TIME = "ARG_ESTATISTICAS_TIME";
    public static final String ARG_A_FAVOR = "ARG_A_FAVOR";
    public static final String ARG_PUSH_MSG = "ARG_PUSH_MSG";
    public static final String ARG_ID_GUIA = "ARG_ID_GUIA";
    public static final String ARG_URL = "ARG_URL";

}

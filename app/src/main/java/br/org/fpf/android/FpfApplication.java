package br.org.fpf.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.core.CrashlyticsCore;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseUser;

import java.io.IOException;
import java.util.List;

import br.org.fpf.android.cache.GuiaCacheManager;
import br.org.fpf.android.cache.base.RealmManager;
import br.org.fpf.android.model.Campeonato;
import br.org.fpf.android.model.Equipe;
import br.org.fpf.android.service.CampeonatoServiceImpl;
import br.org.fpf.android.service.EquipeServiceImpl;
import br.org.fpf.android.service.RetrofitManager;
import br.org.fpf.android.util.SharedPrefManager;
import io.fabric.sdk.android.Fabric;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by ranipieper on 1/14/16.
 */
public class FpfApplication extends MultiDexApplication {

    public static ImageLoader imageLoader;
    private static YouTube mYoutube;

    @Override
    public void onCreate() {
        super.onCreate();

        initCrashlytics();

        Parse.initialize(this, BuildConfig.PARSE_APPLICATION_ID, BuildConfig.PARSE_CLIENT_KEY);
        if (BuildConfig.DEBUG) {
            Parse.setLogLevel(Parse.LOG_LEVEL_VERBOSE);
        } else {
            Parse.setLogLevel(Parse.LOG_LEVEL_NONE);
        }

        SharedPrefManager.init(this);
        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();
        ParseACL.setDefaultACL(defaultACL, true);

        //init retrofit
        initRetrofitManager();

        initCalligraphy();
        initImageLoader();

        initCache();
    }

    private void initCrashlytics() {
        // Set up Crashlytics, disabled for debug builds
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder()
                        .disabled(BuildConfig.DEBUG)
                        .build())
                .build();
        // Initialize Fabric with the debug-disabled crashlytics.
        if (!BuildConfig.DEBUG) {
            Fabric.with(this, crashlyticsKit, new Answers(), new Crashlytics());
        } else {
            Fabric.with(this, crashlyticsKit);
        }
    }

    private void initCache() {
        RealmManager.getInstance(this);
        updateCampeonatos();
        if (SharedPrefManager.getInstance().isDeleteGuiaPaulistao()) {
            GuiaCacheManager.getInstance().deleteAll();
            SharedPrefManager.getInstance().setDeleteGuiaPaulistao(false);
        }
    }

    private void updateCampeonatos() {
        CampeonatoServiceImpl.getCampeonatos(new Callback<List<Campeonato>>() {
            @Override
            public void success(List<Campeonato> campeonatos, Response response) {
                updateEquipes();
            }

            @Override
            public void failure(RetrofitError error) {
                updateEquipes();
            }
        });
    }

    private void updateEquipes() {
        EquipeServiceImpl.getEquipes(new Callback<List<Equipe>>() {
            @Override
            public void success(List<Equipe> equipes, Response response) {
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    private void initRetrofitManager() {
        RetrofitManager.getInstance().initialize(getResources().getConfiguration().locale.getLanguage());
    }

    private void initCalligraphy() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/HelveticaNeue.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
    }

    private void initImageLoader() {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_team_placeholder)
                .showImageForEmptyUri(R.drawable.ic_team_placeholder)
                .showImageOnFail(R.drawable.ic_team_placeholder)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .resetViewBeforeLoading(true)
                .considerExifParams(false)
                .displayer(new FadeInBitmapDisplayer(300))
                .cacheInMemory(true)
                .cacheOnDisc(true).build();


        ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(options)
                .memoryCache(new WeakMemoryCache());

        if (BuildConfig.DEBUG) {
            //builder.writeDebugLogs();
        }
        ImageLoaderConfiguration config = builder.build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private static void initYoutubeApi() {
        mYoutube = new YouTube.Builder(new NetHttpTransport(),
                new JacksonFactory(), new HttpRequestInitializer() {
            @Override
            public void initialize(HttpRequest hr) throws IOException {
            }
        }).setApplicationName("federalpaulista").build();
    }

    public static YouTube getYoutubeApi() {
        if (mYoutube == null) {
            initYoutubeApi();
        }
        return mYoutube;
    }
}

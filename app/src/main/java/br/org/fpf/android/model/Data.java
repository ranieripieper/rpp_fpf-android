package br.org.fpf.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by ranieripieper on 1/29/16.
 */
public class Data extends RealmObject {

    @Expose
    @SerializedName("__type")
    private String type;

    @Expose
    @SerializedName("iso")
    private Date data;

    /**
     * Gets the type
     *
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type
     *
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets the data
     *
     * @return data
     */
    public Date getData() {
        return data;
    }

    /**
     * Sets the data
     *
     * @param data
     */
    public void setData(Date data) {
        this.data = data;
    }
}

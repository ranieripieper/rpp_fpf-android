package br.org.fpf.android.ui.home;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.design.widget.NavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.org.fpf.android.R;
import br.org.fpf.android.cache.PartidaCacheManager;
import br.org.fpf.android.model.Campeonato;
import br.org.fpf.android.model.Partida;
import br.org.fpf.android.model.PushNotificationMessage;
import br.org.fpf.android.service.CampeonatoServiceImpl;
import br.org.fpf.android.ui.base.BaseFragmentActivity;
import br.org.fpf.android.ui.base.ErrorListener;
import br.org.fpf.android.ui.base.ViewUtil;
import br.org.fpf.android.ui.home.adapter.MenuAdapter;
import br.org.fpf.android.ui.home.adapter.MenuItem;
import br.org.fpf.android.ui.home.adapter.SubMenuItem;
import br.org.fpf.android.ui.navigation.Navigator;
import br.org.fpf.android.util.Constants;
import br.org.fpf.android.util.SharedPrefManager;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 1/14/16.
 */
@EActivity(R.layout.activity_main)
public class MainActivity extends BaseFragmentActivity {

    private MenuItem mMenuSelected = null;

    @ViewById(R.id.nav_view)
    NavigationView mNavigationView;

    @ViewById(R.id.recyler_view_menu)
    RecyclerView mRecyclerViewMenu;

    @Extra(Constants.ARG_PUSH_MSG)
    PushNotificationMessage pushMessage;

    private MenuAdapter mMenuAdapter;

    private List<SubMenuItem> mLstSubMenuCampeonato;

    @AfterViews
    protected void afterViews() {

        mActionBarDrawerToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();

        initRecyclerView();
        prepareMenu();
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        mRecyclerViewMenu.setLayoutManager(layoutManager);
        mRecyclerViewMenu.setHasFixedSize(true);
    }

    private void navigateToHome() {
        if (pushMessage != null && pushMessage.getIdPartida() > 0) {
            navigatePush(pushMessage);
        } else {
            if (SharedPrefManager.getInstance().isFirstAccess()) {
                gerenciarNotificacoesClick();
            } else {
                Navigator.navigateToCampeonatoFragment(getContext(), SharedPrefManager.getInstance().getLastCampeonatoSelected());
                selectMenu(SharedPrefManager.getInstance().getLastCampeonatoSelected());
            }
        }
    }

    private void navigatePush(PushNotificationMessage pushMessage) {
        if (pushMessage != null && pushMessage.getIdPartida() > 0) {

            Partida partida = PartidaCacheManager.getInstance().get(pushMessage.getIdPartida());
            long idCampeonato = SharedPrefManager.getInstance().getLastCampeonatoSelected();
            if (partida != null && partida.getIdCampeonato() != null) {
                idCampeonato = partida.getIdCampeonato();
            }
            selectMenu(idCampeonato);
            Navigator.navigateToCampeonatoFragment(getContext(), idCampeonato, pushMessage);

        }
    }

    private void selectMenu(long idCampeonato) {
        mMenuAdapter.collapseAllParents();
        mMenuAdapter.expandParent(0);
        Navigator.navigateToCampeonatoFragment(getContext(), idCampeonato);
        if (mLstSubMenuCampeonato != null) {
            //busca o submenu do campeonato
            for (SubMenuItem subMenuItem : mLstSubMenuCampeonato) {
                if (subMenuItem.getId().equals(getCampeonatoSubMenuId(idCampeonato))) {
                    selectMenu(subMenuItem.getParent(), subMenuItem);
                    break;
                }
            }
        }
    }

    private String getCampeonatoSubMenuId(long id) {
        return "campeonato_" + id;
    }

    private void selectMenu(MenuItem menuItem) {
        selectMenu(menuItem, null);
    }

    private void selectMenu(MenuItem menuItem, SubMenuItem subMenuItem) {
        if (mMenuSelected != null) {
            mMenuSelected.setSelected(false);
            if (mMenuSelected.getChilds() != null) {
                for (SubMenuItem subMenuItem1 : mMenuSelected.getChilds()) {
                    subMenuItem1.setSelected(false);
                }
            }
        }
        menuItem.setSelected(true);
        if (subMenuItem != null) {
            subMenuItem.setSelected(true);
        }

        if (mMenuAdapter != null) {
            if (!menuItem.equals(mMenuSelected)) {
                mMenuAdapter.collapseParent(mMenuSelected);
            }

            mMenuAdapter.notifyDataSetChanged();
        }
        mMenuSelected = menuItem;
        closeDrawers();
    }

    private void prepareMenu() {
        showLoadingView();
        CampeonatoServiceImpl.getCampeonatosInCache(new Callback<List<Campeonato>>() {
            @Override
            public void success(List<Campeonato> campeonatos, Response response) {
                prepareMenu(campeonatos);
                hideLoadingView();
                navigateToHome();
            }

            @Override
            public void failure(RetrofitError error) {
                hideLoadingView();
                if (!isFinishing()) {
                    ViewUtil.showErrorDialog(getContext(), getString(R.string.error_generic), new ErrorListener() {
                        @Override
                        public void retry() {
                            prepareMenu();
                        }

                        @Override
                        public void cancel() {
                        }
                    });
                }
            }
        });
    }

    private void prepareMenu(List<Campeonato> campeonatos) {
        mLstSubMenuCampeonato = new ArrayList<>();

        final MenuItem menuCampeonato = new MenuItem(getString(R.string.campeonatos), R.drawable.icn_campeonatos, mLstSubMenuCampeonato, null);

        for (final Campeonato c : campeonatos) {
            final SubMenuItem subMenuItem = new SubMenuItem(getCampeonatoSubMenuId(c.getId()), c.getNome(), menuCampeonato, new SubMenuItem.SubMenuItemListener() {
                @Override
                public void menuClick(SubMenuItem subMenuItem) {
                    selectMenu(c.getId());
                }
            });
            mLstSubMenuCampeonato.add(subMenuItem);
        }

        final MenuItem menuGuia = new MenuItem(getString(R.string.guia_paulistao), R.drawable.icn_guia, new MenuItem.MenuItemListener() {
            @Override
            public void menuClick(MenuItem menu) {
                menuGuiaClick();
                selectMenu(menu);
            }
        });
        final MenuItem menuTv = new MenuItem(getString(R.string.fpf_tv), R.drawable.icn_tv, new MenuItem.MenuItemListener() {
            @Override
            public void menuClick(MenuItem menu) {
                Navigator.navigateToFpfTVFragment(getContext());
                selectMenu(menu);
            }
        });
        final MenuItem menuNotificacoes = new MenuItem(getString(R.string.notificacoes), R.drawable.icn_alerts_menu, new MenuItem.MenuItemListener() {
            @Override
            public void menuClick(MenuItem menu) {
                gerenciarNotificacoesClick();
                selectMenu(menu);
            }
        });
        final MenuItem menuIntegridade = new MenuItem(getString(R.string.fpf_integridade), R.drawable.icn_integridade, new MenuItem.MenuItemListener() {
            @Override
            public void menuClick(MenuItem menu) {
                Navigator.navigateToIntegridadeHomeFragment(getContext());
                selectMenu(menu);
            }
        });
        final MenuItem menuSobre = new MenuItem(getString(R.string.menu_sobre), R.drawable.icn_about_menu, new MenuItem.MenuItemListener() {
            @Override
            public void menuClick(MenuItem menu) {
                menuSobreClick();
                selectMenu(menu);
            }
        });


        List<MenuItem> menus = new ArrayList<>();
        menus.add(menuCampeonato);
        menus.add(menuGuia);
        menus.add(menuTv);
        menus.add(menuNotificacoes);
        menus.add(menuIntegridade);
        menus.add(menuSobre);

        mMenuAdapter = new MenuAdapter(getContext(), menus);

        mRecyclerViewMenu.setAdapter(mMenuAdapter);

    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout != null && mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawers();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected int getLayoutFragmentContainer() {
        return R.id.layout_fragment_container;
    }

    @Override
    public void setNavBackMode() {
        if (mToolbar != null) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            mActionBarDrawerToggle.setDrawerIndicatorEnabled(false);
            Drawable backButton = getDrawerToggleDelegate().getThemeUpIndicator();
            backButton = DrawableCompat.wrap(backButton);
            DrawableCompat.setTint(backButton, getResources().getColor(android.R.color.white));
            mActionBarDrawerToggle.setHomeAsUpIndicator(backButton);
            mActionBarDrawerToggle.syncState();
        }
    }

    @Override
    public void resetBackMode() {
        if (mToolbar != null) {
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                    } else {
                        updateMenu();
                        mDrawerLayout.openDrawer(GravityCompat.START);
                    }
                }
            });
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            mActionBarDrawerToggle.setDrawerIndicatorEnabled(true);
            mActionBarDrawerToggle.syncState();
        }
    }

    @Override
    public void hideDrawerAndBackButton() {
        if (mToolbar != null) {
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
            mActionBarDrawerToggle.setHomeAsUpIndicator(null);
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            mActionBarDrawerToggle.setDrawerIndicatorEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            mActionBarDrawerToggle.syncState();
        }
    }

    public static void startActivity(Context ctx) {
        ctx.startActivity(new Intent(ctx, MainActivity_.class));
    }

    public static Intent getActivityIntent(Context ctx, PushNotificationMessage push) {
        Intent it = new Intent(ctx, MainActivity_.class);
        it.putExtra(Constants.ARG_PUSH_MSG, push);
        return it;
    }

    private void updateMenu() {
    }

    void menuSobreClick() {
        closeDrawers();
        unselectAllMenu();
        Navigator.navigateToAboutFragment(getContext());
    }

    void gerenciarNotificacoesClick() {
        closeDrawers();
        unselectAllMenu();
        Navigator.navigateToNotificationsFragment(getContext());

    }

    public void menuGuiaClick() {
        closeDrawers();
        unselectAllMenu();
        Navigator.navigateToGuiaHomeFragment(getContext());
    }

    private void unselectAllMenu() {
        if (mMenuSelected != null) {
            mMenuSelected.setSelected(false);
        }
    }

    private void closeDrawers() {
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawers();
        }
    }

    private void tint(TextView txtView, ImageView img, int color) {
        color = ResourcesCompat.getColor(getResources(), color, null);
        if (txtView != null) {
            txtView.setTextColor(color);
        }
        if (img != null) {
            img.setColorFilter(color);
        }
    }
}

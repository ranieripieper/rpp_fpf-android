package br.org.fpf.android.cache;

import java.util.ArrayList;
import java.util.List;

import br.org.fpf.android.cache.base.BaseCache;
import br.org.fpf.android.model.Guia;
import io.realm.Case;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by ranipieper on 11/19/15.
 */
public class GuiaCacheManager extends BaseCache<Guia> {

    private static GuiaCacheManager instance = new GuiaCacheManager();

    String[] DEFAULT_ORDER_FIELDS = {"idCampeonato", "nome"};
    Sort[] DEFAULT_ORDER_SORT = {Sort.ASCENDING, Sort.ASCENDING};

    private GuiaCacheManager() {
    }

    public List<Guia> getAll() {
        RealmResults<Guia> results = getRealm().where(Guia.class)
                .findAllSorted(DEFAULT_ORDER_FIELDS, DEFAULT_ORDER_SORT);
        if (results != null) {
            return getRealm().copyFromRealm(results);
        }
        return new ArrayList<>();

    }

    public List<Guia> getByFilter(String filter) {
        RealmResults<Guia> results = getRealm().where(Guia.class)
                .contains("nome", filter, Case.INSENSITIVE)
                .findAllSorted(DEFAULT_ORDER_FIELDS, DEFAULT_ORDER_SORT);
        return getRealm().copyFromRealm(results);
    }

    @Override
    public String getPrimaryKeyName() {
        return "idEquipe";
    }

    public static GuiaCacheManager getInstance() {
        return instance;
    }

    @Override
    public Class<Guia> getReferenceClass() {
        return Guia.class;
    }

    @Override
    protected long getCacheLifeTime() {
        return 1000 * 60 * 60 * 24 * 15; // 15 dias
    }
}

package br.org.fpf.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by ranieripieper on 1/28/16.
 */
public class Gol extends RealmObject {

    @Expose
    @SerializedName("Equipe")
    private String equipe;

    @Expose
    @SerializedName("Jogador")
    private String jogador;

    @Expose
    @SerializedName("Mmmento")
    private String momento;

    @Expose
    @SerializedName("Periodo")
    private String periodo;

    @Expose
    @SerializedName("Tipo") // Favor - Contra
    private String tipo;

    /**
     * Gets the equipe
     *
     * @return equipe
     */
    public String getEquipe() {
        return equipe;
    }

    /**
     * Sets the equipe
     *
     * @param equipe
     */
    public void setEquipe(String equipe) {
        this.equipe = equipe;
    }

    /**
     * Gets the jogador
     *
     * @return jogador
     */
    public String getJogador() {
        return jogador;
    }

    /**
     * Sets the jogador
     *
     * @param jogador
     */
    public void setJogador(String jogador) {
        this.jogador = jogador;
    }

    /**
     * Gets the momento
     *
     * @return momento
     */
    public String getMomento() {
        return momento;
    }

    /**
     * Sets the momento
     *
     * @param momento
     */
    public void setMomento(String momento) {
        this.momento = momento;
    }

    /**
     * Gets the periodo
     *
     * @return periodo
     */
    public String getPeriodo() {
        return periodo;
    }

    /**
     * Sets the periodo
     *
     * @param periodo
     */
    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    /**
     * Gets the tipo
     *
     * @return tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Sets the tipo
     *
     * @param tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}

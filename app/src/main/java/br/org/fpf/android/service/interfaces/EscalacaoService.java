package br.org.fpf.android.service.interfaces;

import br.org.fpf.android.model.response.EscalacaoResponse;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by ranieripieper on 1/30/16.
 */
public interface EscalacaoService {

    String DEFAULT_ORDER = "-updatedAt";

    @GET("/classes/Escalacao")
    void getEscalacao(@Query("where") String where, @Query("limit") int perPage, @Query("order") String order, Callback<EscalacaoResponse> response);

}

package br.org.fpf.android.ui.integridade;

import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import br.org.fpf.android.R;
import br.org.fpf.android.ui.base.BaseFragment;

/**
 * Created by ranipieper on 8/29/16.
 */
@EFragment(R.layout.fragment_fpf_integridade_faq)
public class IntegridadeFaqFragment extends BaseFragment {

    public static IntegridadeFaqFragment newInstance() {
        Bundle args = new Bundle();

        IntegridadeFaqFragment fragment = new IntegridadeFaqFragment_();
        fragment.setArguments(args);

        return fragment;
    }

    @ViewById(R.id.lbl_faq_text)
    TextView mLblFaqText;

    @AfterViews
    public void afterViews() {
        super.afterViews();
        mLblFaqText.setText(Html.fromHtml(getString(R.string.integridade_faq_text)));
    }

    @Override
    protected String getToolbarTitle() {
        return "";
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }
}

package br.org.fpf.android.ui.fpftv.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import br.org.fpf.android.FpfApplication;
import br.org.fpf.android.R;
import br.org.fpf.android.cache.VideoCacheManager;
import br.org.fpf.android.model.Video;
import br.org.fpf.android.util.DateUtil;
import br.org.fpf.android.util.NumberUtil;
import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by ranipieper on 8/30/16.
 */
@LayoutId(R.layout.row_youtube_video)
public class YoutubeVideoViewHolder extends ItemViewHolder<Video> {

    @ViewId(R.id.card_view)
    RelativeLayout mLayoutRow;

    @ViewId(R.id.lbl_title)
    TextView lblTitle;

    @ViewId(R.id.lbl_data)
    TextView lblData;

    @ViewId(R.id.lbl_views)
    TextView lblViews;

    @ViewId(R.id.img_photo)
    ImageView imgPhoto;

    public YoutubeVideoViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Video video, PositionInfo positionInfo) {
        Video item = VideoCacheManager.getInstance().get(getContext(), video.getId());

        if (item == null) {
            item = video;
        }
        lblTitle.setText(item.getTitle());
        lblData.setText(DateUtil.getDate(getContext(), item.getDt()));
        if (item.getViews() != null) {
            lblViews.setText(getContext().getString(R.string.x_visualizacoes, NumberUtil.getNumberFormated(item.getViews())));
            lblViews.setVisibility(View.VISIBLE);
        } else {
            lblViews.setVisibility(View.INVISIBLE);
        }

        FpfApplication.imageLoader.displayImage(item.getThumb(), imgPhoto);

    }

    @Override
    public void onSetListeners() {
        mLayoutRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YoutubeVideoHolderListener listener = getListener(YoutubeVideoHolderListener.class);
                if (listener != null) {
                    listener.onItemSelected(getItem());
                }

            }
        });
    }


    public interface YoutubeVideoHolderListener {
        void onItemSelected(Video video);
    }
}
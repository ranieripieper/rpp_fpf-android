package br.org.fpf.android.ui.partida;

import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import br.org.fpf.android.R;
import br.org.fpf.android.cache.PartidaCacheManager;
import br.org.fpf.android.model.Partida;
import br.org.fpf.android.ui.base.BaseFragment;
import br.org.fpf.android.util.Constants;
import br.org.fpf.android.util.DateUtil;
import uk.co.ribot.easyadapter.recyclerview.EasyLoadMoreRecyclerView;

/**
 * Created by ranieripieper on 2/2/16.
 */
@EFragment
public abstract class PartidaBaseFragment extends BaseFragment {

    @ViewById(R.id.swipe_refresh_layout)
    @Nullable
    SwipeRefreshLayout mSwipeRefreshLayout;

    @ViewById(R.id.recycler_view)
    EasyLoadMoreRecyclerView mRecyclerView;

    @ViewById(R.id.txt_error_no_results)
    TextView txtError;

    @FragmentArg(Constants.ARG_ID_PARTIDA)
    Long mIdPartida;

    @FragmentArg(Constants.ARG_ID_CAMPEONATO)
    Long mIdCampeonato;

    Partida mPartida;

    protected boolean forceUpdate = true;

    @AfterViews
    public void afterViews() {
        super.afterViews();
        mPartida = PartidaCacheManager.getInstance().get(mIdPartida);
        initSwipeRefresh();
        loadData();
    }

    View getHeaderRecyclerView() {
        View header = LayoutInflater.from(getActivity()).inflate(
                R.layout.include_partida_infos, mRecyclerView, false);

        TextView lblData = (TextView)header.findViewById(R.id.lbl_data_hora_valor);
        TextView lblLocal = (TextView)header.findViewById(R.id.lbl_local_valor);
        TextView lblArbitro = (TextView)header.findViewById(R.id.lbl_arbitro);
        TextView lblCidade = (TextView)header.findViewById(R.id.lbl_cidade);
        TextView lblArbitroValor = (TextView)header.findViewById(R.id.lbl_arbitro_valor);
        TextView lblCidadeValor = (TextView)header.findViewById(R.id.lbl_cidade_valor);
        View viewSepBottom = header.findViewById(R.id.layout_margin_bottom);

        lblLocal.setText(mPartida.getNomeEstadio());
        if (mPartida.getData() != null && mPartida.getData().getData() != null) {
            lblData.setText(DateUtil.DATE_DAY_MONTH_YEAR_TIME.get().format(mPartida.getData().getData()));
        } else {
            lblData.setText(R.string.a_definir);
        }
        if (!TextUtils.isEmpty(mPartida.getCidade())) {
            lblCidadeValor.setText(mPartida.getCidade());
            lblCidade.setVisibility(View.VISIBLE);
            lblCidadeValor.setVisibility(View.VISIBLE);
        }
        if (!TextUtils.isEmpty(mPartida.getCidade())) {
            lblArbitroValor.setText(mPartida.getArbitro());
            lblArbitro.setVisibility(View.VISIBLE);
            lblArbitroValor.setVisibility(View.VISIBLE);
        }

        viewSepBottom.setVisibility(View.VISIBLE);
        return header;
    }

    abstract RecyclerView.LayoutManager getLayoutManager();

    protected void initSwipeRefresh() {
        RecyclerView.LayoutManager layoutManager = getLayoutManager();

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    forceUpdate = true;
                    loadData();
                }
            });
        }
    }

    @Click(R.id.txt_error_no_results)
    void clickError() {
        forceUpdate = true;
        if (isAdded()) {
            txtError.setVisibility(View.GONE);
            loadData();
        }
    }

    abstract void loadData();

    @Override
    protected boolean needConfigToolbar() {
        return false;
    }

    @Override
    protected String getToolbarTitle() {
        return "";
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }
}

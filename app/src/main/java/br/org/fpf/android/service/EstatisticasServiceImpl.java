package br.org.fpf.android.service;

import br.org.fpf.android.cache.EstatisticaCacheManager;
import br.org.fpf.android.cache.PartidaCacheManager;
import br.org.fpf.android.model.Estatistica;
import br.org.fpf.android.model.Partida;
import br.org.fpf.android.model.response.EstatisticaResponse;
import br.org.fpf.android.helper.PartidaHelper;
import br.org.fpf.android.util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 3/1/16.
 */
public class EstatisticasServiceImpl {

    public static void getEstatisticas(final Long idPartida, final Callback<Estatistica> callback) {

        String idPartidaWhere = String.format("{\"IdPartida\":%d}", idPartida);

        Partida partida = PartidaCacheManager.getInstance().get(idPartida);
        final boolean persistEstatistica = PartidaHelper.isFinished(partida);
        Estatistica estatisticaBd = null;
        if (persistEstatistica) {
            estatisticaBd = EstatisticaCacheManager.getInstance().get(idPartida);
        }

        if (estatisticaBd != null && callback != null) {
            callback.success(estatisticaBd, null);
        } else {
            RetrofitManager.getInstance().getPartidaService().getEstatisticas(idPartidaWhere, Constants.REG_POR_PAG_MAX, new Callback<EstatisticaResponse>() {
                @Override
                public void success(EstatisticaResponse estatisticaResponse, Response response) {
                    //salva as estatisticas
                    if (!estatisticaResponse.isEmpty() && persistEstatistica) {
                        EstatisticaCacheManager.getInstance().putAll(estatisticaResponse.getResults());
                    }
                    if (callback != null) {
                        Estatistica estatistica = null;
                        if (!estatisticaResponse.isEmpty()) {
                            estatistica = estatisticaResponse.getResults().get(0);
                        }
                        callback.success(estatistica, response);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (callback != null) {
                        callback.failure(error);
                    }
                }
            });
        }
    }

}

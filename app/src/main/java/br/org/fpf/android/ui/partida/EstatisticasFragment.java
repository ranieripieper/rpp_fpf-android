package br.org.fpf.android.ui.partida;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;

import java.util.ArrayList;
import java.util.List;

import br.org.fpf.android.R;
import br.org.fpf.android.cache.EstatisticaCacheManager;
import br.org.fpf.android.model.Estatistica;
import br.org.fpf.android.service.EstatisticasServiceImpl;
import br.org.fpf.android.helper.EstatisticaHelper;
import br.org.fpf.android.ui.partida.adapter.EstatisticasAdapter;
import br.org.fpf.android.util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 3/1/16.
 */
@EFragment(R.layout.fragment_estatisticas)
public class EstatisticasFragment extends PartidaBaseFragment {

    protected EstatisticasAdapter mAdapter;

    private Boolean isRefreshing = false;

    private boolean needUpdate = true;

    public static EstatisticasFragment newInstance(Long idCampeonato, Long idPartida) {
        Bundle args = new Bundle();

        EstatisticasFragment_ fragment = new EstatisticasFragment_();

        args.putLong(Constants.ARG_ID_CAMPEONATO, idCampeonato);
        args.putLong(Constants.ARG_ID_PARTIDA, idPartida);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            updateView();
        }
    }

    private void updateView() {
        if (isAdded()) {
            loadData();
        }
    }

    protected void loadData() {
        if (!needUpdate && !forceUpdate) {
            hideLoadingView(mSwipeRefreshLayout);
            return;
        }
        synchronized (isRefreshing) {
            if (isAdded() && !isRefreshing) {
                isRefreshing = true;
                mRecyclerView.setVisibility(View.VISIBLE);
                callService();
            } else if (isAdded()) {
                hideLoadingView(mSwipeRefreshLayout);
            }
        }
    }

    public void callService() {
        if (!isAdded()) {
            return;
        }
        txtError.setVisibility(View.GONE);
        showLoadingView(mSwipeRefreshLayout);

        EstatisticasServiceImpl.getEstatisticas(mIdPartida, new Callback<Estatistica>() {
            @Override
            public void success(Estatistica estatistica, Response response) {
                if (estatistica != null) {
                    if (response == null) { //recuperou do banco local
                        prepareEstatisticas(estatistica.getIdPartida());
                    } else {
                        prepareEstatisticas(estatistica);
                    }
                } else {
                    showEmpty();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (isShowLoadingAndError()) {
                    showErrorTextView(txtError, R.string.error_generic_refresh);
                }

                hideLoadingView(mSwipeRefreshLayout);
                synchronized (isRefreshing) {
                    isRefreshing = false;
                }
            }
        });
    }

    @Background
    void prepareEstatisticas(Long id) {
        Estatistica estatistica = EstatisticaCacheManager.getInstance().get(getContext(), id);
        if (estatistica != null) {
            needUpdate = false;
        }
        prepareEstatisticasToView(estatistica);
    }

    @Background
    void prepareEstatisticas(Estatistica estatistica) {
        prepareEstatisticasToView(estatistica);
    }

    void prepareEstatisticasToView(Estatistica estatistica) {
        if (estatistica == null) {
            showEstatisticas(null);
            return;
        }
        List<EstatisticasAdapter.EstatisticaItem> items = new ArrayList<>();
        EstatisticasAdapter.EstatisticaItem item = new EstatisticasAdapter.EstatisticaItem();

        //posse de bola
        item = new EstatisticasAdapter.EstatisticaItem();
        item.setTitulo(R.string.posse_de_bola);
        item.setLayout(R.layout.row_estatisticas_posse_bola);
        item.setValorMandante(EstatisticaHelper.getPosseDeBola(estatistica.getPosseBolaMandante()));
        item.setValorVisitante(EstatisticaHelper.getPosseDeBola(estatistica.getPosseBolaVisitante()));
        item.setBarraCentral(true);
        items.add(item);

        //finalizações
        item = new EstatisticasAdapter.EstatisticaItem();
        item.setTitulo(R.string.finalizacoes);
        item.setLayout(R.layout.row_estatisticas_certas_erradas);
        item.setValorMandante(EstatisticaHelper.getValue(estatistica.getFinalizacaoCertaMandante()));
        item.setValorVisitante(EstatisticaHelper.getValue(estatistica.getFinalizacaoCertaVisitante()));
        item.setValorMandanteErrados(EstatisticaHelper.getValue(estatistica.getFinalizacaoErradaMandante()));
        item.setValorVisitanteErrados(EstatisticaHelper.getValue(estatistica.getFinalizacaoErradaVisitante()));
        item.setBarraCentral(false);
        items.add(item);

        //faltas cometidas
        item = new EstatisticasAdapter.EstatisticaItem();
        item.setTitulo(R.string.faltas_cometidas);
        item.setLayout(R.layout.row_estatisticas_icon);
        item.setIcon(R.drawable.icn_faltas);
        item.setValorMandante(EstatisticaHelper.getValue(estatistica.getFaltaCometidaMandante()));
        item.setValorVisitante(EstatisticaHelper.getValue(estatistica.getFaltaCometidaVisitante()));
        item.setBarraCentral(true);
        items.add(item);

        //passes
        item = new EstatisticasAdapter.EstatisticaItem();
        item.setTitulo(R.string.passes);
        item.setLayout(R.layout.row_estatisticas_certas_erradas);
        item.setValorMandante(EstatisticaHelper.getValue(estatistica.getPasseCertoMandante()));
        item.setValorVisitante(EstatisticaHelper.getValue(estatistica.getPasseCertoVisitante()));
        item.setValorMandanteErrados(EstatisticaHelper.getValue(estatistica.getPasseErradoMandante()));
        item.setValorVisitanteErrados(EstatisticaHelper.getValue(estatistica.getPasseErradoVisitante()));
        item.setBarraCentral(false);
        items.add(item);

        //escanteios
        item = new EstatisticasAdapter.EstatisticaItem();
        item.setTitulo(R.string.escanteios);
        item.setLayout(R.layout.row_estatisticas_icon);
        item.setIcon(R.drawable.icn_escanteio);
        item.setValorMandante(EstatisticaHelper.getValue(estatistica.getEscanteioProMandante()));
        item.setValorVisitante(EstatisticaHelper.getValue(estatistica.getEscanteioProVisitante()));
        item.setBarraCentral(true);
        items.add(item);

        //desarmes
        item = new EstatisticasAdapter.EstatisticaItem();
        item.setTitulo(R.string.desarmes);
        item.setLayout(R.layout.row_estatisticas_certas_erradas);
        item.setValorMandante(EstatisticaHelper.getValue(estatistica.getDesarmeCertoMandante()));
        item.setValorVisitante(EstatisticaHelper.getValue(estatistica.getDesarmeCertoVisitante()));
        item.setValorMandanteErrados(EstatisticaHelper.getValue(estatistica.getDesarmeErradoMandante()));
        item.setValorVisitanteErrados(EstatisticaHelper.getValue(estatistica.getDesarmeErradoVisitante()));
        item.setBarraCentral(false);
        items.add(item);

        //cartoes amarelos
        item = new EstatisticasAdapter.EstatisticaItem();
        item.setTitulo(R.string.cartoes_amarelos);
        item.setLayout(R.layout.row_estatisticas_icon);
        item.setIcon(R.drawable.icn_cartao_amarelo);
        item.setValorMandante(EstatisticaHelper.getValue(estatistica.getCartaoAmareloMandante()));
        item.setValorVisitante(EstatisticaHelper.getValue(estatistica.getCartaoAmareloVisitante()));
        item.setBarraCentral(false);
        items.add(item);

        //cartos vermelhos
        item = new EstatisticasAdapter.EstatisticaItem();
        item.setTitulo(R.string.cartoes_vermelhos);
        item.setLayout(R.layout.row_estatisticas_icon);
        item.setIcon(R.drawable.icn_cartao_vermelho);
        item.setValorMandante(EstatisticaHelper.getValue(estatistica.getCartaoVermelhoMandante()));
        item.setValorVisitante(EstatisticaHelper.getValue(estatistica.getCartaoVermelhoVisitante()));
        item.setBarraCentral(true);
        items.add(item);

        //cruzamentos
        item = new EstatisticasAdapter.EstatisticaItem();
        item.setTitulo(R.string.cruzamentos);
        item.setLayout(R.layout.row_estatisticas_certas_erradas);
        item.setValorMandante(EstatisticaHelper.getValue(estatistica.getCruzamentoCertoMandante()));
        item.setValorVisitante(EstatisticaHelper.getValue(estatistica.getCruzamentoCertoVisitante()));
        item.setValorMandanteErrados(EstatisticaHelper.getValue(estatistica.getCruzamentoErradoMandante()));
        item.setValorVisitanteErrados(EstatisticaHelper.getValue(estatistica.getCruzamentoErradoVisitante()));
        item.setBarraCentral(true);
        items.add(item);

        //lançamentos
        item = new EstatisticasAdapter.EstatisticaItem();
        item.setTitulo(R.string.lancamentos);
        item.setLayout(R.layout.row_estatisticas_certas_erradas);
        item.setValorMandante(EstatisticaHelper.getValue(estatistica.getLancamentoCertoMandante()));
        item.setValorVisitante(EstatisticaHelper.getValue(estatistica.getLancamentoCertoVisitante()));
        item.setValorMandanteErrados(EstatisticaHelper.getValue(estatistica.getLancamentoErradoMandante()));
        item.setValorVisitanteErrados(EstatisticaHelper.getValue(estatistica.getLancamentoErradoVisitante()));
        item.setBarraCentral(false);
        items.add(item);

        //penaltis
        item = new EstatisticasAdapter.EstatisticaItem();
        item.setTitulo(R.string.penaltis);
        item.setLayout(R.layout.row_estatisticas_icon);
        item.setIcon(R.drawable.icn_penalty);
        item.setValorMandante(EstatisticaHelper.getValue(estatistica.getPenaltiRecebidoMandante()));
        item.setValorVisitante(EstatisticaHelper.getValue(estatistica.getPenaltiRecebidoVisitante()));
        item.setBarraCentral(null);
        items.add(item);

        showEstatisticas(items);
    }

    @UiThread
    void showEstatisticas(List<EstatisticasAdapter.EstatisticaItem> items) {
        if (!isAdded()) {
            return;
        }
        if (items == null || items.isEmpty()) {
            showEmpty();
            return;
        }

        if (mAdapter == null) {
            createAdapter(items);
        } else {
            mAdapter.removeAllItems();
            mAdapter.addItems(items);
        }

        if (mRecyclerView.getAdapter() == null) {
            mRecyclerView.setAdapter(mAdapter);
        }
        synchronized (isRefreshing) {
            isRefreshing = false;
        }
        hideLoadingView(mSwipeRefreshLayout);
    }

    protected void createAdapter(List<EstatisticasAdapter.EstatisticaItem> items) {
        mAdapter = new EstatisticasAdapter(items);

        mAdapter.notifyDataSetChanged();
        mRecyclerView.setAdapter(mAdapter);
    }

    private boolean isShowLoadingAndError() {
        if (mAdapter == null || mAdapter.getItemCount() <= 0) {
            return true;
        }
        return false;
    }

    private void showEmpty() {
        if (!isAdded()) {
            return;
        }
        if (mAdapter == null || mAdapter.getItemCount() > 0) {
            txtError.setText(R.string.error_estatisticas_vazia);
            txtError.setVisibility(View.VISIBLE);
        }

        hideLoadingView(mSwipeRefreshLayout);
        synchronized (isRefreshing) {
            isRefreshing = false;
        }
    }

    LinearLayoutManager getLayoutManager() {
        return new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
    }

}

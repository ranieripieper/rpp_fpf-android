package br.org.fpf.android.helper;

import android.content.Context;

import java.util.List;

import br.org.fpf.android.cache.NarracaoCacheManager;
import br.org.fpf.android.model.Narracao;
import br.org.fpf.android.util.Constants;

/**
 * Created by ranipieper on 3/3/16.
 */
public class NarracaoHelper {

    public static boolean hasNarracaoFimJogo(Context context, Long idPartida) {
        //verifica se tem narração ACAO IMPORTANTE - FIM DE JOGO
        List<Narracao> narracaoList = NarracaoCacheManager.getInstance().getNarracaoByIdPartidaAndAcaoImportante(context, idPartida, Constants.NARRACAO_FIM_JOGO);
        if (narracaoList != null && !narracaoList.isEmpty()) {
            return true;
        }
        return false;
    }
}

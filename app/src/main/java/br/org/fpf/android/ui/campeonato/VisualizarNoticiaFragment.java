package br.org.fpf.android.ui.campeonato;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import br.org.fpf.android.FpfApplication;
import br.org.fpf.android.R;
import br.org.fpf.android.model.Noticia;
import br.org.fpf.android.service.RetrofitManager;
import br.org.fpf.android.ui.base.BaseFragment;
import br.org.fpf.android.helper.NoticiaHelper;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranieripieper on 5/5/16.
 */
@EFragment(R.layout.fragment_visualizar_noticia)
public class VisualizarNoticiaFragment extends BaseFragment {

    @ViewById(R.id.lbl_title)
    TextView mTxtTitle;

    @ViewById(R.id.lbl_data)
    TextView mTxtData;

    @ViewById(R.id.lbl_texto)
    TextView mTxtTexto;

    @ViewById(R.id.img_photo)
    ImageView mImgPhoto;

    private Noticia mNoticia;

    public static VisualizarNoticiaFragment newInstance(Noticia noticia) {
        Bundle args = new Bundle();

        VisualizarNoticiaFragment fragment = new VisualizarNoticiaFragment_();
        fragment.mNoticia = noticia;
        fragment.setArguments(args);

        return fragment;
    }

    @AfterViews
    public void afterViews() {
        super.afterViews();
        mTxtTitle.setText(mNoticia.getTitulo());
        mTxtData.setText(mNoticia.getData());
        if (TextUtils.isEmpty(mNoticia.getImagem())) {
            mImgPhoto.setVisibility(View.GONE);
        } else {
            FpfApplication.imageLoader.loadImage(mNoticia.getImagem(), new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    try {
                        if (isAdded()) {
                            mImgPhoto.setImageBitmap(loadedImage);
                            mImgPhoto.setVisibility(View.VISIBLE);
                        }
                    } catch (OutOfMemoryError e) {
                        Log.e("ERROR", "OutOfMemoryError: " + e.getMessage());
                    }
                }
                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                }
            });
        }

        callServiceGetTexto();
    }

    private void callServiceGetTexto() {
        showLoadingView();
        RetrofitManager.getInstance().getNoticiaService(mNoticia.getUrl()).getNoticia(new Callback<Noticia>() {
            @Override
            public void success(Noticia noticia, Response response) {
                if (noticia != null && isAdded()) {
                    mTxtTexto.setText(NoticiaHelper.getTexto(noticia));
                    mNoticia.setTexto(noticia.getTexto());
                }
                hideLoadingView();
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error);
            }
        });
    }

    @Override
    protected String getToolbarTitle() {
        return "";
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }
}

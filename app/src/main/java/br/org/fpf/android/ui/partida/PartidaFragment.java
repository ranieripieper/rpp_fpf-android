package br.org.fpf.android.ui.partida;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.org.fpf.android.FpfApplication;
import br.org.fpf.android.R;
import br.org.fpf.android.cache.EquipeCacheManager;
import br.org.fpf.android.cache.PartidaCacheManager;
import br.org.fpf.android.model.Equipe;
import br.org.fpf.android.model.Narracao;
import br.org.fpf.android.model.Partida;
import br.org.fpf.android.service.NarracaoServiceImpl;
import br.org.fpf.android.service.PartidaServiceImpl;
import br.org.fpf.android.ui.base.BaseFragment;
import br.org.fpf.android.ui.base.ErrorListener;
import br.org.fpf.android.helper.NarracaoHelper;
import br.org.fpf.android.helper.PartidaHelper;
import br.org.fpf.android.ui.partida.adapter.PartidaViewPagerAdapter;
import br.org.fpf.android.util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 1/26/16.
 */
@EFragment(R.layout.fragment_partida)
public class PartidaFragment extends BaseFragment {

    @ViewById(R.id.tablayout)
    TabLayout mTabLayoutGoal;

    @ViewById(R.id.layout_header)
    View layoutHeader;

    @ViewById(R.id.viewpager)
    public ViewPager mViewPager;

    @ViewById(R.id.img_time_mandante)
    ImageView imgTimeMandante;

    @ViewById(R.id.img_time_visitante)
    ImageView imgTimeVisitante;

    @ViewById(R.id.lbl_mandante)
    TextView lblMandante;

    @ViewById(R.id.lbl_visitante)
    TextView lblVisitante;

    @ViewById(R.id.lbl_placar_visitante)
    TextView lblPlacarVisitante;

    @ViewById(R.id.lbl_placar_mandante)
    TextView lblPlacarMandante;

    @ViewById(R.id.lbl_placar_visitante_penalti)
    TextView lblPlacarVisitantePenalti;

    @ViewById(R.id.lbl_placar_mandante_penalti)
    TextView lblPlacarMandantePenalti;

    @FragmentArg(Constants.ARG_ID_PARTIDA)
    Long mIdPartida;

    @FragmentArg(Constants.ARG_ID_CAMPEONATO)
    Long mIdCampeonato;

    Partida mPartida;

    public static PartidaFragment newInstance(Long idCampeonato, Long idPartida) {
        Bundle args = new Bundle();

        PartidaFragment_ fragment = new PartidaFragment_();
        if (idCampeonato != null) {
            args.putLong(Constants.ARG_ID_CAMPEONATO, idCampeonato);
        }

        args.putLong(Constants.ARG_ID_PARTIDA, idPartida);

        fragment.setArguments(args);

        return fragment;
    }

    public static PartidaFragment newInstance(Long idPartida) {
        Bundle args = new Bundle();

        PartidaFragment_ fragment = new PartidaFragment_();
        args.putLong(Constants.ARG_ID_PARTIDA, idPartida);

        fragment.setArguments(args);

        return fragment;
    }

    private boolean init = false;

    @AfterViews
    public void afterViews() {
        super.afterViews();

        if (mIdCampeonato == null) {
            final Handler handler = new Handler();
            Runnable refresh = new Runnable() {
                public void run() {
                    if (isAdded()) {
                        callServicePartida();
                    }
                }
            };
            handler.postDelayed(refresh, Constants.DELAY_VIEW_PAGE);
        } else {
            init();
            init = true;
        }
    }

    private void callServicePartida() {
        callServicePartida(false);
    }

    private void callServicePartida(final boolean isRefreshing) {
        if (!isRefreshing) {
            showLoadingView();
        }

        PartidaServiceImpl.getPartida(getContext(), mIdPartida, new Callback<Partida>() {
            @Override
            public void success(Partida partida, Response response) {
                if (init) {
                    refresh();
                } else {
                    init();
                }
                hideLoadingView();
            }

            @Override
            public void failure(RetrofitError error) {
                if (isRefreshing) {
                    return;
                }
                showError(error, R.string.error_generic, new ErrorListener() {
                    @Override
                    public void retry() {
                        callServicePartida();
                    }

                    @Override
                    public void cancel() {
                        finish();
                    }
                });
            }
        });
    }

    private boolean forceUpdatePartida = false;

    private void autoRefresh() {
        final Handler handler = new Handler();
        final Runnable refresh = new Runnable() {
            public void run() {
                if (isAdded()) {
                    if (forceUpdatePartida) {
                        callServicePartida(true);
                    } else {
                        refresh();
                    }
                    forceUpdatePartida = !forceUpdatePartida;
                }
            }
        };
        handler.postDelayed(refresh, Constants.REFRESH_NARRACAO);
    }

    private void refresh() {
        if (isAdded()) {
            PartidaCacheManager.getInstance().getRealm().refresh();
            mPartida = PartidaCacheManager.getInstance().get(mIdPartida);
            mIdCampeonato = mPartida.getIdCampeonato();
            setupHeader();
            if (!Constants.PERIODO_PARTIDA_ENCERRADA.equalsIgnoreCase(mPartida.getPeriodoAtual())) {
                autoRefresh();
            }
        }
    }

    private void init() {
        mPartida = PartidaCacheManager.getInstance().get(mIdPartida);
        mIdCampeonato = mPartida.getIdCampeonato();

        setupHeader();
        if (mPartida.isTemNarracao()) {
            callServiceNarracao();
        } else {
            setupViewPager(false);
        }

        init = true;

        configToolbar();

        autoRefresh();
    }

    private void callServiceNarracao() {
        showLoadingView();

        if (PartidaHelper.isFinished(mPartida) && NarracaoHelper.hasNarracaoFimJogo(getContext(), mIdPartida)) {
            processResultNarracao(true);
        } else {

            NarracaoServiceImpl.getNarracao(mIdPartida, null, new Callback<List<Narracao>>() {
                @Override
                public void success(List<Narracao> narracoes, Response response) {
                    processResultNarracao(narracoes != null && !narracoes.isEmpty());
                }

                @Override
                public void failure(RetrofitError error) {
                    showError(error, R.string.error_generic, new ErrorListener() {
                        @Override
                        public void retry() {
                            init();
                        }

                        @Override
                        public void cancel() {
                            finish();
                        }
                    });
                }
            });
        }

    }

    private void processResultNarracao(boolean hasNarracao) {
        setupViewPager(hasNarracao);
        hideLoadingView();
    }

    private void setupHeader() {
        setEquipe(mPartida.getIdEquipeMandante(), imgTimeMandante, lblMandante);
        setEquipe(mPartida.getIdEquipeVisitante(), imgTimeVisitante, lblVisitante);

        if (mPartida.getPlacarMandante() != null) {
            lblPlacarMandante.setText(mPartida.getPlacarMandante().toString());
        } else {
            lblPlacarMandante.setVisibility(View.INVISIBLE);
        }

        if (mPartida.getPlacarVisitante() != null) {
            lblPlacarVisitante.setText(mPartida.getPlacarVisitante().toString());
        } else {
            lblPlacarVisitante.setVisibility(View.INVISIBLE);
        }

        if (mPartida.getPlacarMandante() != null && mPartida.getPlacarVisitante() != null && mPartida.getPlacarPenaltisMandante() != null && mPartida.getPlacarPenaltisVisitante() != null) {
            lblPlacarMandantePenalti.setVisibility(View.VISIBLE);
            lblPlacarVisitantePenalti.setVisibility(View.VISIBLE);
            if (getContext() != null) {
                lblPlacarMandantePenalti.setText(getContext().getString(R.string.placar_penalti, mPartida.getPlacarPenaltisMandante()));
                lblPlacarVisitantePenalti.setText(getContext().getString(R.string.placar_penalti, mPartida.getPlacarPenaltisVisitante()));
            }
        } else {
            lblPlacarMandantePenalti.setVisibility(View.GONE);
            lblPlacarVisitantePenalti.setVisibility(View.GONE);
        }

        layoutHeader.setVisibility(View.VISIBLE);
    }

    private void setEquipe(Long id, ImageView img, TextView lbl) {
        Equipe equipe = EquipeCacheManager.getInstance().get(id);
        if (equipe == null) {
            img.setImageResource(R.drawable.ic_team_placeholder);
            lbl.setText(R.string.a_definir);
        } else {
            FpfApplication.imageLoader.displayImage(equipe.getUrlLogo(), img);
            lbl.setText(equipe.getNome());
        }
    }

    private void setupViewPager(boolean hasNarracao) {
        if (isAdded()) {
            PartidaViewPagerAdapter adapter;

            adapter = new PartidaViewPagerAdapter(getChildFragmentManager());

            mTabLayoutGoal.setVisibility(View.VISIBLE);

            if (mPartida.isTemNarracao() && hasNarracao) {
                adapter.addFrag(NarracaoFragment.newInstance(mIdCampeonato, mIdPartida), getString(R.string.narracao));
            }
            adapter.addFrag(EscalacaoFragment.newInstance(mIdCampeonato, mIdPartida), getString(R.string.escalacao));

            if (mPartida.isTemEstatistica()) {
                adapter.addFrag(EstatisticasFragment.newInstance(mIdCampeonato, mIdPartida), getString(R.string.estatisticas));
            }

            mViewPager.setAdapter(adapter);

            if (adapter.getCount() <= 1) {
                mTabLayoutGoal.setVisibility(View.GONE);
            }
            mViewPager.setOffscreenPageLimit(3);
            mTabLayoutGoal.setupWithViewPager(mViewPager);

            mViewPager.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected String getToolbarTitle() {
        if (mIdCampeonato != null) {
            if (mIdCampeonato.equals(Constants.ID_A1)) {
                return getString(R.string.serie_a1);
            } else if (mIdCampeonato.equals(Constants.ID_A2)) {
                return getString(R.string.serie_a2);
            } else if (mIdCampeonato.equals(Constants.ID_A3)) {
                return getString(R.string.serie_a3);
            }
        }

        return "";
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }

}

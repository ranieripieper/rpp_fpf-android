package br.org.fpf.android.ui.partida.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.org.fpf.android.R;

/**
 * Created by ranipieper on 3/1/16.
 */
public class EstatisticasAdapter extends RecyclerView.Adapter<EstatisticasAdapter.ViewHolder> {
    private List<EstatisticaItem> mDataset = new ArrayList<>();

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mLblTitulo;
        public TextView mLblValorMandante;
        public TextView mLblValorVisitante;
        public ImageView mImgIcon;
        public TextView mLblValorMandanteErros;
        public TextView mLblValorVisitanteErros;
        public View mBarraEsquerda;
        public View mBarraCentro;
        public View mBarraDireita;

        public ViewHolder(View v) {
            super(v);
            mLblTitulo = (TextView) v.findViewById(R.id.lbl_titulo);
            mLblValorMandante = (TextView) v.findViewById(R.id.lbl_valor_mandante);
            mLblValorVisitante = (TextView) v.findViewById(R.id.lbl_valor_visitante);
            mLblValorMandanteErros = (TextView) v.findViewById(R.id.lbl_valor_mandante_errado);
            mLblValorVisitanteErros = (TextView) v.findViewById(R.id.lbl_valor_visitante_errado);
            mImgIcon = (ImageView) v.findViewById(R.id.img_icon);

            mBarraEsquerda = v.findViewById(R.id.view_barra_esquerda);
            mBarraCentro = v.findViewById(R.id.view_barra_centro);
            mBarraDireita = v.findViewById(R.id.view_barra_direita);
        }
    }

    public EstatisticasAdapter(List<EstatisticaItem> dataset) {
        mDataset = dataset;
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getLayout();
    }

    public EstatisticaItem getItem(int position) {
        return mDataset.get(position);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public EstatisticasAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        EstatisticaItem item = getItem(position);
        holder.mLblTitulo.setText(item.getTitulo());
        holder.mLblValorMandante.setText(item.getValorMandante());
        holder.mLblValorVisitante.setText(item.getValorVisitante());

        if (item.getLayout() == R.layout.row_estatisticas_certas_erradas) {
            holder.mLblValorMandanteErros.setText(item.getValorMandanteErrados());
            holder.mLblValorVisitanteErros.setText(item.getValorVisitanteErrados());
        } else if (item.getLayout() == R.layout.row_estatisticas_icon) {
            holder.mImgIcon.setImageResource(item.getIcon());
            holder.mImgIcon.bringToFront();
        }

        if (item.isBarraCentral() == null) {
            holder.mBarraCentro.setVisibility(View.INVISIBLE);
            holder.mBarraDireita.setVisibility(View.GONE);
            holder.mBarraEsquerda.setVisibility(View.GONE);
        } else if (item.isBarraCentral()) {
            holder.mBarraCentro.setVisibility(View.VISIBLE);
            holder.mBarraDireita.setVisibility(View.GONE);
            holder.mBarraEsquerda.setVisibility(View.GONE);
        } else {
            holder.mBarraCentro.setVisibility(View.GONE);
            holder.mBarraDireita.setVisibility(View.VISIBLE);
            holder.mBarraEsquerda.setVisibility(View.VISIBLE);
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void removeAllItems() {
        if (this.mDataset != null) {
            this.mDataset.removeAll(this.mDataset);
        }
    }

    public void addItems(List<EstatisticaItem> items) {
        if (this.mDataset != null) {
            this.mDataset.addAll(items);
        }
        notifyDataSetChanged();
    }

    public static class EstatisticaItem {
        private Integer titulo;
        private Integer subtitulo;
        private int layout;
        private String valorMandante;
        private String valorVisitante;
        private String valorMandanteErrados;
        private String valorVisitanteErrados;
        private Integer icon;
        private Boolean barraCentral;

        public int getLayout() {
            return layout;
        }

        public void setLayout(int layout) {
            this.layout = layout;
        }

        public String getValorMandante() {
            return valorMandante;
        }

        public void setValorMandante(String valorMandante) {
            this.valorMandante = valorMandante;
        }

        public String getValorVisitante() {
            return valorVisitante;
        }

        public void setValorVisitante(String valorVisitante) {
            this.valorVisitante = valorVisitante;
        }

        public String getValorMandanteErrados() {
            return valorMandanteErrados;
        }

        public void setValorMandanteErrados(String valorMandanteErrados) {
            this.valorMandanteErrados = valorMandanteErrados;
        }

        public String getValorVisitanteErrados() {
            return valorVisitanteErrados;
        }

        public void setValorVisitanteErrados(String valorVisitanteErrados) {
            this.valorVisitanteErrados = valorVisitanteErrados;
        }

        public Integer getIcon() {
            return icon;
        }

        public void setIcon(Integer icon) {
            this.icon = icon;
        }

        public Integer getTitulo() {
            return titulo;
        }

        public void setTitulo(Integer titulo) {
            this.titulo = titulo;
        }

        public Integer getSubtitulo() {
            return subtitulo;
        }

        public void setSubtitulo(Integer subtitulo) {
            this.subtitulo = subtitulo;
        }

        public Boolean isBarraCentral() {
            return barraCentral;
        }

        public void setBarraCentral(Boolean barraCentral) {
            this.barraCentral = barraCentral;
        }
    }
}
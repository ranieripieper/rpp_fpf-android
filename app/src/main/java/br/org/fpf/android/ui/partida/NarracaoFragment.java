package br.org.fpf.android.ui.partida;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;

import java.util.ArrayList;
import java.util.List;

import br.org.fpf.android.R;
import br.org.fpf.android.cache.EscalacaoCacheManager;
import br.org.fpf.android.cache.NarracaoCacheManager;
import br.org.fpf.android.model.Narracao;
import br.org.fpf.android.model.Partida;
import br.org.fpf.android.service.NarracaoServiceImpl;
import br.org.fpf.android.service.PartidaServiceImpl;
import br.org.fpf.android.helper.NarracaoHelper;
import br.org.fpf.android.helper.PartidaHelper;
import br.org.fpf.android.ui.partida.adapter.NarracaoViewHolder;
import br.org.fpf.android.util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;

/**
 * Created by ranipieper on 1/14/16.
 */
@EFragment(R.layout.fragment_narracao)
public class NarracaoFragment extends PartidaBaseFragment {

    protected EasyLoadMoreRecyclerAdapter mAdapter;

    private Boolean isRefreshing = false;

    public static NarracaoFragment newInstance(Long idCampeonato, Long idPartida) {
        Bundle args = new Bundle();

        NarracaoFragment_ fragment = new NarracaoFragment_();

        args.putLong(Constants.ARG_ID_CAMPEONATO, idCampeonato);
        args.putLong(Constants.ARG_ID_PARTIDA, idPartida);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    protected void initSwipeRefresh() {
        super.initSwipeRefresh();

        //recupera as informações do cache
        List<Narracao> narracaoList = NarracaoCacheManager.getInstance().getNarracaoByIdPartida(mIdPartida);
        if (narracaoList != null && !narracaoList.isEmpty()) {
            renderNarracao(new ArrayList<Narracao>(narracaoList));
        }
    }

    protected void loadData() {
        synchronized (isRefreshing) {
            if (isAdded() && !isRefreshing) {
                isRefreshing = true;
                showLoadingView(mSwipeRefreshLayout);
                mRecyclerView.setVisibility(View.VISIBLE);

                callService();
            } else if (isAdded()) {
                hideLoadingView(mSwipeRefreshLayout);
            }
        }
    }

    public void callService() {
        boolean cache = false;
        if (forceUpdate) {
            cache = false;
            forceUpdate = false;
        } else {
            if (PartidaHelper.isFinished(mPartida) && NarracaoHelper.hasNarracaoFimJogo(getContext(), mIdPartida)) {
                cache = true;
            }
        }
        Long lastId = null;
        if (mAdapter != null && mAdapter.getItems() != null && mAdapter.getItems().size() > 0) {
            lastId = ((Narracao) mAdapter.getItem(0)).getId();
        }
        NarracaoServiceImpl.getNarracao(cache, mIdPartida, lastId, new Callback<List<Narracao>>() {
            @Override
            public void success(List<Narracao> narracaoList, Response response) {
                renderNarracao(narracaoList);
                hideLoadingView(mSwipeRefreshLayout);
                autoRefresh();
            }

            @Override
            public void failure(RetrofitError error) {
                hideLoadingView(mSwipeRefreshLayout);
                autoRefresh();
            }
        });
    }

    private void autoRefresh() {
        if (PartidaHelper.isFinished(mPartida) && mAdapter != null) {
            return;
        }
        forceUpdate = true;
        synchronized (isRefreshing) {
            isRefreshing = false;
            final Handler handler = new Handler();
            Runnable refresh = new Runnable() {
                public void run() {
                    if (isAdded()) {
                        loadData();
                    }
                }
            };
            handler.postDelayed(refresh, Constants.REFRESH_NARRACAO);
        }
    }

    public synchronized void renderNarracao(List<Narracao> narracaoList) {
        if (!isAdded()) {
            return;
        }
        if (mAdapter == null) {
            createAdapter(narracaoList);
        } else {
            mAdapter.addItems(0, narracaoList);
            verifyUpdateEscalacao(narracaoList);
        }

        if (mRecyclerView != null && mRecyclerView.getAdapter() == null) {
            mRecyclerView.setAdapter(mAdapter);
        }
        synchronized (isRefreshing) {
            isRefreshing = false;
        }
    }

    void verifyUpdateEscalacao(List<Narracao> narracaoList) {
        for (Narracao narracao : narracaoList) {
            if (isAdded()) {
                if (needUpdateEscalacao(narracao)) {
                    EscalacaoCacheManager.getInstance().delete(getActivity(), narracao.getIdPartida());
                    return;
                }
            }
        }
    }

    public boolean needUpdateEscalacao(Narracao narracao) {
        if (!TextUtils.isEmpty(narracao.getAcaoImportante())) {
            if (Constants.NARRACAO_CARTAO_AMARELO.equalsIgnoreCase(narracao.getAcaoImportante().trim())) {
                return true;
            } else if (Constants.NARRACAO_GOL.equalsIgnoreCase(narracao.getAcaoImportante().trim())) {
                updatePartida();
                return true;
            } else if (Constants.NARRACAO_SUBSTITUICAO.equalsIgnoreCase(narracao.getAcaoImportante().trim())) {
                return true;
            } else if (Constants.NARRACAO_CARTAO_VERMELHO.equalsIgnoreCase(narracao.getAcaoImportante().trim())) {
                return true;
            }
        }

        return false;
    }

    @Background
    void updatePartida() {
        PartidaServiceImpl.getPartida(getActivity(), mIdPartida, new Callback<Partida>() {
            @Override
            public void success(Partida partida, Response response) {

            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    LinearLayoutManager getLayoutManager() {
        return new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
    }

    protected void createAdapter(List<Narracao> narracaoList) {
        mAdapter = new EasyLoadMoreRecyclerAdapter(
                getActivity(),
                NarracaoViewHolder.class,
                narracaoList,
                this,
                R.layout.loading_recycler_view, getHeaderRecyclerView());

        mAdapter.notifyDataSetChanged();
    }
}

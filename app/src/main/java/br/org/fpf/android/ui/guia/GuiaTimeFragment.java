package br.org.fpf.android.ui.guia;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.org.fpf.android.FpfApplication;
import br.org.fpf.android.R;
import br.org.fpf.android.cache.GuiaCacheManager;
import br.org.fpf.android.model.Guia;
import br.org.fpf.android.model.RealmString;
import br.org.fpf.android.ui.base.BaseFragment;
import br.org.fpf.android.ui.guia.adapter.UniformeViewPagerAdapter;
import br.org.fpf.android.ui.navigation.Navigator;
import br.org.fpf.android.util.Constants;

/**
 * Created by ranipieper on 3/3/16.
 */
@EFragment(R.layout.fragment_guia_time)
public class GuiaTimeFragment extends BaseFragment {

    private int MAP_ZOOM = 15;

    @ViewById(R.id.scroll_view)
    ScrollView mScrollView;

    @ViewById(R.id.img_distintivo)
    ImageView mImgDistintivo;

    @ViewById(R.id.lbl_nome_time)
    TextView mLblNometime;

    @ViewById(R.id.lbl_descricao)
    TextView mLblDescricao;

    //Mascote
    @ViewById(R.id.img_mascote)
    ImageView mImgMascote;
    @ViewById(R.id.lbl_mascote)
    TextView mLblMascote;
    private Boolean mascoteAnimate = true;

    //Uniforme
    @ViewById(R.id.lbl_uniforme)
    TextView mLblUniforme;
    @ViewById(R.id.indicator_uniforme_1)
    ImageView mIndicatorUniforme1;
    @ViewById(R.id.indicator_uniforme_2)
    ImageView mIndicatorUniforme2;
    @ViewById(R.id.view_pager_uniformes)
    ViewPager mViewPagerUniforme;
    @ViewById(R.id.layout_uniforme)
    View mLayoutUniforme;
    @ViewById(R.id.view_sep_uniforme)
    View mViewSepUniforme;
    @ViewById(R.id.view_sep_uniforme_right)
    View mViewSepUniformeRight;
    private Boolean uniformeAnimate = true;

    //estadio
    @ViewById(R.id.layout_estadio_frame)
    View mLayoutEstadioFrame;
    @ViewById(R.id.layout_estadio)
    View mLayoutEstadio;
    @ViewById(R.id.lbl_estadio_nome)
    TextView mLblEstadioNome;
    @ViewById(R.id.lbl_estadio_capacidade)
    TextView mLblEstadioCapacidade;
    @ViewById(R.id.lbl_estadio_inauguracao)
    TextView mLblEstadioInauguracao;
    @ViewById(R.id.view_sep_estadio)
    View mViewSepEstadio;
    @ViewById(R.id.view_sep_estadio_right)
    View mViewSepEstadioRight;

    //cidade
    @ViewById(R.id.lbl_cidade)
    TextView mLblCidade;

    @ViewById(R.id.layout_cidade_desc)
    View mLayoutCidadeDesc;
    @ViewById(R.id.view_sep_cidade_desc)
    View mViewSepCidadeDesc;
    @ViewById(R.id.view_sep_cidade_desc_right)
    View mViewSepCidadeDescRight;
    @ViewById(R.id.layout_cidade_info)
    View mLayoutCidadeInfo;
    @ViewById(R.id.view_sep_cidade_info)
    View mViewSepCidadeInfo;

    @ViewById(R.id.lbl_cidade_desc)
    TextView mLblCidadeDesc;
    @ViewById(R.id.img_cidade)
    ImageView mImgCidade;

    @ViewById(R.id.lbl_cidade_area_valor)
    TextView mLblCidadeArea;
    @ViewById(R.id.lbl_cidade_populacao_valor)
    TextView mLblCidadePopulacao;
    @ViewById(R.id.lbl_cidade_densidade_valor)
    TextView mLblCidadeDensidade;
    @ViewById(R.id.lbl_cidade_idhm_valor)
    TextView mLblCidadeIdhm;

    @ViewById(R.id.lbl_cidade_area)
    TextView mLblCidadeAreaLabel;
    @ViewById(R.id.lbl_cidade_populacao)
    TextView mLblCidadePopulacaoLabel;
    @ViewById(R.id.lbl_cidade_densidade)
    TextView mLblCidadeDensidadeLabel;
    @ViewById(R.id.lbl_cidade_idhm)
    TextView mLblCidadeIdhmLabel;

    @ViewById(R.id.map_view_cidade)
    MapView mMapViewCidade;
    GoogleMap mMapCidade;

    @FragmentArg(Constants.ARG_ID_GUIA)
    Long mIdGuia;

    private Guia mGuia;

    private List<View.OnScrollChangeListener> mOnScrollChangeListener = new ArrayList();
    private View.OnScrollChangeListener mascoteOnScrollChangeListener;
    private View.OnScrollChangeListener uniformeOnScrollChangeListener;

    public static GuiaTimeFragment newInstance(Long idGuia) {
        Bundle args = new Bundle();
        args.putLong(Constants.ARG_ID_GUIA, idGuia);

        GuiaTimeFragment_ fragment = new GuiaTimeFragment_();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGuia = GuiaCacheManager.getInstance().get(mIdGuia);
    }

    @AfterViews
    public void afterViews() {
        super.afterViews();

        mLblNometime.setText(mGuia.getNome());
        FpfApplication.imageLoader.displayImage(mGuia.getUrlDistintivo(), mImgDistintivo);
        mLblDescricao.setText(mGuia.getDescricao());

        setUniforme();
        setMascote();
        setEstadio();
        setCidade();
        setElenco();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            mScrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    synchronized (mOnScrollChangeListener) {
                        if (mOnScrollChangeListener != null) {
                            //copy List - ConcurrentModificationException
                            List<View.OnScrollChangeListener> onScrollChangeListenerTmp = new ArrayList<View.OnScrollChangeListener>();
                            onScrollChangeListenerTmp.addAll(mOnScrollChangeListener);
                            if (onScrollChangeListenerTmp != null && !onScrollChangeListenerTmp.isEmpty()) {
                                for (View.OnScrollChangeListener listener : onScrollChangeListenerTmp) {
                                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                                        listener.onScrollChange(v, scrollX, scrollY, oldScrollX, oldScrollY);
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }

    }

    private void setElenco() {
        if (mGuia.getElenco() == null || mGuia.getElenco().isEmpty()) {
            getView().findViewById(R.id.layout_elenco).setVisibility(View.GONE);
            getView().findViewById(R.id.view_sep_elenco).setVisibility(View.GONE);
        }
    }

    private void setCidade() {
        if (isAdded()) {
            if (mMapViewCidade != null) {


                mMapViewCidade.onCreate(null);
                mMapViewCidade.onResume();
                if (mGuia.getCoordenada() != null
                        && mGuia.getCoordenada().getLatitude() != null
                        && mGuia.getCoordenada().getLongitude() != null) {
                    mMapViewCidade.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap googleMap) {
                            mMapCidade = googleMap;
                            mMapCidade.getUiSettings().setMyLocationButtonEnabled(false);
                            mMapCidade.getUiSettings().setAllGesturesEnabled(false);
                            mMapCidade.getUiSettings().setScrollGesturesEnabled(false);
                            mMapCidade.getUiSettings().setZoomControlsEnabled(false);
                            mMapCidade.getUiSettings().setZoomGesturesEnabled(false);

                            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(mGuia.getCoordenada().getLatitude(), mGuia.getCoordenada().getLongitude()), MAP_ZOOM);
                            mMapCidade.animateCamera(cameraUpdate);
                            mMapCidade.addMarker(new MarkerOptions()
                                    .position(new LatLng(mGuia.getCoordenada().getLatitude(), mGuia.getCoordenada().getLongitude()))
                                    .title(mGuia.getCidade()));
                        }
                    });
                } else {
                    mMapViewCidade.setVisibility(View.GONE);
                }
            }
            mLblCidade.setText(mGuia.getCidade());
            if (TextUtils.isEmpty(mGuia.getDescricaoCidade())) {
                mLayoutCidadeDesc.setVisibility(View.GONE);
                mViewSepCidadeDesc.setVisibility(View.GONE);
                mViewSepCidadeDescRight.setVisibility(View.GONE);
            } else {
                mLblCidadeDesc.setText(mGuia.getDescricaoCidade());
            }
            FpfApplication.imageLoader.loadImage(mGuia.getUrlCidade(), new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    if (isAdded()) {
                        mImgCidade.setImageBitmap(loadedImage);
                        mImgCidade.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });

            if (TextUtils.isEmpty(mGuia.getCidadeArea())
                    & TextUtils.isEmpty(mGuia.getPopulacao())
                    && TextUtils.isEmpty(mGuia.getDensidadeDemografica())
                    && TextUtils.isEmpty(mGuia.getIdhm())
                    ) {
                mLayoutCidadeInfo.setVisibility(View.GONE);
                mViewSepCidadeInfo.setVisibility(View.GONE);

            } else {
                if (TextUtils.isEmpty(mGuia.getCidadeArea())) {
                    mLblCidadeArea.setVisibility(View.GONE);
                    mLblCidadeAreaLabel.setVisibility(View.GONE);
                } else {
                    mLblCidadeArea.setText(mGuia.getCidadeArea());
                }

                if (TextUtils.isEmpty(mGuia.getPopulacao())) {
                    mLblCidadePopulacao.setVisibility(View.GONE);
                    mLblCidadePopulacaoLabel.setVisibility(View.GONE);
                } else {
                    mLblCidadePopulacao.setText(mGuia.getPopulacao());
                }

                if (TextUtils.isEmpty(mGuia.getDensidadeDemografica())) {
                    mLblCidadeDensidade.setVisibility(View.GONE);
                    mLblCidadeDensidadeLabel.setVisibility(View.GONE);
                } else {
                    mLblCidadeDensidade.setText(mGuia.getDensidadeDemografica());
                }

                if (TextUtils.isEmpty(mGuia.getIdhm())) {
                    mLblCidadeIdhm.setVisibility(View.GONE);
                    mLblCidadeIdhmLabel.setVisibility(View.GONE);
                } else {
                    mLblCidadeIdhm.setText(mGuia.getIdhm());
                }
            }
        }
    }

    private void setEstadio() {
        if (TextUtils.isEmpty(mGuia.getNomeEstadio()) || TextUtils.isEmpty(mGuia.getUrlEstadio())) {
            mLayoutEstadioFrame.setVisibility(View.GONE);
            mViewSepEstadio.setVisibility(View.GONE);
            mViewSepEstadioRight.setVisibility(View.GONE);
        } else {
            if (!TextUtils.isEmpty(mGuia.getUrlEstadio())) {
                FpfApplication.imageLoader.loadImage(mGuia.getUrlEstadio(), new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        if (isAdded()) {
                            BitmapDrawable estadio = new BitmapDrawable(getResources(), loadedImage);
                            mLayoutEstadio.setBackgroundDrawable(estadio);
                        }
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                    }
                });
            }

            mLblEstadioNome.setText(mGuia.getNomeEstadio());
            mLblEstadioInauguracao.setText(mGuia.getInauguracaoEstadio());
            mLblEstadioCapacidade.setText(mGuia.getCapacidadeEstadio());
        }
    }

    private void setUniforme() {
        if (isAdded()) {
            mLblUniforme.setText(mGuia.getUniformeFabricante());
            if (mGuia.getUrlUniformes() == null || mGuia.getUrlUniformes().isEmpty()
                    || mGuia.getUrlUniformes().get(0) == null
                    || TextUtils.isEmpty(mGuia.getUrlUniformes().get(0).getValue())) {
                mLayoutUniforme.setVisibility(View.GONE);
                mViewSepUniforme.setVisibility(View.GONE);
                mViewSepUniformeRight.setVisibility(View.GONE);
                return;
            }
            if (mGuia.getUrlUniformes() == null
                    || mGuia.getUrlUniformes().isEmpty()
                    || mGuia.getUrlUniformes().size() <= 1) {
                mIndicatorUniforme1.setVisibility(View.GONE);
                mIndicatorUniforme2.setVisibility(View.GONE);
            }

            mViewPagerUniforme.setVisibility(View.INVISIBLE);
            mViewSepUniforme.setVisibility(View.INVISIBLE);
            mViewSepUniformeRight.setVisibility(View.INVISIBLE);

            FpfApplication.imageLoader.loadImage(mGuia.getUrlUniformes().get(0).getValue(), new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    initUniformeAnimation(loadedImage);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                }
            });
        }
    }

    private void initUniformeAnimation(Bitmap loadedImage) {
        if (isAdded()) {
            UniformeViewPagerAdapter adapter = new UniformeViewPagerAdapter(getChildFragmentManager());
            for (RealmString imageUrl : mGuia.getUrlUniformes()) {
                adapter.addFrag(UniformeFragment.newInstance(imageUrl.getValue()));
            }
            mViewPagerUniforme.setAdapter(adapter);

            mViewPagerUniforme.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    if (isAdded()) {
                        if (position == 0) {
                            mIndicatorUniforme1.setImageResource(R.drawable.oval_222_dark);
                            mIndicatorUniforme2.setImageResource(R.drawable.oval_222);
                        } else {
                            mIndicatorUniforme1.setImageResource(R.drawable.oval_222);
                            mIndicatorUniforme2.setImageResource(R.drawable.oval_222_dark);
                        }
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            mViewPagerUniforme.setVisibility(View.VISIBLE);
            mViewPagerUniforme.bringToFront();
            mViewPagerUniforme.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            mViewPagerUniforme.setTranslationX(-200);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                uniformeOnScrollChangeListener = new View.OnScrollChangeListener() {
                    @Override
                    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                        verifyAndAnimationUniforme();
                    }
                };
                synchronized (mOnScrollChangeListener) {
                    if (isAdded()) {
                        mOnScrollChangeListener.add(uniformeOnScrollChangeListener);
                    }
                }
            } else {
                if (isAdded()) {
                    mOnScrollChangedListenerUniforme = new ViewTreeObserver.OnScrollChangedListener() {
                        @Override
                        public void onScrollChanged() {
                            verifyAndAnimationUniforme();
                        }
                    };
                    mScrollView.getViewTreeObserver().addOnScrollChangedListener(mOnScrollChangedListenerUniforme);
                }
            }

            mScrollView.post(new Runnable() {
                @Override
                public void run() {
                    verifyAndAnimationUniforme();
                }
            });
        }
    }

    private void verifyAndAnimationUniforme() {
        synchronized (uniformeAnimate) {
            if (uniformeAnimate && isAdded()) {
                Rect scrollBounds = new Rect();
                mScrollView.getHitRect(scrollBounds);
                if (!mViewPagerUniforme.getLocalVisibleRect(scrollBounds)
                        || scrollBounds.height() < mViewPagerUniforme.getHeight()) {
                    // imageView is not within or only partially within the visible window
                } else {
                    animateUniforme();
                }
            }
        }
    }

    private void animateUniforme() {
        synchronized (uniformeAnimate) {
            if (isAdded()) {
                ViewCompat.animate(mViewPagerUniforme)
                        .setDuration(MASCOTE_UNIFORME_ANIN_DURATION)
                        .translationX(0);
                uniformeAnimate = false;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    synchronized (mOnScrollChangeListener) {
                        mOnScrollChangeListener.remove(uniformeOnScrollChangeListener);

                        if (mOnScrollChangeListener.isEmpty()) {
                            mScrollView.setOnScrollChangeListener(null);
                        }
                    }
                } else if (mOnScrollChangedListener != null) {
                    mScrollView.getViewTreeObserver().removeOnScrollChangedListener(mOnScrollChangedListenerUniforme);
                }

                if (mGuia.getUrlUniformes() == null
                        || mGuia.getUrlUniformes().isEmpty()
                        || mGuia.getUrlUniformes().size() <= 1) {
                    mIndicatorUniforme1.setVisibility(View.GONE);
                    mIndicatorUniforme2.setVisibility(View.GONE);
                } else {
                    mIndicatorUniforme1.setVisibility(View.VISIBLE);
                    mIndicatorUniforme2.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void setMascote() {
        if (isAdded()) {
            mLblMascote.setText(mGuia.getMascote());
            FpfApplication.imageLoader.loadImage(mGuia.getUrlMascote(), new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    mImgMascote.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    initMascoteAnimation(loadedImage);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                }
            });
        }
    }

    private void initMascoteAnimation(Bitmap loadedImage) {
        mImgMascote.setImageBitmap(loadedImage);
        mImgMascote.setVisibility(View.VISIBLE);
        mImgMascote.bringToFront();
        mImgMascote.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

        mImgMascote.setTranslationX(mImgMascote.getMeasuredWidth()/2f);
        mImgMascote.setTranslationY(-20);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            mascoteOnScrollChangeListener = new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    verifyAndAnimationMascote();
                }
            };
            synchronized (mOnScrollChangeListener) {
                mOnScrollChangeListener.add(mascoteOnScrollChangeListener);
            }
        } else {
            mOnScrollChangedListener = new ViewTreeObserver.OnScrollChangedListener() {
                @Override
                public void onScrollChanged() {
                    verifyAndAnimationMascote();
                }
            };
            mScrollView.getViewTreeObserver().addOnScrollChangedListener(mOnScrollChangedListener);
        }
        mScrollView.post(new Runnable() {
            @Override
            public void run() {
                verifyAndAnimationMascote();
            }
        });

    }

    private void verifyAndAnimationMascote() {
        synchronized (mascoteAnimate) {
            if (mascoteAnimate && isAdded()) {
                Rect scrollBounds = new Rect();
                mScrollView.getHitRect(scrollBounds);
                if (!mImgMascote.getLocalVisibleRect(scrollBounds)
                        || scrollBounds.height() < mImgMascote.getHeight()) {
                    // imageView is not within or only partially within the visible window
                } else {
                    animateMascote();
                }
            }
        }
    }

    private int MASCOTE_UNIFORME_ANIN_DURATION = 500;
    private ViewTreeObserver.OnScrollChangedListener mOnScrollChangedListener;
    private ViewTreeObserver.OnScrollChangedListener mOnScrollChangedListenerUniforme;

    private void animateMascote() {
        synchronized (mascoteAnimate) {
            if (isAdded()) {
                ViewCompat.animate(mImgMascote)
                        .setDuration(MASCOTE_UNIFORME_ANIN_DURATION)
                        .translationX(0);
                mascoteAnimate = false;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    synchronized (mOnScrollChangeListener) {
                        mOnScrollChangeListener.remove(mascoteOnScrollChangeListener);

                        if (mOnScrollChangeListener.isEmpty()) {
                            mScrollView.setOnScrollChangeListener(null);
                        }
                    }
                } else if (mOnScrollChangedListener != null) {
                    mScrollView.getViewTreeObserver().removeOnScrollChangedListener(mOnScrollChangedListener);
                }
            }
        }
    }

    private Boolean clicked = false;

    @Click(R.id.layout_elenco)
    void elencoClick() {
        synchronized (clicked) {
            if (!clicked) {
                clicked = true;
                Navigator.navigateToElencoFragment(getContext(), mIdGuia);
            }
        }
    }

    @Override
    public void onResumeFromBackStack() {
        super.onResumeFromBackStack();
        clicked = false;
    }

    @Override
    protected String getToolbarTitle() {
        return "";
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }

    @Override
    public void onResume() {
        if (mMapViewCidade != null) {
            mMapViewCidade.onResume();
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        if (mMapViewCidade != null) {
            mMapViewCidade.onPause();
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMapViewCidade != null) {
            mMapViewCidade.onDestroy();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mMapViewCidade != null) {
            mMapViewCidade.onLowMemory();
        }
    }
}

package br.org.fpf.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

/**
 * Created by ranieripieper on 1/16/16.
 */
public class PushNotificationMessage implements Parcelable {

    @Expose
    private long idPartida;
    @Expose
    private String alert;

    /**
     * Gets the alert
     *
     * @return alert
     */
    public String getAlert() {
        return alert;
    }

    /**
     * Sets the alert
     *
     * @param alert
     */
    public void setAlert(String alert) {
        this.alert = alert;
    }

    /**
     * Gets the idPartida
     *
     * @return idPartida
     */
    public long getIdPartida() {
        return idPartida;
    }

    /**
     * Sets the idPartida
     *
     * @param idPartida
     */
    public void setIdPartida(long idPartida) {
        this.idPartida = idPartida;
    }


    public PushNotificationMessage() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.idPartida);
        dest.writeString(this.alert);
    }

    protected PushNotificationMessage(Parcel in) {
        this.idPartida = in.readLong();
        this.alert = in.readString();
    }

    public static final Creator<PushNotificationMessage> CREATOR = new Creator<PushNotificationMessage>() {
        @Override
        public PushNotificationMessage createFromParcel(Parcel source) {
            return new PushNotificationMessage(source);
        }

        @Override
        public PushNotificationMessage[] newArray(int size) {
            return new PushNotificationMessage[size];
        }
    };
}

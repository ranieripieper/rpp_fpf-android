package br.org.fpf.android.ui.estatisticas;

import android.os.Bundle;
import android.view.View;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;

import java.util.ArrayList;

import br.org.fpf.android.R;
import br.org.fpf.android.model.EstatisticasTime;
import br.org.fpf.android.model.Guia;
import br.org.fpf.android.service.GuiaServiceImpl;
import br.org.fpf.android.ui.guia.adapter.ElencoViewHolder;
import br.org.fpf.android.util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.ribot.easyadapter.EasyRecyclerAdapter;

/**
 * Created by ranipieper on 3/9/16.
 */
@EFragment(R.layout.fragment_estatisticas_time_page)
public class ElencoEstatisticasTimeFragment extends EstatisticasTimeItemFragment {

    public static ElencoEstatisticasTimeFragment newInstance(EstatisticasTime estatisticasTime) {
        Bundle args = new Bundle();

        ElencoEstatisticasTimeFragment_ fragment = new ElencoEstatisticasTimeFragment_();
        args.putParcelable(Constants.ARG_ESTATISTICAS_TIME, estatisticasTime);
        fragment.setArguments(args);

        return fragment;
    }

    @AfterViews
    public void afterViews() {
        super.afterViews();
        mViewFooter.setVisibility(View.GONE);
        loadData();
    }

    private void loadData() {
        showLoadingView();
        mLblError.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);
        GuiaServiceImpl.getGuia(mEstatisticasTime.getIdEquipe(), new Callback<Guia>() {
            @Override
            public void success(Guia guia, Response response) {
                processResult(guia);
                hideLoadingView();
            }

            @Override
            public void failure(RetrofitError error) {
                if (isAdded()) {
                    showError(R.string.error_generic);
                }
            }
        });
    }

    private void processResult(Guia guia) {
        if (isAdded()) {
            if (guia != null && guia.getElenco() != null && !guia.getElenco().isEmpty()) {
                mAdapter.addItems(guia.getElenco());
                mRecyclerView.setVisibility(View.VISIBLE);
            } else {
                showError(R.string.msg_info_nao_disponivel);
            }

        }
    }

    private void showError(int msg) {
        mLblError.setText(msg);
        mRecyclerView.setVisibility(View.GONE);
    }

    @Override
    protected EasyRecyclerAdapter createAdapter() {
        return new EasyRecyclerAdapter<>(
                getActivity(),
                ElencoViewHolder.class,
                new ArrayList());
    }
}
package br.org.fpf.android.cache;

import br.org.fpf.android.cache.base.BaseCache;
import br.org.fpf.android.model.EquipeNotificacao;

/**
 * Created by ranipieper on 11/19/15.
 */
public class EquipeNotificacaoCacheManager extends BaseCache<EquipeNotificacao> {

    private static EquipeNotificacaoCacheManager instance = new EquipeNotificacaoCacheManager();

    private EquipeNotificacaoCacheManager() {
    }

    public static EquipeNotificacaoCacheManager getInstance() {
        return instance;
    }

    @Override
    public String getPrimaryKeyName() {
        return "idEquipe";
    }

    public void updateSent(long id, boolean sent) {
        getRealm().beginTransaction();
        EquipeNotificacao equipeNot = getToUpdate(id);
        if (equipeNot != null) {
            equipeNot.setSentToParse(sent);
        }
        getRealm().commitTransaction();
    }

    @Override
    public Class<EquipeNotificacao> getReferenceClass() {
        return EquipeNotificacao.class;
    }

    @Override
    protected long getCacheLifeTime() {
        return 1000 * 60 * 60 * 24 * 15; // 15 dias
    }
}

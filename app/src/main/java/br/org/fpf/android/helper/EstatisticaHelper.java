package br.org.fpf.android.helper;

/**
 * Created by ranipieper on 3/1/16.
 */
public class EstatisticaHelper {

    public static String getPosseDeBola(Double value) {
        if (value == null) {
            return "-";
        } else {
            return String.format("%.2f%s", value, "%");
        }
    }

    public static String getValue(Integer value) {
        if (value == null) {
            return "-";
        } else {
            return value.toString();
        }
    }
}

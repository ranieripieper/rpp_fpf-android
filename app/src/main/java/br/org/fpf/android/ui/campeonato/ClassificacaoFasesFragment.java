package br.org.fpf.android.ui.campeonato;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.h6ah4i.android.tablayouthelper.TabLayoutHelper;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import br.org.fpf.android.BuildConfig;
import br.org.fpf.android.R;
import br.org.fpf.android.cache.CampeonatoCacheManager;
import br.org.fpf.android.cache.ClassificacaoCacheManager;
import br.org.fpf.android.model.Campeonato;
import br.org.fpf.android.model.Classificacao;
import br.org.fpf.android.model.Equipe;
import br.org.fpf.android.service.ClassificacaoServiceImpl;
import br.org.fpf.android.service.EquipeServiceImpl;
import br.org.fpf.android.ui.base.BaseFragment;
import br.org.fpf.android.ui.custom.FpfViewPagerAdapter;
import br.org.fpf.android.util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranieripieper on 1/29/16.
 */
@EFragment(R.layout.fragment_classificacao_fases)
public class ClassificacaoFasesFragment extends BaseFragment {

    @ViewById(R.id.tablayout)
    TabLayout mTabLayout;

    @ViewById(R.id.viewpager)
    ViewPager mViewPager;

    @ViewById(R.id.txt_error_no_results)
    TextView mTxtErrorNoResult;

    @FragmentArg(Constants.ARG_ID_CAMPEONATO)
    Long mIdCampeonato;

    private TabLayoutHelper mTabLayoutHelper;

    public static ClassificacaoFasesFragment newInstance(Long idCampeonato) {
        Bundle args = new Bundle();

        ClassificacaoFasesFragment_ fragment = new ClassificacaoFasesFragment_();
        args.putLong(Constants.ARG_ID_CAMPEONATO, idCampeonato);

        fragment.setArguments(args);

        return fragment;
    }

    @AfterViews
    public void afterViews() {
        super.afterViews();

        callService();
    }

    protected void callServiceClassificacao() {
        showLoadingView();
        ClassificacaoServiceImpl.getClassificacao(mIdCampeonato, new Callback<List<Classificacao>>() {
            @Override
            public void success(List<Classificacao> classificacaoList, Response response) {
                if (isAdded()) {
                    if (classificacaoList == null || classificacaoList.isEmpty()) {
                        hideLoadingView();
                        showErrorTextView(mTxtErrorNoResult, R.string.msg_classificacao_vazia);
                    } else {
                        prepareViewPager();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (BuildConfig.DEBUG) {
                    error.printStackTrace();
                }
                hideLoadingView();
                showErrorTextView(mTxtErrorNoResult, R.string.error_generic_refresh);
            }
        });
    }

    @Click(R.id.txt_error_no_results)
    void errorClick() {
        callService();
    }

    protected void callService() {
        mTxtErrorNoResult.setVisibility(View.GONE);
        showLoadingView();
        EquipeServiceImpl.getEquipesInCache(new Callback<List<Equipe>>() {
            @Override
            public void success(List<Equipe> equipes, Response response) {
                callServiceClassificacao();
            }

            @Override
            public void failure(RetrofitError error) {
                showErrorTextView(mTxtErrorNoResult, R.string.error_generic_refresh);
            }
        });
    }

    @Background
    void prepareViewPager() {
        Set<String> fases = new LinkedHashSet<>();

        List<Classificacao> classificacaoList = ClassificacaoCacheManager.getInstance().getClassificacaoByIdCampeonato(getContext(), mIdCampeonato);

        adapter = new FpfViewPagerAdapter(getChildFragmentManager());

        for (Classificacao p : classificacaoList) {
            fases.add(p.getFase());
        }

        Campeonato campeonato = CampeonatoCacheManager.getInstance().get(getActivity(), mIdCampeonato);

        int currentPage = -1;
        int pos = 0;

        for (String fr : fases) {
            adapter.addFrag(ClassificacaoFragment.newInstance(mIdCampeonato, fr), fr, isAdded());
            if (campeonato != null && fr.equalsIgnoreCase(campeonato.getFaseAtual())) {
                currentPage = pos;
            }
            pos++;
        }

        setupViewPager(classificacaoList, fases, currentPage);

    }

    FpfViewPagerAdapter adapter;

    @UiThread
    void setupViewPager(List<Classificacao> classificacaoList, Set<String> fases, int currentPage) {

        if (isAdded()) {
            mViewPager.setOffscreenPageLimit(1);

            mViewPager.setAdapter(adapter);

            mViewPager.setVisibility(View.VISIBLE);

            if (fases.size() > 1) {
                mTabLayoutHelper = new TabLayoutHelper(mTabLayout, mViewPager);
                mTabLayoutHelper.setAutoAdjustTabModeEnabled(true);
                // initialize the TabLayoutHelper instance
                mTabLayout.setVisibility(View.VISIBLE);
                final Handler handler = new Handler();
                Runnable refresh = new Runnable() {
                    public void run() {
                        if (isAdded()) {
                            hideLoadingView();
                            TabLayoutHelper mTabLayoutHelper = new TabLayoutHelper(mTabLayout, mViewPager);
                            mTabLayoutHelper.setAutoAdjustTabModeEnabled(true);
                            mTabLayout.setVisibility(View.VISIBLE);
                            mViewPager.setVisibility(View.VISIBLE);
                        }
                    }
                };
                showLoadingView();
                handler.postDelayed(refresh, 100);

                if (currentPage >= 0) {
                    mTabLayout.getTabAt(currentPage).select();
                    mViewPager.setCurrentItem(currentPage);
                }
            } else {
                mTabLayout.setVisibility(View.GONE);
            }
        }
        hideLoadingView();
    }

    @Override
    public void onDestroyView() {
        // release the TabLayoutHelper instance
        if (mTabLayoutHelper != null) {
            mTabLayoutHelper.release();
            mTabLayoutHelper = null;
        }

        super.onDestroyView();
    }

    @Override
    protected boolean needConfigToolbar() {
        return false;
    }

    @Override
    protected String getToolbarTitle() {
        return "";
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.DRAWER;
    }

}

package br.org.fpf.android.cache;

import java.util.ArrayList;
import java.util.List;

import br.org.fpf.android.cache.base.BaseCache;
import br.org.fpf.android.model.Equipe;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by ranipieper on 11/19/15.
 */
public class EquipeCacheManager extends BaseCache<Equipe> {

    private static EquipeCacheManager instance = new EquipeCacheManager();

    private EquipeCacheManager() {
    }

    public static EquipeCacheManager getInstance() {
        return instance;
    }

    public List<Equipe> getEquipeToPush() {

        String[] DEFAULT_ORDER_FIELDS = {"nome"};
        Sort[] DEFAULT_ORDER_SORT = {Sort.ASCENDING};

        RealmResults<Equipe> results = getRealm().where(Equipe.class)
                .equalTo("push", true)
                .findAllSorted(DEFAULT_ORDER_FIELDS, DEFAULT_ORDER_SORT);

        if (results != null) {
            return getRealm().copyFromRealm(results);
        }
        return new ArrayList();
    }

    @Override
    public Class<Equipe> getReferenceClass() {
        return Equipe.class;
    }

    @Override
    protected long getCacheLifeTime() {
        return 1000 * 60 * 60 * 24 * 15; // 15 dias
    }
}

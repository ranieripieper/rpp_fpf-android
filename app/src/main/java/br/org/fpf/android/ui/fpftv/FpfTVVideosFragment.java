package br.org.fpf.android.ui.fpftv;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.org.fpf.android.BuildConfig;
import br.org.fpf.android.FpfApplication;
import br.org.fpf.android.R;
import br.org.fpf.android.cache.VideoCacheManager;
import br.org.fpf.android.helper.VideoHelper;
import br.org.fpf.android.model.Video;
import br.org.fpf.android.service.background.UpdateVideosViewsService_;
import br.org.fpf.android.service.background.VideoService;
import br.org.fpf.android.ui.base.BaseFragment;
import br.org.fpf.android.ui.fpftv.adapter.YoutubeVideoViewHolder;
import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;
import uk.co.ribot.easyadapter.recyclerview.EasyLoadMoreRecyclerView;

/**
 * Created by ranipieper on 1/26/16.
 */
@EFragment(R.layout.fragment_fpf_tv_videos)
public class FpfTVVideosFragment extends BaseFragment implements YoutubeVideoViewHolder.YoutubeVideoHolderListener {

    @ViewById(R.id.swipe_refresh_layout)
    @Nullable
    SwipeRefreshLayout mSwipeRefreshLayout;

    @ViewById(R.id.recycler_view)
    EasyLoadMoreRecyclerView mRecyclerView;

    @ViewById(R.id.txt_error_no_results)
    TextView txtError;

    private String nextPageToken = null;

    protected boolean forceUpdate = true;

    protected EasyLoadMoreRecyclerAdapter mAdapter;

    private Boolean isRefreshing = false;

    public static FpfTVVideosFragment newInstance() {
        Bundle args = new Bundle();

        FpfTVVideosFragment fragment = FpfTVVideosFragment_.builder().build();
        fragment.setArguments(args);

        return fragment;
    }

    @AfterViews
    public void afterViews() {
        super.afterViews();
        initSwipeRefresh();
        loadData();
    }

    protected void initSwipeRefresh() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    forceUpdate = true;
                    nextPageToken = null;
                    loadData();
                }
            });
        }
    }

    protected void loadData() {
        synchronized (isRefreshing) {
            if (isRefreshing) {
                return;
            }
            isRefreshing = true;
        }
        if (isAdded()) {
            callService();
        }
    }

    @Background
    public void callBackgroundService() {
        try {
            YouTube.Search.List searchService = FpfApplication.getYoutubeApi()
                    .search()
                    .list("id")
                    .setFields("items,nextPageToken")
                    .setChannelId(BuildConfig.CHANNEL_ID)
                    .setPart("snippet,id")
                    .setOrder("date")
                    .setMaxResults(50l)
                    .setType("video");

            if (!TextUtils.isEmpty(nextPageToken)) {
                searchService.setPageToken(nextPageToken);
            }

            searchService.setKey(BuildConfig.YOUTUBE_API_KEY);

            SearchListResponse searchResponse = searchService.execute();
            List<SearchResult> searchResultList = searchResponse.getItems();

            final List<Video> videos = VideoHelper.transformVideo(searchResultList);

            VideoCacheManager.getInstance().updateWithoutNulls(videos);

            nextPageToken = searchResponse.getNextPageToken();

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    renderVideos(videos);
                }
            });

        } catch (final Exception e) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showError(e);
                }
            });
        }
    }

    public synchronized void showError(Exception e) {
        if (mAdapter == null) {
            showErrorTextView(txtError, R.string.error_generic_refresh);
        }

        hideLoadingView(mSwipeRefreshLayout);
        synchronized (isRefreshing) {
            isRefreshing = false;
        }
    }

    public synchronized void renderVideos(List<Video> videoList) {
        if (!isAdded()) {
            return;
        }
        UpdateVideosViewsService_.intent(getActivity())
                .updateVideos(VideoService.getListVideosIds(videoList))
                .start();
        mRecyclerView.setVisibility(View.VISIBLE);
        if (!isAdded()) {
            return;
        }
        if (mAdapter == null) {
            createAdapter(videoList);
        } else {
            mAdapter.addItems(videoList);
        }

        if (mRecyclerView != null && mRecyclerView.getAdapter() == null) {
            mRecyclerView.setAdapter(mAdapter);
        }
        synchronized (isRefreshing) {
            isRefreshing = false;
        }
        mRecyclerView.onLoadingMoreFinish();
        if (TextUtils.isEmpty(nextPageToken)) {
            disableLoadingMore(mRecyclerView);
        }
        hideLoadingView(mSwipeRefreshLayout);

    }

    protected void createAdapter(List<Video> videoList) {
        mAdapter = new EasyLoadMoreRecyclerAdapter(
                getActivity(),
                YoutubeVideoViewHolder.class,
                videoList,
                this,
                R.layout.loading_recycler_view);

        mAdapter.notifyDataSetChanged();
    }

    public void callService() {
        if (!isAdded()) {
            return;
        }
        if (TextUtils.isEmpty(nextPageToken)) {
            mRecyclerView.setVisibility(View.GONE);
            if (mAdapter != null) {
                mAdapter.removeAllItems();
            }
            mRecyclerView.enableAutoLoadMore(new EasyLoadMoreRecyclerView.LoadMoreListener() {
                @Override
                public void loadMore() {
                    forceUpdate = true;
                    loadData();
                }
            });
        }
        txtError.setVisibility(View.GONE);
        showLoadingView(mSwipeRefreshLayout);

        callBackgroundService();
    }


    @Override
    protected boolean needConfigToolbar() {
        return false;
    }

    @Override
    public void onItemSelected(Video video) {
        PlayerViewActivity.startActivity(getContext(), video.getId());
    }
}


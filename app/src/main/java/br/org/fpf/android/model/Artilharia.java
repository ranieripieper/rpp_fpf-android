package br.org.fpf.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ranipieper on 1/26/16.
 */
public class Artilharia extends BaseModel {

    private int position;

    @Expose
    @SerializedName("Equipe")
    private String equipe;

    @Expose
    @SerializedName("IdEquipe")
    private Long idEquipe;

    @Expose
    @SerializedName("IdJogador")
    private String idJogador;

    @Expose
    @SerializedName("Jogador")
    private String jogador;

    @Expose
    @SerializedName("Total")
    private int total;

    public String getEquipe() {
        return equipe;
    }

    public void setEquipe(String equipe) {
        this.equipe = equipe;
    }

    public Long getIdEquipe() {
        return idEquipe;
    }

    public void setIdEquipe(Long idEquipe) {
        this.idEquipe = idEquipe;
    }

    public String getIdJogador() {
        return idJogador;
    }

    public void setIdJogador(String idJogador) {
        this.idJogador = idJogador;
    }

    public String getJogador() {
        return jogador;
    }

    public void setJogador(String jogador) {
        this.jogador = jogador;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}

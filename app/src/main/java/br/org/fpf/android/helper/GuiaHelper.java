package br.org.fpf.android.helper;

import android.net.Uri;
import android.text.TextUtils;

/**
 * Created by ranipieper on 3/10/16.
 */
public class GuiaHelper {

    public static String getUrl(String value) {
        if (TextUtils.isEmpty(value)) {
            return value;
        }
        return Uri.decode(value.trim());
    }
}

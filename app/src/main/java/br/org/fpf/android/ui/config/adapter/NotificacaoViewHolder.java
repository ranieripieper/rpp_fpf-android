package br.org.fpf.android.ui.config.adapter;

import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.text.Normalizer;

import br.org.fpf.android.BuildConfig;
import br.org.fpf.android.FpfApplication;
import br.org.fpf.android.R;
import br.org.fpf.android.cache.EquipeCacheManager;
import br.org.fpf.android.cache.EquipeNotificacaoCacheManager;
import br.org.fpf.android.model.Equipe;
import br.org.fpf.android.model.EquipeNotificacao;
import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by ranieripieper on 1/31/16.
 */
@LayoutId(R.layout.row_notificacao)
public class NotificacaoViewHolder extends ItemViewHolder<Equipe> implements CompoundButton.OnCheckedChangeListener {

    @ViewId(R.id.img_time)
    ImageView imgTime;

    @ViewId(R.id.switch_selected)
    SwitchCompat switchNotificacao;


    public NotificacaoViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Equipe itemParam, PositionInfo positionInfo) {

        Equipe equipe = EquipeCacheManager.getInstance().get(itemParam.getId());

        switchNotificacao.setOnCheckedChangeListener(null);

        if (equipe == null) {
            imgTime.setImageResource(R.drawable.ic_team_placeholder);
            switchNotificacao.setText("");
        } else {
            FpfApplication.imageLoader.displayImage(equipe.getUrlLogo(), imgTime);
            switchNotificacao.setText(equipe.getNome());
            if (EquipeNotificacaoCacheManager.getInstance().get(equipe.getId()) != null) {
                switchNotificacao.setChecked(true);
            } else {
                switchNotificacao.setChecked(false);
            }
            switchNotificacao.setOnCheckedChangeListener(this);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        boolean callListener = false;
        if (isChecked) {
            EquipeNotificacao eq = EquipeNotificacaoCacheManager.getInstance().get(getItem().getId());
            if (eq == null) {
                if (EquipeNotificacaoCacheManager.getInstance().count() >= BuildConfig.MAX_TEANS_NOTIFICATIONS) {
                    switchNotificacao.setChecked(false);
                    Toast.makeText(getContext(), R.string.msg_max_notifications, Toast.LENGTH_SHORT).show();
                } else {
                    EquipeNotificacao eqNot = new EquipeNotificacao();
                    eqNot.setChannel(getChannel(getItem()));
                    eqNot.setIdEquipe(getItem().getId());
                    EquipeNotificacaoCacheManager.getInstance().put(eqNot);
                    callListener = true;
                }
            }
        } else {
            EquipeNotificacaoCacheManager.getInstance().delete(getItem().getId());
            callListener = true;
        }
        NotificacaoHolderListener listener = getListener(NotificacaoHolderListener.class);
        if (listener != null && callListener) {
            listener.onEquipeChecked(getItem(), isChecked);
        }
    }

    private String getChannel(Equipe equipe) {
        String value  = Normalizer.normalize(equipe.getNome(), Normalizer.Form.NFD);
        value = value.replaceAll("[^\\p{ASCII}]", "").replaceAll("[^a-zA-Z]", "-");;

        return value;
    }

    @Override
    public void onSetListeners() {
    }

    public interface NotificacaoHolderListener {
        void onEquipeChecked(Equipe equipe, boolean isChecked);
    }
}
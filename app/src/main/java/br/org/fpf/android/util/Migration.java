package br.org.fpf.android.util;

import java.util.Date;

import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

/**
 * Created by ranipieper on 3/1/16.
 */
public class Migration implements RealmMigration {

    private static Migration instance = new Migration();

    public static Migration getInstance() {
        return instance;
    }

    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {

        RealmSchema schema = realm.getSchema();

        if (oldVersion == 1) {
            createEstatisticas(schema);
            createGuia(schema);
            oldVersion++;
        }
        if (oldVersion == 2) {
            updateCampeonato(schema);
            oldVersion++;
        }
        if (oldVersion == 3) {
            updateCampeonatoVisible(schema);
            oldVersion++;
        }
        if (oldVersion == 4) {
            createVideos(schema);
            oldVersion++;
        }
    }

    private void createVideos(RealmSchema schema) {
        schema.create("Video")
                .addField("id", String.class, FieldAttribute.PRIMARY_KEY, FieldAttribute.REQUIRED)
                .addField("title", String.class)
                .addField("thumb", String.class)
                .addField("description", String.class)
                .addField("dt", Date.class)
                .addField("views", Long.class);
    }

    private void updateCampeonatoVisible(RealmSchema schema) {
        schema.get("Campeonato")
                .addField("visible", Boolean.class);
    }

    private void updateCampeonato(RealmSchema schema) {
        schema.get("Campeonato")
                .addField("idCampeonatoNoticias", Integer.class)
                .addField("nomeCampeonatoApp", String.class)
                .addField("ordem", Long.class);

        schema.get("Equipe")
                .addField("push", Boolean.class)
                .removeField("serie");
    }

    private void createEstatisticas(RealmSchema schema) {
        schema.create("Estatistica")
                .addField("idPartida", Long.class, FieldAttribute.PRIMARY_KEY, FieldAttribute.REQUIRED)
                .addField("cartaoAmareloMandante", Integer.class)
                .addField("cartaoAmareloVisitante", Integer.class)
                .addField("cartaoVermelhoMandante", Integer.class)
                .addField("cartaoVermelhoVisitante", Integer.class)
                .addField("cruzamentoCertoMandante", Integer.class)
                .addField("cruzamentoCertoVisitante", Integer.class)
                .addField("cruzamentoErradoMandante", Integer.class)
                .addField("cruzamentoErradoVisitante", Integer.class)
                .addField("desarmeCertoMandante", Integer.class)
                .addField("desarmeCertoVisitante", Integer.class)
                .addField("desarmeErradoMandante", Integer.class)
                .addField("desarmeErradoVisitante", Integer.class)
                .addField("escanteioContraMandante", Integer.class)
                .addField("escanteioContraVisitante", Integer.class)
                .addField("escanteioProMandante", Integer.class)
                .addField("escanteioProVisitante", Integer.class)
                .addField("faltaCometidaMandante", Integer.class)
                .addField("faltaCometidaVisitante", Integer.class)
                .addField("faltaRecebidaMandante", Integer.class)
                .addField("faltaRecebidaVisitante", Integer.class)
                .addField("finalizacaoCertaMandante", Integer.class)
                .addField("finalizacaoCertaVisitante", Integer.class)
                .addField("finalizacaoErradaMandante", Integer.class)
                .addField("finalizacaoErradaVisitante", Integer.class)
                .addField("lancamentoCertoMandante", Integer.class)
                .addField("lancamentoCertoVisitante", Integer.class)
                .addField("lancamentoErradoMandante", Integer.class)
                .addField("lancamentoErradoVisitante", Integer.class)
                .addField("passeCertoMandante", Integer.class)
                .addField("passeCertoVisitante", Integer.class)
                .addField("passeErradoMandante", Integer.class)
                .addField("passeErradoVisitante", Integer.class)
                .addField("penaltiCometidoMandante", Integer.class)
                .addField("penaltiCometidoVisitante", Integer.class)
                .addField("penaltiRecebidoMandante", Integer.class)
                .addField("penaltiRecebidoVisitante", Integer.class)
                .addField("posseBolaMandante", Double.class)
                .addField("posseBolaVisitante", Double.class)
                .addField("objectId", String.class)
                .addField("createdAt", Date.class)
                .addField("updatedAt", Date.class);
    }

    private void createGuia(RealmSchema schema) {
        schema.create("GeoPoint")
                .addField("latitude", Double.class)
                .addField("longitude", Double.class);

        schema.create("RealmString")
                .addField("value", String.class);

        schema.create("Elenco")
                .addField("local", String.class)
                .addField("nascimento", String.class)
                .addField("nome", String.class)
                .addField("posicao", String.class);

        schema.create("Guia")
                .addField("idEquipe", Long.class, FieldAttribute.PRIMARY_KEY, FieldAttribute.REQUIRED)
                .addField("nome", String.class)
                .addField("urlDistintivo", String.class)
                .addField("capacidadeEstadio", String.class)
                .addField("cidade", String.class)
                .addField("descricaoCidade", String.class)
                .addField("cidadeArea", String.class)
                .addField("cidadeEstadio", String.class)
                .addField("densidadeDemografica", String.class)
                .addField("descricao", String.class)
                .addField("idhm", String.class)
                .addField("inauguracaoEstadio", String.class)
                .addField("mascote", String.class)
                .addField("nomeEstadio", String.class)
                .addField("populacao", String.class)
                .addField("urlCidade", String.class)
                .addField("urlEstadio", String.class)
                .addField("urlMascote", String.class)
                .addField("uniformeFabricante", String.class)
                .addField("idCampeonato", Long.class)
                .addRealmObjectField("coordenada", schema.get("GeoPoint"))
                .addRealmListField("urlUniformes", schema.get("RealmString"))
                .addRealmListField("elenco", schema.get("Elenco"))
                .addField("objectId", String.class)
                .addField("createdAt", Date.class)
                .addField("updatedAt", Date.class);
    }


}

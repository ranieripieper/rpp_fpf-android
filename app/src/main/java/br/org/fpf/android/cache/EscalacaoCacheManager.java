package br.org.fpf.android.cache;

import android.content.Context;

import java.util.List;

import br.org.fpf.android.cache.base.BaseCache;
import br.org.fpf.android.model.Escalacao;
import br.org.fpf.android.model.EscalacaoJogador;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.Sort;

/**
 * Created by ranipieper on 11/19/15.
 */
public class EscalacaoCacheManager extends BaseCache<Escalacao> {

    private static EscalacaoCacheManager instance = new EscalacaoCacheManager();

    String[] DEFAULT_ORDER_FIELDS = {"updatedAt"};
    Sort[] DEFAULT_ORDER_SORT = {Sort.DESCENDING};

    private EscalacaoCacheManager() {
    }

    public static EscalacaoCacheManager getInstance() {
        return instance;
    }

    @Override
    public String getPrimaryKeyName() {
        return "idPartida";
    }

    public Escalacao getEscalacaoByIdPartida(Long idPartida, boolean toUpdate) {
        return getEscalacaoByIdPartida(getRealm(), idPartida, toUpdate);
    }

    public Escalacao getEscalacaoByIdPartida(Long idPartida) {
        return getEscalacaoByIdPartida(getRealm(), idPartida, false);
    }

    public Escalacao getEscalacaoByIdPartida(Context ctx, Long idPartida) {
        return getEscalacaoByIdPartida(getRealm(ctx), idPartida, false);
    }

    public Escalacao getEscalacaoByIdPartida(Context ctx, Long idPartida, boolean toUpdate) {
        return getEscalacaoByIdPartida(getRealm(ctx), idPartida, toUpdate);
    }

    private Escalacao getEscalacaoByIdPartida(Realm realm, Long idPartida, boolean toUpdate) {
        if (!toUpdate) {
            realm.refresh();
        }

        Escalacao escalacao = realm.where(Escalacao.class)
                .equalTo("idPartida", idPartida)
                .findFirst();

        if (toUpdate) {
            return escalacao;
        } else if (escalacao != null) {
            return realm.copyFromRealm(escalacao);
        }
        return null;
        /*
        List<Escalacao> lst = realm.where(Escalacao.class)
                .equalTo("idPartida", idPartida)
                .findAllSorted(DEFAULT_ORDER_FIELDS, DEFAULT_ORDER_SORT);

        if (lst != null && !lst.isEmpty()) {
            if (toUpdate) {
                return lst.get(0);
            } else {
                return getRealm().copyFromRealm(lst.get(0));
            }
        }

        return null;
        */
    }

    public void updateEscalacao(Context ctx, long idPartida, List<EscalacaoJogador> mandante, List<EscalacaoJogador> visitante, boolean isPreparedToView) {
        getRealm(ctx).beginTransaction();
        Escalacao escalacao = getEscalacaoByIdPartida(getRealm(ctx), idPartida, true);
        if (escalacao != null) {
            escalacao.setEscalacaoMandante(getEscalacaoJogadorMandanteToUpdate(ctx, mandante));
            escalacao.setEscalacaoVisitante(getEscalacaoJogadorMandanteToUpdate(ctx, visitante));
            escalacao.setIsPreparedToView(isPreparedToView);
        }
        getRealm(ctx).commitTransaction();
    }

    private RealmList<EscalacaoJogador> getEscalacaoJogadorMandanteToUpdate(Context ctx, List<EscalacaoJogador> lst) {
        RealmList<EscalacaoJogador> escalacaoJogador = new RealmList<>();
        if (lst != null) {
            for (EscalacaoJogador ej : lst) {
                EscalacaoJogador ejRealm = EscalacaoJogadorCacheManager.getInstance().getToUpdate(ctx, ej.getId());
                if (ejRealm != null) {
                    escalacaoJogador.add(ejRealm);
                }
            }
        }

        return escalacaoJogador;
    }

    @Override
    public void put(Escalacao esc) {
        setIdPartida(esc);
        super.put(esc);
    }

    @Override
    public void putAll(List<Escalacao> lst) {
        if (lst != null) {
            for (Escalacao esc : lst) {
                setIdPartida(esc);
            }
            super.putAll(lst);
        }
    }

    public void setIdPartida(Escalacao esc) {
        if (esc != null) {
            if (esc.getEscalacaoMandante() != null) {
                for (EscalacaoJogador ej : esc.getEscalacaoMandante()) {
                    ej.setIdPartida(esc.getIdPartida());
                    ej.setId(esc.getIdPartida() + "_" + ej.getIdJogador());
                }
            }
            if (esc.getEscalacaoVisitante() != null) {
                for (EscalacaoJogador ej : esc.getEscalacaoVisitante()) {
                    ej.setIdPartida(esc.getIdPartida());
                    ej.setId(esc.getIdPartida() + "_" + ej.getIdJogador());
                }
            }
        }
    }

    @Override
    public Class<Escalacao> getReferenceClass() {
        return Escalacao.class;
    }

    @Override
    protected long getCacheLifeTime() {
        return 1000 * 60 * 60 * 24 * 15; // 15 dias
    }
}

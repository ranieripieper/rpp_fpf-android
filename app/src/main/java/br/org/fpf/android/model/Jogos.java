package br.org.fpf.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ranipieper on 3/8/16.
 */
public class Jogos implements Parcelable {

    @Expose
    @SerializedName("Equipe")
    private String nomeEquipe;

    @Expose
    @SerializedName("GolsContra")
    private long golsContra;

    @Expose
    @SerializedName("GolsPro")
    private long golsPro;

    @Expose
    @SerializedName("IdEquipe")
    private long idEquipe;

    private Equipe equipe;
    private boolean mandante;

    public Equipe getEquipe() {
        return equipe;
    }

    public String getNomeEquipe() {
        return nomeEquipe;
    }

    public void setNomeEquipe(String nomeEquipe) {
        this.nomeEquipe = nomeEquipe;
    }

    public Long getGolsContra() {
        return golsContra;
    }

    public void setGolsContra(Long golsContra) {
        this.golsContra = golsContra;
    }

    public Long getGolsPro() {
        return golsPro;
    }

    public void setGolsPro(Long golsPro) {
        this.golsPro = golsPro;
    }

    public long getIdEquipe() {
        return idEquipe;
    }

    public void setIdEquipe(long idEquipe) {
        this.idEquipe = idEquipe;
    }

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }

    public boolean isMandante() {
        return mandante;
    }

    public void setMandante(boolean mandante) {
        this.mandante = mandante;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nomeEquipe);
        dest.writeValue(this.golsContra);
        dest.writeValue(this.golsPro);
        dest.writeLong(this.idEquipe);
        dest.writeByte(mandante ? (byte) 1 : (byte) 0);
    }

    public Jogos() {
    }

    protected Jogos(Parcel in) {
        this.nomeEquipe = in.readString();
        this.golsContra = (Long) in.readValue(Long.class.getClassLoader());
        this.golsPro = (Long) in.readValue(Long.class.getClassLoader());
        this.idEquipe = in.readLong();
        this.mandante = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Jogos> CREATOR = new Parcelable.Creator<Jogos>() {
        @Override
        public Jogos createFromParcel(Parcel source) {
            return new Jogos(source);
        }

        @Override
        public Jogos[] newArray(int size) {
            return new Jogos[size];
        }
    };
}

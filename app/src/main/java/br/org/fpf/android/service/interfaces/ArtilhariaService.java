package br.org.fpf.android.service.interfaces;

import br.org.fpf.android.model.response.ArtilhariaResponse;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by ranipieper on 1/26/16.
 */
public interface ArtilhariaService {

     String DEFAULT_ORDER = "-Total,Jogador";

    @GET("/classes/Artilharia")
    void getArtilharia(@Query("where") String where, @Query("order") String order, @Query("limit") int perPage, @Query("skip") int skip, Callback<ArtilhariaResponse> response);
}

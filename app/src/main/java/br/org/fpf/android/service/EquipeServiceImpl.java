package br.org.fpf.android.service;

import java.util.ArrayList;
import java.util.List;

import br.org.fpf.android.cache.EquipeCacheManager;
import br.org.fpf.android.model.Equipe;
import br.org.fpf.android.model.response.EquipeResponse;
import br.org.fpf.android.util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranieripieper on 1/27/16.
 */
public class EquipeServiceImpl {


    public static void getEquipesToPush(final Callback<List<Equipe>> callback) {
        List<Equipe> equipes = EquipeCacheManager.getInstance().getEquipeToPush();
        if (equipes.isEmpty()) {
                getEquipes(new Callback<List<Equipe>>() {
                    @Override
                    public void success(List<Equipe> equipesServer, Response response) {
                        callback.success(EquipeCacheManager.getInstance().getEquipeToPush(), response);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (callback != null) {
                            callback.failure(error);
                        }
                    }
                });
        } else {
            List<Equipe> result = new ArrayList();
            result.addAll(equipes);
            callback.success(result, null);
        }
    }

    public static void getEquipes(boolean cache, final Callback<List<Equipe>> callback) {
        if (cache) {
            getEquipesInCache(callback);
        } else {
            getEquipes(callback);
        }
    }

    public static void getEquipesInCache(final Callback<List<Equipe>> callback) {
        List<Equipe> equipes = EquipeCacheManager.getInstance().getAll();
        if (equipes.isEmpty()) {
            getEquipes(callback);
        } else {
            callback.success(equipes, null);
        }
    }

    public static void getEquipes(final Callback<List<Equipe>> callback) {

        RetrofitManager.getInstance().getEquipeService().getEquipes("", Constants.REG_POR_PAG_MAX, new Callback<EquipeResponse>() {
            @Override
            public void success(EquipeResponse equipeResponse, Response response) {
                //salva as equipes
                if (!equipeResponse.isEmpty()) {
                    EquipeCacheManager.getInstance().putAll(equipeResponse.getResults());
                }
                if (callback != null) {
                    callback.success(EquipeCacheManager.getInstance().getAll(), response);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.failure(error);
                }
            }
        });
    }
}

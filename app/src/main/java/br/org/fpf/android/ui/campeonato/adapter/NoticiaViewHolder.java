package br.org.fpf.android.ui.campeonato.adapter;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import br.org.fpf.android.FpfApplication;
import br.org.fpf.android.R;
import br.org.fpf.android.model.Noticia;
import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by broto on 10/14/15.
 */
@LayoutId(R.layout.row_noticia)
public class NoticiaViewHolder extends ItemViewHolder<Noticia> {

    @ViewId(R.id.card_view)
    LinearLayout cardView;

    @ViewId(R.id.lbl_title)
    TextView lblTitle;

    @ViewId(R.id.lbl_data)
    TextView lblData;

    @ViewId(R.id.img_photo)
    ImageView imgPhoto;

    public NoticiaViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Noticia item, PositionInfo positionInfo) {
        lblTitle.setText(item.getTitulo());
        lblData.setText(item.getData());
        imgPhoto.setVisibility(View.GONE);
        if (TextUtils.isEmpty(item.getImagem())) {
            imgPhoto.setVisibility(View.GONE);
        } else {
            FpfApplication.imageLoader.loadImage(item.getImagem(), new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    imgPhoto.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    imgPhoto.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    try {
                        imgPhoto.setImageBitmap(loadedImage);
                        imgPhoto.setVisibility(View.VISIBLE);
                    } catch (OutOfMemoryError e) {
                    }
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    imgPhoto.setVisibility(View.GONE);
                }
            });
        }
    }

    @Override
    public void onSetListeners() {
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Noticia item = getItem();
                NoticiaHolderListener listener = getListener(NoticiaHolderListener.class);
                if (listener != null) {
                    listener.onItemSelected(getItem());
                }

            }
        });
    }

    public interface NoticiaHolderListener {
        void onItemSelected(Noticia noticia);
    }
}
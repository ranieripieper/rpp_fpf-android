package br.org.fpf.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ranieripieper on 1/30/16.
 */
public class Escalacao extends RealmObject {

    @Expose
    @PrimaryKey
    @SerializedName("IdPartida")
    private Long idPartida;

    @Expose
    @SerializedName("IdMandante")
    private Long idMandante;

    @Expose
    @SerializedName("Mandante")
    private RealmList<EscalacaoJogador> escalacaoMandante;

    @Expose
    @SerializedName("Visitante")
    private RealmList<EscalacaoJogador> escalacaoVisitante;

    @Expose
    @SerializedName("IdVisitante")
    private Long idVisitante;

    @Expose
    @SerializedName("objectId")
    private String objectId;

    @Expose
    @SerializedName("createdAt")
    private Date createdAt;

    @Expose
    @SerializedName("updatedAt")
    private Date updatedAt;

    private boolean isPreparedToView;

    /**
     * Gets the idPartida
     *
     * @return idPartida
     */
    public Long getIdPartida() {
        return idPartida;
    }

    /**
     * Sets the idPartida
     *
     * @param idPartida
     */
    public void setIdPartida(Long idPartida) {
        this.idPartida = idPartida;
    }

    /**
     * Gets the idMandante
     *
     * @return idMandante
     */
    public Long getIdMandante() {
        return idMandante;
    }

    /**
     * Sets the idMandante
     *
     * @param idMandante
     */
    public void setIdMandante(Long idMandante) {
        this.idMandante = idMandante;
    }

    /**
     * Gets the escalacaoMandante
     *
     * @return escalacaoMandante
     */
    public RealmList<EscalacaoJogador> getEscalacaoMandante() {
        return escalacaoMandante;
    }

    /**
     * Sets the escalacaoMandante
     *
     * @param escalacaoMandante
     */
    public void setEscalacaoMandante(RealmList<EscalacaoJogador> escalacaoMandante) {
        this.escalacaoMandante = escalacaoMandante;
    }

    /**
     * Gets the escalacaoVisitante
     *
     * @return escalacaoVisitante
     */
    public RealmList<EscalacaoJogador> getEscalacaoVisitante() {
        return escalacaoVisitante;
    }

    /**
     * Sets the escalacaoVisitante
     *
     * @param escalacaoVisitante
     */
    public void setEscalacaoVisitante(RealmList<EscalacaoJogador> escalacaoVisitante) {
        this.escalacaoVisitante = escalacaoVisitante;
    }

    /**
     * Gets the idVisitante
     *
     * @return idVisitante
     */
    public Long getIdVisitante() {
        return idVisitante;
    }

    /**
     * Sets the idVisitante
     *
     * @param idVisitante
     */
    public void setIdVisitante(Long idVisitante) {
        this.idVisitante = idVisitante;
    }

    /**
     * Gets the objectId
     *
     * @return objectId
     */
    public String getObjectId() {
        return objectId;
    }

    /**
     * Sets the objectId
     *
     * @param objectId
     */
    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    /**
     * Gets the createdAt
     *
     * @return createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * Sets the createdAt
     *
     * @param createdAt
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Gets the updatedAt
     *
     * @return updatedAt
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * Sets the updatedAt
     *
     * @param updatedAt
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * Gets the isPreparedToView
     *
     * @return isPreparedToView
     */
    public boolean isPreparedToView() {
        return isPreparedToView;
    }

    /**
     * Sets the isPreparedToView
     *
     * @param isPreparedToView
     */
    public void setIsPreparedToView(boolean isPreparedToView) {
        this.isPreparedToView = isPreparedToView;
    }
}

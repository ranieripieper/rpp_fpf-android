package br.org.fpf.android.ui.config.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.org.fpf.android.R;
import br.org.fpf.android.cache.EquipeNotificacaoCacheManager;
import br.org.fpf.android.model.Equipe;
import br.org.fpf.android.model.EquipeNotificacao;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderAdapter;
import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;
import uk.co.ribot.easyadapter.ItemViewHolder;

/**
 * Created by ranieripieper on 1/28/16.
 */
public class NotificacaoAdapter<T> extends EasyLoadMoreRecyclerAdapter<T> implements StickyHeaderAdapter<NotificacaoAdapter.HeaderHolder> {

    @Override
    public NotificacaoAdapter.HeaderHolder onCreateHeaderViewHolder(ViewGroup viewGroup) {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.header_config_notificacao, viewGroup, false);
        return new HeaderHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(NotificacaoAdapter.HeaderHolder headerHolder, int position) {
        long headerId = getHeaderId(position);
        if (headerId == 1) {
            headerHolder.mHeaderTextView.setVisibility(View.VISIBLE);
            headerHolder.mLblXde4.setVisibility(View.VISIBLE);
            long totalTimes =  EquipeNotificacaoCacheManager.getInstance().count();
            headerHolder.mLblXde4.setText(mContext.getString(R.string.txt_x_de_4, totalTimes));
        } else {
            headerHolder.mHeaderTextView.setVisibility(View.INVISIBLE);
            headerHolder.mLblXde4.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public long getHeaderId(int position) {
        Equipe obj = (Equipe) getItem(position);
        EquipeNotificacao equipeNotificacao = EquipeNotificacaoCacheManager.getInstance().get(obj.getId());
        if (equipeNotificacao != null && equipeNotificacao.isSentToParse()) {
            return 1;
        }
        return 2;
    }

    /**
     * Header View Holder.
     */
    protected class HeaderHolder extends RecyclerView.ViewHolder {

        public TextView mHeaderTextView;
        public TextView mLblXde4;

        /**
         * Constructor with the View.
         *
         * @param view view object
         */
        public HeaderHolder(View view) {
            super(view);
            mHeaderTextView = (TextView) view.findViewById(R.id.lbl_header);
            mLblXde4 = (TextView) view.findViewById(R.id.lbl_x_de_4);
        }
    }

    /**
     * Constructs and EasyAdapter with a Context, an {@link ItemViewHolder} class, and list of items.
     *
     * @param context             a valid Context
     * @param itemViewHolderClass your {@link ItemViewHolder} implementation class
     * @param listItems           the list of items to load in the Adapter
     */
    public NotificacaoAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, List<T> listItems, Integer itemLayoutLoadMoreId) {
        super(context, itemViewHolderClass, listItems, itemLayoutLoadMoreId);
    }

    /**
     * Constructs and EasyAdapter with a Context and an {@link ItemViewHolder} class.
     *
     * @param context             a valid Context
     * @param itemViewHolderClass your {@link ItemViewHolder} implementation class
     */
    public NotificacaoAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, Integer itemLayoutLoadMoreId) {
        super(context, itemViewHolderClass, itemLayoutLoadMoreId);
    }

    /**
     * Constructs and EasyAdapter with a Context, an {@link ItemViewHolder} class, list of items and a generic listener.
     *
     * @param context             a valid Context
     * @param itemViewHolderClass your {@link ItemViewHolder} implementation class
     * @param listItems           the list of items to load in the Adapter
     * @param listener            a generic object that you can access from your {@link ItemViewHolder} by calling
     *                            {@link ItemViewHolder#getListener()}, This can be used to pass a listener to the view holder that then you
     *                            can cast and use as a callback.
     */
    public NotificacaoAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, List<T> listItems, Object listener, Integer itemLayoutLoadMoreId) {
        super(context, itemViewHolderClass, listItems, listener, itemLayoutLoadMoreId);
    }

    /**
     * Constructs and EasyAdapter with a Context, an {@link ItemViewHolder} class, list of items and a generic listener.
     *
     * @param context
     * @param itemViewHolderClass
     * @param listItems
     * @param listener
     * @param itemLayoutLoadMoreId
     * @param header
     */
    public NotificacaoAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, List<T> listItems, Object listener, Integer itemLayoutLoadMoreId, View header) {
        super(context, itemViewHolderClass, listItems, listener, itemLayoutLoadMoreId);
    }

    /**
     * Constructs and EasyAdapter with a Context, an {@link ItemViewHolder} class and a generic listener.
     *
     * @param context             a valid Context
     * @param itemViewHolderClass your {@link ItemViewHolder} implementation class
     * @param listener            a generic object that you can access from your {@link ItemViewHolder} by calling
     *                            {@link ItemViewHolder#getListener()}, This can be used to pass a listener to the view holder that then you
     *                            can cast and use as a callback.
     */
    public NotificacaoAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, Object listener, Integer itemLayoutLoadMoreId) {
        super(context, itemViewHolderClass, listener, itemLayoutLoadMoreId);
    }

}

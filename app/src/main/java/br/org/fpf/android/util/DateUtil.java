package br.org.fpf.android.util;

import android.content.Context;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import br.org.fpf.android.R;

/**
 * Created by ranipieper on 1/14/16.
 */
public class DateUtil {

    public static final ThreadLocal<DateFormat> DATE_YEAR_MONTH_DAY = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd");
        }
    };

    public static final ThreadLocal<DateFormat> DATE_DAY_MONTH_YEAR = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("dd/MM/yyyy");
        }
    };

    public static final ThreadLocal<DateFormat> DATE_DAY_MONTH_YEAR_TIME = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("dd/MM/yyyy - HH'h'mm");
        }
    };


    public static final String getDate(Context context, Date date) {
        Calendar actualDate = Calendar.getInstance();

        long diffInMillis = actualDate.getTimeInMillis() - date.getTime();
        long diffInMonths = diffInMillis / 1000 / (86400 * 24);
        long diffInDays = diffInMillis / 1000 / (86400);
        long diffInHours = (diffInMillis / 1000 - 86400 * diffInDays) / 3600;
        long diffInMins = (diffInMillis / 1000 - 86400 * diffInDays - 3600 * diffInHours) / 60;
        long diffInSecs = (diffInMillis / 1000 - 86400 * diffInDays - 3600 * diffInHours - 60 * diffInMins);

        if (diffInMonths <= 0) {
            if (diffInDays <= 0) {
                if (diffInHours <= 0) {
                    if (diffInMins <= 0) {
                        if (diffInSecs == 1) {
                            return context.getString(R.string.um_segundo_atras);
                        } else {
                            return context.getString(R.string.x_segundos_atras, diffInSecs);
                        }
                    } else {
                        if (diffInMins == 1) {
                            return context.getString(R.string.um_minuto_atras);
                        } else {
                            return context.getString(R.string.x_minutos_atras, diffInMins);
                        }
                    }
                } else {
                    if (diffInHours == 1) {
                        return context.getString(R.string.uma_hora);
                    } else {
                        return context.getString(R.string.x_horas, diffInHours);
                    }
                }
            } else {
                if (diffInDays >= 7) {
                    long diffWeeks = diffInDays / 7;
                    if (diffWeeks == 1) {
                        return context.getString(R.string.uma_semana);
                    } else {
                        return context.getString(R.string.x_semanas, diffWeeks);
                    }
                } else {
                    if (diffInDays == 1) {
                        return context.getString(R.string.um_dia);
                    } else {
                        return context.getString(R.string.x_dias, diffInDays);
                    }

                }
            }
        } else {
            if (diffInMonths == 1) {
                return context.getString(R.string.um_mes);
            } else {
                if (diffInMonths >= 12) {
                    long diffInYears = diffInMonths / 12;
                    if (diffInYears == 1) {
                        return context.getString(R.string.um_ano);
                    } else {
                        return context.getString(R.string.x_anos, diffInYears);
                    }
                } else {
                    return context.getString(R.string.x_meses, diffInMonths);
                }
            }

        }

        //return DATE_DAY_MONTH_YEAR.get().format(date);
    }

    public static Date resetDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return resetCalender(calendar).getTime();
    }

    public static Calendar resetCalender(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }
}

package br.org.fpf.android.ui.guia;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import br.org.fpf.android.R;
import br.org.fpf.android.cache.GuiaCacheManager;
import br.org.fpf.android.model.Guia;
import br.org.fpf.android.ui.base.BaseFragment;
import br.org.fpf.android.ui.guia.adapter.ElencoViewHolder;
import br.org.fpf.android.util.Constants;
import uk.co.ribot.easyadapter.EasyRecyclerAdapter;

/**
 * Created by ranipieper on 3/2/16.
 */
@EFragment(R.layout.fragment_elenco)
public class ElencoFragment extends BaseFragment {

    @ViewById(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @FragmentArg(Constants.ARG_ID_GUIA)
    Long mIdGuia;

    @ViewById(R.id.img_uniforme)
    ImageView mImage;

    public static ElencoFragment newInstance(Long idGuia) {
        Bundle args = new Bundle();

        ElencoFragment_ fragment = new ElencoFragment_();
        args.putLong(Constants.ARG_ID_GUIA, idGuia);
        fragment.setArguments(args);

        return fragment;
    }

    @AfterViews
    public void afterViews() {
        super.afterViews();

        Guia guia = GuiaCacheManager.getInstance().get(mIdGuia);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        EasyRecyclerAdapter adapter = new EasyRecyclerAdapter<>(
                getActivity(),
                ElencoViewHolder.class,
                new ArrayList(guia.getElenco()));

        mRecyclerView.setAdapter(adapter);
    }

    @Override
    protected String getToolbarTitle() {
        return "";
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }
}

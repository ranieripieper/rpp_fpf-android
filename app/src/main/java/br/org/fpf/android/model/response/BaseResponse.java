package br.org.fpf.android.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ranipieper on 1/14/16.
 */
public class BaseResponse<T> {


    @Expose
    @SerializedName("results")
    private List<T> results;

    public boolean isEmpty() {
        if (results == null || results.isEmpty()) {
            return true;
        }
        return false;
    }

    public List<T> getResults() {
        return results;
    }

    public void setResults(List<T> results) {
        this.results = results;
    }

}

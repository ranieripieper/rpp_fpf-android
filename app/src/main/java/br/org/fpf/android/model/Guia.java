package br.org.fpf.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import br.org.fpf.android.helper.GuiaHelper;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ranipieper on 3/2/16.
 */
public class Guia extends RealmObject {

    @Expose
    @PrimaryKey
    @SerializedName("IdEquipe")
    private Long idEquipe;

    @Expose
    @SerializedName("Nome")
    private String nome;

    @Expose
    @SerializedName("URLDistintivo")
    private String urlDistintivo;

    @Expose
    @SerializedName("CapacidadeEstadio")
    private String capacidadeEstadio;

    @Expose
    @SerializedName("Cidade")
    private String cidade;

    @Expose
    @SerializedName("DescricaoCidade")
    private String descricaoCidade;

    @Expose
    @SerializedName("CidadeArea")
    private String cidadeArea;

    @Expose
    @SerializedName("CidadeEstadio")
    private String cidadeEstadio;

    @Expose
    @SerializedName("Coordenada")
    private GeoPoint coordenada;

    @Expose
    @SerializedName("DensidadeDemografica")
    private String densidadeDemografica;

    @Expose
    @SerializedName("Descricao")
    private String descricao;

    @Expose
    @SerializedName("Elenco")
    private RealmList<Elenco> elenco;

    @Expose
    @SerializedName("IDHM")
    private String idhm;

    @Expose
    @SerializedName("InauguracaoEstadio")
    private String inauguracaoEstadio;

    @Expose
    @SerializedName("Mascote")
    private String mascote;

    @Expose
    @SerializedName("NomeEstadio")
    private String nomeEstadio;

    @Expose
    @SerializedName("Populacao")
    private String populacao;

    @Expose
    @SerializedName("URLCidade")
    private String urlCidade;

    @Expose
    @SerializedName("URLEstadio")
    private String urlEstadio;

    @Expose
    @SerializedName("URLMascote")
    private String urlMascote;

    @Expose
    @SerializedName("URLUniformes")
    private RealmList<RealmString> urlUniformes;

    @Expose
    @SerializedName("UniformeFabricante")
    private String uniformeFabricante;

    @Expose
    @SerializedName("IdCampeonato")
    private Long idCampeonato;

    @Expose
    @SerializedName("objectId")
    private String objectId;

    @Expose
    @SerializedName("createdAt")
    private Date createdAt;

    @Expose
    @SerializedName("updatedAt")
    private Date updatedAt;

    public Long getIdEquipe() {
        return idEquipe;
    }

    public void setIdEquipe(Long idEquipe) {
        this.idEquipe = idEquipe;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getUrlDistintivo() {
        return GuiaHelper.getUrl(urlDistintivo);
    }

    public void setUrlDistintivo(String urlDistintivo) {
        this.urlDistintivo = urlDistintivo;
    }

    public String getCapacidadeEstadio() {
        return capacidadeEstadio;
    }

    public void setCapacidadeEstadio(String capacidadeEstadio) {
        this.capacidadeEstadio = capacidadeEstadio;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getDescricaoCidade() {
        return descricaoCidade;
    }

    public void setDescricaoCidade(String descricaoCidade) {
        this.descricaoCidade = descricaoCidade;
    }

    public String getCidadeArea() {
        return cidadeArea;
    }

    public void setCidadeArea(String cidadeArea) {
        this.cidadeArea = cidadeArea;
    }

    public String getCidadeEstadio() {
        return cidadeEstadio;
    }

    public void setCidadeEstadio(String cidadeEstadio) {
        this.cidadeEstadio = cidadeEstadio;
    }

    public GeoPoint getCoordenada() {
        return coordenada;
    }

    public void setCoordenada(GeoPoint coordenada) {
        this.coordenada = coordenada;
    }

    public String getDensidadeDemografica() {
        return densidadeDemografica;
    }

    public void setDensidadeDemografica(String densidadeDemografica) {
        this.densidadeDemografica = densidadeDemografica;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public RealmList<Elenco> getElenco() {
        return elenco;
    }

    public void setElenco(RealmList<Elenco> elenco) {
        this.elenco = elenco;
    }

    public String getIdhm() {
        return idhm;
    }

    public void setIdhm(String idhm) {
        this.idhm = idhm;
    }

    public String getInauguracaoEstadio() {
        return inauguracaoEstadio;
    }

    public void setInauguracaoEstadio(String inauguracaoEstadio) {
        this.inauguracaoEstadio = inauguracaoEstadio;
    }

    public String getMascote() {
        return mascote;
    }

    public void setMascote(String mascote) {
        this.mascote = mascote;
    }

    public String getNomeEstadio() {
        return nomeEstadio;
    }

    public void setNomeEstadio(String nomeEstadio) {
        this.nomeEstadio = nomeEstadio;
    }

    public String getPopulacao() {
        return populacao;
    }

    public void setPopulacao(String populacao) {
        this.populacao = populacao;
    }

    public String getUrlCidade() {
        return GuiaHelper.getUrl(urlCidade);
    }

    public void setUrlCidade(String urlCidade) {
        this.urlCidade = urlCidade;
    }

    public String getUrlEstadio() {
        return GuiaHelper.getUrl(urlEstadio);
    }

    public void setUrlEstadio(String urlEstadio) {
        this.urlEstadio = urlEstadio;
    }

    public String getUrlMascote() {
        return GuiaHelper.getUrl(urlMascote);
    }

    public void setUrlMascote(String urlMascote) {
        this.urlMascote = urlMascote;
    }

    public RealmList<RealmString> getUrlUniformes() {
        return urlUniformes;
    }

    public void setUrlUniformes(RealmList<RealmString> urlUniformes) {
        this.urlUniformes = urlUniformes;
    }

    public String getUniformeFabricante() {
        return uniformeFabricante;
    }

    public void setUniformeFabricante(String uniformeFabricante) {
        this.uniformeFabricante = uniformeFabricante;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getIdCampeonato() {
        return idCampeonato;
    }

    public void setIdCampeonato(Long idCampeonato) {
        this.idCampeonato = idCampeonato;
    }
}

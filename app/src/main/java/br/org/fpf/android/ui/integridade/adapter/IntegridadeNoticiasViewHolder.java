package br.org.fpf.android.ui.integridade.adapter;

import android.support.v4.content.res.ResourcesCompat;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.org.fpf.android.R;
import br.org.fpf.android.model.IntegridadeNoticia;
import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by ranipieper on 3/3/16.
 */
@LayoutId(R.layout.row_integridade_noticia)
public class IntegridadeNoticiasViewHolder extends ItemViewHolder<IntegridadeNoticia> {

    @ViewId(R.id.card_view)
    LinearLayout mLayoutRow;

    @ViewId(R.id.lbl_titulo)
    TextView mLblTitulo;

    @ViewId(R.id.lbl_subtitulo)
    TextView mLblSubTitulo;

    public IntegridadeNoticiasViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(IntegridadeNoticia item, PositionInfo positionInfo) {
        mLblTitulo.setText(item.getTitulo());
        mLblSubTitulo.setText(item.getSubtitulo());

        if (positionInfo.getPosition() % 2 == 0) {
            mLayoutRow.setBackgroundDrawable(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.row_2_selector, null));
        } else {
            mLayoutRow.setBackgroundDrawable(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.row_1_selector, null));
        }
    }

    @Override
    public void onSetListeners() {
        mLayoutRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntegridadeNoticiaHolderListener listener = getListener(IntegridadeNoticiaHolderListener.class);
                if (listener != null) {
                    listener.onItemSelected(getItem());
                }

            }
        });
    }

    public interface IntegridadeNoticiaHolderListener {
        void onItemSelected(IntegridadeNoticia noticia);
    }
}
package br.org.fpf.android.service.interfaces;

import br.org.fpf.android.model.response.CampeonatoResponse;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by ranipieper on 1/14/16.
 */
public interface CampeonatoService {

    String TEMPORADA_2016 = "{\"Temporada\":\"2016\"}";

    @GET("/classes/Campeonato")
    void getCampeonatos(@Query("limit") int perPage, Callback<CampeonatoResponse> response);

    @GET("/classes/Campeonato")
    void getCampeonatos(@Query("where") String where, @Query("limit") int perPage, Callback<CampeonatoResponse> response);
}

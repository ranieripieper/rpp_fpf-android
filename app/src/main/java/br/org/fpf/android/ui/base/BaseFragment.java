package br.org.fpf.android.ui.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.org.fpf.android.R;
import retrofit.RetrofitError;
import uk.co.ribot.easyadapter.recyclerview.EasyLoadMoreRecyclerView;

/**
 * Created by ranipieper on 1/14/16.
 */
@EFragment
public abstract class BaseFragment extends Fragment implements ViewDialogInterface {

    private ProgressDialog progress;

    @Nullable
    @ViewById(R.id.loading)
    public ProgressBar loading;

    protected boolean mHasMenu = false;

    public enum TOOLBAR_TYPE {
        DRAWER,
        BACK_BUTTON,
        NOTHING
    }

    public void afterViews() {
        setHasOptionsMenu(hasMenu());
        configToolbar();
        hideKeyboard();
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    public void setHasOptionsMenu(boolean hasMenu) {
        super.setHasOptionsMenu(true);
        mHasMenu = hasMenu;
    }

    public void onResumeFromBackStack() {
        configToolbar();
        hideKeyboard();
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible && isAdded()) {
            configToolbar();
        }
    }

    public void hideKeyboard() {
        if (isAdded() && getView() != null) {
            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }
    }

    public void showLoadingView() {
        hideKeyboard();
        if (loading != null) {
            loading.setVisibility(View.VISIBLE);
        } else if (progress == null || !progress.isShowing()) {
            progress = ProgressDialog.show(getActivity(), getString(R.string.wait),
                    getString(R.string.loading), true);
            hideKeyboard();
        }
    }

    public void hideLoadingView() {
        if (isAdded() && loading != null) {
            loading.setVisibility(View.GONE);
        }
        if (isAdded() && progress != null && progress.isShowing()) {
            progress.dismiss();
            progress = null;
        }
    }

    protected void hideLoadingView(final SwipeRefreshLayout mSwipeRefreshLayout) {
        if (isAdded() && mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    if (isAdded()) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                }
            });
        }
        hideLoadingView();
    }

    protected void showLoadingView(final SwipeRefreshLayout mSwipeRefreshLayout) {

        if (isAdded() && mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
            mSwipeRefreshLayout.setColorSchemeResources(R.color.sunflower);
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            });
        }
    }

    protected void configToolbar() {
        if (needConfigToolbar() && isAdded()) {
            setToolbarTitle(getToolbarTitle());
            if (TOOLBAR_TYPE.BACK_BUTTON.equals(getToolbarType())) {
                setNavBackMode();
            } else if (TOOLBAR_TYPE.DRAWER.equals(getToolbarType())) {
                resetBackMode();
            } else if (TOOLBAR_TYPE.NOTHING.equals(getToolbarType())) {
                hideDrawerAndBackButton();
            }
        }
    }

    protected boolean needConfigToolbar() {
        return true;
    }

    //ViewDialogInterface

    public void showErrorTextView(TextView txtView, int resMessage) {
        if (isAdded()) {
            txtView.setText(resMessage);
            txtView.setVisibility(View.VISIBLE);
        }
        hideLoadingView();
    }

    @Override
    public void showError(RetrofitError error) {
        if (isAdded()) {
            showError(error, R.string.error_generic);
        }
    }

    @Override
    public void showError(RetrofitError error, int defaultError, boolean ignoreUnauthorized) {
        if (isAdded()) {
            hideLoadingView();
            ViewUtil.showError(getActivity(), error, defaultError, ignoreUnauthorized, null);
        }
    }

    @Override
    public void showError(RetrofitError error, int defaultError, ErrorListener errorListener) {
        if (isAdded()) {
            hideLoadingView();
            ViewUtil.showError(getActivity(), error, defaultError, false, errorListener);
        }
    }

    @Override
    public void showError(RetrofitError error, int defaultError) {
        if (isAdded()) {
            hideLoadingView();
            ViewUtil.showError(getActivity(), error, defaultError);
        }
    }

    @Override
    public void showErrorDialog(List<String> lstMessage) {
        if (isAdded()) {
            hideLoadingView();
            ViewUtil.showErrorDialog(getActivity(), lstMessage);
        }
    }

    @Override
    public void showErrorDialog(String message) {
        if (isAdded()) {
            hideLoadingView();
            ViewUtil.showErrorDialog(getActivity(), message);
        }
    }

    @Override
    public void showErrorDialog(int message) {
        if (isAdded()) {
            hideLoadingView();
            showErrorDialog(getString(message));
        }
    }

    @Override
    public void showToastError(Context ctx, String message) {
        if (isAdded()) {
            hideLoadingView();
            ViewUtil.showToastError(ctx, message);
        }
    }

    @Override
    public void showToastError(Context ctx, RetrofitError error) {
        if (isAdded()) {
            hideLoadingView();
            ViewUtil.showToastError(ctx, error);
        }
    }

    @Override
    public void showSuccessDialog(Context ctx, int message) {
        if (isAdded()) {
            showSuccessDialog(ctx, ctx.getString(message));
        }
    }

    @Override
    public void showSuccessDialog(Context ctx, String message) {
        if (isAdded()) {
            ViewUtil.showSuccessDialog(ctx, message);
        }
    }

    @Override
    public void showSuccessDialog(Context ctx, int message, final boolean finish) {
        if (isAdded()) {
            ViewUtil.showSuccessDialog(ctx, message, new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                    if (finish) {
                        finish();
                    }
                }
            });
        }
    }

    @Override
    public void showSuccessDialog(Context ctx, int message, final MaterialDialog.SingleButtonCallback callback) {
        if (isAdded()) {
            ViewUtil.showSuccessDialog(ctx, message, callback);
        }
    }


    /**
     * Call back when user presses back button or back on the navigation.
     */
    protected void onBackPressed() {
        hideKeyboard();
    }

    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.NOTHING;
    }

    protected String getToolbarTitle() {
        return getString(R.string.app_name);
    }

    public void setNavBackMode() {
        if (getActivity() instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) getActivity()).setNavBackMode();
        }
    }

    public void resetBackMode() {
        if (getActivity() instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) getActivity()).resetBackMode();
        }
    }

    public void hideDrawerAndBackButton() {
        if (getActivity() instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) getActivity()).hideDrawerAndBackButton();
        }
    }


    public void setToolbarTitle(String s) {
        if (getActivity() instanceof BaseActivity) {
            ((BaseFragmentActivity) getActivity()).setToolbarTitle(s);
        }
    }

    public void setToolbarTitle(int stringId) {
        if (getActivity() instanceof BaseActivity) {
            ((BaseFragmentActivity) getActivity()).setToolbarTitle(stringId);
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (!mHasMenu) {
            menu.clear();
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    protected void finish() {
        if (getActivity() != null) {
            ((BaseFragmentActivity) getActivity()).onBackPressed();
        }
    }

    protected boolean hasMenu() {
        return false;
    }

    protected void TODO() {
        Toast.makeText(getActivity(), "TODO", Toast.LENGTH_SHORT).show();
    }

    protected void TODO(String msg) {
        Toast.makeText(getActivity(), "TODO " + msg, Toast.LENGTH_SHORT).show();
    }

    protected void disableLoadingMore(EasyLoadMoreRecyclerView easyLoadMoreRecyclerView, int totalItems) {
        ViewUtil.disableLoadingMore(easyLoadMoreRecyclerView, totalItems, isAdded());
    }

    protected void disableLoadingMore(EasyLoadMoreRecyclerView easyLoadMoreRecyclerView) {
        ViewUtil.disableLoadingMore(easyLoadMoreRecyclerView, isAdded());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }

    public void invalidateOptionsMenu() {
        if (getActivity() != null && isAdded()) {
            getActivity().invalidateOptionsMenu();
        }
    }

    public Context getContext() {
        return getActivity();
    }

}

package br.org.fpf.android.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.squareup.okhttp.OkHttpClient;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

import br.org.fpf.android.BuildConfig;
import br.org.fpf.android.model.RealmString;
import br.org.fpf.android.service.interfaces.ArtilhariaService;
import br.org.fpf.android.service.interfaces.CampeonatoService;
import br.org.fpf.android.service.interfaces.ClassificacaoService;
import br.org.fpf.android.service.interfaces.EquipeService;
import br.org.fpf.android.service.interfaces.EscalacaoService;
import br.org.fpf.android.service.interfaces.EstatisticasTimeService;
import br.org.fpf.android.service.interfaces.GuiaService;
import br.org.fpf.android.service.interfaces.NarracaoService;
import br.org.fpf.android.service.interfaces.NoticiaService;
import br.org.fpf.android.service.interfaces.PartidaService;
import br.org.fpf.android.util.DateUtil;
import io.realm.RealmList;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by ranipieper on 1/14/16.
 */
public class RetrofitManager {

    private static final String API_BASE_URL = "https://api.parse.com/1";
    private static final String API_BASE_URL_NOTICIA = "http://www.fpf.org.br";

    public RestAdapter restAdapter;
    public RestAdapter restAdapterNoticia;
    final ConcurrentHashMap<Class, Object> services;
    private static RetrofitManager instance;

    private RetrofitManager() {
        services = new ConcurrentHashMap();
    }

    public static RetrofitManager getInstance() {
        if (instance == null) {
            instance = new RetrofitManager();
        }
        return instance;
    }

    public static Gson getGson() {
        Type token = new TypeToken<RealmList<RealmString>>(){}.getType();
        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .registerTypeAdapter(Date.class, dateDeserializer)
                .registerTypeAdapter(Date.class, dateSerializer)
                .registerTypeAdapter(token, new TypeAdapter<RealmList<RealmString>>() {

                    @Override
                    public void write(JsonWriter out, RealmList<RealmString> value) throws IOException {
                    }

                    @Override
                    public RealmList<RealmString> read(JsonReader in) throws IOException {
                        RealmList<RealmString> list = new RealmList<RealmString>();
                        in.beginArray();
                        while (in.hasNext()) {
                            list.add(new RealmString(in.nextString()));
                        }
                        in.endArray();
                        return list;
                    }
                })
                .create();
        return gson;
    }

    public static Gson getGsonRealm() {
        Type token = new TypeToken<RealmList<RealmString>>(){}.getType();
        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .registerTypeAdapter(Date.class, dateDeserializer)
                .registerTypeAdapter(Date.class, dateSerializer)
                .registerTypeAdapter(token, new TypeAdapter<RealmList<RealmString>>() {

                    @Override
                    public void write(JsonWriter out, RealmList<RealmString> value) throws IOException {
                    }

                    @Override
                    public RealmList<RealmString> read(JsonReader in) throws IOException {
                        RealmList<RealmString> list = new RealmList<RealmString>();
                        in.beginArray();
                        while (in.hasNext()) {
                            list.add(new RealmString(in.nextString()));
                        }
                        in.endArray();
                        return list;
                    }
                })
                .create();
        return gson;
    }

    public void initialize(final String locale) {

        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("X-Parse-Application-Id", BuildConfig.PARSE_APPLICATION_ID);
                request.addHeader("X-Parse-REST-API-Key", BuildConfig.PARSE_REST_KEY);
            }
        };

        restAdapter = new RestAdapter.Builder()
                .setEndpoint(API_BASE_URL)
                .setConverter(new GsonConverter(getGson()))
                .setRequestInterceptor(requestInterceptor)
                .setClient(new OkClient(new OkHttpClient()))
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.FULL).setLog(new RestAdapter.Log() {
                    public void log(String msg) {
                        Log.i("retrofit", msg);
                    }
                }).build();

        restAdapterNoticia = new RestAdapter.Builder()
                .setEndpoint(API_BASE_URL_NOTICIA)
                .setConverter(new GsonConverter(getGson()))
                .setRequestInterceptor(requestInterceptor)
                .setClient(new OkClient(new OkHttpClient()))
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.FULL).setLog(new RestAdapter.Log() {
                    public void log(String msg) {
                        Log.i("retrofit", msg);
                    }
                }).build();
    }

    public RestAdapter getHostAdapter(String baseHost){
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(baseHost)
                .setConverter(new GsonConverter(getGson()))
                .setClient(new OkClient(new OkHttpClient()))
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.FULL).setLog(new RestAdapter.Log() {
                    public void log(String msg) {
                        Log.i("retrofit", msg);
                    }
                }).build();

        return restAdapter;
    }

    //Services

    public EstatisticasTimeService getEstatisticasTimeService() {
        return (EstatisticasTimeService)this.getService(EstatisticasTimeService.class);
    }

    public GuiaService getGuiaService() {
        return (GuiaService)this.getService(GuiaService.class);
    }

    public EquipeService getEquipeService() {
        return (EquipeService)this.getService(EquipeService.class);
    }

    public NarracaoService getNarracaoService() {
        return (NarracaoService)this.getService(NarracaoService.class);
    }

    public EscalacaoService getEscalacaoService() {
        return (EscalacaoService)this.getService(EscalacaoService.class);
    }

    public PartidaService getPartidaService() {
        return (PartidaService)this.getService(PartidaService.class);
    }

    public ClassificacaoService getClassificacaoService() {
        return (ClassificacaoService)this.getService(ClassificacaoService.class);
    }

    public CampeonatoService getCampeonatoService() {
        return (CampeonatoService)this.getService(CampeonatoService.class);
    }

    public ArtilhariaService getArtilhariaService() {
        return (ArtilhariaService)this.getService(ArtilhariaService.class);
    }

    public NoticiaService getNoticiaService() {
        return (NoticiaService)this.getServiceNoticia(NoticiaService.class);
    }

    public NoticiaService getNoticiaService(String url) {
        return getHostAdapter(url).create(NoticiaService.class);
    }

    protected <T> Object getService(Class<T> cls) {
        if(!this.services.contains(cls)) {
            this.services.putIfAbsent(cls, this.restAdapter.create(cls));
        }

        return this.services.get(cls);
    }

    protected <T> Object getServiceNoticia(Class<T> cls) {
        if(!this.services.contains(cls)) {
            this.services.putIfAbsent(cls, this.restAdapterNoticia.create(cls));
        }

        return this.services.get(cls);
    }

    static JsonDeserializer<Date> dateDeserializer = new JsonDeserializer<Date>() {
        @Override
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            if (json != null) {
                try {
                    Date dt = DATE_SERVICE_1.get().parse(json.getAsString());
                    if (dt == null) {
                        return tryOtherFormat(json);
                    }
                    return dt;
                } catch (ParseException e) {
                    return tryOtherFormat(json);
                }
            }

            return null;
        }

        private Date tryOtherFormat(JsonElement json) {
            try {
                return DATE_SERVICE_2.get().parse(json.getAsString());
            } catch (ParseException e1) {
                return tryOtherFormat2(json);
            }
        }

        private Date tryOtherFormat2(JsonElement json) {
            try {
                return DateUtil.DATE_YEAR_MONTH_DAY.get().parse(json.getAsString());
            } catch (ParseException e1) {
            }
            return null;
        }
    };


    static JsonSerializer<Date> dateSerializer = new JsonSerializer<Date>() {
        @Override
        public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext
                context) {
            return src == null ? null : new JsonPrimitive(src.getTime());
        }
    };

    private static final ThreadLocal<DateFormat> DATE_SERVICE_1 = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ") {
                public StringBuffer format(Date date, StringBuffer toAppendTo, java.text.FieldPosition pos) {
                    StringBuffer toFix = super.format(date, toAppendTo, pos);
                    return toFix.insert(toFix.length()-2, ':');
                };

                public Date parse(String text, ParsePosition pos) {
                    int indexOf = text.indexOf(':', text.length() - 4);
                    if (indexOf > 0) {
                        text = text.substring(0, indexOf) + text.substring(indexOf+1, text.length());
                        return super.parse(text, pos);
                    } else {
                        return null;
                    }

                }

            };
        }
    };

    private static final ThreadLocal<DateFormat> DATE_SERVICE_2 = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        }
    };

}

package br.org.fpf.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ranieripieper on 1/27/16.
 */
public class Equipe extends RealmObject {

    @PrimaryKey
    @Expose
    @SerializedName("Id")
    private Long id;

    @Expose
    @SerializedName("Nome")
    private String nome;

    @Expose
    @SerializedName("Sigla")
    private String sigla;

    @Expose
    @SerializedName("TemEstatisticaCompleta")
    private String temEstatisticaCompleta;

    @Expose
    @SerializedName("URLLogo")
    private String urlLogo;

    @Expose
    @SerializedName("objectId")
    private String objectId;

    @Expose
    @SerializedName("createdAt")
    private Date createdAt;

    @Expose
    @SerializedName("updatedAt")
    private Date updatedAt;

    @Expose
    @SerializedName("Push")
    private Boolean push;

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * Gets the id
     *
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the nome
     *
     * @return nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the nome
     *
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Gets the sigla
     *
     * @return sigla
     */
    public String getSigla() {
        return sigla;
    }

    /**
     * Sets the sigla
     *
     * @param sigla
     */
    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    /**
     * Gets the temEstatisticaCompleta
     *
     * @return temEstatisticaCompleta
     */
    public String getTemEstatisticaCompleta() {
        return temEstatisticaCompleta;
    }

    /**
     * Sets the temEstatisticaCompleta
     *
     * @param temEstatisticaCompleta
     */
    public void setTemEstatisticaCompleta(String temEstatisticaCompleta) {
        this.temEstatisticaCompleta = temEstatisticaCompleta;
    }

    /**
     * Gets the urlLogo
     *
     * @return urlLogo
     */
    public String getUrlLogo() {
        return urlLogo;
    }

    /**
     * Sets the urlLogo
     *
     * @param urlLogo
     */
    public void setUrlLogo(String urlLogo) {
        this.urlLogo = urlLogo;
    }

    /**
     * Gets the push
     *
     * @return push
     */
    public Boolean isPush() {
        return push;
    }

    /**
     * Sets the push
     *
     * @param push
     */
    public void setPush(Boolean push) {
        this.push = push;
    }
}

package br.org.fpf.android.helper;

import br.org.fpf.android.model.Partida;
import br.org.fpf.android.util.Constants;

/**
 * Created by ranipieper on 3/3/16.
 */
public class PartidaHelper {

    public static boolean isFinished(Partida partida) {
        if (partida != null && Constants.PERIODO_PARTIDA_ENCERRADA.equalsIgnoreCase(partida.getPeriodoAtual())) {
            return true;
        }
        return false;
    }

    public static boolean isCanceled(Partida partida) {
        if (partida != null && Constants.PERIODO_PARTIDA_CANCELADA.equalsIgnoreCase(partida.getPeriodoAtual())) {
            return true;
        }
        return false;
    }

}

package br.org.fpf.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ranipieper on 3/1/16.
 */
public class Estatistica extends RealmObject{

    @Expose
    @PrimaryKey
    @SerializedName("IdPartida")
    private Long idPartida;

    @Expose
    @SerializedName("CartaoAmareloMandante")
    private Integer cartaoAmareloMandante;

    @Expose
    @SerializedName("CartaoAmareloVisitante")
    private Integer cartaoAmareloVisitante;

    @Expose
    @SerializedName("CartaoVermelhoMandante")
    private Integer cartaoVermelhoMandante;

    @Expose
    @SerializedName("CartaoVermelhoVisitante")
    private Integer cartaoVermelhoVisitante;

    @Expose
    @SerializedName("CruzamentoCertoMandante")
    private Integer cruzamentoCertoMandante;

    @Expose
    @SerializedName("CruzamentoCertoVisitante")
    private Integer cruzamentoCertoVisitante;

    @Expose
    @SerializedName("CruzamentoErradoMandante")
    private Integer cruzamentoErradoMandante;

    @Expose
    @SerializedName("CruzamentoErradoVisitante")
    private Integer cruzamentoErradoVisitante;

    @Expose
    @SerializedName("DesarmeCertoMandante")
    private Integer desarmeCertoMandante;

    @Expose
    @SerializedName("DesarmeCertoVisitante")
    private Integer desarmeCertoVisitante;

    @Expose
    @SerializedName("DesarmeErradoMandante")
    private Integer desarmeErradoMandante;

    @Expose
    @SerializedName("DesarmeErradoVisitante")
    private Integer desarmeErradoVisitante;

    @Expose
    @SerializedName("EscanteioContraMandante")
    private Integer escanteioContraMandante;

    @Expose
    @SerializedName("EscanteioContraVisitante")
    private Integer escanteioContraVisitante;

    @Expose
    @SerializedName("EscanteioProMandante")
    private Integer escanteioProMandante;
    @Expose
    @SerializedName("EscanteioProVisitante")
    private Integer escanteioProVisitante;

    @Expose
    @SerializedName("FaltaCometidaMandante")
    private Integer faltaCometidaMandante;

    @Expose
    @SerializedName("FaltaCometidaVisitante")
    private Integer faltaCometidaVisitante;

    @Expose
    @SerializedName("FaltaRecebidaMandante")
    private Integer faltaRecebidaMandante;

    @Expose
    @SerializedName("FaltaRecebidaVisitante")
    private Integer faltaRecebidaVisitante;

    @Expose
    @SerializedName("FinalizacaoCertaMandante")
    private Integer finalizacaoCertaMandante;

    @Expose
    @SerializedName("FinalizacaoCertaVisitante")
    private Integer finalizacaoCertaVisitante;

    @Expose
    @SerializedName("FinalizacaoErradaMandante")
    private Integer finalizacaoErradaMandante;

    @Expose
    @SerializedName("FinalizacaoErradaVisitante")
    private Integer finalizacaoErradaVisitante;

    @Expose
    @SerializedName("LancamentoCertoMandante")
    private Integer lancamentoCertoMandante;

    @Expose
    @SerializedName("LancamentoCertoVisitante")
    private Integer lancamentoCertoVisitante;

    @Expose
    @SerializedName("LancamentoErradoMandante")
    private Integer lancamentoErradoMandante;

    @Expose
    @SerializedName("LancamentoErradoVisitante")
    private Integer lancamentoErradoVisitante;

    @Expose
    @SerializedName("PasseCertoMandante")
    private Integer passeCertoMandante;

    @Expose
    @SerializedName("PasseCertoVisitante")
    private Integer passeCertoVisitante;

    @Expose
    @SerializedName("PasseErradoMandante")
    private Integer passeErradoMandante;

    @Expose
    @SerializedName("PasseErradoVisitante")
    private Integer passeErradoVisitante;

    @Expose
    @SerializedName("PenaltiCometidoMandante")
    private Integer penaltiCometidoMandante;

    @Expose
    @SerializedName("PenaltiCometidoVisitante")
    private Integer penaltiCometidoVisitante;

    @Expose
    @SerializedName("PenaltiRecebidoMandante")
    private Integer penaltiRecebidoMandante;

    @Expose
    @SerializedName("PenaltiRecebidoVisitante")
    private Integer penaltiRecebidoVisitante;

    @Expose
    @SerializedName("PosseBolaMandante")
    private Double posseBolaMandante;

    @Expose
    @SerializedName("PosseBolaVisitante")
    private Double posseBolaVisitante;

    @Expose
    @SerializedName("objectId")
    private String objectId;

    @Expose
    @SerializedName("createdAt")
    private Date createdAt;

    @Expose
    @SerializedName("updatedAt")
    private Date updatedAt;

    public Long getIdPartida() {
        return idPartida;
    }

    public void setIdPartida(Long idPartida) {
        this.idPartida = idPartida;
    }

    public Integer getCartaoAmareloMandante() {
        return cartaoAmareloMandante;
    }

    public void setCartaoAmareloMandante(Integer cartaoAmareloMandante) {
        this.cartaoAmareloMandante = cartaoAmareloMandante;
    }

    public Integer getCartaoAmareloVisitante() {
        return cartaoAmareloVisitante;
    }

    public void setCartaoAmareloVisitante(Integer cartaoAmareloVisitante) {
        this.cartaoAmareloVisitante = cartaoAmareloVisitante;
    }

    public Integer getCartaoVermelhoMandante() {
        return cartaoVermelhoMandante;
    }

    public void setCartaoVermelhoMandante(Integer cartaoVermelhoMandante) {
        this.cartaoVermelhoMandante = cartaoVermelhoMandante;
    }

    public Integer getCartaoVermelhoVisitante() {
        return cartaoVermelhoVisitante;
    }

    public void setCartaoVermelhoVisitante(Integer cartaoVermelhoVisitante) {
        this.cartaoVermelhoVisitante = cartaoVermelhoVisitante;
    }

    public Integer getCruzamentoCertoMandante() {
        return cruzamentoCertoMandante;
    }

    public void setCruzamentoCertoMandante(Integer cruzamentoCertoMandante) {
        this.cruzamentoCertoMandante = cruzamentoCertoMandante;
    }

    public Integer getCruzamentoCertoVisitante() {
        return cruzamentoCertoVisitante;
    }

    public void setCruzamentoCertoVisitante(Integer cruzamentoCertoVisitante) {
        this.cruzamentoCertoVisitante = cruzamentoCertoVisitante;
    }

    public Integer getCruzamentoErradoMandante() {
        return cruzamentoErradoMandante;
    }

    public void setCruzamentoErradoMandante(Integer cruzamentoErradoMandante) {
        this.cruzamentoErradoMandante = cruzamentoErradoMandante;
    }

    public Integer getCruzamentoErradoVisitante() {
        return cruzamentoErradoVisitante;
    }

    public void setCruzamentoErradoVisitante(Integer cruzamentoErradoVisitante) {
        this.cruzamentoErradoVisitante = cruzamentoErradoVisitante;
    }

    public Integer getDesarmeCertoMandante() {
        return desarmeCertoMandante;
    }

    public void setDesarmeCertoMandante(Integer desarmeCertoMandante) {
        this.desarmeCertoMandante = desarmeCertoMandante;
    }

    public Integer getDesarmeCertoVisitante() {
        return desarmeCertoVisitante;
    }

    public void setDesarmeCertoVisitante(Integer desarmeCertoVisitante) {
        this.desarmeCertoVisitante = desarmeCertoVisitante;
    }

    public Integer getDesarmeErradoMandante() {
        return desarmeErradoMandante;
    }

    public void setDesarmeErradoMandante(Integer desarmeErradoMandante) {
        this.desarmeErradoMandante = desarmeErradoMandante;
    }

    public Integer getDesarmeErradoVisitante() {
        return desarmeErradoVisitante;
    }

    public void setDesarmeErradoVisitante(Integer desarmeErradoVisitante) {
        this.desarmeErradoVisitante = desarmeErradoVisitante;
    }

    public Integer getEscanteioContraMandante() {
        return escanteioContraMandante;
    }

    public void setEscanteioContraMandante(Integer escanteioContraMandante) {
        this.escanteioContraMandante = escanteioContraMandante;
    }

    public Integer getEscanteioContraVisitante() {
        return escanteioContraVisitante;
    }

    public void setEscanteioContraVisitante(Integer escanteioContraVisitante) {
        this.escanteioContraVisitante = escanteioContraVisitante;
    }

    public Integer getEscanteioProMandante() {
        return escanteioProMandante;
    }

    public void setEscanteioProMandante(Integer escanteioProMandante) {
        this.escanteioProMandante = escanteioProMandante;
    }

    public Integer getEscanteioProVisitante() {
        return escanteioProVisitante;
    }

    public void setEscanteioProVisitante(Integer escanteioProVisitante) {
        this.escanteioProVisitante = escanteioProVisitante;
    }

    public Integer getFaltaCometidaMandante() {
        return faltaCometidaMandante;
    }

    public void setFaltaCometidaMandante(Integer faltaCometidaMandante) {
        this.faltaCometidaMandante = faltaCometidaMandante;
    }

    public Integer getFaltaCometidaVisitante() {
        return faltaCometidaVisitante;
    }

    public void setFaltaCometidaVisitante(Integer faltaCometidaVisitante) {
        this.faltaCometidaVisitante = faltaCometidaVisitante;
    }

    public Integer getFaltaRecebidaMandante() {
        return faltaRecebidaMandante;
    }

    public void setFaltaRecebidaMandante(Integer faltaRecebidaMandante) {
        this.faltaRecebidaMandante = faltaRecebidaMandante;
    }

    public Integer getFaltaRecebidaVisitante() {
        return faltaRecebidaVisitante;
    }

    public void setFaltaRecebidaVisitante(Integer faltaRecebidaVisitante) {
        this.faltaRecebidaVisitante = faltaRecebidaVisitante;
    }

    public Integer getFinalizacaoCertaMandante() {
        return finalizacaoCertaMandante;
    }

    public void setFinalizacaoCertaMandante(Integer finalizacaoCertaMandante) {
        this.finalizacaoCertaMandante = finalizacaoCertaMandante;
    }

    public Integer getFinalizacaoCertaVisitante() {
        return finalizacaoCertaVisitante;
    }

    public void setFinalizacaoCertaVisitante(Integer finalizacaoCertaVisitante) {
        this.finalizacaoCertaVisitante = finalizacaoCertaVisitante;
    }

    public Integer getFinalizacaoErradaMandante() {
        return finalizacaoErradaMandante;
    }

    public void setFinalizacaoErradaMandante(Integer finalizacaoErradaMandante) {
        this.finalizacaoErradaMandante = finalizacaoErradaMandante;
    }

    public Integer getFinalizacaoErradaVisitante() {
        return finalizacaoErradaVisitante;
    }

    public void setFinalizacaoErradaVisitante(Integer finalizacaoErradaVisitante) {
        this.finalizacaoErradaVisitante = finalizacaoErradaVisitante;
    }

    public Integer getLancamentoCertoMandante() {
        return lancamentoCertoMandante;
    }

    public void setLancamentoCertoMandante(Integer lancamentoCertoMandante) {
        this.lancamentoCertoMandante = lancamentoCertoMandante;
    }

    public Integer getLancamentoCertoVisitante() {
        return lancamentoCertoVisitante;
    }

    public void setLancamentoCertoVisitante(Integer lancamentoCertoVisitante) {
        this.lancamentoCertoVisitante = lancamentoCertoVisitante;
    }

    public Integer getLancamentoErradoMandante() {
        return lancamentoErradoMandante;
    }

    public void setLancamentoErradoMandante(Integer lancamentoErradoMandante) {
        this.lancamentoErradoMandante = lancamentoErradoMandante;
    }

    public Integer getLancamentoErradoVisitante() {
        return lancamentoErradoVisitante;
    }

    public void setLancamentoErradoVisitante(Integer lancamentoErradoVisitante) {
        this.lancamentoErradoVisitante = lancamentoErradoVisitante;
    }

    public Integer getPasseCertoMandante() {
        return passeCertoMandante;
    }

    public void setPasseCertoMandante(Integer passeCertoMandante) {
        this.passeCertoMandante = passeCertoMandante;
    }

    public Integer getPasseCertoVisitante() {
        return passeCertoVisitante;
    }

    public void setPasseCertoVisitante(Integer passeCertoVisitante) {
        this.passeCertoVisitante = passeCertoVisitante;
    }

    public Integer getPasseErradoMandante() {
        return passeErradoMandante;
    }

    public void setPasseErradoMandante(Integer passeErradoMandante) {
        this.passeErradoMandante = passeErradoMandante;
    }

    public Integer getPasseErradoVisitante() {
        return passeErradoVisitante;
    }

    public void setPasseErradoVisitante(Integer passeErradoVisitante) {
        this.passeErradoVisitante = passeErradoVisitante;
    }

    public Integer getPenaltiCometidoMandante() {
        return penaltiCometidoMandante;
    }

    public void setPenaltiCometidoMandante(Integer penaltiCometidoMandante) {
        this.penaltiCometidoMandante = penaltiCometidoMandante;
    }

    public Integer getPenaltiCometidoVisitante() {
        return penaltiCometidoVisitante;
    }

    public void setPenaltiCometidoVisitante(Integer penaltiCometidoVisitante) {
        this.penaltiCometidoVisitante = penaltiCometidoVisitante;
    }

    public Integer getPenaltiRecebidoMandante() {
        return penaltiRecebidoMandante;
    }

    public void setPenaltiRecebidoMandante(Integer penaltiRecebidoMandante) {
        this.penaltiRecebidoMandante = penaltiRecebidoMandante;
    }

    public Integer getPenaltiRecebidoVisitante() {
        return penaltiRecebidoVisitante;
    }

    public void setPenaltiRecebidoVisitante(Integer penaltiRecebidoVisitante) {
        this.penaltiRecebidoVisitante = penaltiRecebidoVisitante;
    }

    public Double getPosseBolaMandante() {
        return posseBolaMandante;
    }

    public void setPosseBolaMandante(Double posseBolaMandante) {
        this.posseBolaMandante = posseBolaMandante;
    }

    public Double getPosseBolaVisitante() {
        return posseBolaVisitante;
    }

    public void setPosseBolaVisitante(Double posseBolaVisitante) {
        this.posseBolaVisitante = posseBolaVisitante;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}

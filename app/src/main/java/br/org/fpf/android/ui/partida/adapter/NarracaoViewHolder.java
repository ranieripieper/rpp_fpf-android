package br.org.fpf.android.ui.partida.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.org.fpf.android.R;
import br.org.fpf.android.model.Narracao;
import br.org.fpf.android.util.Constants;
import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by broto on 10/14/15.
 */
@LayoutId(R.layout.row_narracao)
public class NarracaoViewHolder extends ItemViewHolder<Narracao> {

    @ViewId(R.id.lbl_descricao)
    TextView lblNarracao;

    @ViewId(R.id.lbl_momento)
    TextView lblMomento;

    @ViewId(R.id.img_icon)
    ImageView imgIcon;

    public NarracaoViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Narracao item, PositionInfo positionInfo) {
        lblNarracao.setText(item.getDescricao());
        lblMomento.setText(getMomento(item));
        Integer icon = getIcon(item);
        if (icon == null) {
            imgIcon.setVisibility(View.INVISIBLE);
        } else {
            imgIcon.setImageResource(icon);
            imgIcon.setVisibility(View.VISIBLE);
        }
    }

    private Integer getIcon(Narracao item) {
        if (Constants.NARRACAO_CARTAO_AMARELO.equalsIgnoreCase(item.getAcaoImportante())) {
            return R.drawable.icn_cartao_amarelo;
        } else if (Constants.NARRACAO_CARTAO_VERMELHO.equalsIgnoreCase(item.getAcaoImportante())) {
            return R.drawable.icn_cartao_vermelho;
        } if (Constants.NARRACAO_SUBSTITUICAO.equalsIgnoreCase(item.getAcaoImportante())) {
            return R.drawable.icn_substituicao;
        } if (Constants.NARRACAO_GOL.equalsIgnoreCase(item.getAcaoImportante())) {
            return R.drawable.icn_gol;
        }
        return null;
    }

    private String getMomento(Narracao item) {
        if (TextUtils.isEmpty(item.getMomento())) {
            return "";
        } else {
            return getContext().getString(R.string.min_x, item.getMomento().split(":")[0]);
        }
    }

    @Override
    public void onSetListeners() {
    }
}
package br.org.fpf.android.ui.campeonato;

import android.os.Bundle;
import android.view.View;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;

import java.util.ArrayList;
import java.util.List;

import br.org.fpf.android.BuildConfig;
import br.org.fpf.android.R;
import br.org.fpf.android.model.Noticia;
import br.org.fpf.android.service.RetrofitManager;
import br.org.fpf.android.ui.campeonato.adapter.NoticiaViewHolder;
import br.org.fpf.android.ui.navigation.Navigator;
import br.org.fpf.android.util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;

/**
 * Created by ranipieper on 1/26/16.
 */
@EFragment(R.layout.fragment_campeonato_pages)
public class NoticiasFragment extends CampeonatoPageBaseFragment implements NoticiaViewHolder.NoticiaHolderListener {

    private List<Noticia> mNoticias = new ArrayList<>();

    public static NoticiasFragment newInstance(Long idCampeonatoNoticia) {
        Bundle args = new Bundle();

        NoticiasFragment_ fragment = new NoticiasFragment_();
        args.putLong(Constants.ARG_ID_CAMPEONATO, idCampeonatoNoticia);

        fragment.setArguments(args);

        return fragment;
    }

    @AfterViews
    public void afterViews() {
        super.afterViews();
    }

    @Override
    protected void preLoadData() {
        if (mPage == 0) {
            mNoticias = new ArrayList<>();
            if (mAdapter != null) {
                mAdapter.removeAllItems();
            }
        }
    }

    protected void callService() {
        if (mPage != 0 && !forceUpdate && !mNoticias.isEmpty()) {
            processResponse(mNoticias);
            hideLoadingView(mSwipeRefreshLayout);
        } else {

            RetrofitManager.getInstance().getNoticiaService().getNoticias(mIdCampeonato, new Callback<List<Noticia>>() {
                @Override
                public void success(List<Noticia> noticias, Response response) {
                    if (isAdded()) {
                        if (noticias != null && !noticias.isEmpty()) {
                            mNoticias.addAll(noticias);
                            processResponse(noticias);
                            forceUpdate = false;
                        } else {
                            processResponse(null);
                        }

                        hideLoadingView(mSwipeRefreshLayout);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (BuildConfig.DEBUG) {
                        error.printStackTrace();
                    }
                    showError();
                }
            });
        }
    }

    protected void processResponse(List<Noticia> noticiaList) {
        hideLoadingView();
        if (!isAdded()) {
            return;
        }
        if ((noticiaList == null || noticiaList.isEmpty()) && mPage == 0) {
            showErrorTextView(mTxtErrorNoResult, R.string.msg_noticias_vazia);
        } else if (noticiaList != null) {
            disableLoadingMore(mRecyclerView, noticiaList.size());
            renderNoticias(noticiaList);
            mRecyclerView.setVisibility(View.VISIBLE);
        }

    }

    public void renderNoticias(List<Noticia> noticiaList) {
        if (!isAdded()) {
            return;
        }
        if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }

        if (mAdapter == null) {
            createAdapter(new ArrayList(noticiaList));
        } else {
            mAdapter.addItems(noticiaList);
        }

        if (mRecyclerView.getAdapter() == null) {
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    protected void createAdapter(List<Noticia> noticiaList) {
        mAdapter = new EasyLoadMoreRecyclerAdapter(
                getActivity(),
                NoticiaViewHolder.class,
                noticiaList,
                this,
                R.layout.loading_recycler_view);
    }

    @Override
    public void onItemSelected(Noticia noticia) {
        Navigator.navigateToVisualizarNoticiaFragment(getContext(), noticia);
    }
}


package br.org.fpf.android.service.interfaces;

import br.org.fpf.android.model.response.EstatisticasTimeResponse;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by ranipieper on 3/8/16.
 */
public interface EstatisticasTimeService {

    @GET("/classes/EstatisticasDoTime")
    void getEstatisticas(@Query("where") String where, Callback<EstatisticasTimeResponse> response);
}

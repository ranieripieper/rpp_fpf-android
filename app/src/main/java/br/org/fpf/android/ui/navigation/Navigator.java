package br.org.fpf.android.ui.navigation;

import android.content.Context;

import br.org.fpf.android.model.Noticia;
import br.org.fpf.android.model.PushNotificationMessage;
import br.org.fpf.android.ui.base.BaseFragmentActivity;
import br.org.fpf.android.ui.campeonato.CampeonatoFragment;
import br.org.fpf.android.ui.campeonato.VisualizarNoticiaFragment;
import br.org.fpf.android.ui.config.AboutFragment;
import br.org.fpf.android.ui.config.NotificationsFragment;
import br.org.fpf.android.ui.estatisticas.EstatisticasTimeFragment;
import br.org.fpf.android.ui.fpftv.FpfTVFragment;
import br.org.fpf.android.ui.guia.ElencoFragment;
import br.org.fpf.android.ui.guia.GuiaHomeFragment;
import br.org.fpf.android.ui.guia.GuiaTimeFragment;
import br.org.fpf.android.ui.home.MainActivity;
import br.org.fpf.android.ui.integridade.IntegridadeDicasFragment;
import br.org.fpf.android.ui.integridade.IntegridadeFaqFragment;
import br.org.fpf.android.ui.integridade.IntegridadeHomeFragment;
import br.org.fpf.android.ui.integridade.IntegridadeNoticiasFragment;
import br.org.fpf.android.ui.partida.PartidaFragment;
import br.org.fpf.android.util.SharedPrefManager;

/**
 * Created by broto on 9/11/15.
 */
public class Navigator {

    //Activities
    public static void navigateToMainActivity(Context context) {
        MainActivity.startActivity(context);
    }

    //Fragments
    public static void navigateToAboutFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(AboutFragment.newInstance(), true);
        }
    }

    public static void navigateToPartidaFragment(Context context, Long idCampeonato, Long idPartida) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(PartidaFragment.newInstance(idCampeonato, idPartida));
        }
    }

    public static void navigateToCampeonatoFragment(Context context, Long idCampeonato) {
        if (context != null) {
            SharedPrefManager.getInstance().setLastCampeonatoSelected(idCampeonato);
            ((BaseFragmentActivity) context).addFragment(CampeonatoFragment.newInstance(idCampeonato), true);
        }
    }

    public static void navigateToCampeonatoFragment(Context context, Long idCampeonato, PushNotificationMessage pushMsg) {
        if (context != null) {
            if (pushMsg != null && pushMsg.getIdPartida() > 0) {
                ((BaseFragmentActivity) context).addFragment(CampeonatoFragment.newInstance(idCampeonato), PartidaFragment.newInstance(pushMsg.getIdPartida()), true);
            } else {
                ((BaseFragmentActivity) context).addFragment(CampeonatoFragment.newInstance(idCampeonato), true);
            }
            /*
            if (context instanceof MainActivity) {
                if (Constants.ID_A1.equals(idCampeonato)) {
                    ((MainActivity) context).menuSerieA1Click();
                } else if (Constants.ID_A2.equals(idCampeonato)) {
                    ((MainActivity) context).menuSerieA2Click();
                } else if (Constants.ID_A3.equals(idCampeonato)) {
                    ((MainActivity) context).menuSerieA3Click();
                }

            }
            */
        }
    }

    public static void navigateToNotificationsFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(NotificationsFragment.newInstance(), true);
        }
    }

    //Guia
    public static void navigateToGuiaHomeFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(GuiaHomeFragment.newInstance(), true);
        }
    }

    public static void navigateToGuiaTimeFragment(Context context, Long idGuia) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(GuiaTimeFragment.newInstance(idGuia));
        }
    }

    public static void navigateToElencoFragment(Context context, Long idGuia) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(ElencoFragment.newInstance(idGuia));
        }
    }

    public static void navigateToEstatisticasTimeFragment(Context context, Long idTime) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(EstatisticasTimeFragment.newInstance(idTime));
        }
    }

    //Noticia
    public static void navigateToVisualizarNoticiaFragment(Context context, Noticia noticia) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(VisualizarNoticiaFragment.newInstance(noticia));
        }
    }

    //Integridade
    public static void navigateToIntegridadeHomeFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(IntegridadeHomeFragment.newInstance(), true);
        }
    }

    public static void navigateToIntegridadeDicasFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(IntegridadeDicasFragment.newInstance());
        }
    }

    public static void navigateToIntegridadeFaqFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(IntegridadeFaqFragment.newInstance());
        }
    }

    public static void navigateToIntegridadeNoticiasFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(IntegridadeNoticiasFragment.newInstance());
        }
    }

    public static void navigateToFpfTVFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(FpfTVFragment.newInstance(), true);
        }
    }


}

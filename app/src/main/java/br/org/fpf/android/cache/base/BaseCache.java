package br.org.fpf.android.cache.base;


import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by broto on 11/3/15.
 */
public abstract class BaseCache<T extends RealmObject> implements Cache<T>{

    public BaseCache() {
    }

    public void updateWithoutNulls(List<T> lst) {
        RealmManager.getInstance().updateWithoutNulls(lst, getReferenceClass());
    }

    public void updateWithoutNulls(Context context, List<T> lst) {
        RealmManager.getInstance().updateWithoutNulls(lst, getReferenceClass());
    }

    @Override
    public void put(T t) {
        RealmManager.getInstance().put(t);
    }

    @Override
    public void put(Context ctx, T t) {
        RealmManager.getInstance(ctx).put(t);
    }

    @Override
    public void putAll(List<T> lst) {
        RealmManager.getInstance().putAll((List<RealmObject>) lst);
    }

    public void putAll(Context ctx, List<T> lst) {
        RealmManager.getInstance(ctx).putAll((List<RealmObject>) lst);
    }

    @Override
    public void delete(final String id) {
        RealmManager.getInstance().delete(getPrimaryKeyName(), id, getReferenceClass());
    }

    @Override
    public void delete(Context ctx, final Long id) {
        RealmManager.getInstance(ctx).delete(getPrimaryKeyName(), id, getReferenceClass());
    }

    @Override
    public void delete(final Long id) {
        RealmManager.getInstance().delete(getPrimaryKeyName(), id, getReferenceClass());
    }

    @Override
    public void deleteAll() {
        RealmManager.getInstance().deleteAll(getReferenceClass());
        getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                CacheExpire sync = getRealm().where(CacheExpire.class).equalTo("className", getReferenceClass().getName()).findFirst();
                if (sync != null) {
                    sync.removeFromRealm();
                }
            }
        });
    }

    @Override
    public String getPrimaryKeyName() {
        return "id";
    }

    @Override
    public T get(Long id) {
        T result = getRealm().where(getReferenceClass()).equalTo(getPrimaryKeyName(), id).findFirst();
        if (result != null) {
            return getRealm().copyFromRealm(result);
        } else {
            return null;
        }

    }

    @Override
    public T get(Context context, Long id) {
        getRealm(context).refresh();
        T result = getRealm(context).where(getReferenceClass()).equalTo(getPrimaryKeyName(), id).findFirst();

        if (result != null) {
            return getRealm(context).copyFromRealm(result);
        } else {
            return null;
        }
    }

    @Override
    public T get(Context context, String id) {
        getRealm(context).refresh();
        T result = getRealm(context).where(getReferenceClass()).equalTo(getPrimaryKeyName(), id).findFirst();
        if (result != null) {
            return getRealm(context).copyFromRealm(result);
        } else {
            return null;
        }
    }

    @Override
    public T get(String id) {
        T result = getRealm().where(getReferenceClass()).equalTo(getPrimaryKeyName(), id).findFirst();
        if (result != null) {
            return getRealm().copyFromRealm(result);
        } else {
            return null;
        }
    }

    @Override
    public long count() {
        return RealmManager.getInstance().count(getReferenceClass());
    }

    @Override
    public List<T> getAll() {
        RealmResults<T> results = getRealm().where(getReferenceClass()).findAll();
        if (results != null && !results.isEmpty()) {
            return getRealm().copyFromRealm(results);
        } else {
            return new ArrayList<>();
        }
    }

    public List<T> getAll(Context ctx) {
        getRealm(ctx).refresh();
        RealmResults<T> results = getRealm(ctx).where(getReferenceClass()).findAll();
        if (results != null && !results.isEmpty()) {
            return getRealm(ctx).copyFromRealm(results);
        } else {
            return new ArrayList<>();
        }
    }

    public T getToUpdate(Long id) {
        return getRealm().where(getReferenceClass()).equalTo(getPrimaryKeyName(), id).findFirst();
    }

    public T getToUpdate(String id) {
        return getRealm().where(getReferenceClass()).equalTo(getPrimaryKeyName(), id).findFirst();
    }

    public T getToUpdate(Context ctx, Long id) {
        return getRealm(ctx).where(getReferenceClass()).equalTo(getPrimaryKeyName(), id).findFirst();
    }

    public T getToUpdate(Context ctx, String id) {
        return getRealm(ctx).where(getReferenceClass()).equalTo(getPrimaryKeyName(), id).findFirst();
    }

    protected long getCacheLifeTime() {
        return CacheExpire.DEFAULT_CACHE_TIME_LIMIT;
    }

    public Realm getRealm() {
        return RealmManager.getInstance().getRealm();
    }

    public Realm getRealm(Context context) {
        return RealmManager.getInstance(context).getRealm();
    }

    public abstract Class<T> getReferenceClass();
}

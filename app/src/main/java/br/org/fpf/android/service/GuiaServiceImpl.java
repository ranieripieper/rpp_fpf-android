package br.org.fpf.android.service;

import java.util.ArrayList;
import java.util.List;

import br.org.fpf.android.cache.GuiaCacheManager;
import br.org.fpf.android.model.Guia;
import br.org.fpf.android.model.response.GuiaResponse;
import br.org.fpf.android.util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranieripieper on 1/27/16.
 */
public class GuiaServiceImpl {

    public static void getGuias(final Callback<List<Guia>> callback) {

        RetrofitManager.getInstance().getGuiaService().getGuias(Constants.REG_POR_PAG_MAX, new Callback<GuiaResponse>() {
            @Override
            public void success(GuiaResponse guiaResponse, Response response) {

                if (!guiaResponse.isEmpty()) {
                    GuiaCacheManager.getInstance().putAll(guiaResponse.getResults());
                    GuiaCacheManager.getInstance().getRealm().refresh();
                }
                if (callback != null) {
                    List<Guia> result = new ArrayList();
                    result.addAll(GuiaCacheManager.getInstance().getAll());
                    callback.success(result, response);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    List<Guia> guias = GuiaCacheManager.getInstance().getAll();
                    if (guias == null || guias.isEmpty()) {
                        callback.failure(error);
                    } else {
                        List<Guia> result = new ArrayList();
                        result.addAll(guias);
                        callback.success(result, null);
                    }
                }
            }
        });
    }

    public static void getGuia(final Long idEquipe, final Callback<Guia> callback) {
        Guia guia = GuiaCacheManager.getInstance().get(idEquipe);
        if (guia != null) {
            if (callback != null) {
                callback.success(guia, null);
            }
        } else {
            String where = String.format("{\"IdEquipe\":%d}", idEquipe);
            RetrofitManager.getInstance().getGuiaService().getGuias(where, new Callback<GuiaResponse>() {
                @Override
                public void success(GuiaResponse guiaResponse, Response response) {
                    if (!guiaResponse.isEmpty()) {
                        GuiaCacheManager.getInstance().putAll(guiaResponse.getResults());
                        GuiaCacheManager.getInstance().getRealm().refresh();
                    }
                    if (callback != null) {
                        callback.success(GuiaCacheManager.getInstance().get(idEquipe), response);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (callback != null) {
                        callback.failure(error);
                    }
                }
            });
        }
    }
}

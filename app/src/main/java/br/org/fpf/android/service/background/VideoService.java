package br.org.fpf.android.service.background;

import android.content.Context;
import android.text.TextUtils;

import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import com.google.api.services.youtube.model.VideoListResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.org.fpf.android.BuildConfig;
import br.org.fpf.android.FpfApplication;
import br.org.fpf.android.cache.VideoCacheManager;
import br.org.fpf.android.helper.VideoHelper;
import br.org.fpf.android.model.Video;
import br.org.fpf.android.util.Constants;

/**
 * Created by ranipieper on 8/30/16.
 */
public class VideoService {

    public static void updateListVideos() {
        updateListVideos(null);
    }

    private static void updateListVideos(String pageToken) {
        try {
            YouTube.Search.List searchService = FpfApplication.getYoutubeApi()
                    .search()
                    .list("id")
                    .setFields("items,nextPageToken")
                    .setChannelId(BuildConfig.CHANNEL_ID)
                    .setPart("snippet,id")
                    .setOrder("date")
                    .setMaxResults(50l)
                    .setType("video");

            if (!TextUtils.isEmpty(pageToken)) {
                searchService.setPageToken(pageToken);
            }

            searchService.setKey(BuildConfig.YOUTUBE_API_KEY);

            SearchListResponse searchResponse = searchService.execute();
            List<SearchResult> searchResultList = searchResponse.getItems();

            List<Video> videos = VideoHelper.transformVideo(searchResultList);

            VideoCacheManager.getInstance().updateWithoutNulls(videos);

            if (!TextUtils.isEmpty(searchResponse.getNextPageToken())) {
                updateListVideos(searchResponse.getNextPageToken());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<String> getListVideosIds(List<Video> lstVideos) {
        List<String> lstVideosIds = new ArrayList<>();

        if (lstVideos != null && !lstVideos.isEmpty()) {

            String videosIds = "";

            for (int item = 0; item < lstVideos.size(); item++) {
                Video video = lstVideos.get(item);
                videosIds += String.format("%s,", video.getId());
                if (item != 0
                        && ((item + 1) % Constants.YOUTUBE_MAX_RESULTS == 0)
                        && item + 1 < lstVideos.size()) {
                    videosIds = videosIds.substring(0, videosIds.length() - 1);
                    lstVideosIds.add(videosIds);
                    videosIds = "";
                }
            }
            videosIds = videosIds.substring(0, videosIds.length() - 1);
            lstVideosIds.add(videosIds);
        }

        return lstVideosIds;
    }

    public static void updateVideos(Context context, List<String> lstVideosIds) {
        try {

            if (lstVideosIds == null || lstVideosIds.isEmpty()) {
                return;
            } else {

                YouTube.Videos.List videoService = FpfApplication.getYoutubeApi()
                        .videos()
                        .list("id,snippet,contentDetails,statistics")
                        .setMaxResults(Constants.YOUTUBE_MAX_RESULTS)
                        .setFields("items(id,statistics(viewCount))")
                        .setMaxResults(Constants.YOUTUBE_MAX_RESULTS)
                        .setKey(BuildConfig.YOUTUBE_API_KEY);

                for (String nextIds : lstVideosIds) {
                    videoService.setId(nextIds);
                    VideoListResponse videos = videoService.execute();

                    for (com.google.api.services.youtube.model.Video youtubeVideo : videos.getItems()) {
                        Video video = VideoCacheManager.getInstance().get(context, youtubeVideo.getId());
                        if (video == null) {
                            continue;
                        }

                        if (youtubeVideo.getStatistics() != null && youtubeVideo.getStatistics().getViewCount() != null) {
                            video.setViews(youtubeVideo.getStatistics().getViewCount().longValue());
                        }

                        VideoCacheManager.getInstance().updateWithoutNulls(context, Arrays.asList(video));
                    }
                }

            }

        } catch (Exception e) {
        }

        return;
    }

}

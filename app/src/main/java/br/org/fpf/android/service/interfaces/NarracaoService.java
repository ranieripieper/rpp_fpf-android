package br.org.fpf.android.service.interfaces;

import br.org.fpf.android.model.response.NarracaoResponse;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by ranieripieper on 1/30/16.
 */
public interface NarracaoService {
    @GET("/classes/Narracao")
    void getNarracao(@Query("where") String where, @Query("limit") int perPage, @Query("skip") int skip, Callback<NarracaoResponse> response);
}

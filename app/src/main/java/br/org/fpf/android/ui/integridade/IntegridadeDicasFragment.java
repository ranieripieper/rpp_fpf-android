package br.org.fpf.android.ui.integridade;

import android.os.Bundle;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;

import br.org.fpf.android.R;
import br.org.fpf.android.ui.base.BaseFragment;

/**
 * Created by ranipieper on 8/29/16.
 */
@EFragment(R.layout.fragment_fpf_integridade_dicas)
public class IntegridadeDicasFragment extends BaseFragment {

    public static IntegridadeDicasFragment newInstance() {
        Bundle args = new Bundle();

        IntegridadeDicasFragment fragment = new IntegridadeDicasFragment_();
        fragment.setArguments(args);

        return fragment;
    }


    @AfterViews
    public void afterViews() {
        super.afterViews();
    }

    @Override
    protected String getToolbarTitle() {
        return "";
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }
}

package br.org.fpf.android.ui.guia.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.org.fpf.android.FpfApplication;
import br.org.fpf.android.R;
import br.org.fpf.android.model.Guia;
import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by broto on 10/14/15.
 */
@LayoutId(R.layout.row_guia_home)
public class GuiaHomeViewHolder extends ItemViewHolder<Guia> {

    @ViewId(R.id.ripple)
    View layoutRow;

    @ViewId(R.id.lbl_time)
    TextView lblTime;

    @ViewId(R.id.img_time)
    ImageView imgTime;

    public GuiaHomeViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Guia item, PositionInfo positionInfo) {
        lblTime.setText(item.getNome());
        /*
        if (positionInfo.getPosition() %2 == 0) {
            cardView.setBackgroundColor(getContext().getResources().getColor(R.color.row_1));
        } else {
            cardView.setBackgroundColor(getContext().getResources().getColor(R.color.row_2));
        }*/
        FpfApplication.imageLoader.displayImage(item.getUrlDistintivo(), imgTime);
    }

    @Override
    public void onSetListeners() {
        layoutRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuiaHomeListener listener = getListener(GuiaHomeListener.class);
                if (listener != null) {
                    listener.onItemSelected(getItem());
                }

            }
        });
    }

    public interface GuiaHomeListener {
        void onItemSelected(Guia guia);
    }
}
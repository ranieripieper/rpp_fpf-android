package br.org.fpf.android.ui.partida.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.org.fpf.android.R;
import br.org.fpf.android.model.EscalacaoJogador;
import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by broto on 10/14/15.
 */
@LayoutId(R.layout.row_escalacao)
public class EscalacaoViewHolder extends ItemViewHolder<EscalacaoJogador> {

    @ViewId(R.id.lbl_jogador)
    TextView mLblJogador;

    @ViewId(R.id.lbl_posicao)
    TextView mLblPosicao;

    @ViewId(R.id.lbl_gols)
    TextView mLblGols;

    @ViewId(R.id.ic_subst)
    ImageView mImgSubst;

    @ViewId(R.id.ic_gol)
    ImageView mImgGol;

    @ViewId(R.id.ic_cartao_amarelo)
    View mViewCartaoAmarelo;

    @ViewId(R.id.ic_cartao_vermelho)
    View mViewCartaoVermelho;

    @ViewId(R.id.layout_content)
    View layoutContent;

    @ViewId(R.id.layout_content_bg)
    View layoutContentBg;

    public EscalacaoViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(EscalacaoJogador item, PositionInfo positionInfo) {
        mLblJogador.setText(item.getNome());
        int position = positionInfo.getPosition();

        int marginR = getContext().getResources().getDimensionPixelSize(R.dimen.margin_10);
        int marginL = getContext().getResources().getDimensionPixelSize(R.dimen.margin_10);

        if (position % 4 < 2) {
            if (position % 2 == 0) {
                marginR = 0;
                layoutContentBg.setBackgroundResource(R.drawable.bg_row1);
            } else {
                marginL = 0;
                layoutContentBg.setBackgroundResource(R.drawable.bg_stroke_left_row1);
            }
        } else {
            if (position % 2 == 0) {
                marginR = 0;
                layoutContentBg.setBackgroundResource(R.drawable.bg_row2);
            } else {
                marginL = 0;
                layoutContentBg.setBackgroundResource(R.drawable.bg_stroke_left_row2);
            }
        }
        if (TextUtils.isEmpty(item.getId())) {
            layoutContent.setVisibility(View.INVISIBLE);
        } else {
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)layoutContentBg.getLayoutParams();
            params.setMargins(marginL, 0, marginR, 0);
            layoutContentBg.setLayoutParams(params);
            layoutContent.setVisibility(View.VISIBLE);
            mLblJogador.setText(item.getNome());
            if (!TextUtils.isEmpty(item.getPosicao())) {
                if (item.getPosicao().length() >= 3) {
                    mLblPosicao.setText(item.getPosicao().substring(0, 3));
                } else {
                    mLblPosicao.setText(item.getPosicao());
                }
            } else {
                mLblPosicao.setText("");
            }

            mImgGol.setVisibility(View.GONE);
            mLblGols.setVisibility(View.GONE);
            mViewCartaoAmarelo.setVisibility(View.GONE);
            mViewCartaoVermelho.setVisibility(View.GONE);
            mImgSubst.setVisibility(View.GONE);

            if (item.getNrGols() > 0) {
                mLblGols.setText(String.valueOf(item.getNrGols()));
                mImgGol.setVisibility(View.VISIBLE);
                mLblGols.setVisibility(View.VISIBLE);
            }
            if (item.getNrCartaoAmarelo() > 0) {
                mViewCartaoAmarelo.setVisibility(View.VISIBLE);
            }
            if (item.getNrCartaoVermelho() > 0) {
                mViewCartaoVermelho.setVisibility(View.VISIBLE);
            }
            if (item.isSubstitudo()) {
                mImgSubst.setVisibility(View.VISIBLE);
                mImgSubst.setImageResource(R.drawable.icn_entra);
            } else if (!TextUtils.isEmpty(item.getIdJogadorSubstituto())) {
                mImgSubst.setVisibility(View.VISIBLE);
                mImgSubst.setImageResource(R.drawable.icn_sai);
            }
        }
    }


    @Override
    public void onSetListeners() {
    }
}
package br.org.fpf.android.model;

import android.content.Context;
import android.text.TextUtils;

import br.org.fpf.android.R;
import br.org.fpf.android.util.Constants;

/**
 * Created by ranieripieper on 1/30/16.
 */
public class FaseRodada implements Comparable<FaseRodada> {

    private String fase;
    private Long rodada;
    private Long idCampeonato;

    public FaseRodada(Long idCampeonato, String fase) {
        this.fase = fase;
        this.idCampeonato = idCampeonato;
    }

    public FaseRodada(Long idCampeonato, String fase, Long rodada) {
        this.fase = fase;
        this.rodada = rodada;
        this.idCampeonato = idCampeonato;
    }

    @Override
    public int compareTo(FaseRodada another) {
        return this.getStringCompare().compareTo(another.getStringCompare());
    }

    public boolean groupPartidas() {
        /*
        if (this.getFase().toLowerCase().contains("oitavas")
                || this.getFase().toLowerCase().contains("quartas")
                || this.getFase().toLowerCase().contains("semifinal")
                || this.getFase().toLowerCase().contains("final")
                || (this.idCampeonato.equals(Constants.ID_LIBERTADORES) && this.getFase().toLowerCase().equalsIgnoreCase(Constants.PRIMEIRA_FASE))) {
            return true;
        }

        return false;
        */
        return true;
    }
    public String getFaseToView(Context ctx) {
        if (this.idCampeonato.equals(Constants.ID_A3)) {
            if (TextUtils.isEmpty(getFase()) || getFase().equalsIgnoreCase(Constants.SEGUNDA_FASE)) {
                int faseNr = getFaseNr();
                if (faseNr > 0) {
                    return ctx.getString(R.string.fase_nr_rodada_nr, getFaseNr(), getRodada() - 19);
                }
            }
        }
        if (this.idCampeonato.equals(Constants.ID_PAULISTA_2_DIVISAO)) {
            if (TextUtils.isEmpty(getFase()) || getFase().equalsIgnoreCase(Constants.SEGUNDA_FASE)) {
                int faseNr = getFaseNr();
                if (faseNr > 0) {
                    return ctx.getString(R.string.fase_nr_rodada_nr, getFaseNr(), getRodada() - 14);
                }
            }
        }
        if (this.idCampeonato.equals(Constants.ID_LIBERTADORES)) {
            if (this.getFase().toLowerCase().equalsIgnoreCase(Constants.PRIMEIRA_FASE)) {
                return ctx.getString(R.string.pre_libertadores);
            }
            if (TextUtils.isEmpty(getFase()) || getFase().equalsIgnoreCase(Constants.SEGUNDA_FASE)) {
                return ctx.getString(R.string.rodada_nr, getRodada() - 1);
            }
        }
        if (TextUtils.isEmpty(getFase()) || getFase().equalsIgnoreCase(Constants.PRIMEIRA_FASE)) {
            return ctx.getString(R.string.rodada_nr, getRodada());
        }
        return getFase();
    }

    private int getFaseNr() {
        if (getFase() != null) {
            if (getFase().equalsIgnoreCase(Constants.PRIMEIRA_FASE)) {
                return 1;
            } else if (getFase().equalsIgnoreCase(Constants.SEGUNDA_FASE)) {
                return 2;
            }
        }
        return -1;
    }

    private String getStringCompare() {
        if (this.getFase() != null) {
            if (this.idCampeonato.equals(Constants.ID_LIBERTADORES) && this.getFase().toLowerCase().equalsIgnoreCase(Constants.PRIMEIRA_FASE)) {
                return "A-Pré-Libertadores";
            } else {
                if (this.getFase().toLowerCase().contains("oitavas")) {
                    return String.format("W%s-%d", this.getFase(), 1);//this.getRodada());
                } else if (this.getFase().toLowerCase().contains("quartas")) {
                    return String.format("X%s-%d", this.getFase(), 1);//this.getRodada());
                } else if (this.getFase().toLowerCase().contains("semifinal")) {
                    return String.format("Y%s-%d", this.getFase(), 1);//this.getRodada());
                } else if (this.getFase().toLowerCase().contains("final")) {
                    return String.format("Z%s-%d", this.getFase(), 1);//this.getRodada());
                }
            }
        }
        return String.format("B%s-%s", this.getFase(), (this.getRodada() < 10 ? String.format("0%d", this.getRodada()) : this.getRodada().toString()));

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FaseRodada that = (FaseRodada) o;

        if (fase != null ? !fase.equals(that.fase) : that.fase != null) return false;
        return !(rodada != null ? !rodada.equals(that.rodada) : that.rodada != null);

    }

    @Override
    public int hashCode() {
        int result = fase != null ? fase.hashCode() : 0;
        result = 31 * result + (rodada != null ? rodada.hashCode() : 0);
        return result;
    }

    /**
     * Gets the fase
     *
     * @return fase
     */
    public String getFase() {
        return fase;
    }

    /**
     * Sets the fase
     *
     * @param fase
     */
    public void setFase(String fase) {
        this.fase = fase;
    }

    /**
     * Gets the rodada
     *
     * @return rodada
     */
    public Long getRodada() {
        return rodada;
    }

    /**
     * Sets the rodada
     *
     * @param rodada
     */
    public void setRodada(Long rodada) {
        this.rodada = rodada;
    }
}

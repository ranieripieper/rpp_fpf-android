package br.org.fpf.android.ui.home.adapter;

import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;

import br.org.fpf.android.R;

/**
 * Created by ranipieper on 8/26/16.
 */
public class SubMenuItemViewHolder extends ChildViewHolder {

    public TextView mLblSubMenu;
    public View mRow;

    public SubMenuItemViewHolder(View itemView) {
        super(itemView);

        mLblSubMenu = (TextView) itemView.findViewById(R.id.lbl_submenu);
        mRow = itemView.findViewById(R.id.row);
    }
}
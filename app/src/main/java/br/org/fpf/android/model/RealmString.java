package br.org.fpf.android.model;

import br.org.fpf.android.helper.GuiaHelper;
import io.realm.RealmObject;

/**
 * Created by ranipieper on 3/2/16.
 */
public class RealmString extends RealmObject {

    private String value;

    public RealmString() {
    }

    public RealmString(String value) {
        this.value = value;
    }

    public String getValue() {
        return GuiaHelper.getUrl(value);
    }

    public void setValue(String value) {
        this.value = value;
    }

}

package br.org.fpf.android.ui.partida;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.text.TextUtils;
import android.view.View;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;

import java.util.ArrayList;
import java.util.List;

import br.org.fpf.android.R;
import br.org.fpf.android.cache.EscalacaoCacheManager;
import br.org.fpf.android.cache.EscalacaoJogadorCacheManager;
import br.org.fpf.android.cache.NarracaoCacheManager;
import br.org.fpf.android.cache.PartidaCacheManager;
import br.org.fpf.android.model.Escalacao;
import br.org.fpf.android.model.EscalacaoJogador;
import br.org.fpf.android.model.Narracao;
import br.org.fpf.android.model.Partida;
import br.org.fpf.android.service.EscalacaoServiceImpl;
import br.org.fpf.android.ui.partida.adapter.EscalacaoViewHolder;
import br.org.fpf.android.util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;

/**
 * Created by ranipieper on 1/14/16.
 */
@EFragment(R.layout.fragment_escalacao)
public class EscalacaoFragment extends PartidaBaseFragment {

    protected EasyLoadMoreRecyclerAdapter mAdapter;

    private Boolean isRefreshing = false;

    public static EscalacaoFragment newInstance(Long idCampeonato, Long idPartida) {
        Bundle args = new Bundle();

        EscalacaoFragment_ fragment = new EscalacaoFragment_();

        args.putLong(Constants.ARG_ID_CAMPEONATO, idCampeonato);
        args.putLong(Constants.ARG_ID_PARTIDA, idPartida);

        fragment.setArguments(args);

        return fragment;
    }

    protected void loadData() {
        synchronized (isRefreshing) {
            if (isRefreshing) {
                return;
            }
            isRefreshing = true;
        }
        if (isAdded()) {
            mRecyclerView.setVisibility(View.VISIBLE);
            callService();
        }
    }

    private boolean isShowLoadingAndError() {
        if (mAdapter == null || mAdapter.getItemCount() <= 0) {
            return true;
        }
        return false;
    }

    public void callService() {
        if (!isAdded()) {
            return;
        }
        txtError.setVisibility(View.GONE);
        showLoadingView(mSwipeRefreshLayout);

        boolean cache = false;
        /*
        if (Constants.PERIODO_PARTIDA_ENCERRADA.equals(mPartida.getPeriodoAtual())) {
            cache = true;
        }*/
        EscalacaoServiceImpl.getEscalacao(mIdPartida, cache, new Callback<Escalacao>() {
            @Override
            public void success(Escalacao escalacao, Response response) {
                preRenderEscalacao(escalacao);
            }

            @Override
            public void failure(RetrofitError error) {
                if (isShowLoadingAndError()) {
                    showErrorTextView(txtError, R.string.error_generic_refresh);
                }

                hideLoadingView(mSwipeRefreshLayout);
                synchronized (isRefreshing) {
                    isRefreshing = false;
                }
            }
        });
    }

    public void preRenderEscalacao(Escalacao escalacao) {
        if (!isAdded()) {
            return;
        }

        if (escalacao != null) {
            if (escalacao.isPreparedToView()) {
                renderEscalacao();
            } else {
                prepareEscalacaoToView();
            }
        } else {
            showEmpty();
        }
    }

    private void showEmpty() {
        if (!isAdded()) {
            return;
        }
        if (mAdapter == null || mAdapter.getItemCount() > 0) {
            txtError.setText(R.string.error_escalacao_vazia);
            txtError.setVisibility(View.VISIBLE);
        }

        synchronized (isRefreshing) {
            isRefreshing = false;
        }

        hideLoadingView(mSwipeRefreshLayout);
    }

    GridLayoutManager getLayoutManager() {
        return new GridLayoutManager(getActivity(), 2);
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            updateView();
        }
    }

    private void updateView() {
        if (isAdded()) {
            Escalacao escalacao = EscalacaoCacheManager.getInstance().getEscalacaoByIdPartida(mIdPartida);
            //if (escalacao == null || !Constants.PERIODO_PARTIDA_ENCERRADA.equals(mPartida.getPeriodoAtual())) {
            loadData();
            //}
        }
    }

    @Background
    void prepareEscalacaoToView() {
        if (isAdded()) {
            Escalacao escalacao = EscalacaoCacheManager.getInstance().getEscalacaoByIdPartida(getContext(), mIdPartida);
            Partida partidaBackground = PartidaCacheManager.getInstance().get(getContext(), mIdPartida);

            if (escalacao != null) {
                List<EscalacaoJogador> newEscalacaoJogadoresMandante = getEscalacaoToView(escalacao.getEscalacaoMandante(), partidaBackground.getTecnicoMandante(), partidaBackground.getIdEquipeMandante());
                List<EscalacaoJogador> newEscalacaoJogadoresVisitante = getEscalacaoToView(escalacao.getEscalacaoVisitante(), partidaBackground.getTecnicoVisitante(), partidaBackground.getIdEquipeVisitante());

                EscalacaoCacheManager.getInstance().updateEscalacao(getActivity(), escalacao.getIdPartida(), newEscalacaoJogadoresMandante, newEscalacaoJogadoresVisitante, true);
            }
            renderEscalacao();
        }
    }

    private List<EscalacaoJogador> getEscalacaoToView(List<EscalacaoJogador> escalacaoJogadores, String tecnico, Long idEquipe) {
        List<EscalacaoJogador> newEscalacaoJogadores = new ArrayList<>();

        if (isAdded()) {
            List<String> idsIgnore = new ArrayList<>();
            for (EscalacaoJogador ej : escalacaoJogadores) {
                if (!TextUtils.isEmpty(ej.getIdJogadorSubstituto())) {
                    idsIgnore.add(ej.getIdJogadorSubstituto());
                }
            }

            for (int i = 0; i < escalacaoJogadores.size(); i++) {
                EscalacaoJogador ej = escalacaoJogadores.get(i);
                if (!idsIgnore.contains(ej.getIdJogador())) {
                    newEscalacaoJogadores.add(getMetricasJogador(ej, false));
                }

                if (!TextUtils.isEmpty(ej.getIdJogadorSubstituto())) {
                    EscalacaoJogador escalacaoJogador = EscalacaoJogadorCacheManager.getInstance().getEscalacaoJogadorByIdPartida(getContext(), ej.getIdPartida(), ej.getIdJogadorSubstituto());
                    newEscalacaoJogadores.add(getMetricasJogador(escalacaoJogador, true));
                }
            }

            //adiciona o técnico
            EscalacaoJogador ejTecnico = new EscalacaoJogador();
            ejTecnico.setIdPartida(mIdPartida);
            ejTecnico.setNome(tecnico);
            ejTecnico.setIdJogador(String.valueOf(-mIdPartida));
            if (isAdded()) {
                ejTecnico.setPosicao(getString(R.string.tecnico));
                ejTecnico.setId(mIdPartida + "_" + ejTecnico.getIdJogador() + "_" + idEquipe);
                EscalacaoJogadorCacheManager.getInstance().put(getContext(), ejTecnico);
                newEscalacaoJogadores.add(EscalacaoJogadorCacheManager.getInstance().get(getContext(), ejTecnico.getId()));
            }

        }
        return newEscalacaoJogadores;
    }

    private EscalacaoJogador getMetricasJogador(EscalacaoJogador escalacaoJogador, boolean substitudo) {

        List<Narracao> narracaoList = NarracaoCacheManager.getInstance().getNarracaoAcoesImportantesByIdPartida(getContext(), mIdPartida, Long.valueOf(escalacaoJogador.getIdJogador()));

        if (narracaoList != null) {
            int nrCartaoAmarelo = 0;
            int nrCartaoVermelho = 0;
            int nrGols = 0;

            for (Narracao narracao : narracaoList) {
                if (Constants.NARRACAO_CARTAO_AMARELO.equalsIgnoreCase(narracao.getAcaoImportante())) {
                    nrCartaoAmarelo++;
                } else if (Constants.NARRACAO_CARTAO_VERMELHO.equalsIgnoreCase(narracao.getAcaoImportante())) {
                    nrCartaoVermelho++;
                } else if (Constants.NARRACAO_GOL.equalsIgnoreCase(narracao.getAcaoImportante())) {
                    nrGols++;
                }
            }
            if (nrCartaoAmarelo > 0 || nrCartaoVermelho > 0 || nrGols > 0 || substitudo) {
                escalacaoJogador = EscalacaoJogadorCacheManager.getInstance().updateEscalacaoJogador(getContext(), escalacaoJogador.getId(), nrCartaoAmarelo, nrCartaoVermelho, nrGols, substitudo);
            }
        }
        return escalacaoJogador;
    }

    private EscalacaoJogador getEscalacaoJogador(List<EscalacaoJogador> list, int i) {
        if (i < list.size()) {
            return list.get(i);
        } else {
            return new EscalacaoJogador(); // fake
        }
    }

    @UiThread
    void renderEscalacao() {
        if (isAdded()) {
            Escalacao escalacao = EscalacaoCacheManager.getInstance().getEscalacaoByIdPartida(mIdPartida);

            List<EscalacaoJogador> lst = new ArrayList<>();
            int count = 0;
            if (escalacao != null) {
                count = Math.max(escalacao.getEscalacaoMandante().size(), escalacao.getEscalacaoVisitante().size());
            }

            for (int i = 0; i < count; i++) {
                lst.add(getEscalacaoJogador(escalacao.getEscalacaoMandante(), i));
                lst.add(getEscalacaoJogador(escalacao.getEscalacaoVisitante(), i));
            }

            if (mAdapter != null) {
                mAdapter.removeAllItems();
                mAdapter.addItems(lst);
            } else {

                View header = getHeaderRecyclerView();
                header.findViewById(R.id.view_margin_bottom).setVisibility(View.VISIBLE);
                mAdapter = new EasyLoadMoreRecyclerAdapter(
                        getActivity(),
                        EscalacaoViewHolder.class,
                        lst,
                        this,
                        R.layout.loading_recycler_view,
                        header);

                mAdapter.notifyDataSetChanged();
            }

            if (mRecyclerView.getAdapter() == null) {
                mRecyclerView.setAdapter(mAdapter);
            }

            hideLoadingView(mSwipeRefreshLayout);
            synchronized (isRefreshing) {
                isRefreshing = false;
            }
        }
    }
}

package br.org.fpf.android.ui.campeonato;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import br.org.fpf.android.R;
import br.org.fpf.android.ui.base.BaseFragment;
import br.org.fpf.android.util.Constants;
import retrofit.RetrofitError;
import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;
import uk.co.ribot.easyadapter.recyclerview.EasyLoadMoreRecyclerView;

/**
 * Created by ranipieper on 1/26/16.
 */
@EFragment
public abstract class CampeonatoPageBaseFragment extends BaseFragment {

    @ViewById(R.id.swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @ViewById(R.id.recycler_view)
    EasyLoadMoreRecyclerView mRecyclerView;

    @ViewById(R.id.txt_error_no_results)
    TextView mTxtErrorNoResult;

    protected EasyLoadMoreRecyclerAdapter mAdapter;

    @FragmentArg(Constants.ARG_ID_CAMPEONATO)
    Long mIdCampeonato;

    public Boolean forceUpdate = true;

    protected Integer mPage = 0;

    public void afterViews() {
        super.afterViews();
        mAdapter = null;
        initSwipeRefresh();

        loadData();
    }

    protected void initSwipeRefresh() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                forceUpdate = true;
                mPage = 0;
                loadData();
            }
        });
    }

    protected void loadData() {
        if (isAdded()) {
            mTxtErrorNoResult.setVisibility(View.GONE);
            preLoadData();
            if (mPage == 0) {
                if (isEnableLoadMore()) {
                    mRecyclerView.enableAutoLoadMore(new EasyLoadMoreRecyclerView.LoadMoreListener() {
                        @Override
                        public void loadMore() {
                            forceUpdate = true;
                            mPage++;
                            loadData();
                        }
                    });
                }
                showLoadingView(mSwipeRefreshLayout);

                mRecyclerView.setVisibility(View.VISIBLE);
            }
            callService();
        }
    }

    protected boolean isEnableLoadMore() {
        return true;
    }

    protected void showError() {
        hideLoadingView(mSwipeRefreshLayout);
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setVisibility(View.GONE);
        }
        mRecyclerView.setVisibility(View.GONE);
        if (mAdapter != null) {
            mAdapter.removeAllItems();
        }
        showErrorTextView(mTxtErrorNoResult, R.string.error_generic_refresh);
    }

    public void showError(RetrofitError error) {
        if (mPage == 0) {
            mTxtErrorNoResult.setVisibility(View.VISIBLE);
            mSwipeRefreshLayout.setVisibility(View.GONE);
        }
    }

    @Click(R.id.txt_error_no_results)
    void errorClick() {
        loadData();
    }

    protected abstract void preLoadData();

    protected abstract void callService();

    @Override
    protected String getToolbarTitle() {
        return "";
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.DRAWER;
    }

    @Override
    protected boolean needConfigToolbar() {
        return false;
    }

}

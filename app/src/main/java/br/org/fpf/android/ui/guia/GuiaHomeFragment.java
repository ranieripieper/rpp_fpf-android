package br.org.fpf.android.ui.guia;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.org.fpf.android.BuildConfig;
import br.org.fpf.android.R;
import br.org.fpf.android.cache.GuiaCacheManager;
import br.org.fpf.android.model.Guia;
import br.org.fpf.android.service.GuiaServiceImpl;
import br.org.fpf.android.ui.base.BaseFragment;
import br.org.fpf.android.ui.base.ErrorListener;
import br.org.fpf.android.ui.custom.HeaderDecoration;
import br.org.fpf.android.ui.guia.adapter.GuiaHomeAdapter;
import br.org.fpf.android.ui.guia.adapter.GuiaHomeViewHolder;
import br.org.fpf.android.ui.navigation.Navigator;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderDecoration;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.ribot.easyadapter.recyclerview.EasyLoadMoreRecyclerView;

/**
 * Created by ranipieper on 1/15/16.
 */
@EFragment(R.layout.fragment_guia_home)
public class GuiaHomeFragment extends BaseFragment implements GuiaHomeViewHolder.GuiaHomeListener {

    @Nullable
    @ViewById(R.id.swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @ViewById(R.id.recycler_view)
    EasyLoadMoreRecyclerView mRecyclerView;

    private MenuItem mSearchMenu;

    private Boolean itemSelected = false;

    private Boolean isRefreshing = false;
    private boolean isUpdate = false;

    protected GuiaHomeAdapter mAdapter;

    private boolean showMenu = false;

    private RecyclerView.ItemDecoration mHeaderDecoration;

    public static GuiaHomeFragment newInstance() {
        Bundle args = new Bundle();

        GuiaHomeFragment_ fragment = new GuiaHomeFragment_();
        fragment.setArguments(args);

        return fragment;
    }

    @AfterViews
    public void afterViews() {
        super.afterViews();
        initSwipeRefresh();
    }

    protected void initSwipeRefresh() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    isUpdate = true;
                    loadData();
                }
            });
        }

        createAdapter(new ArrayList<Guia>());

        loadData();
    }

    protected void createAdapter(List<Guia> list) {

        View mHeader = LayoutInflater.from(getContext()).inflate(R.layout.include_header_guia, null);

        mAdapter = new GuiaHomeAdapter(
                getActivity(),
                GuiaHomeViewHolder.class,
                list,
                this,
                R.layout.loading_recycler_view);

        mAdapter.notifyDataSetChanged();
        mRecyclerView.setNestedScrollingEnabled(true);
        mRecyclerView.setAdapter(mAdapter);
        mHeaderDecoration = new HeaderDecoration(mHeader, false, 0.5f, 0.5f, 1);
        mRecyclerView.addItemDecoration(mHeaderDecoration);
        StickyHeaderDecoration mStickyHeaderDecoration = new StickyHeaderDecoration(mAdapter);
        mRecyclerView.addItemDecoration(mStickyHeaderDecoration);
    }

    private void loadData() {
        synchronized (isRefreshing) {
            if (isAdded() && !isRefreshing) {
                isRefreshing = true;
                mRecyclerView.setVisibility(View.VISIBLE);
                callService();
            } else if (isAdded()) {
                hideLoadingView(mSwipeRefreshLayout);
            }
        }
    }

    public void callService() {
        if (!isAdded()) {
            return;
        }
        if (isUpdate) {
            showLoadingView(mSwipeRefreshLayout);
        } else {
            showLoadingView();
        }

        GuiaServiceImpl.getGuias(new Callback<List<Guia>>() {
            @Override
            public void success(List<Guia> guias, Response response) {
                if (guias != null && !guias.isEmpty()) {
                    renderGuias(guias);
                    showMenu = true;
                    invalidateOptionsMenu();
                } else {
                    showEmpty();
                }
                hideLoadingView(mSwipeRefreshLayout);
                synchronized (isRefreshing) {
                    isRefreshing = false;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (isAdded()) {
                    //verifica se tem guia em cache
                    if (GuiaCacheManager.getInstance().count() > 0) {
                        renderGuias(GuiaCacheManager.getInstance().getAll());
                    } else {
                        showError(error, R.string.error_generic, new ErrorListener() {
                            @Override
                            public void retry() {
                                isUpdate = false;
                                loadData();
                            }

                            @Override
                            public void cancel() {
                            }
                        });
                    }
                }

                if (BuildConfig.DEBUG) {
                    error.printStackTrace();
                }
                hideLoadingView(mSwipeRefreshLayout);
                synchronized (isRefreshing) {
                    isRefreshing = false;
                }
            }
        });
    }

    private void renderGuias(List<Guia> guias) {
        if (!isAdded()) {
            return;
        }
        if (mAdapter == null) {
            createAdapter(new ArrayList<Guia>());
        }

        mAdapter.removeAllItems();
        mAdapter.addItems(guias);
    }

    private void showEmpty() {
        showErrorDialog(R.string.error_guia_vazio);
        hideLoadingView(mSwipeRefreshLayout);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        if (showMenu) {
            inflater.inflate(R.menu.menu_guia_home, menu);
            mSearchMenu = menu.findItem(R.id.menu_search);

            final SearchView searchView = (SearchView) mSearchMenu.getActionView();
            searchView.setImeOptions(EditorInfo.IME_ACTION_SEARCH);

            searchView.setIconifiedByDefault(true);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    applyFilter(query);
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    applyFilter(newText);
                    return false;
                }
            });
        }
    }

    private void applyFilter(String query) {
        if (TextUtils.isEmpty(query)) {
            renderGuias(new ArrayList<Guia>(GuiaCacheManager.getInstance().getAll()));
            if (mRecyclerView != null && mHeaderDecoration != null) {
                mRecyclerView.removeItemDecoration(mHeaderDecoration);
                mRecyclerView.addItemDecoration(mHeaderDecoration);
            }
        } else {
            renderGuias(new ArrayList<Guia>(GuiaCacheManager.getInstance().getByFilter(query)));
            if (mRecyclerView != null && mHeaderDecoration != null) {
                mRecyclerView.removeItemDecoration(mHeaderDecoration);
            }
        }
    }

    @Override
    public void onResumeFromBackStack() {
        super.onResumeFromBackStack();
        applyFilter(null);
        synchronized (itemSelected) {
            itemSelected = false;
        }
    }

    @Override
    public void onItemSelected(Guia guia) {
        synchronized (itemSelected) {
            if (!itemSelected) {
                Navigator.navigateToGuiaTimeFragment(getContext(), guia.getIdEquipe());
                itemSelected = true;
            }
        }
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.guia_paulistao);
    }

    @Override
    protected void onBackPressed() {
        if (mSearchMenu != null && !((SearchView) mSearchMenu.getActionView()).isIconified()) {
            ((SearchView) mSearchMenu.getActionView()).onActionViewCollapsed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.DRAWER;
    }
}

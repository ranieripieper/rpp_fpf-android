package br.org.fpf.android.cache.base;

import android.content.Context;

import java.util.List;

import br.org.fpf.android.BuildConfig;
import br.org.fpf.android.service.RetrofitManager;
import br.org.fpf.android.util.Migration;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by jeffcailteux on 7/29/15.
 */
public class RealmManager {

    private Realm realm;
    private static final long REALMVERSION = 5;

    public static final ThreadLocal<RealmManager> realmManager = new ThreadLocal<RealmManager>() {
        @Override
        protected RealmManager initialValue() {
            return new RealmManager();
        }
    };

    private RealmManager() {
    }

    private static Realm getRealm(Context context) {
        RealmConfiguration config;
        if (!BuildConfig.DEBUG) {
            config = new RealmConfiguration.Builder(context)
                    .schemaVersion(REALMVERSION)
                    .migration(Migration.getInstance())
                    .build();
        } else {
            config = new RealmConfiguration.Builder(context)
                    .schemaVersion(REALMVERSION)
                    .deleteRealmIfMigrationNeeded()
                    .build();
        }

        Realm.setDefaultConfiguration(config);
        return Realm.getInstance(config);
    }

    /**
     * Gets default instance in MainThread
     *
     * @return
     */
    public static RealmManager getInstance() {
        return getInstance(null);
    }

    public static void refresh() {
        if (getInstance().getRealm() != null) {
            getInstance().getRealm().refresh();
        }
    }

    /**
     * Gets instance in actual Thread
     *
     * @param context
     * @return
     */
    public static RealmManager getInstance(Context context) {
        RealmManager rm = realmManager.get();
        if (rm.realm == null && context != null) {
            rm.realm = getRealm(context);
        } else if (rm.realm == null && context == null) {
            rm.realm = Realm.getDefaultInstance();
        }
        return rm;
    }

    public <T extends RealmObject> void updateWithoutNulls(Context context, List<T> lst, Class<T> type) {
        if (lst != null) {
            Realm realm = getRealm(context);
            realm.beginTransaction();
            String json = RetrofitManager.getGsonRealm().toJson(lst);
            realm.createOrUpdateAllFromJson(type, json);
            realm.commitTransaction();
        }
    }

    public <T extends RealmObject> void updateWithoutNulls(List<T> lst, Class<T> type) {
        if (lst != null) {
            RealmManager rm = realmManager.get();
            rm.realm.beginTransaction();
            String json = RetrofitManager.getGsonRealm().toJson(lst);
            rm.realm.createOrUpdateAllFromJson(type, json);

            rm.realm.commitTransaction();
        }
    }

    /**
     * Gets all objects
     *
     * @param type
     * @param sortField
     * @param sortAscending
     * @param <T>
     * @return
     */
    public <T extends RealmObject> List<T> getAll(Class<T> type, String sortField, boolean sortAscending) {
        return getAll(type, sortField, sortAscending ? Sort.ASCENDING : Sort.DESCENDING);
    }

    /**
     * Gets all objects
     *
     * @param type
     * @param sortField
     * @param sort
     * @param <T>
     * @return
     */
    public <T extends RealmObject> List<T> getAll(Class<T> type, String sortField, Sort sort) {
        RealmResults<T> allRealmObjects = realm.allObjectsSorted(type, sortField, sort);
        return allRealmObjects;
    }

    /**
     * Gets total items
     *
     * @param type
     * @param <T>
     * @return
     */
    public <T extends RealmObject> long count(Class<T> type) {
        return realm.where(type).count();
    }

    public void put(RealmObject t) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(t);
        realm.commitTransaction();
        realm.refresh();
    }

    public <T extends RealmObject> void insert(RealmObject obj, Class<T> type) {
        realm.beginTransaction();
        realm.copyToRealm(obj);
        realm.commitTransaction();
    }

    public void putAll(List<RealmObject> t) {
        realm.beginTransaction();

        for (RealmObject item : t) {
            realm.copyToRealmOrUpdate(item);
        }

        realm.commitTransaction();
        realm.refresh();
    }

    public <T extends RealmObject> void delete(final String primeryKeyName, final Long id, final Class<T> type) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                T obj = realm.where(type).equalTo(primeryKeyName, id).findFirst();
                if (obj != null) {
                    obj.removeFromRealm();
                }
            }
        });
        realm.refresh();
    }

    public <T extends RealmObject> void delete(final String primeryKeyName, final String id, final Class<T> type) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                T obj = realm.where(type).equalTo(primeryKeyName, id).findFirst();
                if (obj != null) {
                    obj.removeFromRealm();
                }
            }
        });
        realm.refresh();
    }


    public <T extends RealmObject> void deleteAll(Class<T> type) {
        realm.beginTransaction();
        realm.where(type).findAll().clear();
        realm.commitTransaction();
        realm.refresh();
    }

    public <T extends RealmObject> T get(final String primeryKeyName, String id, Class<T> type) {
        return realm.where(type).equalTo(primeryKeyName, id).findFirst();
    }

    public <T extends RealmObject> List<T> getAll(Class<T> type) {
        return realm.where(type).findAll();
    }

    public Realm getRealm() {
        return realm;
    }
}

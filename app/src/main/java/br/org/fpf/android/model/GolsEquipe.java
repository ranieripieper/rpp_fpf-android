package br.org.fpf.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ranipieper on 3/8/16.
 */
public class GolsEquipe implements Parcelable, Comparable<GolsEquipe> {

    @Expose
    @SerializedName("Equipe")
    private String equipe;

    @Expose
    @SerializedName("Jogador")
    private String jogador;

    @Expose
    @SerializedName("EquipeId")
    private long equipeId;

    @Expose
    @SerializedName("Gols")
    private long gols;

    public String getEquipe() {
        return equipe;
    }

    public void setEquipe(String equipe) {
        this.equipe = equipe;
    }

    public long getEquipeId() {
        return equipeId;
    }

    public void setEquipeId(long equipeId) {
        this.equipeId = equipeId;
    }

    public long getGols() {
        return gols;
    }

    public void setGols(long gols) {
        this.gols = gols;
    }

    @Override
    public int compareTo(GolsEquipe another) {
        return Long.valueOf(another.getGols()).compareTo(gols);
    }

    public String getJogador() {
        return jogador;
    }

    public void setJogador(String jogador) {
        this.jogador = jogador;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.equipe);
        dest.writeString(this.jogador);
        dest.writeLong(this.equipeId);
        dest.writeLong(this.gols);
    }

    public GolsEquipe() {
    }

    protected GolsEquipe(Parcel in) {
        this.equipe = in.readString();
        this.jogador = in.readString();
        this.equipeId = in.readLong();
        this.gols = in.readLong();
    }

    public static final Creator<GolsEquipe> CREATOR = new Creator<GolsEquipe>() {
        @Override
        public GolsEquipe createFromParcel(Parcel source) {
            return new GolsEquipe(source);
        }

        @Override
        public GolsEquipe[] newArray(int size) {
            return new GolsEquipe[size];
        }
    };
}

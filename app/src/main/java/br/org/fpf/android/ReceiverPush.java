package br.org.fpf.android;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.parse.ParseAnalytics;
import com.parse.ParsePushBroadcastReceiver;

import java.util.Random;

import br.org.fpf.android.model.PushNotificationMessage;
import br.org.fpf.android.service.RetrofitManager;
import br.org.fpf.android.ui.home.MainActivity;


/**
 * Created by ranipieper on 12/10/15.
 */
public class ReceiverPush extends ParsePushBroadcastReceiver {

    private static final String TAG = "ReceiverPush";

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        try {
            String data = intent.getStringExtra("com.parse.Data");
            PushNotificationMessage pushNotificationMsg = RetrofitManager.getGson().fromJson(data, PushNotificationMessage.class);

            if (pushNotificationMsg != null) {
                showNotification(context, intent, pushNotificationMsg);
            }

        } catch (Exception e) {
            Log.e(ReceiverPush.class.toString(), "Unexpected JSONException when receiving push data: ", e);
        }
    }

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        ParseAnalytics.trackAppOpenedInBackground(intent);

        PushNotificationMessage pushNotificationMsg = null;
        try {
            String data = intent.getStringExtra("com.parse.Data");
            pushNotificationMsg = RetrofitManager.getGson().fromJson(data, PushNotificationMessage.class);
        } catch (Exception e) {
            Log.e(ReceiverPush.class.toString(), "Unexpected JSONException when receiving push data: ", e);
        }

        Class<? extends Activity> cls = getActivity(context, intent);
        Intent activityIntent = MainActivity.getActivityIntent(context, pushNotificationMsg);
        activityIntent.putExtras(intent.getExtras());
        if (Build.VERSION.SDK_INT >= 16) {
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addParentStack(cls);
            stackBuilder.addNextIntent(activityIntent);
            stackBuilder.startActivities();
        } else {
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(activityIntent);
        }
    }

    public static void showNotification(Context context, final Intent intent, PushNotificationMessage message) {

        Bitmap circularBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.img_not_big);

        Bundle extras = intent.getExtras();
        Random random = new Random();
        int contentIntentRequestCode = random.nextInt();
        int deleteIntentRequestCode = random.nextInt();
        String packageName = context.getPackageName();
        Intent contentIntent = new Intent("com.parse.push.intent.OPEN");
        contentIntent.putExtras(extras);
        contentIntent.setPackage(packageName);
        Intent deleteIntent = new Intent("com.parse.push.intent.DELETE");
        deleteIntent.putExtras(extras);
        deleteIntent.setPackage(packageName);
        PendingIntent pContentIntent = PendingIntent.getBroadcast(context, contentIntentRequestCode, contentIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent pDeleteIntent = PendingIntent.getBroadcast(context, deleteIntentRequestCode, deleteIntent, PendingIntent.FLAG_CANCEL_CURRENT);


        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.img_not_small_negativa)
                        .setContentTitle(context.getString(R.string.APP_NAME))
                        .setContentText(message.getAlert())
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message.getAlert()))
                        .setAutoCancel(true)
                        .setCategory(NotificationCompat.CATEGORY_SOCIAL)
                        .setGroup(context.getString(R.string.APP_NAME))
                        .setContentIntent(pContentIntent)
                        .setDeleteIntent(pDeleteIntent)
                        .setGroupSummary(true)
                        .setColor(context.getResources().getColor(R.color.black))
                        .setLargeIcon(circularBitmap);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        mBuilder.setDefaults(Notification.DEFAULT_ALL);

        Long id = message.getIdPartida();
        if (id == null) {
            id = Double.valueOf(Math.random() * 100).longValue();
        }
        notificationManager.notify(Long.valueOf(id).intValue(), mBuilder.build());
    }
}
package br.org.fpf.android.ui.home.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;

import br.org.fpf.android.R;

/**
 * Created by ranipieper on 8/26/16.
 */
public class MenuItemViewHolder extends ParentViewHolder {

    public TextView mLblMenu;
    public ImageView mImgMenu;

    public MenuItemViewHolder(View itemView) {
        super(itemView);

        mLblMenu = (TextView) itemView.findViewById(R.id.lbl_menu);
        mImgMenu = (ImageView) itemView.findViewById(R.id.img_menu);
    }
}
package br.org.fpf.android.ui.estatisticas.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.org.fpf.android.FpfApplication;
import br.org.fpf.android.R;
import br.org.fpf.android.cache.EquipeCacheManager;
import br.org.fpf.android.model.Equipe;
import br.org.fpf.android.model.Jogos;
import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by ranipieper on 3/9/16.
 */
@LayoutId(R.layout.row_estatisticas_time_jogos)
public class JogosViewHolder extends ItemViewHolder<Jogos> {

    @ViewId(R.id.img_time_mandante)
    ImageView imgTime;

    @ViewId(R.id.lbl_placar_mandante)
    TextView lblPlacarTime;

    @ViewId(R.id.img_time_visitante)
    ImageView imgTimeAdversario;

    @ViewId(R.id.lbl_placar_visitante)
    TextView lblPlacarTimeAdversario;

    public JogosViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Jogos item, PositionInfo positionInfo) {

        Equipe equipeAdversaria = EquipeCacheManager.getInstance().get(item.getIdEquipe());
        Equipe equipe = item.getEquipe();

        if (item.isMandante()) {
            FpfApplication.imageLoader.displayImage(equipe.getUrlLogo(), imgTime);
            FpfApplication.imageLoader.displayImage(equipeAdversaria.getUrlLogo(), imgTimeAdversario);
            lblPlacarTime.setText(String.valueOf(item.getGolsPro()));
            lblPlacarTimeAdversario.setText(String.valueOf(item.getGolsContra()));
        } else {
            FpfApplication.imageLoader.displayImage(equipeAdversaria.getUrlLogo(), imgTime);
            FpfApplication.imageLoader.displayImage(equipe.getUrlLogo(), imgTimeAdversario);
            lblPlacarTime.setText(String.valueOf(item.getGolsContra()));
            lblPlacarTimeAdversario.setText(String.valueOf(item.getGolsPro()));
        }
    }

    @Override
    public void onSetListeners() {
    }

}
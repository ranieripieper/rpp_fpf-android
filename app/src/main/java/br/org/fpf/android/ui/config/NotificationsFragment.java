package br.org.fpf.android.ui.config;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Button;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.greysonparrelli.permiso.Permiso;
import com.parse.ParseInstallation;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

import br.org.fpf.android.R;
import br.org.fpf.android.cache.EquipeCacheManager;
import br.org.fpf.android.cache.EquipeNotificacaoCacheManager;
import br.org.fpf.android.model.Equipe;
import br.org.fpf.android.model.EquipeNotificacao;
import br.org.fpf.android.service.EquipeServiceImpl;
import br.org.fpf.android.ui.base.BaseFragment;
import br.org.fpf.android.ui.base.ErrorListener;
import br.org.fpf.android.ui.base.ViewUtil;
import br.org.fpf.android.ui.config.adapter.NotificacaoAdapter;
import br.org.fpf.android.ui.config.adapter.NotificacaoViewHolder;
import br.org.fpf.android.ui.navigation.Navigator;
import br.org.fpf.android.util.Constants;
import br.org.fpf.android.util.SharedPrefManager;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderDecoration;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.ribot.easyadapter.recyclerview.EasyLoadMoreRecyclerView;

/**
 * Created by ranipieper on 1/15/16.
 */
@EFragment(R.layout.fragment_notifications)
public class NotificationsFragment extends BaseFragment implements NotificacaoViewHolder.NotificacaoHolderListener {

    @ViewById(R.id.recycler_view)
    EasyLoadMoreRecyclerView mRecyclerView;

    @ViewById(R.id.btSaveTeams)
    Button btSave;

    private List<Equipe> mEquipeList;

    private NotificacaoAdapter mAdapter;

    public static NotificationsFragment newInstance() {
        Bundle args = new Bundle();

        NotificationsFragment_ fragment = new NotificationsFragment_();
        fragment.setArguments(args);

        return fragment;
    }

    @AfterViews
    public void afterViews() {
        super.afterViews();

        initRecyclerView();

        setTextButton();

        loadData();
    }

    private void setTextButton() {
        if (SharedPrefManager.getInstance().isFirstAccess()) {
            btSave.setText(R.string.escolher_depois);
            btSave.setTag(R.string.escolher_depois);
            btSave.setVisibility(View.VISIBLE);
        } else {
            btSave.setText(R.string.save_teams);
            btSave.setTag(R.string.save_teams);
            btSave.setVisibility(View.VISIBLE);
        }
    }

    private void loadData() {
        showLoadingView();
        EquipeServiceImpl.getEquipesToPush(new Callback<List<Equipe>>() {
            @Override
            public void success(List<Equipe> equipes, Response response) {
                processResponse(equipes);
            }

            @Override
            public void failure(RetrofitError error) {
                showDialogError(error);
            }
        });
    }

    private void showDialogError(RetrofitError error) {
        showError(error, R.string.error_generic, new ErrorListener() {
            @Override
            public void retry() {
                loadData();
            }

            @Override
            public void cancel() {
                if (btSave.getTag().equals(R.string.save_teams)) {
                    btSave.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    protected void processResponse(List<Equipe> equipeList) {
        if (!isAdded()) {
            return;
        }
        removeOldEquipesNotification();
        mEquipeList = equipeList;
        if (equipeList == null || equipeList.isEmpty()) {
            showDialogError(null);
        } else {
            disableLoadingMore(mRecyclerView, equipeList.size());
            renderEquipes();
            mRecyclerView.setVisibility(View.VISIBLE);
        }
        hideLoadingView();

    }

    public void renderEquipes() {
        if (!isAdded()) {
            return;
        }

        mEquipeList = new ArrayList<>(EquipeCacheManager.getInstance().getEquipeToPush());
        createAdapter(mEquipeList);

        if (mRecyclerView.getAdapter() == null) {
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    private StickyHeaderDecoration mStickyHeaderDecoration;

    protected void createAdapter(List<Equipe> equipeList) {

        mAdapter = new NotificacaoAdapter(
                getActivity(),
                NotificacaoViewHolder.class,
                new ArrayList(),
                this,
                R.layout.loading_recycler_view);
        mRecyclerView.setAdapter(null);

        List<EquipeNotificacao> eqNotList = EquipeNotificacaoCacheManager.getInstance().getAll();
        List<Equipe> newList = new ArrayList<>();
        List<Equipe> eqSelected = new ArrayList<>();
        List<Equipe> eqNotSelected = new ArrayList<>();

        if (eqNotList != null) {
            //remove equipes que não tem push mais
            for (Equipe eq : equipeList) {
                if (EquipeNotificacaoCacheManager.getInstance().get(eq.getId()) != null && eq.isPush()) {
                    eqSelected.add(eq);
                } else {
                    eqNotSelected.add(eq);
                }
            }

            newList.addAll(eqSelected);
            newList.addAll(eqNotSelected);
        }

        if (eqSelected.isEmpty()) {
            setTextButton();
        } else {
            btSave.setText(R.string.save_teams);
            btSave.setTag(R.string.save_teams);
        }

        mAdapter.addItems(newList);

        if (mStickyHeaderDecoration != null) {
            mRecyclerView.removeItemDecoration(mStickyHeaderDecoration);
        }
        mStickyHeaderDecoration = new StickyHeaderDecoration(mAdapter);
        mRecyclerView.addItemDecoration(mStickyHeaderDecoration);
    }

    protected void removeOldEquipesNotification() {
        List<EquipeNotificacao> lstEquipeNotificacao = EquipeNotificacaoCacheManager.getInstance().getAll();
        boolean removeParse = false;

        if (lstEquipeNotificacao != null) {
            for (EquipeNotificacao equipeNotificacao : lstEquipeNotificacao) {
                Equipe eq = EquipeCacheManager.getInstance().get(equipeNotificacao.getIdEquipe());

                if (eq != null) {
                    if (eq.isPush() != null && eq.isPush()) {
                        continue;
                    } else {
                        EquipeNotificacaoCacheManager.getInstance().delete(equipeNotificacao.getIdEquipe());
                        removeParse = true;
                    }
                } else {
                    removeParse = true;
                }
            }
        }
        if (removeParse) {
            saveTeamsParse();
        }
    }

    protected void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
    }

    @Override
    public void onEquipeChecked(Equipe equipe, boolean isChecked) {
        checkPermission(equipe, isChecked);
    }

    private void checkPermission(final Equipe equipe, boolean isChecked) {
        if (isChecked) {
            if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
                subscribe(equipe);
            } else {
                Permiso.getInstance().requestPermissions(new Permiso.IOnPermissionResult() {
                    @Override
                    public void onPermissionResult(Permiso.ResultSet resultSet) {
                        if (!resultSet.areAllPermissionsGranted()) {
                            EquipeNotificacaoCacheManager.getInstance().delete(equipe.getId());

                            boolean showRationale = shouldShowRequestPermissionRationale(Manifest.permission.GET_ACCOUNTS);
                            if (!showRationale) {
                                ViewUtil.showConfirmDialog(getContext(), R.string.atencao, R.string.msg_explain_never, new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                        Intent intent = new Intent();
                                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                                        intent.setData(uri);
                                        startActivity(intent);
                                    }
                                });

                            }
                        } else {
                            subscribe(equipe);
                        }
                    }

                    @Override
                    public void onRationaleRequested(Permiso.IOnRationaleProvided callback, String... permissions) {
                        Permiso.getInstance().showRationaleInDialog(getString(R.string.title_explain), getString(R.string.msg_explain), null, callback);
                    }
                }, Manifest.permission.GET_ACCOUNTS);
            }
        } else {
            EquipeNotificacaoCacheManager.getInstance().delete(equipe.getId());
            unsubscribe(equipe);
        }
    }

    private String getChannel(Equipe equipe) {
        String value  = Normalizer.normalize(equipe.getNome(), Normalizer.Form.NFD);
        value = value.replaceAll("[^\\p{ASCII}]", "").replaceAll("[^a-zA-Z]", "-");;

        return value;
    }

    private void subscribe(final Equipe equipe) {
//        showLoadingView();
        EquipeNotificacaoCacheManager.getInstance().updateSent(equipe.getId(), true);
        renderEquipes();
        /*
        ParseInstallation.getCurrentInstallation().saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    ParsePush.subscribeInBackground(getChannel(equipe), new SaveCallback() {
                        @Override
                        public void done(com.parse.ParseException e) {
                            if (e == null) {
                                EquipeNotificacaoCacheManager.getInstance().updateSent(equipe.getId(), true);
                                renderEquipes();
                            } else {
                                rollbackSubscribe(equipe);
                            }
                            hideLoadingView();
                        }
                    });
                } else {
                    rollbackSubscribe(equipe);
                    hideLoadingView();
                    showErrorDialog(R.string.error_generic);
                }
            }
        });
        */
    }

    private void unsubscribe(final Equipe equipe) {
        //showLoadingView();
        EquipeNotificacaoCacheManager.getInstance().delete(equipe.getId());
        renderEquipes();
        /*
        ParseInstallation.getCurrentInstallation().saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    ParsePush.unsubscribeInBackground(getChannel(equipe), new SaveCallback() {
                        @Override
                        public void done(com.parse.ParseException e) {
                            if (e == null) {
                                EquipeNotificacaoCacheManager.getInstance().delete(equipe.getId());
                                renderEquipes();
                            } else {
                                rollbackUnsubscribe(equipe);
                            }
                            hideLoadingView();
                        }
                    });
                } else {

                    rollbackUnsubscribe(equipe);
                    hideLoadingView();
                    showErrorDialog(R.string.error_generic);
                }
            }
        });
        */
    }

    private List<String> getChannels() {
        List<EquipeNotificacao> equipeNotificacaoList = EquipeNotificacaoCacheManager.getInstance().getAll(getContext());
        List<String> result = new ArrayList<>();
        if (equipeNotificacaoList != null) {
            for (EquipeNotificacao eq : equipeNotificacaoList) {
                result.add(eq.getChannel());
            }
        }

        return result;
    }

    @Background
    void saveTeams() {
        saveTeamsParse();
        postSaveTeams();
    }

    private void saveTeamsParse() {
        List<String> channels = getChannels();

        ParseInstallation install = ParseInstallation.getCurrentInstallation();

        install.put("channels", channels);

        install.saveEventually();
    }

    @UiThread
    void postSaveTeams() {
        hideLoadingView();
        if (SharedPrefManager.getInstance().isFirstAccess()) {
            SharedPrefManager.getInstance().setFirstAccess(false);
            Navigator.navigateToCampeonatoFragment(getActivity(), Constants.ID_BRASILEIRAO);
        }
        showSuccessDialog(getActivity(), R.string.config_salvas);
    }

    @Click(R.id.btSaveTeams)
    void saveTeamsClick() {
        if (btSave.getTag().equals(R.string.save_teams)) {
            showLoadingView();
            saveTeams();
        } else {
            SharedPrefManager.getInstance().setFirstAccess(false);
            Navigator.navigateToCampeonatoFragment(getContext(), Constants.ID_BRASILEIRAO);
        }
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.title_notifications);
    }


    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        if (SharedPrefManager.getInstance().isFirstAccess()) {
            return TOOLBAR_TYPE.NOTHING;
        } else {
            return TOOLBAR_TYPE.DRAWER;
        }
    }
}

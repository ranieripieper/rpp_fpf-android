package br.org.fpf.android.ui.campeonato;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import br.org.fpf.android.BuildConfig;
import br.org.fpf.android.R;
import br.org.fpf.android.cache.CampeonatoCacheManager;
import br.org.fpf.android.cache.PartidaCacheManager;
import br.org.fpf.android.model.Campeonato;
import br.org.fpf.android.model.FaseRodada;
import br.org.fpf.android.model.Partida;
import br.org.fpf.android.service.CampeonatoServiceImpl;
import br.org.fpf.android.service.PartidaServiceImpl;
import br.org.fpf.android.ui.base.BaseFragment;
import br.org.fpf.android.ui.custom.FpfViewPagerAdapter;
import br.org.fpf.android.util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranieripieper on 1/29/16.
 */
@EFragment(R.layout.fragment_tabela_rodadas)
public class TabelaRodadasFragment extends BaseFragment {

    @ViewById(R.id.tablayout)
    TabLayout mTabLayout;

    @ViewById(R.id.viewpager)
    ViewPager mViewPager;

    @ViewById(R.id.txt_error_no_results)
    TextView mTxtErrorNoResult;

    @FragmentArg(Constants.ARG_ID_CAMPEONATO)
    Long mIdCampeonato;

    public static TabelaRodadasFragment newInstance(Long idCampeonato) {
        Bundle args = new Bundle();

        TabelaRodadasFragment_ fragment = new TabelaRodadasFragment_();
        args.putLong(Constants.ARG_ID_CAMPEONATO, idCampeonato);

        fragment.setArguments(args);

        return fragment;
    }

    @AfterViews
    public void afterViews() {
        super.afterViews();
        getView().postDelayed(new Runnable() {
            @Override
            public void run() {
                callService();
            }
        }, 500);

    }

    @Override
    public void onResumeFromBackStack() {
        super.onResumeFromBackStack();
        if (mAdapter != null && mViewPager != null) {
            BaseFragment baseFragment = (BaseFragment)mAdapter.getItem(mViewPager.getCurrentItem());
            if (baseFragment != null) {
                baseFragment.onResumeFromBackStack();
            }
        }
    }

    private void callServicePartidas() {
        PartidaServiceImpl.getPartidas(mIdCampeonato, new Callback<List<Partida>>() {
            @Override
            public void success(List<Partida> list, Response response) {
                if (isAdded()) {
                    prepareViewPager();
                    hideLoadingView();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (BuildConfig.DEBUG) {
                    error.printStackTrace();
                }
                hideLoadingView();
                showErrorTextView(mTxtErrorNoResult, R.string.error_generic_refresh);
            }
        });
    }

    @Click(R.id.txt_error_no_results)
    void errorClick() {
        callService();
    }

    protected void callService() {
        mTxtErrorNoResult.setVisibility(View.GONE);
        showLoadingView();
        CampeonatoServiceImpl.getCampeonatosInCache(new Callback<List<Campeonato>>() {
            @Override
            public void success(List<Campeonato> campeonato, Response response) {
                callServicePartidas();
            }

            @Override
            public void failure(RetrofitError error) {
                hideLoadingView();
                showErrorTextView(mTxtErrorNoResult, R.string.error_generic_refresh);
            }
        });
    }

    FpfViewPagerAdapter mAdapter;

    @Background
    void prepareViewPager() {
        List<Partida> partidaList = PartidaCacheManager.getInstance().getPartidasByIdCampeonato(getContext(), mIdCampeonato);

        mAdapter = new FpfViewPagerAdapter(getChildFragmentManager());

        Set<FaseRodada> faseRodadas = new LinkedHashSet<>();

        for (Partida p : partidaList) {
            FaseRodada fr = new FaseRodada(mIdCampeonato, p.getFase(), p.getRodada());
            faseRodadas.add(fr);
        }

        int currentPage = -1;
        int pos = 0;
        Campeonato campeonato = CampeonatoCacheManager.getInstance().get(mIdCampeonato);
        List<FaseRodada> lstFaseRodadas = new ArrayList<>(faseRodadas);
        Collections.sort(lstFaseRodadas);
        for (FaseRodada fr : lstFaseRodadas) {
            if (isAdded()) {
                mAdapter.addFrag(TabelaFragment.newInstance(mIdCampeonato, fr.getFase(), fr.getRodada(), fr.groupPartidas()), getTabText(fr), isAdded());
                if (campeonato != null && fr.getFase().equalsIgnoreCase(campeonato.getFaseAtual()) &&
                        fr.getRodada().equals(campeonato.getRodadaAtual())) {
                    currentPage = pos;
                }
                pos++;
            }
        }

        setupViewPager(currentPage);
    }

    @UiThread
    void setupViewPager(int currentPage) {
        if (isAdded()) {
            mViewPager.setAdapter(mAdapter);
            mTabLayout.setupWithViewPager(mViewPager);
            mViewPager.setVisibility(View.VISIBLE);
            mTabLayout.setVisibility(View.VISIBLE);
            if (currentPage >= 0) {
                mTabLayout.getTabAt(currentPage).select();
                mViewPager.setCurrentItem(currentPage);
            }
        }
    }

    private String getTabText(FaseRodada fr) {
        if (isAdded()) {
            return String.format(" %s ", fr.getFaseToView(getContext()));
        }
        return "";
    }

    @Override
    protected boolean needConfigToolbar() {
        return false;
    }

    @Override
    protected String getToolbarTitle() {
        return "";
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.DRAWER;
    }

}

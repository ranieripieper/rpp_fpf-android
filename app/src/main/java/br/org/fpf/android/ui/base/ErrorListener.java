package br.org.fpf.android.ui.base;

/**
 * Created by ranipieper on 1/14/16.
 */
public interface ErrorListener {

    void retry();
    void cancel();
}

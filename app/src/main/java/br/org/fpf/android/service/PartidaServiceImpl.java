package br.org.fpf.android.service;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import br.org.fpf.android.cache.PartidaCacheManager;
import br.org.fpf.android.model.Partida;
import br.org.fpf.android.model.response.PartidaResponse;
import br.org.fpf.android.util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranieripieper on 1/27/16.
 */
public class PartidaServiceImpl {

    public static void getPartidasInCache(final Callback<List<Partida>> callback) {
        List<Partida> list = PartidaCacheManager.getInstance().getAll();
        if (list.isEmpty()) {
            getPartidas(callback);
        } else {
            callback.success(list, null);
        }
    }

    public static List<Partida> getPartidasInCache(Long idCampeonato, String fase, Long rodada) {
        List<Partida> list = PartidaCacheManager.getInstance().getPartidasByIdCampeonatoFaseRodada(idCampeonato, fase, rodada);
        return new ArrayList<>(list);
    }

    public static void getPartidas(final Long idCampeonato, final String fase, final Long rodada, final Callback<List<Partida>> callback) {
        String where = String.format("{\"IdCampeonato\":%d,\"Fase\":\"%s\",\"Rodada\":%d}", idCampeonato, fase, rodada);

        RetrofitManager.getInstance().getPartidaService().getPartidas(where, Constants.REG_POR_PAG_MAX, 0, new Callback<PartidaResponse>() {
            @Override
            public void success(PartidaResponse partidaResponse, Response response) {
                //salva as partidas
                if (!partidaResponse.isEmpty()) {
                    PartidaCacheManager.getInstance().putAll(partidaResponse.getResults());
                    PartidaCacheManager.getInstance().getRealm().refresh();
                }
                if (callback != null) {
                    List<Partida> result = new ArrayList();
                    result.addAll(PartidaCacheManager.getInstance().getPartidasByIdCampeonatoFaseRodada(idCampeonato, fase, rodada));
                    callback.success(result, response);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.failure(error);
                }
            }
        });
    }

    public static void getPartidas(final Long idCampeonato, final Callback<List<Partida>> callback) {

        String idCampeonatoWhere = String.format("{\"IdCampeonato\":%d}", idCampeonato);

        RetrofitManager.getInstance().getPartidaService().getPartidas(idCampeonatoWhere, Constants.REG_POR_PAG_MAX, 0, new Callback<PartidaResponse>() {
            @Override
            public void success(PartidaResponse partidaResponse, Response response) {
                //salva as partidas
                if (!partidaResponse.isEmpty()) {
                    PartidaCacheManager.getInstance().putAll(partidaResponse.getResults());
                }
                if (callback != null) {
                    List<Partida> result = new ArrayList();
                    result.addAll(PartidaCacheManager.getInstance().getPartidasByIdCampeonato(idCampeonato));
                    callback.success(result, response);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.failure(error);
                }
            }
        });
    }

    public static void getPartidas(final Callback<List<Partida>> callback) {

        RetrofitManager.getInstance().getPartidaService().getPartidas(Constants.REG_POR_PAG_MAX, 0, new Callback<PartidaResponse>() {
            @Override
            public void success(PartidaResponse partidaResponse, Response response) {
                //salva as partidas
                if (!partidaResponse.isEmpty()) {
                    PartidaCacheManager.getInstance().putAll(partidaResponse.getResults());
                }
                if (callback != null) {
                    List<Partida> result = new ArrayList();
                    result.addAll(PartidaCacheManager.getInstance().getAll());
                    callback.success(result, response);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.failure(error);
                }
            }
        });
    }

    public static void getPartida(final Context ctx, final Long idPartida, final Callback<Partida> callback) {
        String idPartidaWhere = String.format("{\"Id\":%d}", idPartida);

        RetrofitManager.getInstance().getPartidaService().getPartidas(idPartidaWhere, Constants.REG_POR_PAG_MAX, 0, new Callback<PartidaResponse>() {
            @Override
            public void success(PartidaResponse partidaResponse, Response response) {
                //salva as partidas
                if (!partidaResponse.isEmpty()) {
                    PartidaCacheManager.getInstance().putAll(ctx, partidaResponse.getResults());
                }
                if (callback != null) {
                    List<Partida> result = new ArrayList();
                    result.addAll(PartidaCacheManager.getInstance().getAll());
                    callback.success(PartidaCacheManager.getInstance().get(idPartida), response);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.failure(error);
                }
            }
        });
    }

}

package br.org.fpf.android.ui.config;

import android.os.Bundle;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;

import br.org.fpf.android.R;
import br.org.fpf.android.ui.base.BaseFragment;

/**
 * Created by ranipieper on 1/15/16.
 */
@EFragment(R.layout.fragment_about)
public class AboutFragment extends BaseFragment {

    public static AboutFragment newInstance() {
        Bundle args = new Bundle();

        AboutFragment_ fragment = new AboutFragment_();
        fragment.setArguments(args);

        return fragment;
    }

    @AfterViews
    public void afterViews() {
        super.afterViews();
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.about);
    }


    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.DRAWER;
    }
}

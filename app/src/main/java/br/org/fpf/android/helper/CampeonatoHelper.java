package br.org.fpf.android.helper;

import android.text.TextUtils;

import br.org.fpf.android.model.Campeonato;

/**
 * Created by ranieripieper on 5/4/16.
 */
public class CampeonatoHelper {

    public static String getNome(Campeonato campeonato) {
        if (TextUtils.isEmpty(campeonato.getNomeCampeonatoApp())) {
            return campeonato.getNome();
        }
        return campeonato.getNomeCampeonatoApp();
    }
}

package br.org.fpf.android.service;

import java.util.ArrayList;
import java.util.List;

import br.org.fpf.android.cache.NarracaoCacheManager;
import br.org.fpf.android.model.Narracao;
import br.org.fpf.android.model.response.NarracaoResponse;
import br.org.fpf.android.util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranieripieper on 1/30/16.
 */
public class NarracaoServiceImpl {

    public static void getNarracao(boolean cache, final Long idPartida, final Long lastId, final Callback<List<Narracao>> callback) {
        if (cache) {
            getNarracaoInCache(idPartida, lastId, callback);
        } else {
            getNarracao(idPartida, lastId, callback);
        }
    }

    public static void getNarracaoInCache(final Long idPartida, final Long lastId, final Callback<List<Narracao>> callback) {
        List<Narracao> lst = NarracaoCacheManager.getInstance().getNarracaoByIdPartida(idPartida, lastId);
        if ((lst == null || lst.isEmpty()) && lastId == null) {
            getNarracao(idPartida, null, callback);
        } else {
            if (callback != null) {
                List<Narracao> result = new ArrayList();
                result.addAll(lst);
                callback.success(result, null);
            }
        }
    }

    public static void getNarracao(final Long idPartida, final Long lastId, final Callback<List<Narracao>> callback) {

        String queryLastId = "";
        if (lastId != null) {
            queryLastId = String.format(",\"Id\":{\"$gt\":%d}", lastId);
        }
        String idPartidaWhere = String.format("{\"IdPartida\":%d%s}", idPartida, queryLastId);

        RetrofitManager.getInstance().getNarracaoService().getNarracao(idPartidaWhere, Constants.REG_POR_PAG_MAX, 0, new Callback<NarracaoResponse>() {
            @Override
            public void success(NarracaoResponse narracaoResponse, Response response) {
                //salva as partidas
                if (!narracaoResponse.isEmpty()) {
                    NarracaoCacheManager.getInstance().putAll(narracaoResponse.getResults());
                }
                if (callback != null) {
                    List<Narracao> result = new ArrayList();
                    result.addAll(NarracaoCacheManager.getInstance().getNarracaoByIdPartida(idPartida, lastId));
                    callback.success(result, response);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.failure(error);
                }
            }
        });
    }
}

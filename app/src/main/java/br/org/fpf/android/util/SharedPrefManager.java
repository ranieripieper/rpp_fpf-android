package br.org.fpf.android.util;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefManager {

    private static SharedPrefManager sharedPrefManager;
    private Context mContext;

	private static final String PREFERENCES = SharedPrefManager.class + "";

    private static final String FIRST_ACCESS = "FIRST_ACCESS";
    private static final String SHOW_INFO_ESTATISTICA_TIME = "SHOW_INFO_ESTATISTICA_TIME";
    private static final String LAST_CAMPEONATO_SELECTED = "LAST_CAMPEONATO_SELECTED";
    private static final String DELETE_GUIA_PAULISTAO = "DELETE_GUIA_PAULISTAO";

    private SharedPrefManager(Context context) {
        this.mContext = context;
    }

    public static SharedPrefManager getInstance() {
        return sharedPrefManager;
    }

    public static void init(Application application) {
        sharedPrefManager = new SharedPrefManager(application);
    }

	private SharedPreferences getSharedPreferences() {
		return mContext.getSharedPreferences(PREFERENCES, 0);
	}

    public Long getLastCampeonatoSelected() {
        long id = getSharedPreferences().getLong(LAST_CAMPEONATO_SELECTED, Constants.ID_BRASILEIRAO);
        if (id > 0) {
            return id;
        }
        return null;
    }

    public void setLastCampeonatoSelected(Long value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putLong(LAST_CAMPEONATO_SELECTED, value);
        editor.commit();
    }

    public boolean isFirstAccess() {
        return getSharedPreferences().getBoolean(FIRST_ACCESS, true);
    }

    public void setShowInfoEstatisticaTime(boolean value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putBoolean(SHOW_INFO_ESTATISTICA_TIME, value);
        editor.commit();
    }

    public boolean isDeleteGuiaPaulistao() {
        return getSharedPreferences().getBoolean(DELETE_GUIA_PAULISTAO, true);
    }

    public void setDeleteGuiaPaulistao(boolean value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putBoolean(DELETE_GUIA_PAULISTAO, value);
        editor.commit();
    }

    public boolean isShowInfoEstatisticaTime() {
        return getSharedPreferences().getBoolean(SHOW_INFO_ESTATISTICA_TIME, true);
    }

    public void setFirstAccess(boolean value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putBoolean(FIRST_ACCESS, value);
        editor.commit();
    }



    private SharedPreferences.Editor getEditor() {
		return getSharedPreferences().edit();
	}

}

package br.org.fpf.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ranieripieper on 1/30/16.
 */
public class Narracao extends RealmObject {

    @Expose
    @PrimaryKey
    @SerializedName("Id")
    private Long id;

    @Expose
    @SerializedName("AcaoImportante")
    private String acaoImportante;

    @Expose
    @SerializedName("Descricao")
    private String descricao;

    @Expose
    @SerializedName("IdEquipe")
    private Long idEquipe;

    @Expose
    @SerializedName("IdJogador")
    private Long idJogador;

    @Expose
    @SerializedName("IdSubstituto")
    private Long idSubstituto;

    @Expose
    @SerializedName("IdPartida")
    private Long idPartida;

    @Expose
    @SerializedName("Momento")
    private String momento;

    @Expose
    @SerializedName("NomeEquipe")
    private String nomeEquipe;

    @Expose
    @SerializedName("NomeJogador")
    private String nomeJogador;

    @Expose
    @SerializedName("NomeSubstituto")
    private String nomeSubstituto;

    @Expose
    @SerializedName("Periodo")
    private String periodo;

    @Expose
    @SerializedName("objectId")
    private String objectId;

    @Expose
    @SerializedName("createdAt")
    private Date createdAt;

    @Expose
    @SerializedName("updatedAt")
    private Date updatedAt;

    /**
     * Gets the id
     *
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the acaoImportante
     *
     * @return acaoImportante
     */
    public String getAcaoImportante() {
        return acaoImportante;
    }

    /**
     * Sets the acaoImportante
     *
     * @param acaoImportante
     */
    public void setAcaoImportante(String acaoImportante) {
        this.acaoImportante = acaoImportante;
    }

    /**
     * Gets the descricao
     *
     * @return descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Sets the descricao
     *
     * @param descricao
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * Gets the idEquipe
     *
     * @return idEquipe
     */
    public Long getIdEquipe() {
        return idEquipe;
    }

    /**
     * Sets the idEquipe
     *
     * @param idEquipe
     */
    public void setIdEquipe(Long idEquipe) {
        this.idEquipe = idEquipe;
    }

    /**
     * Gets the idJogador
     *
     * @return idJogador
     */
    public Long getIdJogador() {
        return idJogador;
    }

    /**
     * Sets the idJogador
     *
     * @param idJogador
     */
    public void setIdJogador(Long idJogador) {
        this.idJogador = idJogador;
    }

    /**
     * Gets the idSubstituto
     *
     * @return idSubstituto
     */
    public Long getIdSubstituto() {
        return idSubstituto;
    }

    /**
     * Sets the idSubstituto
     *
     * @param idSubstituto
     */
    public void setIdSubstituto(Long idSubstituto) {
        this.idSubstituto = idSubstituto;
    }

    /**
     * Gets the idPartida
     *
     * @return idPartida
     */
    public Long getIdPartida() {
        return idPartida;
    }

    /**
     * Sets the idPartida
     *
     * @param idPartida
     */
    public void setIdPartida(Long idPartida) {
        this.idPartida = idPartida;
    }

    /**
     * Gets the momento
     *
     * @return momento
     */
    public String getMomento() {
        return momento;
    }

    /**
     * Sets the momento
     *
     * @param momento
     */
    public void setMomento(String momento) {
        this.momento = momento;
    }

    /**
     * Gets the nomeEquipe
     *
     * @return nomeEquipe
     */
    public String getNomeEquipe() {
        return nomeEquipe;
    }

    /**
     * Sets the nomeEquipe
     *
     * @param nomeEquipe
     */
    public void setNomeEquipe(String nomeEquipe) {
        this.nomeEquipe = nomeEquipe;
    }

    /**
     * Gets the nomeJogador
     *
     * @return nomeJogador
     */
    public String getNomeJogador() {
        return nomeJogador;
    }

    /**
     * Sets the nomeJogador
     *
     * @param nomeJogador
     */
    public void setNomeJogador(String nomeJogador) {
        this.nomeJogador = nomeJogador;
    }

    /**
     * Gets the nomeSubstituto
     *
     * @return nomeSubstituto
     */
    public String getNomeSubstituto() {
        return nomeSubstituto;
    }

    /**
     * Sets the nomeSubstituto
     *
     * @param nomeSubstituto
     */
    public void setNomeSubstituto(String nomeSubstituto) {
        this.nomeSubstituto = nomeSubstituto;
    }

    /**
     * Gets the periodo
     *
     * @return periodo
     */
    public String getPeriodo() {
        return periodo;
    }

    /**
     * Sets the periodo
     *
     * @param periodo
     */
    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    /**
     * Gets the objectId
     *
     * @return objectId
     */
    public String getObjectId() {
        return objectId;
    }

    /**
     * Sets the objectId
     *
     * @param objectId
     */
    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    /**
     * Gets the createdAt
     *
     * @return createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * Sets the createdAt
     *
     * @param createdAt
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Gets the updatedAt
     *
     * @return updatedAt
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * Sets the updatedAt
     *
     * @param updatedAt
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}

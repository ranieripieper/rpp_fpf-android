package br.org.fpf.android.ui.campeonato;

import android.os.Bundle;
import android.view.View;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;

import java.util.ArrayList;
import java.util.List;

import br.org.fpf.android.R;
import br.org.fpf.android.model.Partida;
import br.org.fpf.android.service.PartidaServiceImpl;
import br.org.fpf.android.ui.campeonato.adapter.PartidaViewHolder;
import br.org.fpf.android.ui.navigation.Navigator;
import br.org.fpf.android.util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;

/**
 * Created by ranipieper on 1/14/16.
 */
@EFragment(R.layout.fragment_campeonato_pages)
public class TabelaFragment extends CampeonatoPageBaseFragment implements PartidaViewHolder.PartidaHolderListener {

    @FragmentArg(Constants.ARG_FASE)
    String mFase;

    @FragmentArg(Constants.ARG_RODADA)
    Long mRodada;

    @FragmentArg(Constants.ARG_GROUP_PARTIDAS)
    boolean mGroupPartidas = false;

    private boolean mInit = false;

    public static TabelaFragment newInstance(Long idCampeonato, String fase, long rodada, boolean groupPartidas) {
        Bundle args = new Bundle();

        TabelaFragment_ fragment = new TabelaFragment_();

        args.putLong(Constants.ARG_ID_CAMPEONATO, idCampeonato);
        args.putString(Constants.ARG_FASE, fase);
        args.putLong(Constants.ARG_RODADA, rodada);
        args.putBoolean(Constants.ARG_GROUP_PARTIDAS, groupPartidas);

        fragment.setArguments(args);

        return fragment;
    }

    @AfterViews
    public void afterViews() {
        super.afterViews();
    }

    @Override
    protected void preLoadData() {
        if (!mInit) {
            mInit = true;
            forceUpdate = false;
        }
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            updateView();
        }
    }

    private void updateView() {

    }

    @Override
    protected void callService() {
        if (isAdded()) {
            if (forceUpdate) {
                PartidaServiceImpl.getPartidas(mIdCampeonato, mFase, mRodada, new Callback<List<Partida>>() {
                    @Override
                    public void success(List<Partida> partidaList, Response response) {
                        processResponse(partidaList);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        showError();
                    }
                });
            } else {
                hideLoadingView(mSwipeRefreshLayout);
                List<Partida> list = PartidaServiceImpl.getPartidasInCache(mIdCampeonato, mFase, mRodada);
                processResponse(list);
            }
        }
    }

    protected void processResponse(List<Partida> partidaList) {

        if (!isAdded()) {
            return;
        }
        if (partidaList == null || partidaList.isEmpty()) {
            showErrorTextView(mTxtErrorNoResult, R.string.error_generic);
        } else {
            disableLoadingMore(mRecyclerView, partidaList.size());
            renderPartidas(partidaList);
            mRecyclerView.setVisibility(View.VISIBLE);
        }
        hideLoadingView(mSwipeRefreshLayout);

    }

    public void renderPartidas(List<Partida> partidaList) {
        if (!isAdded()) {
            return;
        }
        if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }

        if (mAdapter == null) {
            createAdapter(getPartidasSorted(partidaList));
        } else {
            mAdapter.removeAllItems();
            mAdapter.addItems(getPartidasSorted(partidaList));
        }
        if (mRecyclerView.getAdapter() == null) {
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    private List<Partida> getPartidasSorted(List<Partida> partidaList) {
        if (mGroupPartidas && partidaList != null) {
            List<Partida> result = new ArrayList<>();
            for (Partida p : partidaList) {
                if (result.contains(p)) {
                    continue;
                }
                result.add(p);
                Partida jogoVolta = getPartidaJogoVolta(p, partidaList);
                if (jogoVolta != null && !result.contains(jogoVolta)) {
                    result.add(jogoVolta);
                }
            }

            return result;
        }
        return partidaList;
    }

    private Partida getPartidaJogoVolta(Partida partidaIda, List<Partida> partidaList) {
        if (partidaIda != null && partidaList != null) {
            for (Partida p : partidaList) {
                if (p.getIdEquipeMandante().equals(partidaIda.getIdEquipeVisitante()) &&
                        p.getIdEquipeVisitante().equals(partidaIda.getIdEquipeMandante())   ) {
                    return p;
                }
            }
        }
        return null;
    }

    protected void createAdapter(List<Partida> partidaList) {
        mAdapter = new EasyLoadMoreRecyclerAdapter(
                getActivity(),
                PartidaViewHolder.class,
                partidaList,
                this,
                R.layout.loading_recycler_view);
    }

    @Override
    public void onResumeFromBackStack() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
        synchronized (itemSelected) {
            itemSelected = false;
        }
        super.onResumeFromBackStack();
    }

    protected boolean isEnableLoadMore() {
        return false;
    }

    private Boolean itemSelected = false;

    @Override
    public void onItemSelected(Partida partida) {
        if (partida.isTemNarracao() || partida.isTemEstatistica()) {
            synchronized (itemSelected) {
                if (!itemSelected) {
                    Navigator.navigateToPartidaFragment(getContext(), mIdCampeonato, partida.getId());
                    itemSelected = true;
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        synchronized (itemSelected) {
            itemSelected = false;
        }
    }
}

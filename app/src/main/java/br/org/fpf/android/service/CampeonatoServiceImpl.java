package br.org.fpf.android.service;

import java.util.List;

import br.org.fpf.android.cache.CampeonatoCacheManager;
import br.org.fpf.android.model.Campeonato;
import br.org.fpf.android.model.response.CampeonatoResponse;
import br.org.fpf.android.service.interfaces.CampeonatoService;
import br.org.fpf.android.util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranieripieper on 1/27/16.
 */
public class CampeonatoServiceImpl {

    public static void getCampeonatosInCache(final Callback<List<Campeonato>> callback) {
        List<Campeonato> lst = CampeonatoCacheManager.getInstance().getAllVisibles();
        if (lst.isEmpty()) {
            getCampeonatos(callback);
        } else {
            callback.success(lst, null);
        }
    }

    public static void getCampeonatos(final Callback<List<Campeonato>> callback) {

        RetrofitManager.getInstance().getCampeonatoService().getCampeonatos(CampeonatoService.TEMPORADA_2016, Constants.REG_POR_PAG_MAX, new Callback<CampeonatoResponse>() {
            @Override
            public void success(CampeonatoResponse campeonatoResponse, Response response) {
                //salva as equipes
                if (!campeonatoResponse.isEmpty()) {
                    CampeonatoCacheManager.getInstance().putAll(campeonatoResponse.getResults());
                }
                if (callback != null) {
                    callback.success(CampeonatoCacheManager.getInstance().getAllVisibles(), response);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.failure(error);
                }
            }
        });
    }
}

package br.org.fpf.android.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ranieripieper on 1/31/16.
 */
public class EquipeNotificacao extends RealmObject {

    @PrimaryKey
    private Long idEquipe;

    private String channel;

    private boolean sentToParse;

    /**
     * Gets the sentToParse
     *
     * @return sentToParse
     */
    public boolean isSentToParse() {
        return sentToParse;
    }

    /**
     * Sets the sentToParse
     *
     * @param sentToParse
     */
    public void setSentToParse(boolean sentToParse) {
        this.sentToParse = sentToParse;
    }

    /**
     * Gets the idEquipe
     *
     * @return idEquipe
     */
    public Long getIdEquipe() {
        return idEquipe;
    }

    /**
     * Sets the idEquipe
     *
     * @param idEquipe
     */
    public void setIdEquipe(Long idEquipe) {
        this.idEquipe = idEquipe;
    }

    /**
     * Gets the channel
     *
     * @return channel
     */
    public String getChannel() {
        return channel;
    }

    /**
     * Sets the channel
     *
     * @param channel
     */
    public void setChannel(String channel) {
        this.channel = channel;
    }
}

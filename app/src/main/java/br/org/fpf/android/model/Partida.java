package br.org.fpf.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ranieripieper on 1/28/16.
 */
public class Partida extends RealmObject {

    @PrimaryKey
    @Expose
    @SerializedName("Id")
    private Long id;

    private Date dtPartida;

    @Expose
    @SerializedName("Arbitro")
    private String arbitro;

    @Expose
    @SerializedName("Cidade")
    private String cidade;

    @Expose
    @SerializedName("Data")
    private Data data;

    @Expose
    @SerializedName("DataDefinir")
    private boolean dataDefinir;

    @Expose
    @SerializedName("Fase")
    private String fase;

    @Expose
    @SerializedName("Gols")
    private RealmList<Gol> gols;

    @Expose
    @SerializedName("HoraDefinir")
    private boolean horaDefinir;

    @Expose
    @SerializedName("Grupo")
    private String grupo;

    @Expose
    @SerializedName("IdCampeonato")
    private Long idCampeonato;

    @Expose
    @SerializedName("IdEquipeMandante")
    private Long idEquipeMandante;

    @Expose
    @SerializedName("IdEquipeVisitante")
    private Long idEquipeVisitante;

    @Expose
    @SerializedName("JogoNaoNecessario")
    private boolean jogoNaoNecessario;

    @Expose
    @SerializedName("NomeCampeonato")
    private String nomeCampeonato;

    @Expose
    @SerializedName("NomeEstadio")
    private String nomeEstadio;

    @Expose
    @SerializedName("NomeMandante")
    private String nomeMandante;

    @Expose
    @SerializedName("NomeVisitante")
    private String nomeVisitante;

    @Expose
    @SerializedName("NumeroJogoChave")
    private Long numeroJogoChave;

    @Expose
    @SerializedName("PeriodoAtual")
    private String periodoAtual;

    @Expose
    @SerializedName("PlacarMandante")
    private Long placarMandante;

    @Expose
    @SerializedName("PlacarPenaltisMandante")
    private Long placarPenaltisMandante;

    @Expose
    @SerializedName("PlacarPenaltisVisitante")
    private Long placarPenaltisVisitante;

    @Expose
    @SerializedName("PlacarVisitante")
    private Long placarVisitante;

    @Expose
    @SerializedName("Publico")
    private Long publico;

    @Expose
    @SerializedName("Renda")
    private Double renda;

    @Expose
    @SerializedName("Rodada")
    private Long rodada;

    @Expose
    @SerializedName("Taca")
    private String taca;

    @Expose
    @SerializedName("TecnicoMandante")
    private String tecnicoMandante;

    @Expose
    @SerializedName("TecnicoVisitante")
    private String tecnicoVisitante;

    @Expose
    @SerializedName("TemEstatistica")
    private boolean temEstatistica;

    @Expose
    @SerializedName("TemNarracao")
    private boolean temNarracao;

    @Expose
    @SerializedName("Temporada")
    private String temporada;

    @Expose
    @SerializedName("objectId")
    private String objectId;

    @Expose
    @SerializedName("createdAt")
    private Date createdAt;

    @Expose
    @SerializedName("updatedAt")
    private Date updatedAt;

    /**
     * Gets the dtPartida
     *
     * @return dtPartida
     */
    public Date getDtPartida() {
        return dtPartida;
    }

    /**
     * Sets the dtPartida
     *
     * @param dtPartida
     */
    public void setDtPartida(Date dtPartida) {
        this.dtPartida = dtPartida;
    }

    /**
     * Gets the data
     *
     * @return data
     */
    public Data getData() {
        return data;
    }

    /**
     * Sets the data
     *
     * @param data
     */
    public void setData(Data data) {
        this.data = data;
    }

    /**
     * Gets the id
     *
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the arbitro
     *
     * @return arbitro
     */
    public String getArbitro() {
        return arbitro;
    }

    /**
     * Sets the arbitro
     *
     * @param arbitro
     */
    public void setArbitro(String arbitro) {
        this.arbitro = arbitro;
    }

    /**
     * Gets the cidade
     *
     * @return cidade
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * Sets the cidade
     *
     * @param cidade
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * Gets the dataDefinir
     *
     * @return dataDefinir
     */
    public boolean isDataDefinir() {
        return dataDefinir;
    }

    /**
     * Sets the dataDefinir
     *
     * @param dataDefinir
     */
    public void setDataDefinir(boolean dataDefinir) {
        this.dataDefinir = dataDefinir;
    }

    /**
     * Gets the fase
     *
     * @return fase
     */
    public String getFase() {
        return fase;
    }

    /**
     * Sets the fase
     *
     * @param fase
     */
    public void setFase(String fase) {
        this.fase = fase;
    }

    /**
     * Gets the gols
     *
     * @return gols
     */
    public RealmList<Gol> getGols() {
        return gols;
    }

    /**
     * Sets the gols
     *
     * @param gols
     */
    public void setGols(RealmList<Gol> gols) {
        this.gols = gols;
    }

    /**
     * Gets the horaDefinir
     *
     * @return horaDefinir
     */
    public boolean isHoraDefinir() {
        return horaDefinir;
    }

    /**
     * Sets the horaDefinir
     *
     * @param horaDefinir
     */
    public void setHoraDefinir(boolean horaDefinir) {
        this.horaDefinir = horaDefinir;
    }

    /**
     * Gets the grupo
     *
     * @return grupo
     */
    public String getGrupo() {
        return grupo;
    }

    /**
     * Sets the grupo
     *
     * @param grupo
     */
    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    /**
     * Gets the idCampeonato
     *
     * @return idCampeonato
     */
    public Long getIdCampeonato() {
        return idCampeonato;
    }

    /**
     * Sets the idCampeonato
     *
     * @param idCampeonato
     */
    public void setIdCampeonato(Long idCampeonato) {
        this.idCampeonato = idCampeonato;
    }

    /**
     * Gets the idEquipeMandante
     *
     * @return idEquipeMandante
     */
    public Long getIdEquipeMandante() {
        return idEquipeMandante;
    }

    /**
     * Sets the idEquipeMandante
     *
     * @param idEquipeMandante
     */
    public void setIdEquipeMandante(Long idEquipeMandante) {
        this.idEquipeMandante = idEquipeMandante;
    }

    /**
     * Gets the idEquipeVisitante
     *
     * @return idEquipeVisitante
     */
    public Long getIdEquipeVisitante() {
        return idEquipeVisitante;
    }

    /**
     * Sets the idEquipeVisitante
     *
     * @param idEquipeVisitante
     */
    public void setIdEquipeVisitante(Long idEquipeVisitante) {
        this.idEquipeVisitante = idEquipeVisitante;
    }

    /**
     * Gets the jogoNaoNecessario
     *
     * @return jogoNaoNecessario
     */
    public boolean isJogoNaoNecessario() {
        return jogoNaoNecessario;
    }

    /**
     * Sets the jogoNaoNecessario
     *
     * @param jogoNaoNecessario
     */
    public void setJogoNaoNecessario(boolean jogoNaoNecessario) {
        this.jogoNaoNecessario = jogoNaoNecessario;
    }

    /**
     * Gets the nomeCampeonato
     *
     * @return nomeCampeonato
     */
    public String getNomeCampeonato() {
        return nomeCampeonato;
    }

    /**
     * Sets the nomeCampeonato
     *
     * @param nomeCampeonato
     */
    public void setNomeCampeonato(String nomeCampeonato) {
        this.nomeCampeonato = nomeCampeonato;
    }

    /**
     * Gets the nomeEstadio
     *
     * @return nomeEstadio
     */
    public String getNomeEstadio() {
        return nomeEstadio;
    }

    /**
     * Sets the nomeEstadio
     *
     * @param nomeEstadio
     */
    public void setNomeEstadio(String nomeEstadio) {
        this.nomeEstadio = nomeEstadio;
    }

    /**
     * Gets the nomeMandante
     *
     * @return nomeMandante
     */
    public String getNomeMandante() {
        return nomeMandante;
    }

    /**
     * Sets the nomeMandante
     *
     * @param nomeMandante
     */
    public void setNomeMandante(String nomeMandante) {
        this.nomeMandante = nomeMandante;
    }

    /**
     * Gets the nomeVisitante
     *
     * @return nomeVisitante
     */
    public String getNomeVisitante() {
        return nomeVisitante;
    }

    /**
     * Sets the nomeVisitante
     *
     * @param nomeVisitante
     */
    public void setNomeVisitante(String nomeVisitante) {
        this.nomeVisitante = nomeVisitante;
    }

    /**
     * Gets the numeroJogoChave
     *
     * @return numeroJogoChave
     */
    public Long getNumeroJogoChave() {
        return numeroJogoChave;
    }

    /**
     * Sets the numeroJogoChave
     *
     * @param numeroJogoChave
     */
    public void setNumeroJogoChave(Long numeroJogoChave) {
        this.numeroJogoChave = numeroJogoChave;
    }

    /**
     * Gets the periodoAtual
     *
     * @return periodoAtual
     */
    public String getPeriodoAtual() {
        return periodoAtual;
    }

    /**
     * Sets the periodoAtual
     *
     * @param periodoAtual
     */
    public void setPeriodoAtual(String periodoAtual) {
        this.periodoAtual = periodoAtual;
    }

    /**
     * Gets the placarMandante
     *
     * @return placarMandante
     */
    public Long getPlacarMandante() {
        return placarMandante;
    }

    /**
     * Sets the placarMandante
     *
     * @param placarMandante
     */
    public void setPlacarMandante(Long placarMandante) {
        this.placarMandante = placarMandante;
    }

    /**
     * Gets the placarPenaltisMandante
     *
     * @return placarPenaltisMandante
     */
    public Long getPlacarPenaltisMandante() {
        return placarPenaltisMandante;
    }

    /**
     * Sets the placarPenaltisMandante
     *
     * @param placarPenaltisMandante
     */
    public void setPlacarPenaltisMandante(Long placarPenaltisMandante) {
        this.placarPenaltisMandante = placarPenaltisMandante;
    }

    /**
     * Gets the placarPenaltisVisitante
     *
     * @return placarPenaltisVisitante
     */
    public Long getPlacarPenaltisVisitante() {
        return placarPenaltisVisitante;
    }

    /**
     * Sets the placarPenaltisVisitante
     *
     * @param placarPenaltisVisitante
     */
    public void setPlacarPenaltisVisitante(Long placarPenaltisVisitante) {
        this.placarPenaltisVisitante = placarPenaltisVisitante;
    }

    /**
     * Gets the placarVisitante
     *
     * @return placarVisitante
     */
    public Long getPlacarVisitante() {
        return placarVisitante;
    }

    /**
     * Sets the placarVisitante
     *
     * @param placarVisitante
     */
    public void setPlacarVisitante(Long placarVisitante) {
        this.placarVisitante = placarVisitante;
    }

    /**
     * Gets the publico
     *
     * @return publico
     */
    public Long getPublico() {
        return publico;
    }

    /**
     * Sets the publico
     *
     * @param publico
     */
    public void setPublico(Long publico) {
        this.publico = publico;
    }

    /**
     * Gets the renda
     *
     * @return renda
     */
    public Double getRenda() {
        return renda;
    }

    /**
     * Sets the renda
     *
     * @param renda
     */
    public void setRenda(Double renda) {
        this.renda = renda;
    }

    /**
     * Gets the rodada
     *
     * @return rodada
     */
    public Long getRodada() {
        return rodada;
    }

    /**
     * Sets the rodada
     *
     * @param rodada
     */
    public void setRodada(Long rodada) {
        this.rodada = rodada;
    }

    /**
     * Gets the taca
     *
     * @return taca
     */
    public String getTaca() {
        return taca;
    }

    /**
     * Sets the taca
     *
     * @param taca
     */
    public void setTaca(String taca) {
        this.taca = taca;
    }

    /**
     * Gets the tecnicoMandante
     *
     * @return tecnicoMandante
     */
    public String getTecnicoMandante() {
        return tecnicoMandante;
    }

    /**
     * Sets the tecnicoMandante
     *
     * @param tecnicoMandante
     */
    public void setTecnicoMandante(String tecnicoMandante) {
        this.tecnicoMandante = tecnicoMandante;
    }

    /**
     * Gets the tecnicoVisitante
     *
     * @return tecnicoVisitante
     */
    public String getTecnicoVisitante() {
        return tecnicoVisitante;
    }

    /**
     * Sets the tecnicoVisitante
     *
     * @param tecnicoVisitante
     */
    public void setTecnicoVisitante(String tecnicoVisitante) {
        this.tecnicoVisitante = tecnicoVisitante;
    }

    /**
     * Gets the temEstatistica
     *
     * @return temEstatistica
     */
    public boolean isTemEstatistica() {
        return temEstatistica;
    }

    /**
     * Sets the temEstatistica
     *
     * @param temEstatistica
     */
    public void setTemEstatistica(boolean temEstatistica) {
        this.temEstatistica = temEstatistica;
    }

    /**
     * Gets the temNarracao
     *
     * @return temNarracao
     */
    public boolean isTemNarracao() {
        return temNarracao;
    }

    /**
     * Sets the temNarracao
     *
     * @param temNarracao
     */
    public void setTemNarracao(boolean temNarracao) {
        this.temNarracao = temNarracao;
    }

    /**
     * Gets the temporada
     *
     * @return temporada
     */
    public String getTemporada() {
        return temporada;
    }

    /**
     * Sets the temporada
     *
     * @param temporada
     */
    public void setTemporada(String temporada) {
        this.temporada = temporada;
    }

    /**
     * Gets the objectId
     *
     * @return objectId
     */
    public String getObjectId() {
        return objectId;
    }

    /**
     * Sets the objectId
     *
     * @param objectId
     */
    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    /**
     * Gets the createdAt
     *
     * @return createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * Sets the createdAt
     *
     * @param createdAt
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Gets the updatedAt
     *
     * @return updatedAt
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * Sets the updatedAt
     *
     * @param updatedAt
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}

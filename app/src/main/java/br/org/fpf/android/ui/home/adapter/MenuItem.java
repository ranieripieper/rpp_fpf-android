package br.org.fpf.android.ui.home.adapter;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ranipieper on 8/26/16.
 */
public class MenuItem implements ParentListItem {

    private String menu;
    private int resource;
    private boolean selected;
    private List<SubMenuItem> childs = new ArrayList<>();
    private MenuItemListener listener;

    public MenuItem(String menu, int resource, boolean selected, List<SubMenuItem> childs, MenuItemListener listener) {
        this.menu = menu;
        this.resource = resource;
        this.selected = selected;
        this.childs = childs;
        this.listener = listener;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return selected;
    }

    @Override
    public List<?> getChildItemList() {
        return childs;
    }

    public MenuItem(String menu, int resource, boolean selected, MenuItemListener listener) {
        this.menu = menu;
        this.resource = resource;
        this.selected = selected;
        this.listener = listener;
    }

    public MenuItem(String menu, int resource,List<SubMenuItem> childs, MenuItemListener listener) {
        this.menu = menu;
        this.resource = resource;
        this.childs = childs;
        this.listener = listener;
    }

    public MenuItem(String menu, int resource, MenuItemListener listener) {
        this.menu = menu;
        this.resource = resource;
        this.listener = listener;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public int getResource() {
        return resource;
    }

    public void setResource(int resource) {
        this.resource = resource;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public List<SubMenuItem> getChilds() {
        return childs;
    }

    public void setChilds(List<SubMenuItem> childs) {
        this.childs = childs;
    }

    public MenuItemListener getListener() {
        return listener;
    }

    public void setListener(MenuItemListener listener) {
        this.listener = listener;
    }

    public interface MenuItemListener {
        void menuClick(MenuItem menuItem);
    }
}

package br.org.fpf.android.service.interfaces;

import java.util.List;

import br.org.fpf.android.model.Noticia;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by ranieripieper on 5/4/16.
 */
public interface NoticiaService {

    @GET("/apinoticias/campeonato/{id_campeonato}")
    void getNoticias(@Path("id_campeonato") Long idCampeonato, Callback<List<Noticia>> response);

    @GET("/")
    void getNoticia(Callback<Noticia> response);
}

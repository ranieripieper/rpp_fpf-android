package br.org.fpf.android.ui.campeonato;

import android.os.Bundle;
import android.view.View;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;

import java.util.List;

import br.org.fpf.android.R;
import br.org.fpf.android.cache.ClassificacaoCacheManager;
import br.org.fpf.android.model.Classificacao;
import br.org.fpf.android.service.ClassificacaoServiceImpl;
import br.org.fpf.android.ui.campeonato.adapter.ClassificacaoAdapter;
import br.org.fpf.android.ui.campeonato.adapter.ClassificacaoViewHolder;
import br.org.fpf.android.ui.navigation.Navigator;
import br.org.fpf.android.util.Constants;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderDecoration;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 1/14/16.
 */
@EFragment(R.layout.fragment_campeonato_pages)
public class ClassificacaoFragment extends CampeonatoPageBaseFragment implements ClassificacaoViewHolder.ClassificacaoHolderListener {

    @FragmentArg(Constants.ARG_FASE)
    String mFase;

    private boolean mInit = false;
    private Boolean itemSelected = false;

    public static ClassificacaoFragment newInstance(Long idCampeonato, String fase) {
        Bundle args = new Bundle();

        ClassificacaoFragment_ fragment = new ClassificacaoFragment_();
        args.putLong(Constants.ARG_ID_CAMPEONATO, idCampeonato);
        args.putString(Constants.ARG_FASE, fase);

        fragment.setArguments(args);

        return fragment;
    }

    @AfterViews
    public void afterViews() {
        super.afterViews();
    }

    @Override
    protected void preLoadData() {
        if (!mInit) {
            mInit = true;
            forceUpdate = false;
        }
    }

    @Override
    protected void callService() {
        if (isAdded()) {
            mTxtErrorNoResult.setVisibility(View.GONE);
            if (forceUpdate) {
                ClassificacaoServiceImpl.getClassificacao(mIdCampeonato, mFase, new Callback<List<Classificacao>>() {
                    @Override
                    public void success(List<Classificacao> classificacao, Response response) {
                        processResponse(classificacao);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        showError();
                    }
                });
            } else {
                List<Classificacao> classificacaoList = ClassificacaoServiceImpl.getClassificacaoInCache(mIdCampeonato, mFase);
                processResponse(classificacaoList);
                hideLoadingView(mSwipeRefreshLayout);
            }
        }
    }

    protected void processResponse(List<Classificacao> classificacaoList) {
        hideLoadingView();
        if (!isAdded()) {
            return;
        }
        if (classificacaoList == null || classificacaoList.isEmpty()) {
            showErrorTextView(mTxtErrorNoResult, R.string.msg_artilharia_vazia);
        } else {
            disableLoadingMore(mRecyclerView, classificacaoList.size());
            renderClassificacao(classificacaoList);
            mRecyclerView.setVisibility(View.VISIBLE);
        }
    }

    public void renderClassificacao(List<Classificacao> classificacaoLists) {
        if (!isAdded()) {
            return;
        }
        if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }

        if (mAdapter == null) {
            createAdapter(classificacaoLists);
        } else {
            mAdapter.removeAllItems();
            mAdapter.addItems(classificacaoLists);
        }

        if (mRecyclerView.getAdapter() == null) {
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    protected void createAdapter(List<Classificacao> classificacaoLists) {
        boolean hasMoreTwoGroups = false;

        if (classificacaoLists != null && classificacaoLists.size() > 1) {
            if (ClassificacaoCacheManager.getInstance().hasOtherGroup(mIdCampeonato, classificacaoLists.get(0).getFase(), classificacaoLists.get(0).getGrupo()) > 0) {
                hasMoreTwoGroups = true;
            }
        }

        mAdapter = new ClassificacaoAdapter(
                getActivity(),
                ClassificacaoViewHolder.class,
                classificacaoLists,
                this,
                R.layout.loading_recycler_view,
                hasMoreTwoGroups);

        mAdapter.notifyDataSetChanged();

        StickyHeaderDecoration mStickyHeaderDecoration = new StickyHeaderDecoration((ClassificacaoAdapter) mAdapter);
        mRecyclerView.addItemDecoration(mStickyHeaderDecoration);

    }

    protected boolean isEnableLoadMore() {
        return false;
    }

    @Override
    public void onTimeSelected(Classificacao classificacao) {
        if (Constants.ID_A1.equals(mIdCampeonato)) {
            synchronized (itemSelected) {
                if (!itemSelected) {
                    Navigator.navigateToEstatisticasTimeFragment(getContext(), classificacao.getIdEquipe());
                }
            }
        }
    }

    @Override
    public void onResumeFromBackStack() {
        super.onResumeFromBackStack();
        synchronized (itemSelected) {
            itemSelected = false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        synchronized (itemSelected) {
            itemSelected = false;
        }
    }
}

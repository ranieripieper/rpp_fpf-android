package br.org.fpf.android.cache;

import android.content.Context;

import br.org.fpf.android.cache.base.BaseCache;
import br.org.fpf.android.model.EscalacaoJogador;

/**
 * Created by ranipieper on 11/19/15.
 */
public class EscalacaoJogadorCacheManager extends BaseCache<EscalacaoJogador> {

    private static EscalacaoJogadorCacheManager instance = new EscalacaoJogadorCacheManager();

    private EscalacaoJogadorCacheManager() {
    }

    public static EscalacaoJogadorCacheManager getInstance() {
        return instance;
    }

    public EscalacaoJogador getEscalacaoJogadorByIdPartida(Context ctx, Long idPartida, String idJogador) {
        getRealm(ctx).refresh();
        EscalacaoJogador result = getRealm(ctx).where(EscalacaoJogador.class)
                .equalTo("idJogador", idJogador)
                .equalTo("idPartida", idPartida)
                .findFirst();
        return getRealm(ctx).copyFromRealm(result);
    }

    public EscalacaoJogador updateEscalacaoJogador(Context ctx, String id, int nrCartaoAmarelo, int nrCartaoVerm, int nrGols, boolean substitudo) {
        getRealm(ctx).refresh();
        getRealm(ctx).beginTransaction();
        EscalacaoJogador ej = getToUpdate(id);
        if (ej != null) {
            ej.setNrCartaoVermelho(nrCartaoVerm);
            ej.setNrCartaoAmarelo(nrCartaoAmarelo);
            ej.setNrGols(nrGols);
            ej.setSubstitudo(substitudo);
        }
        getRealm(ctx).commitTransaction();
        return ej;
    }

    @Override
    public Class<EscalacaoJogador> getReferenceClass() {
        return EscalacaoJogador.class;
    }

}

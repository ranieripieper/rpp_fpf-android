package br.org.fpf.android.ui.campeonato;

import android.os.Bundle;
import android.view.View;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;

import java.util.ArrayList;
import java.util.List;

import br.org.fpf.android.BuildConfig;
import br.org.fpf.android.R;
import br.org.fpf.android.model.Artilharia;
import br.org.fpf.android.model.Equipe;
import br.org.fpf.android.model.response.ArtilhariaResponse;
import br.org.fpf.android.service.EquipeServiceImpl;
import br.org.fpf.android.service.RetrofitManager;
import br.org.fpf.android.service.interfaces.ArtilhariaService;
import br.org.fpf.android.ui.campeonato.adapter.ArtilhariaViewHolder;
import br.org.fpf.android.util.Constants;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;

/**
 * Created by ranipieper on 1/26/16.
 */
@EFragment(R.layout.fragment_campeonato_pages)
public class ArtilhariaFragment extends CampeonatoPageBaseFragment {

    private Integer mLastPosition = 0;
    private Integer mItemCount = 0;
    private Integer mLastTotalGols = -1;

    private List<Artilharia> mArtilharia = new ArrayList<>();

    public static ArtilhariaFragment newInstance(Long idCampeonato) {
        Bundle args = new Bundle();

        ArtilhariaFragment_ fragment = new ArtilhariaFragment_();
        args.putLong(Constants.ARG_ID_CAMPEONATO, idCampeonato);

        fragment.setArguments(args);

        return fragment;
    }

    @AfterViews
    public void afterViews() {
        super.afterViews();
    }

    @Override
    protected void preLoadData() {
        if (mPage == 0) {
            mArtilharia = new ArrayList<>();
            mLastPosition = 0;
            mLastTotalGols = -1;
            mItemCount = 0;
            if (mAdapter != null) {
                mAdapter.removeAllItems();
            }
        }
    }

    protected void callService() {
        EquipeServiceImpl.getEquipes(true, new Callback<List<Equipe>>() {
            @Override
            public void success(List<Equipe> equipes, Response response) {
                callServiceArtilharia();
            }

            @Override
            public void failure(RetrofitError error) {
                if (BuildConfig.DEBUG) {
                    error.printStackTrace();
                }
                showError();
            }
        });
    }

    protected void callServiceArtilharia() {
        if (mPage != 0 && !forceUpdate && !mArtilharia.isEmpty()) {
            processResponse(mArtilharia);
            hideLoadingView(mSwipeRefreshLayout);
        } else {
            RetrofitManager.getInstance().getArtilhariaService().getArtilharia(String.format("{\"IdCampeonato\":%d}", mIdCampeonato), ArtilhariaService.DEFAULT_ORDER, Constants.REG_POR_PAG, Constants.REG_POR_PAG * mPage, new Callback<ArtilhariaResponse>() {
                @Override
                public void success(ArtilhariaResponse artilhariaResponse, Response response) {
                    if (isAdded()) {
                        if (!artilhariaResponse.isEmpty()) {
                            mArtilharia.addAll(artilhariaResponse.getResults());
                            processResponse(artilhariaResponse.getResults());
                            forceUpdate = false;
                        } else {
                            processResponse(null);
                        }

                        hideLoadingView(mSwipeRefreshLayout);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    showError();
                }
            });
        }
    }

    protected void processResponse(List<Artilharia> artilhariaList) {
        hideLoadingView();
        if (!isAdded()) {
            return;
        }
        if ((artilhariaList == null || artilhariaList.isEmpty()) && mPage < 1) {
            showErrorTextView(mTxtErrorNoResult, R.string.msg_artilharia_vazia);
        } else if (artilhariaList != null) {
            disableLoadingMore(mRecyclerView, artilhariaList.size());
            renderArtilharia(artilhariaList);
            mRecyclerView.setVisibility(View.VISIBLE);
        }

    }

    public void renderArtilharia(List<Artilharia> artilhariaList) {
        if (!isAdded()) {
            return;
        }
        if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }

        //seta a posição
        if (artilhariaList != null) {
            for (Artilharia artilharia : artilhariaList) {
                mItemCount++;
                if (artilharia.getTotal() != mLastTotalGols) {
                    //mLastPosition = mItemCount;
                    mLastPosition++;
                }

                mLastTotalGols = artilharia.getTotal();
                artilharia.setPosition(mLastPosition);
            }
        }

        if (mAdapter == null) {
            createAdapter(new ArrayList<>(artilhariaList));
        } else {
            mAdapter.addItems(artilhariaList);
        }

        if (mRecyclerView.getAdapter() == null) {
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    protected void createAdapter(List<Artilharia> artilhariaList) {
        mAdapter = new EasyLoadMoreRecyclerAdapter(
                getActivity(),
                ArtilhariaViewHolder.class,
                artilhariaList,
                this,
                R.layout.loading_recycler_view);
    }

}


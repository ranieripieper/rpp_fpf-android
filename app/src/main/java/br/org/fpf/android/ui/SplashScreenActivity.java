package br.org.fpf.android.ui;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;

import br.org.fpf.android.BuildConfig;
import br.org.fpf.android.R;
import br.org.fpf.android.cache.ClassificacaoCacheManager;
import br.org.fpf.android.cache.EscalacaoCacheManager;
import br.org.fpf.android.cache.base.RealmManager;
import br.org.fpf.android.ui.navigation.Navigator;

/**
 * Created by ranipieper on 1/14/16.
 */
@EActivity(R.layout.activity_splash_screen)
public class SplashScreenActivity  extends AppCompatActivity {

    private static final Integer SPLASH_TIME_OUT = BuildConfig.DEBUG ? 500 : 1500;

    @AfterViews
    protected void afterViews() {
        init();
    }

    @Background
    void init() {

        ClassificacaoCacheManager.getInstance().deleteAll();
        EscalacaoCacheManager.getInstance().deleteAll();
        RealmManager.refresh();
        goToMainActivity();
    }

    @UiThread
    void goToMainActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                showMainActivity();

                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    private void showMainActivity() {
        Navigator.navigateToMainActivity(SplashScreenActivity.this);
    }
}


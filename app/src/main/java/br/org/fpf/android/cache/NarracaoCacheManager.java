package br.org.fpf.android.cache;

import android.content.Context;

import java.util.List;

import br.org.fpf.android.cache.base.BaseCache;
import br.org.fpf.android.model.Narracao;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by ranipieper on 11/19/15.
 */
public class NarracaoCacheManager extends BaseCache<Narracao> {

    private static NarracaoCacheManager instance = new NarracaoCacheManager();

    private NarracaoCacheManager() {
    }

    public static NarracaoCacheManager getInstance() {
        return instance;
    }

    public List<Narracao> getNarracaoByIdPartida(Long idPartida) {
        return getNarracaoByIdPartida(idPartida, null);
    }

    public List<Narracao> getNarracaoByIdPartida(Long idPartida, Long lastId) {

        String[] DEFAULT_ORDER_FIELDS = {"id"};
        Sort[] DEFAULT_ORDER_SORT = {Sort.DESCENDING};

        RealmResults<Narracao> results = getRealm().where(Narracao.class)
                .equalTo("idPartida", idPartida)
                .greaterThan("id", lastId == null ? 0 : lastId)
                .findAllSorted(DEFAULT_ORDER_FIELDS, DEFAULT_ORDER_SORT);

        return getRealm().copyFromRealm(results);
    }

    public List<Narracao> getNarracaoAcoesImportantesByIdPartida(Context ctx, Long idPartida, Long idJogador) {

        getRealm(ctx).refresh();
        String[] DEFAULT_ORDER_FIELDS = {"id"};
        Sort[] DEFAULT_ORDER_SORT = {Sort.DESCENDING};

        RealmResults<Narracao> results = getRealm(ctx).where(Narracao.class)
                .equalTo("idPartida", idPartida)
                .equalTo("idJogador", idJogador)
                .isNotNull("acaoImportante")
                .findAllSorted(DEFAULT_ORDER_FIELDS, DEFAULT_ORDER_SORT);

        return getRealm().copyFromRealm(results);
    }

    public List<Narracao> getNarracaoByIdPartidaAndAcaoImportante(Context ctx, Long idPartida, String acaoImportante) {

        getRealm(ctx).refresh();
        String[] DEFAULT_ORDER_FIELDS = {"id"};
        Sort[] DEFAULT_ORDER_SORT = {Sort.DESCENDING};

        RealmResults<Narracao> results = getRealm(ctx).where(Narracao.class)
                .equalTo("idPartida", idPartida)
                .equalTo("acaoImportante", acaoImportante)
                .findAllSorted(DEFAULT_ORDER_FIELDS, DEFAULT_ORDER_SORT);

        return getRealm().copyFromRealm(results);
    }

    @Override
    public Class<Narracao> getReferenceClass() {
        return Narracao.class;
    }

    @Override
    protected long getCacheLifeTime() {
        return 1000 * 60 * 60 * 24 * 15; // 15 dias
    }
}

package br.org.fpf.android.ui.integridade;

import android.os.Bundle;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;

import br.org.fpf.android.BuildConfig;
import br.org.fpf.android.R;
import br.org.fpf.android.ui.base.BaseFragment;
import br.org.fpf.android.ui.base.ViewUtil;
import br.org.fpf.android.ui.navigation.Navigator;

/**
 * Created by ranipieper on 8/29/16.
 */
@EFragment(R.layout.fragment_fpf_integridade)
public class IntegridadeHomeFragment extends BaseFragment {

    public static IntegridadeHomeFragment newInstance() {
        Bundle args = new Bundle();

        IntegridadeHomeFragment fragment = new IntegridadeHomeFragment_();
        fragment.setArguments(args);

        return fragment;
    }


    @AfterViews
    public void afterViews() {
        super.afterViews();
    }


    @Click(R.id.layout_faq)
    void faqClick() {
        Navigator.navigateToIntegridadeFaqFragment(getContext());
    }

    @Click(R.id.layout_noticias)
    void noticiasClick() {
        Navigator.navigateToIntegridadeNoticiasFragment(getContext());
    }


    @Click(R.id.layout_dicas)
    void dicasClick() {
        Navigator.navigateToIntegridadeDicasFragment(getContext());
    }

    @Click(R.id.lbl_email)
    void emailClick() {
        ViewUtil.sendMail(getContext(), getString(R.string.integridade_email));
    }

    @Click(R.id.layout_manual)
    void manualClick() {
        ViewUtil.openBrowser(getContext(), BuildConfig.URL_CARTILHA_PDF);
    }

    @Click(R.id.lbl_telefone)
    void telefoneClick() {
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.DRAWER;
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.fpf_integridade);
    }
}

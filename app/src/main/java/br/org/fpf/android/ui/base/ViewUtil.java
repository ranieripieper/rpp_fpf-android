package br.org.fpf.android.ui.base;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextUtils;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;

import java.util.List;

import br.org.fpf.android.BuildConfig;
import br.org.fpf.android.R;
import br.org.fpf.android.util.Constants;
import retrofit.RetrofitError;
import uk.co.ribot.easyadapter.recyclerview.EasyLoadMoreRecyclerView;

/**
 * Created by ranipieper on 1/14/16.
 */
public class ViewUtil {

    public static void showError(Context context, RetrofitError error, int defaultError, ErrorListener errorListener) {
        showError(context, error, defaultError, false, errorListener);
    }

    public static void showError(Context context, RetrofitError error, int defaultError) {
        showError(context, error, defaultError, false, null);
    }

    public static void showError(Context context, RetrofitError error, int defaultError, boolean ignoreUnauthorized) {
        showError(context, error, defaultError, ignoreUnauthorized, null);
    }

    public static void showError(Context context, RetrofitError error, int defaultError, boolean ignoreUnauthorized, ErrorListener errorListener) {
        if (error != null) {
            if (BuildConfig.DEBUG) {
                error.printStackTrace();
            }

            showErrorDialog(context, defaultError, errorListener);
        }
    }

    public static void showErrorDialog(Context context, List<String> lstMessage) {
        showErrorDialog(context, getMessage(lstMessage), null);
    }

    public static void showToastError(Context context, List<String> lstMessage) {
        showToastError(context, getMessage(lstMessage));
    }

    public static void showToastError(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void showToastError(Context context, RetrofitError error) {
        Toast.makeText(context, getErrorMessage(context, error), Toast.LENGTH_SHORT).show();
    }

    public static String getMessage(List<String> lstMessage) {
        String message = "";
        for (String msg : lstMessage) {
            message += msg;
            message += "\r\n";
        }
        return message;
    }

    public static void showErrorDialog(Context ctx, int message, ErrorListener errorListener) {
        showErrorDialog(ctx, ctx.getString(message), errorListener);
    }

    public static void showErrorDialog(Context ctx, String message) {
        showErrorDialog(ctx, message, null);
    }

    private static MaterialDialog.Builder getDefaultMaterialDialog(Context ctx) {
        return new MaterialDialog.Builder(ctx)
                .theme(Theme.LIGHT)
                .titleColor(ResourcesCompat.getColor(ctx.getResources(), R.color.black_2, null))
                .contentColor(ResourcesCompat.getColor(ctx.getResources(), R.color.black_1, null))
                .negativeColor(ResourcesCompat.getColor(ctx.getResources(), R.color.black, null))
                .positiveColor(ResourcesCompat.getColor(ctx.getResources(), R.color.black, null))
                .neutralColor(ResourcesCompat.getColor(ctx.getResources(), R.color.black, null))
                .positiveText(R.string.ok);
    }

    public static void showErrorDialog(Context ctx, String message, final ErrorListener errorListener) {
        if (errorListener == null) {
            getDefaultMaterialDialog(ctx)
                    .title(R.string.error)
                    .content(message)
                    .positiveText(R.string.ok)
                            //.icon(ctx.getResources().getDrawable(R.drawable.icn_error_alert))
                    .show();
        } else {
            getDefaultMaterialDialog(ctx)
                    .title(R.string.error)
                    .content(message)
                    .positiveText(R.string.retry)
                    .negativeText(R.string.cancel)
                    .cancelable(false)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                            if (errorListener != null) {
                                errorListener.retry();
                            }
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                            if (errorListener != null) {
                                errorListener.cancel();
                            }
                        }
                    })
                    //.icon(ctx.getResources().getDrawable(R.drawable.icn_error_alert))
                    .show();
        }
    }

    public static void showOptionsDialog(Context ctx, String[] options, MaterialDialog.ListCallback callback) {
        showOptionsDialog(ctx, null, options, callback);
    }

    public static void showOptionsDialog(Context ctx, String title, String[] options, MaterialDialog.ListCallback callback) {
        MaterialDialog.Builder builder = getDefaultMaterialDialog(ctx);
        builder.items(options)
                .itemsCallback(callback);

        if (!TextUtils.isEmpty(title)) {
            builder.title(title);
        }
        builder.show();
    }

    public static void showSuccessDialog(Context ctx, int message) {
        showSuccessDialog(ctx, ctx.getString(message), null);
    }

    public static void showSuccessDialog(Context ctx, int message, MaterialDialog.SingleButtonCallback positiveCallback) {
        showSuccessDialog(ctx, ctx.getString(message), positiveCallback);
    }

    public static void showSuccessDialog(Context ctx, String message) {
        showSuccessDialog(ctx, message, null);
    }

    public static void showSuccessDialog(Context ctx, String message, MaterialDialog.SingleButtonCallback positiveCallback) {
        MaterialDialog.Builder builder = getDefaultMaterialDialog(ctx);
        builder
                .title(R.string.success)
                .content(message)
                .positiveText(R.string.ok)
                .onPositive(positiveCallback);
               // .icon(ctx.getResources().getDrawable(R.drawable.icn_success));
        if (positiveCallback != null) {
            builder.onPositive(positiveCallback);
        }
        builder.show();
    }

    private static String getErrorMessage(Context context, RetrofitError error) {
        /*
        try {
            ResponseResult errorObject = (ResponseResult) error.getBodyAs(ResponseResult.class);

            return getMessage(errorObject.getErrors());

        } catch (Exception e) {
            return context.getString(R.string.error_generic);
        }
        */
        return context.getString(R.string.error_generic);
    }


    public static void showConfirmDialog(Context ctx, int title, int message, MaterialDialog.SingleButtonCallback positiveButton) {
        showConfirmDialog(ctx, ctx.getString(title), ctx.getString(message), positiveButton);
    }

    public static void showConfirmDialog(Context ctx, String title, int message, MaterialDialog.SingleButtonCallback positiveButton) {
        showConfirmDialog(ctx, title, ctx.getString(message), positiveButton);
    }

    public static void showConfirmDialog(Context ctx, String title, String message, MaterialDialog.SingleButtonCallback positiveButton) {
        new MaterialDialog.Builder(ctx)
                .title(title)
                .content(message)
                .positiveText(R.string.yes)
                .negativeText(R.string.no)
                .positiveColorRes(R.color.darkRed)
                .neutralColorRes(R.color.darkRed)
                .negativeColorRes(R.color.darkRed)
                .cancelable(false)
                .onPositive(positiveButton)
                //.icon(ctx.getResources().getDrawable(R.drawable.icn_error_alert))
                .show();

    }

    public static void disableLoadingMore(EasyLoadMoreRecyclerView easyLoadMoreRecyclerView, boolean isAdded) {
        if (isAdded && easyLoadMoreRecyclerView != null) {
            easyLoadMoreRecyclerView.disableAutoLoadMore();
            easyLoadMoreRecyclerView.onLoadingMoreFinish();
        }
    }

    public static void disableLoadingMore(EasyLoadMoreRecyclerView easyLoadMoreRecyclerView, int totalItems, boolean isAdded) {
        if (!isAdded) {
            return;
        }
        if (easyLoadMoreRecyclerView != null) {
            easyLoadMoreRecyclerView.onLoadingMoreFinish();
        }

        int maxItemsPerPage = Constants.REG_POR_PAG;

        if (totalItems < maxItemsPerPage) {
            disableLoadingMore(easyLoadMoreRecyclerView, isAdded);
        }
    }

    public static void sendMail(Context context, String email) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto",email, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        context.startActivity(Intent.createChooser(emailIntent, context.getString(R.string.enviar_email)));
    }

    public static void openBrowser(Context context, String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        context.startActivity(i);
    }

}

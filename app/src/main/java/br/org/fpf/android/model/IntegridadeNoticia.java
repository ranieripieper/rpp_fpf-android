package br.org.fpf.android.model;

/**
 * Created by ranipieper on 8/29/16.
 */
public class IntegridadeNoticia {

    private String titulo;
    private String subtitulo;
    private String url;

    public IntegridadeNoticia(String titulo, String subtitulo, String url) {
        this.titulo = titulo;
        this.subtitulo = subtitulo;
        this.url = url;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSubtitulo() {
        return subtitulo;
    }

    public void setSubtitulo(String subtitulo) {
        this.subtitulo = subtitulo;
    }
}

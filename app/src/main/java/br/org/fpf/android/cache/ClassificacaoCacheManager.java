package br.org.fpf.android.cache;

import android.content.Context;

import java.util.List;

import br.org.fpf.android.cache.base.BaseCache;
import br.org.fpf.android.model.Classificacao;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by ranipieper on 11/19/15.
 */
public class ClassificacaoCacheManager extends BaseCache<Classificacao> {

    private static ClassificacaoCacheManager instance = new ClassificacaoCacheManager();

    private String[] DEFAULT_ORDER_FIELDS = {"grupo", "posicao"};
    private Sort[] DEFAULT_ORDER_SORT = {Sort.ASCENDING, Sort.ASCENDING};

    private ClassificacaoCacheManager() {
    }

    public static ClassificacaoCacheManager getInstance() {
        return instance;
    }

    public List<Classificacao> getClassificacaoByIdCampeonato(Long idCampeonato) {
        RealmResults<Classificacao> results = getRealm().where(Classificacao.class).equalTo("idCampeonato", idCampeonato).findAllSorted(DEFAULT_ORDER_FIELDS, DEFAULT_ORDER_SORT);
        return getRealm().copyFromRealm(results);
    }

    public List<Classificacao> getClassificacaoByIdCampeonato(Context ctx, Long idCampeonato) {
        getRealm(ctx).refresh();
        RealmResults<Classificacao> results = getRealm(ctx).where(Classificacao.class).equalTo("idCampeonato", idCampeonato).findAllSorted(DEFAULT_ORDER_FIELDS, DEFAULT_ORDER_SORT);
        return getRealm(ctx).copyFromRealm(results);
    }

    public List<Classificacao> getClassificacaoByIdCampeonatoFase(Long idCampeonato, String fase) {
        RealmResults<Classificacao> results = getRealm().where(Classificacao.class)
                .equalTo("idCampeonato", idCampeonato)
                .equalTo("fase", fase)
                .findAllSorted(DEFAULT_ORDER_FIELDS, DEFAULT_ORDER_SORT);
        return getRealm().copyFromRealm(results);
    }


    public Long hasOtherGroup(Long idCampeonato, String fase, String grupo) {
        return getRealm().where(Classificacao.class)
                .equalTo("idCampeonato", idCampeonato)
                .equalTo("fase", fase)
                .notEqualTo("grupo", grupo).count();
    }

    @Override
    public void put(Classificacao cls) {
        cls.setId(getCalculateId(cls));
        super.put(cls);
    }

    @Override
    public void putAll(List<Classificacao> lst) {
        if (lst != null) {
            for (Classificacao cls : lst) {
                cls.setId(getCalculateId(cls));
            }
            super.putAll(lst);
        }
    }

    private String getCalculateId(Classificacao cls) {
        return String.format("%s_%s_%s_%s", cls.getGrupo(), cls.getFase(), cls.getIdCampeonato(), cls.getIdEquipe());
    }

    @Override
    public Class<Classificacao> getReferenceClass() {
        return Classificacao.class;
    }

    @Override
    protected long getCacheLifeTime() {
        return 1000 * 60 * 60 * 24 * 15; // 15 dias
    }
}

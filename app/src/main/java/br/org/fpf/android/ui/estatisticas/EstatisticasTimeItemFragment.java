package br.org.fpf.android.ui.estatisticas;

import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import br.org.fpf.android.R;
import br.org.fpf.android.model.EstatisticasTime;
import br.org.fpf.android.ui.base.BaseFragment;
import br.org.fpf.android.util.Constants;
import uk.co.ribot.easyadapter.EasyRecyclerAdapter;

/**
 * Created by ranipieper on 3/8/16.
 */
@EFragment(R.layout.fragment_estatisticas_time_page)
public abstract class EstatisticasTimeItemFragment extends BaseFragment {

    @Nullable
    @ViewById(R.id.lbl_footer_label)
    TextView mLblFooterLabel;

    @Nullable
    @ViewById(R.id.lbl_footer_value)
    TextView mLblFooterValue;

    @ViewById(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @Nullable
    @ViewById(R.id.layout_footer)
    View mViewFooter;

    @Nullable
    @ViewById(R.id.txt_error_no_results)
    TextView mLblError;

    @FragmentArg(Constants.ARG_ESTATISTICAS_TIME)
    EstatisticasTime mEstatisticasTime;

    protected EasyRecyclerAdapter mAdapter;

    public void afterViews() {
        super.afterViews();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = createAdapter();
        mRecyclerView.setAdapter(mAdapter);
    }

    protected abstract EasyRecyclerAdapter createAdapter();

    @Override
    protected boolean needConfigToolbar() {
        return false;
    }
}
package br.org.fpf.android.cache.base;

import android.content.Context;

import java.util.List;

/**
 * Created by broto on 8/16/15.
 */
public interface Cache<T> {

    void put(T t);

    void put(Context ctx, T t);

    void putAll(List<T> t);

    void delete(String id);

    void delete(Long id);

    void delete(Context context, Long id);

    void deleteAll();

    T get(String id);

    T get(Long id);

    T get(Context context, Long id);

    T get(Context context, String id);

    T getToUpdate(Long id);

    T getToUpdate(String id);

    T getToUpdate(Context ctx, Long id);

    T getToUpdate(Context ctx, String id);

    List<T> getAll();

    long count();

    void updateWithoutNulls(List<T> t);

    String getPrimaryKeyName();
}

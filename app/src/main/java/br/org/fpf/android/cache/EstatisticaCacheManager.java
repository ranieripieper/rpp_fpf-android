package br.org.fpf.android.cache;

import br.org.fpf.android.cache.base.BaseCache;
import br.org.fpf.android.model.Estatistica;

/**
 * Created by ranipieper on 11/19/15.
 */
public class EstatisticaCacheManager extends BaseCache<Estatistica> {

    private static EstatisticaCacheManager instance = new EstatisticaCacheManager();

    private EstatisticaCacheManager() {
    }

    @Override
    public String getPrimaryKeyName() {
        return "idPartida";
    }

    public static EstatisticaCacheManager getInstance() {
        return instance;
    }

    @Override
    public Class<Estatistica> getReferenceClass() {
        return Estatistica.class;
    }

    @Override
    protected long getCacheLifeTime() {
        return 1000 * 60 * 60 * 24 * 15; // 15 dias
    }
}

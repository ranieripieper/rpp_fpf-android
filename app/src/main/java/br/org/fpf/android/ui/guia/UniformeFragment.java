package br.org.fpf.android.ui.guia;

import android.os.Bundle;
import android.widget.ImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import br.org.fpf.android.FpfApplication;
import br.org.fpf.android.R;
import br.org.fpf.android.ui.base.BaseFragment;
import br.org.fpf.android.util.Constants;

/**
 * Created by ranipieper on 3/3/16.
 */
@EFragment(R.layout.fragment_uniforme)
public class UniformeFragment extends BaseFragment {

    @FragmentArg(Constants.ARG_URL)
    String mImageUrl;

    @ViewById(R.id.img_uniforme)
    ImageView mImage;

    public static UniformeFragment newInstance(String url) {
        Bundle args = new Bundle();

        UniformeFragment_ fragment = new UniformeFragment_();
        args.putString(Constants.ARG_URL, url);
        fragment.setArguments(args);

        return fragment;
    }

    @AfterViews
    public void afterViews() {
        super.afterViews();
        FpfApplication.imageLoader.displayImage(mImageUrl, mImage);
    }

    @Override
    protected boolean needConfigToolbar() {
        return  false;
    }

}

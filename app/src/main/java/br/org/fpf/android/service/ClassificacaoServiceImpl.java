package br.org.fpf.android.service;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import br.org.fpf.android.cache.ClassificacaoCacheManager;
import br.org.fpf.android.model.Classificacao;
import br.org.fpf.android.model.response.ClassificacaoResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranieripieper on 1/28/16.
 */
public class ClassificacaoServiceImpl {

    public static void getClassificacaoInCache(Long idCampeonato, final Callback<List<Classificacao>> callback) {
        List<Classificacao> classificacaoList = ClassificacaoCacheManager.getInstance().getAll();
        if (classificacaoList.isEmpty()) {
            getClassificacao(idCampeonato, callback);
        } else {
            callback.success(classificacaoList, null);
        }
    }

    public static void getClassificacao(final Long idCampeonato, final Callback<List<Classificacao>> callback) {
        List<Classificacao> lst = ClassificacaoCacheManager.getInstance().getClassificacaoByIdCampeonato(idCampeonato);
        if (lst == null || lst.isEmpty()) {
            getClassificacao(idCampeonato, null, callback);
        } else {
            callback.success(lst, null);
        }
    }

    public static void getClassificacao(final Long idCampeonato, final String fase, final Callback<List<Classificacao>> callback) {
        String where = "";
        if (TextUtils.isEmpty(fase)) {
            where = String.format("{\"IdCampeonato\":%d}", idCampeonato);
        } else {
            where = String.format("{\"IdCampeonato\":%d, \"Fase\":\"%s\"}", idCampeonato, fase);
        }
        RetrofitManager.getInstance().getClassificacaoService().getClassificacaoBy(where, new Callback<ClassificacaoResponse>() {
            @Override
            public void success(ClassificacaoResponse classificacaoResponse, Response response) {
                //salva classificacao
                if (!classificacaoResponse.isEmpty()) {
                    ClassificacaoCacheManager.getInstance().putAll(classificacaoResponse.getResults());
                }
                if (callback != null) {
                    List<Classificacao> result = new ArrayList();
                    if (TextUtils.isEmpty(fase)) {
                        result.addAll(ClassificacaoCacheManager.getInstance().getClassificacaoByIdCampeonato(idCampeonato));
                    } else {
                        result.addAll(ClassificacaoCacheManager.getInstance().getClassificacaoByIdCampeonatoFase(idCampeonato, fase));
                    }

                    callback.success(result, response);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                 if (callback != null) {
                    callback.failure(error);
                }
            }
        });
    }

    public static List<Classificacao> getClassificacaoInCache(final Long idCampeonato, String fase) {
        List<Classificacao> result = new ArrayList();
        result.addAll(ClassificacaoCacheManager.getInstance().getClassificacaoByIdCampeonatoFase(idCampeonato, fase));
        return result;
    }
}

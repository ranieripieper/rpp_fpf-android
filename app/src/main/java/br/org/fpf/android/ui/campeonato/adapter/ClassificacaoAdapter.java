package br.org.fpf.android.ui.campeonato.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.org.fpf.android.R;
import br.org.fpf.android.model.Classificacao;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderAdapter;
import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;
import uk.co.ribot.easyadapter.ItemViewHolder;

/**
 * Created by ranieripieper on 1/28/16.
 */
public class ClassificacaoAdapter<T> extends EasyLoadMoreRecyclerAdapter<T> implements StickyHeaderAdapter<ClassificacaoAdapter.HeaderHolder> {

    private boolean mHasMoreOneGroups = false;

    @Override
    public ClassificacaoAdapter.HeaderHolder onCreateHeaderViewHolder(ViewGroup viewGroup) {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.header_classificacao, viewGroup, false);
        return new HeaderHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(ClassificacaoAdapter.HeaderHolder headerHolder, int position) {
        Classificacao obj = (Classificacao) getItem(position);

        if (!mHasMoreOneGroups) {
            headerHolder.mHeaderTextView.setVisibility(View.GONE);
        } else {
            headerHolder.mHeaderTextView.setVisibility(View.VISIBLE);
        }
        headerHolder.mHeaderTextView.setText(mContext.getString(R.string.grupo_x, obj.getGrupo()));
        headerHolder.itemView.invalidate();

    }

    @Override
    public long getHeaderId(int position) {
        Classificacao obj = (Classificacao) getItem(position);
        String v = "";
        for (int i = 0; i < obj.getGrupo().length(); i++) {
            v += Character.getNumericValue(obj.getGrupo().charAt(i));
        }
        if (TextUtils.isEmpty(v)) {
            return 0;
        }
        return Long.valueOf(v);
    }

    /**
     * Header View Holder.
     */
    protected class HeaderHolder extends RecyclerView.ViewHolder {

        public TextView mHeaderTextView;
        public TextView mLblPontos;

        /**
         * Constructor with the View.
         *
         * @param view view object
         */
        public HeaderHolder(View view) {
            super(view);
            mHeaderTextView = (TextView) view.findViewById(R.id.lbl_header);
            mLblPontos = (TextView) view.findViewById(R.id.lbl_pontos);
        }
    }

    /**
     * Constructs and EasyAdapter with a Context, an {@link ItemViewHolder} class, list of items and a generic listener.
     *
     * @param context             a valid Context
     * @param itemViewHolderClass your {@link ItemViewHolder} implementation class
     * @param listItems           the list of items to load in the Adapter
     * @param listener            a generic object that you can access from your {@link ItemViewHolder} by calling
     *                            {@link ItemViewHolder#getListener()}, This can be used to pass a listener to the view holder that then you
     *                            can cast and use as a callback.
     */
    public ClassificacaoAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, List<T> listItems, Object listener, Integer itemLayoutLoadMoreId, boolean hasMoreOneGroups) {
        super(context, itemViewHolderClass, listItems, listener, itemLayoutLoadMoreId);
        this.mHasMoreOneGroups = hasMoreOneGroups;
    }

    /**
     * Constructs and EasyAdapter with a Context, an {@link ItemViewHolder} class, list of items and a generic listener.
     * @param context
     * @param itemViewHolderClass
     * @param listItems
     * @param listener
     * @param itemLayoutLoadMoreId
     * @param header
     */
    public ClassificacaoAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, List<T> listItems, Object listener, Integer itemLayoutLoadMoreId, View header, boolean hasMoreOneGroups) {
        super(context, itemViewHolderClass, listItems, listener, itemLayoutLoadMoreId, header);
        this.mHasMoreOneGroups = hasMoreOneGroups;
    }


}

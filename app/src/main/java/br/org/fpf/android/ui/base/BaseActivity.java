package br.org.fpf.android.ui.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.greysonparrelli.permiso.Permiso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.org.fpf.android.R;
import retrofit.RetrofitError;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by ranipieper on 1/14/16.
 */
@EActivity
public abstract class BaseActivity extends AppCompatActivity implements ViewDialogInterface {

    @Nullable
    @ViewById(R.id.loading)
    public ProgressBar loading;

    @Nullable
    @ViewById(R.id.toolbar)
    protected Toolbar mToolbar;

    private ProgressDialog progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Permiso.getInstance().setActivity(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Permiso.getInstance().setActivity(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Permiso.getInstance().onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    public void showLoadingView() {
        hideKeyboard();
        if (loading != null) {
            loading.setVisibility(View.VISIBLE);
        } else if (progress == null || !progress.isShowing()) {
            progress = ProgressDialog.show(this, getString(R.string.wait),
                    getString(R.string.loading), true);
            hideKeyboard();
        }
    }

    public void hideLoadingView() {
        if (!isFinishing() && loading != null) {
            loading.setVisibility(View.GONE);
        }
        if (!isFinishing() && progress != null && progress.isShowing()) {
            progress.dismiss();
            progress = null;
        }
    }

    public void hideKeyboard() {
        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
    }

    /**
     * Inicializa toolbar
     */
    @AfterViews
    protected void initToolbar() {
        if (mToolbar != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mToolbar.setElevation(getResources().getDimensionPixelSize(R.dimen.action_bar_elevation));
            }
            setSupportActionBar(mToolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(false);
                getSupportActionBar().setTitle(getString(R.string.app_name));
            }
        }
    }

    /**
     * Calligraphy
     *
     * @param newBase
     */
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    protected Context getContext() {
        return BaseActivity.this;
    }

    //ViewDialogInterface

    @Override
    public void showError(RetrofitError error) {
        showError(error, R.string.error_generic);
    }

    @Override
    public void showError(RetrofitError error, int defaultError, boolean ignoreUnauthorized) {
        hideLoadingView();
        ViewUtil.showError(getContext(), error, defaultError, ignoreUnauthorized);
    }

    @Override
    public void showError(RetrofitError error, int defaultError, ErrorListener errorListener) {
        hideLoadingView();
        ViewUtil.showError(getContext(), error, defaultError, false, errorListener);
    }

    @Override
    public void showError(RetrofitError error, int defaultError) {
        hideLoadingView();
        ViewUtil.showError(getContext(), error, defaultError);
    }

    @Override
    public void showErrorDialog(List<String> lstMessage) {
        hideLoadingView();
        ViewUtil.showErrorDialog(getContext(), lstMessage);
    }

    @Override
    public void showErrorDialog(String message) {
        hideLoadingView();
        ViewUtil.showErrorDialog(getContext(), message);
    }

    @Override
    public void showErrorDialog(int message) {
        hideLoadingView();
        showErrorDialog(getString(message));
    }

    @Override
    public void showToastError(Context ctx, String message) {
        hideLoadingView();
        ViewUtil.showToastError(ctx, message);
    }

    @Override
    public void showToastError(Context ctx, RetrofitError error) {
        hideLoadingView();
        ViewUtil.showToastError(ctx, error);
    }

    @Override
    public void showSuccessDialog(Context ctx, int message) {
        showSuccessDialog(ctx, ctx.getString(message));
    }

    @Override
    public void showSuccessDialog(Context ctx, String message) {
        ViewUtil.showSuccessDialog(ctx, message);
    }

    @Override
    public void showSuccessDialog(Context ctx, int message, final boolean finish) {
        ViewUtil.showSuccessDialog(ctx, message, new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                if (finish) {
                    finish();
                }
            }
        });
    }

    @Override
    public void showSuccessDialog(Context ctx, int message, final MaterialDialog.SingleButtonCallback callback) {
        ViewUtil.showSuccessDialog(ctx, message, callback);
    }

    //Toolbar
    public void setToolbarTitle(String s) {
        if (mToolbar != null) {
            mToolbar.setTitle(s);
        }

    }

    public void setToolbarTitle(int stringId) {
        if (mToolbar != null) {
            mToolbar.setTitle(stringId);
        }
    }

    @Override
    protected void onDestroy() {
        isDestroying = true;
        super.onDestroy();
    }

    protected boolean isDestroying = false;

    protected void TODO() {
        Toast.makeText(getContext(), "TODO", Toast.LENGTH_SHORT).show();
    }

}

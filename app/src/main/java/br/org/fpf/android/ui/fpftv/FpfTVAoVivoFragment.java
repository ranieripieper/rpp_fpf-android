package br.org.fpf.android.ui.fpftv;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.VideoView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import br.org.fpf.android.BuildConfig;
import br.org.fpf.android.R;
import br.org.fpf.android.ui.base.BaseFragment;

/**
 * Created by ranipieper on 1/26/16.
 */
@EFragment(R.layout.fragment_fpf_tv_ao_vivo)
public class FpfTVAoVivoFragment extends BaseFragment {

    @ViewById(R.id.video_view)
    VideoView mVideoView;

    @ViewById(R.id.layout_video)
    View mLayoutVideo;

    @ViewById(R.id.img_play)
    View mImgPlay;

    public static FpfTVAoVivoFragment newInstance() {
        Bundle args = new Bundle();

        FpfTVAoVivoFragment fragment = FpfTVAoVivoFragment_.builder().build();
        fragment.setArguments(args);

        return fragment;
    }

    @AfterViews
    public void afterViews() {
        super.afterViews();

        showLoadingView();
        mLayoutVideo.setVisibility(View.INVISIBLE);
        //Uri video = Uri.parse("http://clips.vorwaerts-gmbh.de/VfE_html5.mp4");
        Uri video = Uri.parse(BuildConfig.URL_AO_VIVO);
        mVideoView.setVideoURI(video);
        mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                showErrorDialog(R.string.error_generic);
                hideLoadingView();
                return false;
            }
        });
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(false);
                hideLoadingView();
                mLayoutVideo.setVisibility(View.VISIBLE);
                mImgPlay.setVisibility(View.VISIBLE);
            }
        });
        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                mImgPlay.setVisibility(View.VISIBLE);
            }
        });

    }

    @Override
    public void setMenuVisibility(boolean visible) {
        super.setMenuVisibility(visible);
        if (!visible && mVideoView != null && mVideoView.isPlaying()) {
            mVideoView.stopPlayback();
            mImgPlay.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void onDestroyView() {
        if (mVideoView != null && mVideoView.isPlaying()) {
            mVideoView.stopPlayback();
            mImgPlay.setVisibility(View.VISIBLE);

        }
        super.onDestroyView();
    }

    @Click(R.id.img_play)
    void playClick() {
        mImgPlay.setVisibility(View.INVISIBLE);
        mVideoView.start();
    }

    @Override
    protected boolean needConfigToolbar() {
        return false;
    }

}


package br.org.fpf.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ranipieper on 1/26/16.
 */
public class Campeonato extends RealmObject {

    @PrimaryKey
    @Expose
    @SerializedName("Id")
    private Long id;

    @Expose
    @SerializedName("Categoria")
    private String categoria;

    @Expose
    @SerializedName("ClassificacaoGrupo")
    private boolean classificacaoGrupo;

    @Expose
    @SerializedName("FaseAtual")
    private String faseAtual;

    @Expose
    @SerializedName("Local")
    private String local;

    @Expose
    @SerializedName("Nome")
    private String nome;

    @Expose
    @SerializedName("QtdRodadas")
    private Long qtdRodadas;

    @Expose
    @SerializedName("RodadaAtual")
    private Long rodadaAtual;

    @Expose
    @SerializedName("TemClassificacao")
    private boolean temClassificacao;

    @Expose
    @SerializedName("Temporada")
    private String temporada;

    @Expose
    @SerializedName("TipoColeta")
    private String tipoColeta;

    @Expose
    @SerializedName("objectId")
    private String objectId;

    @Expose
    @SerializedName("createdAt")
    private Date createdAt;

    @Expose
    @SerializedName("updatedAt")
    private Date updatedAt;

    @Expose
    @SerializedName("IdCampeonatoNoticias")
    private Long idCampeonatoNoticias;

    @Expose
    @SerializedName("NomeCampeonatoApp")
    private String nomeCampeonatoApp;

    @Expose
    @SerializedName("Ordem")
    private Integer ordem;

    @Expose
    @SerializedName("Visible")
    private Boolean visible = true;

    /**
     * Gets the id
     *
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the categoria
     *
     * @return categoria
     */
    public String getCategoria() {
        return categoria;
    }

    /**
     * Sets the categoria
     *
     * @param categoria
     */
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    /**
     * Gets the classificacaoGrupo
     *
     * @return classificacaoGrupo
     */
    public boolean isClassificacaoGrupo() {
        return classificacaoGrupo;
    }

    /**
     * Sets the classificacaoGrupo
     *
     * @param classificacaoGrupo
     */
    public void setClassificacaoGrupo(boolean classificacaoGrupo) {
        this.classificacaoGrupo = classificacaoGrupo;
    }

    /**
     * Gets the faseAtual
     *
     * @return faseAtual
     */
    public String getFaseAtual() {
        return faseAtual;
    }

    /**
     * Sets the faseAtual
     *
     * @param faseAtual
     */
    public void setFaseAtual(String faseAtual) {
        this.faseAtual = faseAtual;
    }

    /**
     * Gets the local
     *
     * @return local
     */
    public String getLocal() {
        return local;
    }

    /**
     * Sets the local
     *
     * @param local
     */
    public void setLocal(String local) {
        this.local = local;
    }

    /**
     * Gets the nome
     *
     * @return nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the nome
     *
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Gets the qtdRodadas
     *
     * @return qtdRodadas
     */
    public Long getQtdRodadas() {
        return qtdRodadas;
    }

    /**
     * Sets the qtdRodadas
     *
     * @param qtdRodadas
     */
    public void setQtdRodadas(Long qtdRodadas) {
        this.qtdRodadas = qtdRodadas;
    }

    /**
     * Gets the rodadaAtual
     *
     * @return rodadaAtual
     */
    public Long getRodadaAtual() {
        return rodadaAtual;
    }

    /**
     * Sets the rodadaAtual
     *
     * @param rodadaAtual
     */
    public void setRodadaAtual(Long rodadaAtual) {
        this.rodadaAtual = rodadaAtual;
    }

    /**
     * Gets the temClassificacao
     *
     * @return temClassificacao
     */
    public boolean isTemClassificacao() {
        return temClassificacao;
    }

    /**
     * Sets the temClassificacao
     *
     * @param temClassificacao
     */
    public void setTemClassificacao(boolean temClassificacao) {
        this.temClassificacao = temClassificacao;
    }

    /**
     * Gets the temporada
     *
     * @return temporada
     */
    public String getTemporada() {
        return temporada;
    }

    /**
     * Sets the temporada
     *
     * @param temporada
     */
    public void setTemporada(String temporada) {
        this.temporada = temporada;
    }

    /**
     * Gets the tipoColeta
     *
     * @return tipoColeta
     */
    public String getTipoColeta() {
        return tipoColeta;
    }

    /**
     * Sets the tipoColeta
     *
     * @param tipoColeta
     */
    public void setTipoColeta(String tipoColeta) {
        this.tipoColeta = tipoColeta;
    }

    /**
     * Gets the objectId
     *
     * @return objectId
     */
    public String getObjectId() {
        return objectId;
    }

    /**
     * Sets the objectId
     *
     * @param objectId
     */
    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    /**
     * Gets the createdAt
     *
     * @return createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * Sets the createdAt
     *
     * @param createdAt
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Gets the updatedAt
     *
     * @return updatedAt
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * Sets the updatedAt
     *
     * @param updatedAt
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getIdCampeonatoNoticias() {
        return idCampeonatoNoticias;
    }

    public void setIdCampeonatoNoticias(Long idCampeonatoNoticias) {
        this.idCampeonatoNoticias = idCampeonatoNoticias;
    }

    public String getNomeCampeonatoApp() {
        return nomeCampeonatoApp;
    }

    public void setNomeCampeonatoApp(String nomeCampeonatoApp) {
        this.nomeCampeonatoApp = nomeCampeonatoApp;
    }

    public Integer getOrdem() {
        return ordem;
    }

    public void setOrdem(Integer ordem) {
        this.ordem = ordem;
    }

    /**
     * Gets the visible
     *
     * @return visible
     */
    public Boolean getVisible() {
        return visible;
    }

    /**
     * Sets the visible
     *
     * @param visible
     */
    public void setVisible(Boolean visible) {
        this.visible = visible;
    }
}

package br.org.fpf.android.service;

import br.org.fpf.android.cache.EscalacaoCacheManager;
import br.org.fpf.android.model.Escalacao;
import br.org.fpf.android.model.response.EscalacaoResponse;
import br.org.fpf.android.service.interfaces.EscalacaoService;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranieripieper on 1/27/16.
 */
public class EscalacaoServiceImpl {

    public static void getEscalacao(final Long idPartida, boolean cache, final Callback<Escalacao> callback) {
        if (cache) {
            getEscalacaoInCache(idPartida, callback);
        } else {
            getEscalacao(idPartida, callback);
        }
    }

    public static void getEscalacaoInCache(final Long idPartida, final Callback<Escalacao> callback) {
        Escalacao escalacao = EscalacaoCacheManager.getInstance().getEscalacaoByIdPartida(idPartida);
        if (escalacao == null ||
                escalacao.getEscalacaoMandante() == null ||
                escalacao.getEscalacaoVisitante() == null ||
                escalacao.getEscalacaoMandante().isEmpty() ||
                escalacao.getEscalacaoVisitante().isEmpty()) {
            getEscalacao(idPartida, callback);
        } else {
            callback.success(escalacao, null);
        }
    }

    public static void getEscalacao(final Long idPartida, final Callback<Escalacao> callback) {

        String idPartidaWhere = String.format("{\"IdPartida\":%d}", idPartida);

        RetrofitManager.getInstance().getEscalacaoService().getEscalacao(idPartidaWhere, 1, EscalacaoService.DEFAULT_ORDER,new Callback<EscalacaoResponse>() {
            @Override
            public void success(EscalacaoResponse escalacaoResponse, Response response) {
                //salva as escalações
                EscalacaoCacheManager.getInstance().delete(idPartida);
                if (!escalacaoResponse.isEmpty()) {
                    EscalacaoCacheManager.getInstance().putAll(escalacaoResponse.getResults());
                }
                if (callback != null) {
                    callback.success(EscalacaoCacheManager.getInstance().getEscalacaoByIdPartida(idPartida), response);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (callback != null) {
                    callback.failure(error);
                }
            }
        });
    }
}

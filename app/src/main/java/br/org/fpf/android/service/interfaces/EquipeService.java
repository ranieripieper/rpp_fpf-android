package br.org.fpf.android.service.interfaces;

import br.org.fpf.android.model.response.EquipeResponse;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by ranipieper on 1/14/16.
 */
public interface EquipeService {

    String ESTADO_SP = "{\"Estado\":\"SP\"}";

    @GET("/classes/Equipe")
    void getEquipes(@Query("where") String where, @Query("limit") int perPage, Callback<EquipeResponse> response);

}

package br.org.fpf.android.ui.home.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.List;

import br.org.fpf.android.R;

/**
 * Created by ranipieper on 8/26/16.
 */
public class MenuAdapter extends ExpandableRecyclerAdapter<MenuItemViewHolder, SubMenuItemViewHolder> {

    private LayoutInflater mInflater;
    private Context mContext;

    public MenuAdapter(Context context, List<MenuItem> itemList) {
        super(itemList);
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public MenuItemViewHolder onCreateParentViewHolder(ViewGroup viewGroup) {
        View view = mInflater.inflate(R.layout.row_menu, viewGroup, false);
        return new MenuItemViewHolder(view);
    }

    @Override
    public SubMenuItemViewHolder onCreateChildViewHolder(ViewGroup viewGroup) {
        View view = mInflater.inflate(R.layout.row_submenu, viewGroup, false);
        return new SubMenuItemViewHolder(view);
    }

    @Override
    public void onBindParentViewHolder(MenuItemViewHolder menuItemViewHolder, int i, ParentListItem parentListItem) {
        final MenuItem obj = (MenuItem) parentListItem;
        menuItemViewHolder.mLblMenu.setText(obj.getMenu());

        int color = Color.WHITE;
        if (obj.isSelected()) {
            color = ResourcesCompat.getColor(mContext.getResources(), R.color.sunflower, null);
        }

        menuItemViewHolder.mLblMenu.setTextColor(color);

        Drawable drawable = ResourcesCompat.getDrawable(mContext.getResources(), obj.getResource(), null);
        Drawable wrappedDrawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTintList(wrappedDrawable, ColorStateList.valueOf(color));
        menuItemViewHolder.mImgMenu.setImageDrawable(wrappedDrawable);

        if(obj.getListener() != null && (obj.getChilds() == null || obj.getChilds().isEmpty())) {
            menuItemViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (obj.getListener() != null) {
                        obj.getListener().menuClick(obj);
                    }
                }
            });
        }
    }

    @Override
    public void onBindChildViewHolder(SubMenuItemViewHolder subMenuItemViewHolder, int i, Object childListItem) {
        final SubMenuItem obj = (SubMenuItem) childListItem;
        subMenuItemViewHolder.mLblSubMenu.setText(obj.getSubmenu());

        int color = Color.WHITE;
        if (obj.isSelected()) {
            color = ResourcesCompat.getColor(mContext.getResources(), R.color.sunflower, null);
        }

        int colorBg = ResourcesCompat.getColor(mContext.getResources(), R.color.bgSubMenu, null);
        if (obj.isSelected()) {
            colorBg = ResourcesCompat.getColor(mContext.getResources(), R.color.bgSubMenuSelected, null);
        }
        subMenuItemViewHolder.mRow.setBackgroundColor(colorBg);
        subMenuItemViewHolder.mLblSubMenu.setTextColor(color);

        if(obj.getListener() != null) {
            subMenuItemViewHolder.mLblSubMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (obj.getListener() != null) {
                        obj.getListener().menuClick(obj);
                    }
                }
            });
        }
    }
}

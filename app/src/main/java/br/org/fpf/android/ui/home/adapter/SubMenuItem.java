package br.org.fpf.android.ui.home.adapter;

/**
 * Created by ranipieper on 8/26/16.
 */
public class SubMenuItem {

    private String id;
    private String submenu;
    private MenuItem parent;
    private boolean selected;
    private SubMenuItemListener listener;

    public SubMenuItem(String id, String submenu, MenuItem parent, boolean selected, SubMenuItemListener listener) {
        this.id = id;
        this.submenu = submenu;
        this.selected = selected;
        this.parent = parent;
        this.listener = listener;
    }

    public SubMenuItem(String id, String submenu, MenuItem parent, SubMenuItemListener listener) {
        this.id = id;
        this.submenu = submenu;
        this.parent = parent;
        this.listener = listener;
    }

    public String getSubmenu() {
        return submenu;
    }

    public void setSubmenu(String submenu) {
        this.submenu = submenu;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public SubMenuItemListener getListener() {
        return listener;
    }

    public void setListener(SubMenuItemListener listener) {
        this.listener = listener;
    }

    public MenuItem getParent() {
        return parent;
    }

    public void setParent(MenuItem parent) {
        this.parent = parent;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public interface SubMenuItemListener {
        void menuClick(SubMenuItem subMenuItem);
    }

}

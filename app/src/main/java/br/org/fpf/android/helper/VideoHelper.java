package br.org.fpf.android.helper;

import com.google.api.services.youtube.model.SearchResult;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.org.fpf.android.model.Video;

/**
 * Created by ranipieper on 8/30/16.
 */
public class VideoHelper {

    public static List<Video> transformVideo(List<SearchResult> searchResultList) {
        List<Video> result = new ArrayList<>();

        for (SearchResult searchResult : searchResultList) {
            Video video = new Video();
            video.setTitle(searchResult.getSnippet().getTitle());
            video.setDescription(searchResult.getSnippet().getDescription());
            video.setDt(new Date(searchResult.getSnippet().getPublishedAt().getValue()));
            video.setId(searchResult.getId().getVideoId());
            video.setThumb(getThumb(searchResult));
            result.add(video);
        }

        return result;
    }

    private static String getThumb(SearchResult searchResult) {
        if (searchResult.getSnippet().getThumbnails() != null) {
            if (searchResult.getSnippet().getThumbnails().getHigh() != null) {
                return searchResult.getSnippet().getThumbnails().getHigh().getUrl();
            } else if (searchResult.getSnippet().getThumbnails().getDefault() != null) {
                return searchResult.getSnippet().getThumbnails().getDefault().getUrl();
            } else if (searchResult.getSnippet().getThumbnails().getMedium() != null) {
                return searchResult.getSnippet().getThumbnails().getMedium().getUrl();
            } else if (searchResult.getSnippet().getThumbnails().getStandard() != null) {
                return searchResult.getSnippet().getThumbnails().getStandard().getUrl();
            }
        }

        return "";

    }
}

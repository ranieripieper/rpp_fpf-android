package br.org.fpf.android.ui.campeonato.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import br.org.fpf.android.FpfApplication;
import br.org.fpf.android.R;
import br.org.fpf.android.cache.EquipeCacheManager;
import br.org.fpf.android.cache.PartidaCacheManager;
import br.org.fpf.android.model.Equipe;
import br.org.fpf.android.model.Partida;
import br.org.fpf.android.helper.PartidaHelper;
import br.org.fpf.android.util.DateUtil;
import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by broto on 10/14/15.
 */
@LayoutId(R.layout.row_partida)
public class PartidaViewHolder extends ItemViewHolder<Partida> {

    @ViewId(R.id.ripple)
    RelativeLayout mRipple;

    @ViewId(R.id.img_time_mandante)
    ImageView imgTimeMandante;

    @ViewId(R.id.img_time_visitante)
    ImageView imgTimeVisitante;

    @ViewId(R.id.lbl_mandante)
    TextView lblMandante;

    @ViewId(R.id.lbl_visitante)
    TextView lblVisitante;

    @ViewId(R.id.lbl_placar_visitante)
    TextView lblPlacarVisitante;

    @ViewId(R.id.lbl_placar_mandante)
    TextView lblPlacarMandante;

    @ViewId(R.id.lbl_placar_visitante_penalti)
    TextView lblPlacarVisitantePenalti;

    @ViewId(R.id.lbl_placar_mandante_penalti)
    TextView lblPlacarMandantePenalti;

    @ViewId(R.id.lbl_data_hora_valor)
    TextView lblData;

    @ViewId(R.id.lbl_local_valor)
    TextView lblLocal;

    @ViewId(R.id.img_indicacao)
    View imgIndicacao;


    public PartidaViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Partida itemParam, PositionInfo positionInfo) {

        Partida item = PartidaCacheManager.getInstance().get(itemParam.getId());

        setEquipe(item.getIdEquipeMandante(), imgTimeMandante, lblMandante);
        setEquipe(item.getIdEquipeVisitante(), imgTimeVisitante, lblVisitante);

        lblLocal.setText(item.getNomeEstadio());
        if (item.getData() != null && item.getData().getData() != null) {
            lblData.setText(DateUtil.DATE_DAY_MONTH_YEAR_TIME.get().format(item.getData().getData()));
        } else {
            lblData.setText(R.string.a_definir);
        }

        if (item.getPlacarMandante() != null) {
            lblPlacarMandante.setText(String.valueOf(item.getPlacarMandante()));
            lblPlacarMandante.setVisibility(View.VISIBLE);
        } else {
            lblPlacarMandante.setVisibility(View.INVISIBLE);
        }

        if (item.getPlacarVisitante() != null) {
            lblPlacarVisitante.setText(String.valueOf(item.getPlacarVisitante()));
            lblPlacarVisitante.setVisibility(View.VISIBLE);
        } else {
            lblPlacarVisitante.setVisibility(View.INVISIBLE);
        }

        if (item.getPlacarMandante() != null && item.getPlacarVisitante() != null && item.getPlacarPenaltisMandante() != null && item.getPlacarPenaltisVisitante() != null) {
            lblPlacarMandantePenalti.setVisibility(View.VISIBLE);
            lblPlacarVisitantePenalti.setVisibility(View.VISIBLE);
            lblPlacarMandantePenalti.setText(getContext().getString(R.string.placar_penalti, item.getPlacarPenaltisMandante()));
            lblPlacarVisitantePenalti.setText(getContext().getString(R.string.placar_penalti, item.getPlacarPenaltisVisitante()));
        } else {
            lblPlacarMandantePenalti.setVisibility(View.GONE);
            lblPlacarVisitantePenalti.setVisibility(View.GONE);
        }
        if (item.isTemEstatistica() || item.isTemNarracao()) {
            mRipple.setEnabled(true);
            imgIndicacao.setVisibility(View.VISIBLE);
        } else {
            mRipple.setEnabled(false);
            imgIndicacao.setVisibility(View.GONE);
        }

        if (PartidaHelper.isCanceled(item)) {
            mRipple.setAlpha(0.3f);
        } else {
            mRipple.setAlpha(1.f);
        }
    }

    private void setEquipe(Long id, ImageView img, TextView lbl) {
        Equipe equipe = EquipeCacheManager.getInstance().get(id);
        if (equipe == null) {
            img.setImageResource(R.drawable.ic_team_placeholder);
            lbl.setText(R.string.a_definir);
        } else {
            FpfApplication.imageLoader.displayImage(equipe.getUrlLogo(), img);
            lbl.setText(equipe.getNome());
        }
    }

    @Override
    public void onSetListeners() {
        mRipple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Partida item = getItem();
                if (item.isTemEstatistica() || item.isTemNarracao()) {
                    PartidaHolderListener listener = getListener(PartidaHolderListener.class);
                    if (listener != null) {
                        listener.onItemSelected(getItem());
                    }
                }
            }
        });
    }

    public interface PartidaHolderListener {
        void onItemSelected(Partida partida);
    }
}
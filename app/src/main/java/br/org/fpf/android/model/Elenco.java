package br.org.fpf.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by ranipieper on 3/2/16.
 */
public class Elenco extends RealmObject {

    @Expose
    @SerializedName("LOCAL")
    private String local;

    @Expose
    @SerializedName("NASC")
    private String nascimento;

    @Expose
    @SerializedName("NOME")
    private String nome;

    @Expose
    @SerializedName("POS")
    private String posicao;

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getNascimento() {
        return nascimento;
    }

    public void setNascimento(String nascimento) {
        this.nascimento = nascimento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPosicao() {
        return posicao;
    }

    public void setPosicao(String posicao) {
        this.posicao = posicao;
    }
}

package br.org.fpf.android.service.interfaces;

import br.org.fpf.android.model.response.EstatisticaResponse;
import br.org.fpf.android.model.response.PartidaResponse;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by ranieripieper on 1/28/16.
 */
public interface PartidaService {

    @GET("/classes/Partida")
    void getPartidas(@Query("limit") int perPage, @Query("skip") int skip, Callback<PartidaResponse> response);

    @GET("/classes/Partida")
    void getPartidas(@Query("where") String where, @Query("limit") int perPage, @Query("skip") int skip, Callback<PartidaResponse> response);

    @GET("/classes/EstatisticasDaPartida")
    void getEstatisticas(@Query("where") String where, @Query("limit") int perPage, Callback<EstatisticaResponse> response);
}

package br.org.fpf.android.ui.estatisticas;

import android.os.Bundle;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;

import java.util.Collections;
import java.util.List;

import br.org.fpf.android.R;
import br.org.fpf.android.model.EstatisticasTime;
import br.org.fpf.android.model.GolsEquipe;
import br.org.fpf.android.ui.estatisticas.adapter.GolsViewHolder;
import br.org.fpf.android.util.Constants;
import uk.co.ribot.easyadapter.EasyRecyclerAdapter;

/**
 * Created by ranipieper on 3/8/16.
 */
@EFragment(R.layout.fragment_estatisticas_time_page)
public class GolsEstatisticasFragment extends EstatisticasTimeItemFragment {

    @FragmentArg(Constants.ARG_A_FAVOR)
    boolean mGolsPro;

    public static GolsEstatisticasFragment newInstance(EstatisticasTime estatisticasTime, boolean aFavor) {
        Bundle args = new Bundle();

        GolsEstatisticasFragment_ fragment = new GolsEstatisticasFragment_();
        args.putParcelable(Constants.ARG_ESTATISTICAS_TIME, estatisticasTime);
        args.putBoolean(Constants.ARG_A_FAVOR, aFavor);
        fragment.setArguments(args);

        return fragment;
    }

    @AfterViews
    public void afterViews() {
        super.afterViews();
        mLblFooterLabel.setText(R.string.total_gols);
        long total = 0;
        for (GolsEquipe gol : getGolsEquipe()) {
            total += gol.getGols();
        }
        mLblFooterValue.setText(String.valueOf(total));
    }

    @Override
    protected EasyRecyclerAdapter createAdapter() {
        List<GolsEquipe> gols = getGolsEquipe();
        Collections.sort(gols);
        return new EasyRecyclerAdapter(
                getActivity(),
                GolsViewHolder.class,
                gols);
    }

    private List<GolsEquipe> getGolsEquipe() {
        if (mGolsPro) {
            List<GolsEquipe> gols = mEstatisticasTime.getGolsPro();
            for (GolsEquipe gol : gols) {
                gol.setEquipeId(mEstatisticasTime.getIdEquipe());
                gol.setEquipe(mEstatisticasTime.getEquipe());
            }
            return gols;
        } else {
            return mEstatisticasTime.getGolsContra();
        }
    }
}
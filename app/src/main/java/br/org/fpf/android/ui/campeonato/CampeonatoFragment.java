package br.org.fpf.android.ui.campeonato;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.h6ah4i.android.tablayouthelper.TabLayoutHelper;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import br.org.fpf.android.R;
import br.org.fpf.android.cache.CampeonatoCacheManager;
import br.org.fpf.android.model.Campeonato;
import br.org.fpf.android.ui.base.BaseFragment;
import br.org.fpf.android.ui.custom.FpfViewPagerAdapter;
import br.org.fpf.android.helper.CampeonatoHelper;
import br.org.fpf.android.util.Constants;

/**
 * Created by ranipieper on 1/26/16.
 */
@EFragment(R.layout.fragment_campeonato)
public class CampeonatoFragment extends BaseFragment {

    @ViewById(R.id.tablayout)
    TabLayout mTabLayout;

    @ViewById(R.id.viewpager)
    public ViewPager mViewPager;

    FpfViewPagerAdapter mAdapter;

    @FragmentArg(Constants.ARG_ID_CAMPEONATO)
    Long mIdCampeonato;

    Campeonato mCampeonato;

    private TabLayoutHelper mTabLayoutHelper;

    public static CampeonatoFragment newInstance(Long idCampeonato) {
        Bundle args = new Bundle();

        CampeonatoFragment_ fragment = new CampeonatoFragment_();
        args.putLong(Constants.ARG_ID_CAMPEONATO, idCampeonato);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCampeonato = CampeonatoCacheManager.getInstance().get(mIdCampeonato);
    }

    @AfterViews
    public void afterViews() {
        super.afterViews();

        setupViewPager();
    }

    @Override
    public void onResumeFromBackStack() {
        super.onResumeFromBackStack();
        if (mAdapter != null && mViewPager != null) {
            BaseFragment baseFragment = (BaseFragment)mAdapter.getItem(mViewPager.getCurrentItem());
            if (baseFragment != null) {
                baseFragment.onResumeFromBackStack();
            }
        }
    }

    private void setupViewPager() {
        mAdapter = new FpfViewPagerAdapter(getChildFragmentManager());

        Campeonato campeonato = CampeonatoCacheManager.getInstance().get(mIdCampeonato);

        if (campeonato != null && campeonato.isTemClassificacao()) {
            mAdapter.addFrag(ClassificacaoFasesFragment.newInstance(mIdCampeonato), getString(R.string.classificacao), isAdded());
        }
        mAdapter.addFrag(TabelaRodadasFragment.newInstance(mIdCampeonato), getString(R.string.tabela), isAdded());
        mAdapter.addFrag(ArtilhariaFragment.newInstance(mIdCampeonato), getString(R.string.artilharia), isAdded());

        if (campeonato != null && campeonato.getIdCampeonatoNoticias() != null) {
            mAdapter.addFrag(NoticiasFragment.newInstance(campeonato.getIdCampeonatoNoticias()), getString(R.string.noticias), isAdded());
        }

        mViewPager.setAdapter(mAdapter);
        //mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setOffscreenPageLimit(2);
        mTabLayout.setVisibility(View.VISIBLE);
        final Handler handler = new Handler();
        Runnable refresh = new Runnable() {
            public void run() {
                if (isAdded()) {
                    hideLoadingView();
                    TabLayoutHelper mTabLayoutHelper = new TabLayoutHelper(mTabLayout, mViewPager);
                    mTabLayoutHelper.setAutoAdjustTabModeEnabled(true);
                    mTabLayout.setVisibility(View.VISIBLE);
                    mViewPager.setVisibility(View.VISIBLE);
                }
            }
        };
        showLoadingView();
        handler.postDelayed(refresh, 100);

        hideLoadingView();
    }

    @Override
    protected String getToolbarTitle() {
        if (mCampeonato != null) {
            return CampeonatoHelper.getNome(mCampeonato);
        } else {
            return "";
        }
    }

    @Override
    public void onDestroyView() {
        // release the TabLayoutHelper instance
        if (mTabLayoutHelper != null) {
            mTabLayoutHelper.release();
            mTabLayoutHelper = null;
        }

        super.onDestroyView();
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.DRAWER;
    }



}

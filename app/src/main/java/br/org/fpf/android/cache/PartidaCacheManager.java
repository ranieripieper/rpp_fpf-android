package br.org.fpf.android.cache;

import android.content.Context;

import java.util.List;

import br.org.fpf.android.cache.base.BaseCache;
import br.org.fpf.android.model.Partida;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by ranipieper on 11/19/15.
 */
public class PartidaCacheManager extends BaseCache<Partida> {

    private static PartidaCacheManager instance = new PartidaCacheManager();

    private PartidaCacheManager() {
    }

    public static PartidaCacheManager getInstance() {
        return instance;
    }

    @Override
    public void put(Partida obj) {
        obj.setDtPartida(obj.getData() != null ? obj.getData().getData() : null);
        super.put(obj);
    }

    @Override
    public void putAll(List<Partida> lst) {
        if (lst != null) {
            for (Partida obj : lst) {
                obj.setDtPartida(obj.getData() != null ? obj.getData().getData() : null);
            }
            super.putAll(lst);
        }
    }

    public void putAll(Context ctx, List<Partida> lst) {
        if (lst != null) {
            for (Partida obj : lst) {
                obj.setDtPartida(obj.getData() != null ? obj.getData().getData() : null);
            }
            if (ctx == null) {
                super.putAll(lst);
            } else {
                super.putAll(ctx, lst);
            }
        }
    }

    public List<Partida> getPartidasByIdCampeonatoFaseRodada(Long idCampeonato, String fase, Long rodada) {

        String[] DEFAULT_ORDER_FIELDS = {"dtPartida", "placarMandante", "nomeMandante"};
        Sort[] DEFAULT_ORDER_SORT = {Sort.ASCENDING, Sort.DESCENDING, Sort.ASCENDING};

        RealmResults<Partida> results = getRealm().where(Partida.class)
                .equalTo("idCampeonato", idCampeonato)
                .equalTo("fase", fase)
                .equalTo("rodada", rodada)
                .findAllSorted(DEFAULT_ORDER_FIELDS, DEFAULT_ORDER_SORT);

        return getRealm().copyFromRealm(results);
    }

    public List<Partida> getPartidasByIdCampeonato(Context ctx, Long idCampeonato) {
        getRealm(ctx).refresh();
        String[] DEFAULT_ORDER_FIELDS = {"dtPartida", "nomeMandante"};
        Sort[] DEFAULT_ORDER_SORT = {Sort.ASCENDING, Sort.ASCENDING};

        RealmResults<Partida> results = getRealm(ctx).where(Partida.class)
                .equalTo("idCampeonato", idCampeonato)
                .findAllSorted(DEFAULT_ORDER_FIELDS, DEFAULT_ORDER_SORT);

        return getRealm(ctx).copyFromRealm(results);
    }

    public List<Partida> getPartidasByIdCampeonato(Long idCampeonato) {

        String[] DEFAULT_ORDER_FIELDS = {"dtPartida", "nomeMandante"};
        Sort[] DEFAULT_ORDER_SORT = {Sort.ASCENDING, Sort.ASCENDING};

        RealmResults<Partida> results = getRealm().where(Partida.class)
                .equalTo("idCampeonato", idCampeonato)
                .findAllSorted(DEFAULT_ORDER_FIELDS, DEFAULT_ORDER_SORT);

        return getRealm().copyFromRealm(results);
    }

    @Override
    public Class<Partida> getReferenceClass() {
        return Partida.class;
    }

    @Override
    protected long getCacheLifeTime() {
        return 1000 * 60 * 60 * 24 * 15; // 15 dias
    }
}
